-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 05, 2020 at 07:17 AM
-- Server version: 10.3.21-MariaDB
-- PHP Version: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rompos`
--

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_groups`
--

DROP TABLE IF EXISTS `rompos_aauth_groups`;
CREATE TABLE `rompos_aauth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rompos_aauth_groups`
--

INSERT INTO `rompos_aauth_groups` (`id`, `name`, `definition`, `description`, `is_admin`) VALUES
(4, 'master', 'Master Group', 'Can create users, install modules, manage options', 1),
(5, 'administrator', 'Admin Group', 'Can install modules, manage options', 1),
(6, 'user', 'User Group', 'Just a user', 1),
(7, 'store.cashier', 'Cashier', 'Role with limited permission for sale', 1),
(8, 'store.manager', 'Shop Manager', 'Role with management permissions for the shop.', 1),
(9, 'sub-store.manager', 'Manager of sub-shop', 'Role with management permissions of a sub-shop.', 1),
(10, 'store.demo', 'Role test', 'Role with permissions to test the features of NexoPOS.', 1),
(11, 'gastro.chief', 'Chief', 'This role can manage the kitchen', 1),
(12, 'gastro.waiter', 'Waiter', 'This role can manage the order', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_perms`
--

DROP TABLE IF EXISTS `rompos_aauth_perms`;
CREATE TABLE `rompos_aauth_perms` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rompos_aauth_perms`
--

INSERT INTO `rompos_aauth_perms` (`id`, `name`, `definition`, `description`) VALUES
(1, 'manage_core', 'Manage Core', 'Allow core management'),
(2, 'create_options', 'Create Options', 'Allow option creation'),
(3, 'edit_options', 'Edit Options', 'Allow option edition'),
(4, 'read_options', 'Read Options', 'Allow option read'),
(5, 'delete_options', 'Delete Options', 'Allow option deletion.'),
(6, 'install_modules', 'Install Modules', 'Let user install modules.'),
(7, 'update_modules', 'Update Modules', 'Let user update modules'),
(8, 'delete_modules', 'Delete Modules', 'Let user delete modules'),
(9, 'toggle_modules', 'Enable/Disable Modules', 'Let user enable/disable modules'),
(10, 'extract_modules', 'Extract Modules', 'Let user extract modules'),
(11, 'create_users', 'Create Users', 'Allow create users.'),
(12, 'edit_users', 'Edit Users', 'Allow edit users.'),
(13, 'delete_users', 'Delete Users', 'Allow delete users.'),
(14, 'edit_profile', 'Create Options', 'Allow option creation'),
(15, 'nexo.create.orders', 'Create orders', ''),
(16, 'nexo.view.orders', 'View order list', ''),
(17, 'nexo.edit.orders', 'Edit orders', ''),
(18, 'nexo.delete.orders', 'Delete orders', ''),
(19, 'nexo.create.items', 'Create items', ''),
(20, 'nexo.view.items', 'See products list', ''),
(21, 'nexo.edit.items', 'Modifying items', ''),
(22, 'nexo.delete.items', 'Delete items', ''),
(23, 'nexo.create.categories', 'Creating categories', ''),
(24, 'nexo.view.categories', 'See the list of categories', ''),
(25, 'nexo.edit.categories', 'Edit categories', ''),
(26, 'nexo.delete.categories', 'Delete Categories', ''),
(27, 'nexo.create.departments', 'Create departments', ''),
(28, 'nexo.view.departments', 'See the list of departments', ''),
(29, 'nexo.edit.departments', 'Modify departments', ''),
(30, 'nexo.delete.departments', 'Delete departments', ''),
(31, 'nexo.create.providers', 'Create suppliers', ''),
(32, 'nexo.view.providers', 'See the list of suppliers', ''),
(33, 'nexo.edit.providers', 'Edit providers', ''),
(34, 'nexo.delete.providers', 'Delete providers', ''),
(35, 'nexo.create.supplies', 'Create supplies', ''),
(36, 'nexo.view.supplies', 'See the list of supplies', ''),
(37, 'nexo.edit.supplies', 'Edit supplies', ''),
(38, 'nexo.delete.supplies', 'Delete supplies', ''),
(39, 'nexo.create.customers-groups', 'Create Customer Groups', ''),
(40, 'nexo.view.customers-groups', 'Customers groups lists', ''),
(41, 'nexo.edit.customers-groups', 'Edit Customer Groups', ''),
(42, 'nexo.delete.customers-groups', 'Delete Client Groups', ''),
(43, 'nexo.create.customers', 'Create customers', ''),
(44, 'nexo.view.customers', 'See the list of customers', ''),
(45, 'nexo.edit.customers', 'Edit clients', ''),
(46, 'nexo.delete.customers', 'Delete customers', ''),
(47, 'nexo.create.invoices', 'Create Invoices', ''),
(48, 'nexo.view.invoices', 'See the list of invoices', ''),
(49, 'nexo.edit.invoices', 'Edit Invoices', ''),
(50, 'nexo.delete.invoices', 'Delete Invoices', ''),
(51, 'nexo.create.taxes', 'Create taxes', ''),
(52, 'nexo.view.taxes', 'See the list of taxes', ''),
(53, 'nexo.edit.taxes', 'Change taxes', ''),
(54, 'nexo.delete.taxes', 'Remove taxes', ''),
(55, 'nexo.create.registers', 'Create a cash register', ''),
(56, 'nexo.view.registers', 'See the list of cash registers', ''),
(57, 'nexo.edit.registers', 'Modify a cash register', ''),
(58, 'nexo.delete.registers', 'Delete a cash register', ''),
(59, 'nexo.use.registers', 'Use a cash register', ''),
(60, 'nexo.view.registers-history', 'View the history of a caisse', ''),
(61, 'nexo.create.backups', 'Create & backups', ''),
(62, 'nexo.view.backups', 'See the list of backups', ''),
(63, 'nexo.edit.backups', 'Edit backups', ''),
(64, 'nexo.delete.backups', 'Remove backups', ''),
(65, 'nexo.create.stock-adjustment', 'Create stock adjustments', ''),
(66, 'nexo.view.stock-adjustment', 'See the list of adjustments', ''),
(67, 'nexo.edit.stock-adjustment', 'Edit adjustments', ''),
(68, 'nexo.delete.stock-adjustment', 'Remove adjustments', ''),
(69, 'nexo.create.stores', 'Create shops', ''),
(70, 'nexo.view.stores', 'See the list of shops', ''),
(71, 'nexo.edit.stores', 'Edit shops', ''),
(72, 'nexo.delete.stores', 'Remove shops', ''),
(73, 'nexo.enter.stores', 'Use a shop', ''),
(74, 'nexo.create.coupons', 'Create coupons', ''),
(75, 'nexo.view.coupons', 'See the list of coupons', ''),
(76, 'nexo.edit.coupons', 'Edit Coupons', ''),
(77, 'nexo.delete.coupons', 'Delete Coupons', ''),
(78, 'nexo.view.refund', 'Consult a refund', ''),
(79, 'nexo.create.refund', 'Create a refund', ''),
(80, 'nexo.edit.refund', 'Edit a refund', ''),
(81, 'nexo.delete.refund', 'Delete a refund', ''),
(82, 'nexo.read.detailed-report', 'Read detailed sales', ''),
(83, 'nexo.read.best-sales', 'Top sellers', ''),
(84, 'nexo.read.daily-sales', 'Read daily sales', ''),
(85, 'nexo.read.incomes-losses', 'Incomes and Losses', ''),
(86, 'nexo.read.expenses-listings', 'Expenses List', ''),
(87, 'nexo.read.cash-flow', 'Read the cash flow', ''),
(88, 'nexo.read.annual-sales', 'Read income and losses', ''),
(89, 'nexo.read.cashier-performances', 'Cashiers performance', ''),
(90, 'nexo.read.customer-statistics', 'Read customer statistics', ''),
(91, 'nexo.read.inventory-tracking', 'Read stock tracking', ''),
(92, 'nexo.manage.settings', 'Option settings', ''),
(93, 'nexo.manage.stores-settings', 'Stores settings', ''),
(94, 'gastro.view.tokitchen', 'View the button \"to kitchen\"', ''),
(95, 'gastro.view.paybutton', 'View the button \"Pay\"', ''),
(96, 'gastro.view.discountbutton', 'View the discount button', ''),
(97, 'gastro.use.waiter-screen', 'Use the waiter screen', ''),
(98, 'gastro.use.kitchen-screen', 'Use the kitchen screen', ''),
(99, 'gastro.use.merge-meal', 'Use the merging meal feature', ''),
(100, 'nexo.manage.reports', 'Manage reports data', NULL),
(101, 'create_restaurant_tables', 'Create restaurant tables', NULL),
(102, 'edit_restaurant_tables', 'Edit restaurant tables', NULL),
(103, 'delete_restaurant_tables', 'Delete restaurant tables', NULL),
(104, 'manage_restaurant_appointments', 'Manage appointments', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_perm_to_group`
--

DROP TABLE IF EXISTS `rompos_aauth_perm_to_group`;
CREATE TABLE `rompos_aauth_perm_to_group` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rompos_aauth_perm_to_group`
--

INSERT INTO `rompos_aauth_perm_to_group` (`perm_id`, `group_id`) VALUES
(1, 4),
(1, 8),
(2, 4),
(2, 5),
(2, 8),
(3, 4),
(3, 5),
(3, 8),
(4, 4),
(4, 5),
(4, 8),
(5, 4),
(5, 5),
(5, 8),
(6, 4),
(6, 5),
(6, 8),
(7, 4),
(7, 5),
(7, 8),
(8, 4),
(8, 5),
(8, 8),
(9, 4),
(9, 5),
(9, 8),
(10, 4),
(10, 5),
(10, 8),
(11, 4),
(11, 8),
(12, 4),
(12, 8),
(13, 4),
(13, 8),
(14, 0),
(14, 4),
(14, 5),
(14, 6),
(14, 8),
(14, 9),
(15, 0),
(15, 4),
(15, 7),
(15, 8),
(16, 0),
(16, 4),
(16, 8),
(17, 0),
(17, 4),
(17, 8),
(18, 0),
(18, 4),
(18, 8),
(19, 0),
(19, 4),
(19, 8),
(20, 0),
(20, 4),
(20, 8),
(21, 0),
(21, 4),
(21, 8),
(22, 0),
(22, 4),
(22, 8),
(23, 0),
(23, 4),
(23, 8),
(24, 0),
(24, 4),
(24, 8),
(25, 0),
(25, 4),
(25, 8),
(26, 0),
(26, 4),
(26, 8),
(27, 0),
(27, 4),
(27, 8),
(28, 0),
(28, 4),
(28, 8),
(29, 0),
(29, 4),
(29, 8),
(30, 0),
(30, 4),
(30, 8),
(31, 0),
(31, 4),
(31, 8),
(32, 0),
(32, 4),
(32, 8),
(33, 0),
(33, 4),
(33, 8),
(34, 0),
(34, 4),
(34, 8),
(35, 0),
(35, 4),
(35, 8),
(36, 0),
(36, 4),
(36, 8),
(37, 0),
(37, 4),
(37, 8),
(38, 0),
(38, 4),
(38, 8),
(39, 0),
(39, 4),
(39, 8),
(40, 0),
(40, 4),
(40, 8),
(41, 0),
(41, 4),
(41, 8),
(42, 0),
(42, 4),
(42, 8),
(43, 0),
(43, 4),
(43, 8),
(44, 0),
(44, 4),
(44, 8),
(45, 0),
(45, 4),
(45, 8),
(46, 0),
(46, 4),
(46, 8),
(47, 0),
(47, 4),
(47, 8),
(48, 0),
(48, 4),
(48, 8),
(49, 0),
(49, 4),
(49, 8),
(50, 0),
(50, 4),
(50, 8),
(51, 0),
(51, 4),
(51, 8),
(52, 0),
(52, 4),
(52, 8),
(53, 0),
(53, 4),
(53, 8),
(54, 0),
(54, 4),
(54, 8),
(55, 0),
(55, 4),
(55, 8),
(56, 0),
(56, 4),
(56, 8),
(57, 0),
(57, 4),
(57, 8),
(58, 0),
(58, 4),
(58, 8),
(59, 4),
(59, 7),
(59, 8),
(60, 0),
(60, 4),
(60, 8),
(60, 9),
(61, 0),
(61, 4),
(61, 8),
(62, 0),
(62, 4),
(62, 8),
(63, 0),
(63, 4),
(63, 8),
(64, 0),
(64, 4),
(64, 8),
(65, 0),
(65, 4),
(65, 8),
(66, 0),
(66, 4),
(66, 8),
(67, 0),
(67, 4),
(67, 8),
(68, 0),
(68, 4),
(68, 8),
(69, 0),
(69, 4),
(69, 8),
(70, 0),
(70, 4),
(70, 7),
(70, 8),
(71, 0),
(71, 4),
(71, 8),
(72, 0),
(72, 4),
(72, 8),
(73, 0),
(73, 4),
(73, 7),
(73, 8),
(73, 9),
(74, 0),
(74, 4),
(74, 8),
(75, 0),
(75, 4),
(75, 8),
(76, 0),
(76, 4),
(76, 8),
(77, 0),
(77, 4),
(77, 8),
(78, 0),
(78, 4),
(78, 8),
(79, 0),
(79, 4),
(79, 8),
(80, 0),
(80, 4),
(80, 8),
(81, 0),
(81, 4),
(81, 8),
(82, 0),
(82, 4),
(82, 8),
(82, 9),
(83, 0),
(83, 4),
(83, 8),
(83, 9),
(84, 0),
(84, 4),
(84, 8),
(84, 9),
(85, 0),
(85, 4),
(85, 8),
(85, 9),
(86, 0),
(86, 4),
(86, 8),
(86, 9),
(87, 0),
(87, 4),
(87, 8),
(87, 9),
(88, 0),
(88, 4),
(88, 8),
(88, 9),
(89, 0),
(89, 4),
(89, 8),
(89, 9),
(90, 0),
(90, 4),
(90, 8),
(90, 9),
(91, 0),
(91, 4),
(91, 8),
(91, 9),
(92, 0),
(92, 4),
(92, 8),
(92, 9),
(93, 0),
(93, 4),
(93, 8),
(93, 9),
(94, 0),
(94, 4),
(94, 7),
(94, 8),
(94, 12),
(95, 0),
(95, 4),
(95, 7),
(95, 8),
(96, 0),
(96, 4),
(96, 8),
(96, 12),
(97, 0),
(97, 4),
(97, 8),
(97, 12),
(98, 0),
(98, 4),
(98, 8),
(98, 11),
(99, 0),
(99, 4),
(99, 8),
(99, 12),
(100, 4),
(100, 8),
(101, 4),
(101, 8),
(102, 4),
(102, 8),
(103, 4),
(103, 8),
(104, 4),
(104, 8);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_perm_to_user`
--

DROP TABLE IF EXISTS `rompos_aauth_perm_to_user`;
CREATE TABLE `rompos_aauth_perm_to_user` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_pms`
--

DROP TABLE IF EXISTS `rompos_aauth_pms`;
CREATE TABLE `rompos_aauth_pms` (
  `id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `read` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_system_variables`
--

DROP TABLE IF EXISTS `rompos_aauth_system_variables`;
CREATE TABLE `rompos_aauth_system_variables` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_users`
--

DROP TABLE IF EXISTS `rompos_aauth_users`;
CREATE TABLE `rompos_aauth_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `last_login_attempt` datetime DEFAULT NULL,
  `forgot_exp` text DEFAULT NULL,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text DEFAULT NULL,
  `verification_code` text DEFAULT NULL,
  `ip_address` text DEFAULT NULL,
  `login_attempts` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rompos_aauth_users`
--

INSERT INTO `rompos_aauth_users` (`id`, `email`, `pass`, `name`, `banned`, `last_login`, `last_activity`, `last_login_attempt`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `ip_address`, `login_attempts`) VALUES
(2, 'example@example.com', '52b3a93aac36bd14b3a1c9e7118f79981d14d39c6fd5118884d7544e58232a8d', 'rompos', 0, '2020-02-04 08:53:39', '2020-02-04 08:53:39', '2020-02-04 08:00:00', NULL, '2019-06-18 00:00:00', 'PKzhLvqZBfW2iTuV', '', '127.0.0.1', NULL),
(3, 'user1@example.com', '4e4cdff9436d4b9797eeb35d6965e73bd0bd9d5dc939ab2c190a179cf8df960d', 'user1', 0, '2019-02-27 18:09:52', '2019-02-27 18:09:52', '2020-02-04 04:00:00', NULL, NULL, NULL, '', '192.168.1.237', 1),
(4, 'admin@example.com', '17c3c93e95e9dbb33dec79b0389bb7931b5581b292eeadc4d04321c577013ecf', 'admin', 0, '2019-08-14 09:15:31', '2019-08-14 09:15:31', '2019-11-29 05:00:00', NULL, NULL, NULL, '', '192.168.0.162', 1),
(5, 'shopmanager@example.com', '472017e31a15efc0360b51fa1cc09e7c5d34f5d8a244e2d6ba93a863ab27b96a', 'shopmanager', 0, '2019-03-02 17:10:59', '2019-03-02 17:10:59', '2019-03-02 17:00:00', NULL, NULL, NULL, '', '192.168.1.237', NULL),
(6, 'waiter@example.com', '0b5758740f60a30779ab82ac69eeae59d3672814868cb2c118588e2c86c76cab', 'waiter', 0, '2019-07-25 06:44:22', '2019-07-25 06:44:22', '2019-11-29 05:00:00', NULL, NULL, NULL, '', '127.0.0.1', 2),
(7, 'chief@example.com', 'c82df20f8e8e72bfcb378fb0d6deec63c9f46f607403cf94788a93888cc4336a', 'chief', 0, '2019-11-29 05:30:01', '2019-11-29 05:30:01', '2019-11-29 05:00:00', NULL, NULL, NULL, '', '127.0.0.1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_user_to_group`
--

DROP TABLE IF EXISTS `rompos_aauth_user_to_group`;
CREATE TABLE `rompos_aauth_user_to_group` (
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `group_id` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rompos_aauth_user_to_group`
--

INSERT INTO `rompos_aauth_user_to_group` (`user_id`, `group_id`) VALUES
(2, 4),
(3, 6),
(4, 5),
(5, 8),
(6, 12),
(7, 11);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_aauth_user_variables`
--

DROP TABLE IF EXISTS `rompos_aauth_user_variables`;
CREATE TABLE `rompos_aauth_user_variables` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_arrivages`
--

DROP TABLE IF EXISTS `rompos_nexo_arrivages`;
CREATE TABLE `rompos_nexo_arrivages` (
  `ID` int(11) NOT NULL,
  `TITRE` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `VALUE` float NOT NULL,
  `ITEMS` int(11) NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `FOURNISSEUR_REF_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rompos_nexo_arrivages`
--

INSERT INTO `rompos_nexo_arrivages` (`ID`, `TITRE`, `DESCRIPTION`, `VALUE`, `ITEMS`, `REF_PROVIDER`, `DATE_CREATION`, `DATE_MOD`, `AUTHOR`, `FOURNISSEUR_REF_ID`) VALUES
(2, 'test supply', '', 0, 0, 0, '2019-05-17 02:30:29', '0000-00-00 00:00:00', 2, 0),
(3, 'Test supply 2', '', 5.61, 3, 1, '2019-05-17 02:30:49', '0000-00-00 00:00:00', 2, 0),
(4, 'test 3', '', 0.96, 2, 1, '2019-05-17 02:34:52', '0000-00-00 00:00:00', 2, 0),
(5, 'test 5', '', 0, 0, 0, '2019-05-18 08:18:10', '0000-00-00 00:00:00', 2, 0),
(6, 'title 5', '', 0.25, 1, 1, '2019-05-18 08:18:31', '0000-00-00 00:00:00', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_articles`
--

DROP TABLE IF EXISTS `rompos_nexo_articles`;
CREATE TABLE `rompos_nexo_articles` (
  `ID` int(11) NOT NULL,
  `DESIGN` varchar(200) NOT NULL,
  `ALTERNATIVE_NAME` varchar(200) NOT NULL,
  `REF_RAYON` int(11) NOT NULL,
  `REF_SHIPPING` int(11) NOT NULL,
  `REF_CATEGORIE` int(11) NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `REF_TAXE` int(11) NOT NULL,
  `TAX_TYPE` varchar(200) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `SKU` varchar(220) NOT NULL,
  `QUANTITE_RESTANTE` int(11) NOT NULL,
  `QUANTITE_VENDU` int(11) NOT NULL,
  `DEFECTUEUX` int(11) NOT NULL,
  `PRIX_DACHAT` float NOT NULL,
  `FRAIS_ACCESSOIRE` float NOT NULL,
  `COUT_DACHAT` float NOT NULL,
  `TAUX_DE_MARGE` float NOT NULL,
  `PRIX_DE_VENTE` float NOT NULL,
  `PRIX_DE_VENTE_TTC` float NOT NULL,
  `SHADOW_PRICE` float NOT NULL,
  `TAILLE` varchar(200) NOT NULL,
  `POIDS` varchar(200) NOT NULL,
  `COULEUR` varchar(200) NOT NULL,
  `HAUTEUR` varchar(200) NOT NULL,
  `LARGEUR` varchar(200) NOT NULL,
  `PRIX_PROMOTIONEL` float NOT NULL,
  `SPECIAL_PRICE_START_DATE` datetime NOT NULL,
  `SPECIAL_PRICE_END_DATE` datetime NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `APERCU` varchar(200) NOT NULL,
  `CODEBAR` varchar(200) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `TYPE` int(11) NOT NULL,
  `SHOW_ON_SOK` tinyint(1) NOT NULL,
  `STATUS` int(11) NOT NULL,
  `STOCK_ENABLED` int(11) NOT NULL,
  `STOCK_ALERT` varchar(200) NOT NULL,
  `ALERT_QUANTITY` int(11) NOT NULL,
  `EXPIRATION_DATE` datetime NOT NULL,
  `ON_EXPIRE_ACTION` varchar(200) NOT NULL,
  `AUTO_BARCODE` int(11) NOT NULL,
  `BARCODE_TYPE` varchar(200) NOT NULL,
  `REF_MODIFIERS_GROUP` varchar(200) NOT NULL,
  `USE_VARIATION` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_articles_meta`
--

DROP TABLE IF EXISTS `rompos_nexo_articles_meta`;
CREATE TABLE `rompos_nexo_articles_meta` (
  `ID` int(11) NOT NULL,
  `REF_ARTICLE` int(11) NOT NULL,
  `KEY` varchar(250) NOT NULL,
  `VALUE` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_articles_stock_flow`
--

DROP TABLE IF EXISTS `rompos_nexo_articles_stock_flow`;
CREATE TABLE `rompos_nexo_articles_stock_flow` (
  `ID` int(11) NOT NULL,
  `REF_ARTICLE_BARCODE` varchar(250) NOT NULL,
  `BEFORE_QUANTITE` int(11) NOT NULL,
  `QUANTITE` int(11) NOT NULL,
  `AFTER_QUANTITE` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `REF_COMMAND_CODE` varchar(11) NOT NULL,
  `REF_SHIPPING` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `UNIT_PRICE` float NOT NULL,
  `TOTAL_PRICE` float NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `DESCRIPTION` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_articles_variations`
--

DROP TABLE IF EXISTS `rompos_nexo_articles_variations`;
CREATE TABLE `rompos_nexo_articles_variations` (
  `ID` int(11) NOT NULL,
  `REF_ARTICLE` int(11) NOT NULL,
  `VAR_DESIGN` varchar(250) NOT NULL,
  `VAR_DESCRIPTION` varchar(250) NOT NULL,
  `VAR_PRIX_DE_VENTE` float NOT NULL,
  `VAR_QUANTITE_TOTALE` int(11) NOT NULL,
  `VAR_QUANTITE_RESTANTE` int(11) NOT NULL,
  `VAR_QUANTITE_VENDUE` int(11) NOT NULL,
  `VAR_COULEUR` varchar(250) NOT NULL,
  `VAR_TAILLE` varchar(250) NOT NULL,
  `VAR_POIDS` varchar(250) NOT NULL,
  `VAR_HAUTEUR` varchar(250) NOT NULL,
  `VAR_LARGEUR` varchar(250) NOT NULL,
  `VAR_SHADOW_PRICE` float NOT NULL,
  `VAR_SPECIAL_PRICE_START_DATE` datetime NOT NULL,
  `VAR_SPECIAL_PRICE_END_DATE` datetime NOT NULL,
  `VAR_APERCU` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_categories`
--

DROP TABLE IF EXISTS `rompos_nexo_categories`;
CREATE TABLE `rompos_nexo_categories` (
  `ID` int(11) NOT NULL,
  `NOM` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `PARENT_REF_ID` int(11) NOT NULL,
  `THUMB` text NOT NULL,
  `COLOR` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_clients`
--

DROP TABLE IF EXISTS `rompos_nexo_clients`;
CREATE TABLE `rompos_nexo_clients` (
  `ID` int(11) NOT NULL,
  `NOM` varchar(200) NOT NULL,
  `PRENOM` varchar(200) NOT NULL,
  `POIDS` int(11) NOT NULL,
  `TEL` varchar(200) NOT NULL,
  `EMAIL` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_NAISSANCE` datetime NOT NULL,
  `ADRESSE` text NOT NULL,
  `NBR_COMMANDES` int(11) NOT NULL,
  `OVERALL_COMMANDES` int(11) NOT NULL,
  `DISCOUNT_ACTIVE` int(11) NOT NULL,
  `TOTAL_SPEND` float NOT NULL,
  `LAST_ORDER` varchar(200) NOT NULL,
  `AVATAR` varchar(200) NOT NULL,
  `STATE` varchar(200) NOT NULL,
  `CITY` varchar(200) NOT NULL,
  `POST_CODE` varchar(200) NOT NULL,
  `COUNTRY` varchar(200) NOT NULL,
  `COMPANY_NAME` varchar(200) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `REF_GROUP` int(11) NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `password` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rompos_nexo_clients`
--

INSERT INTO `rompos_nexo_clients` (`ID`, `NOM`, `PRENOM`, `POIDS`, `TEL`, `EMAIL`, `DESCRIPTION`, `DATE_NAISSANCE`, `ADRESSE`, `NBR_COMMANDES`, `OVERALL_COMMANDES`, `DISCOUNT_ACTIVE`, `TOTAL_SPEND`, `LAST_ORDER`, `AVATAR`, `STATE`, `CITY`, `POST_CODE`, `COUNTRY`, `COMPANY_NAME`, `DATE_CREATION`, `DATE_MOD`, `REF_GROUP`, `AUTHOR`, `password`, `username`) VALUES
(1, 'test customer', '', 0, '', '', '', '0000-00-00 00:00:00', '', 7, 7, 0, 0, '', '', '', '', '', '', '', '2019-01-26 09:21:14', '2019-02-27 13:07:26', 1, 2, '$2y$10$BWXkeE5Io4DgHREqW3ormuwg4SCfx6dLU0R80AFRMZTy0kDnh4htO', 'rompos');

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_clients_address`
--

DROP TABLE IF EXISTS `rompos_nexo_clients_address`;
CREATE TABLE `rompos_nexo_clients_address` (
  `id` int(11) NOT NULL,
  `type` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `enterprise` varchar(200) NOT NULL,
  `address_1` varchar(200) NOT NULL,
  `address_2` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `pobox` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `ref_client` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rompos_nexo_clients_address`
--

INSERT INTO `rompos_nexo_clients_address` (`id`, `type`, `name`, `surname`, `enterprise`, `address_1`, `address_2`, `city`, `pobox`, `country`, `state`, `phone`, `email`, `ref_client`) VALUES
(1, 'shipping', '', '', '', 'New York', '', '', '', '', '', '', '', 1),
(2, 'billing', '', '', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_clients_groups`
--

DROP TABLE IF EXISTS `rompos_nexo_clients_groups`;
CREATE TABLE `rompos_nexo_clients_groups` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `DISCOUNT_TYPE` varchar(220) NOT NULL,
  `DISCOUNT_PERCENT` float NOT NULL,
  `DISCOUNT_AMOUNT` float NOT NULL,
  `DISCOUNT_ENABLE_SCHEDULE` varchar(220) NOT NULL,
  `DISCOUNT_START` datetime NOT NULL,
  `DISCOUNT_END` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rompos_nexo_clients_groups`
--

INSERT INTO `rompos_nexo_clients_groups` (`ID`, `NAME`, `DESCRIPTION`, `DATE_CREATION`, `DATE_MODIFICATION`, `DISCOUNT_TYPE`, `DISCOUNT_PERCENT`, `DISCOUNT_AMOUNT`, `DISCOUNT_ENABLE_SCHEDULE`, `DISCOUNT_START`, `DISCOUNT_END`, `AUTHOR`) VALUES
(1, 'Customer Group 1', '', '2019-02-27 13:06:53', '0000-00-00 00:00:00', 'percent', 1, 0, 'false', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_clients_meta`
--

DROP TABLE IF EXISTS `rompos_nexo_clients_meta`;
CREATE TABLE `rompos_nexo_clients_meta` (
  `ID` int(11) NOT NULL,
  `KEY` varchar(200) NOT NULL,
  `VALUE` text NOT NULL,
  `REF_CLIENT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes`;
CREATE TABLE `rompos_nexo_commandes` (
  `ID` int(11) NOT NULL,
  `TITRE` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(200) NOT NULL,
  `CODE` varchar(250) NOT NULL,
  `REF_CLIENT` int(11) NOT NULL,
  `REF_REGISTER` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `RESTAURANT_ORDER_TYPE` varchar(200) NOT NULL,
  `RESTAURANT_ORDER_STATUS` varchar(200) NOT NULL,
  `RESTAURANT_BOOKED_FOR` datetime DEFAULT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `PAYMENT_TYPE` varchar(220) NOT NULL,
  `AUTHOR` varchar(200) NOT NULL,
  `SOMME_PERCU` float NOT NULL,
  `REMISE` float NOT NULL,
  `RABAIS` float NOT NULL,
  `RISTOURNE` float NOT NULL,
  `REMISE_TYPE` varchar(200) NOT NULL,
  `REMISE_PERCENT` float NOT NULL,
  `RABAIS_PERCENT` float NOT NULL,
  `RISTOURNE_PERCENT` float NOT NULL,
  `TOTAL` float NOT NULL,
  `DISCOUNT_TYPE` varchar(200) NOT NULL,
  `TVA` float NOT NULL,
  `GROUP_DISCOUNT` float DEFAULT NULL,
  `REF_SHIPPING_ADDRESS` int(11) DEFAULT NULL,
  `TOTAL_TAXES` float NOT NULL,
  `REF_TAX` int(11) NOT NULL,
  `SHIPPING_AMOUNT` float NOT NULL,
  `STATUS` varchar(200) NOT NULL,
  `EXPIRATION_DATE` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_coupons`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_coupons`;
CREATE TABLE `rompos_nexo_commandes_coupons` (
  `ID` int(11) NOT NULL,
  `REF_COMMAND` int(11) NOT NULL,
  `REF_COUPON` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_meta`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_meta`;
CREATE TABLE `rompos_nexo_commandes_meta` (
  `ID` int(11) NOT NULL,
  `REF_ORDER_ID` int(11) NOT NULL,
  `KEY` varchar(250) NOT NULL,
  `VALUE` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_paiements`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_paiements`;
CREATE TABLE `rompos_nexo_commandes_paiements` (
  `ID` int(11) NOT NULL,
  `REF_COMMAND_CODE` varchar(250) NOT NULL,
  `MONTANT` float NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `PAYMENT_TYPE` varchar(200) NOT NULL,
  `OPERATION` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_produits`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_produits`;
CREATE TABLE `rompos_nexo_commandes_produits` (
  `ID` int(11) NOT NULL,
  `REF_PRODUCT_CODEBAR` varchar(250) NOT NULL,
  `RESTAURANT_PRODUCT_REAL_BARCODE` varchar(200) NOT NULL,
  `REF_COMMAND_CODE` varchar(250) NOT NULL,
  `QUANTITE` int(11) NOT NULL,
  `PRIX` float NOT NULL,
  `PRIX_BRUT` float NOT NULL,
  `PRIX_TOTAL` float NOT NULL,
  `PRIX_BRUT_TOTAL` float NOT NULL,
  `DISCOUNT_TYPE` varchar(200) NOT NULL,
  `DISCOUNT_AMOUNT` float NOT NULL,
  `DISCOUNT_PERCENT` float NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `ALTERNATIVE_NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `INLINE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_produits_meta`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_produits_meta`;
CREATE TABLE `rompos_nexo_commandes_produits_meta` (
  `ID` int(11) NOT NULL,
  `REF_COMMAND_PRODUCT` int(11) NOT NULL,
  `REF_COMMAND_CODE` varchar(200) NOT NULL,
  `KEY` varchar(250) NOT NULL,
  `VALUE` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_refunds`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_refunds`;
CREATE TABLE `rompos_nexo_commandes_refunds` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(200) NOT NULL,
  `SUB_TOTAL` float NOT NULL,
  `TOTAL` float NOT NULL,
  `SHIPPING` float NOT NULL,
  `PAYMENT_TYPE` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `REF_ORDER` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_refunds_products`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_refunds_products`;
CREATE TABLE `rompos_nexo_commandes_refunds_products` (
  `ID` int(11) NOT NULL,
  `REF_ITEM` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `REF_REFUND` int(11) NOT NULL,
  `PRICE` float NOT NULL,
  `QUANTITY` float NOT NULL,
  `TOTAL_PRICE` float NOT NULL,
  `STATUS` varchar(200) NOT NULL,
  `DESCRIPTION` text DEFAULT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_shippings`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_shippings`;
CREATE TABLE `rompos_nexo_commandes_shippings` (
  `id` int(11) NOT NULL,
  `ref_shipping` int(11) NOT NULL,
  `ref_order` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `address_1` varchar(200) NOT NULL,
  `address_2` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL,
  `pobox` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `enterprise` varchar(200) DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `price` float NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_commandes_status`
--

DROP TABLE IF EXISTS `rompos_nexo_commandes_status`;
CREATE TABLE `rompos_nexo_commandes_status` (
  `id` int(11) NOT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `woocommerce_id` int(11) NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tm` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_coupons`
--

DROP TABLE IF EXISTS `rompos_nexo_coupons`;
CREATE TABLE `rompos_nexo_coupons` (
  `ID` int(11) NOT NULL,
  `CODE` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DISCOUNT_TYPE` varchar(200) NOT NULL,
  `AMOUNT` float NOT NULL,
  `EXPIRY_DATE` datetime NOT NULL,
  `USAGE_COUNT` int(11) NOT NULL,
  `INDIVIDUAL_USE` int(11) NOT NULL,
  `PRODUCTS_IDS` text NOT NULL,
  `EXCLUDE_PRODUCTS_IDS` text NOT NULL,
  `USAGE_LIMIT` int(11) NOT NULL,
  `USAGE_LIMIT_PER_USER` int(11) NOT NULL,
  `LIMIT_USAGE_TO_X_ITEMS` int(11) NOT NULL,
  `FREE_SHIPPING` int(11) NOT NULL,
  `PRODUCT_CATEGORIES` text NOT NULL,
  `EXCLUDE_PRODUCT_CATEGORIES` text NOT NULL,
  `EXCLUDE_SALE_ITEMS` int(11) NOT NULL,
  `MINIMUM_AMOUNT` float NOT NULL,
  `MAXIMUM_AMOUNT` float NOT NULL,
  `USED_BY` text NOT NULL,
  `REWARDED_CASHIER` int(11) NOT NULL,
  `EMAIL_RESTRICTIONS` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_daily_log`
--

DROP TABLE IF EXISTS `rompos_nexo_daily_log`;
CREATE TABLE `rompos_nexo_daily_log` (
  `ID` int(11) NOT NULL,
  `JSON` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_fournisseurs`
--

DROP TABLE IF EXISTS `rompos_nexo_fournisseurs`;
CREATE TABLE `rompos_nexo_fournisseurs` (
  `ID` int(11) NOT NULL,
  `NOM` varchar(200) NOT NULL,
  `BP` varchar(200) NOT NULL,
  `TEL` varchar(200) NOT NULL,
  `EMAIL` varchar(200) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `PAYABLE` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rompos_nexo_fournisseurs`
--

INSERT INTO `rompos_nexo_fournisseurs` (`ID`, `NOM`, `BP`, `TEL`, `EMAIL`, `DATE_CREATION`, `DATE_MOD`, `AUTHOR`, `DESCRIPTION`, `PAYABLE`) VALUES
(1, 'test suplier', '', '', '', '2019-02-04 19:29:05', '2019-02-04 19:31:51', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_fournisseurs_history`
--

DROP TABLE IF EXISTS `rompos_nexo_fournisseurs_history`;
CREATE TABLE `rompos_nexo_fournisseurs_history` (
  `ID` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `BEFORE_AMOUNT` float NOT NULL,
  `AMOUNT` float NOT NULL,
  `AFTER_AMOUNT` float NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `REF_INVOICE` int(11) NOT NULL,
  `REF_SUPPLY` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_historique`
--

DROP TABLE IF EXISTS `rompos_nexo_historique`;
CREATE TABLE `rompos_nexo_historique` (
  `ID` int(11) NOT NULL,
  `TITRE` varchar(200) NOT NULL,
  `DETAILS` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_items_filters`
--

DROP TABLE IF EXISTS `rompos_nexo_items_filters`;
CREATE TABLE `rompos_nexo_items_filters` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_notices`
--

DROP TABLE IF EXISTS `rompos_nexo_notices`;
CREATE TABLE `rompos_nexo_notices` (
  `ID` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `TITLE` varchar(200) NOT NULL,
  `MESSAGE` text NOT NULL,
  `ICON` varchar(200) NOT NULL,
  `LINK` varchar(200) NOT NULL,
  `REF_USER` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_premium_backups`
--

DROP TABLE IF EXISTS `rompos_nexo_premium_backups`;
CREATE TABLE `rompos_nexo_premium_backups` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `FILE_LOCATION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_premium_factures`
--

DROP TABLE IF EXISTS `rompos_nexo_premium_factures`;
CREATE TABLE `rompos_nexo_premium_factures` (
  `ID` int(11) NOT NULL,
  `INTITULE` varchar(200) NOT NULL,
  `REF` varchar(200) NOT NULL,
  `MONTANT` float NOT NULL,
  `REF_CATEGORY` int(11) NOT NULL,
  `REF_PROVIDER` int(11) NOT NULL,
  `REF_USER` int(11) NOT NULL,
  `IMAGE` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_premium_factures_categories`
--

DROP TABLE IF EXISTS `rompos_nexo_premium_factures_categories`;
CREATE TABLE `rompos_nexo_premium_factures_categories` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_premium_factures_items`
--

DROP TABLE IF EXISTS `rompos_nexo_premium_factures_items`;
CREATE TABLE `rompos_nexo_premium_factures_items` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `PRICE` float NOT NULL,
  `QUANTITY` float NOT NULL,
  `TOTAL` float NOT NULL,
  `FLAT_DISCOUNT` float NOT NULL,
  `PERCENTAGE_DISCOUNT` float NOT NULL,
  `DISCOUNT_TYPE` varchar(200) NOT NULL,
  `REF_INVOICE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_rayons`
--

DROP TABLE IF EXISTS `rompos_nexo_rayons`;
CREATE TABLE `rompos_nexo_rayons` (
  `ID` int(11) NOT NULL,
  `TITRE` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_registers`
--

DROP TABLE IF EXISTS `rompos_nexo_registers`;
CREATE TABLE `rompos_nexo_registers` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `IMAGE_URL` text DEFAULT NULL,
  `NPS_URL` varchar(200) NOT NULL,
  `ASSIGNED_PRINTER` varchar(200) NOT NULL,
  `AUTHOR` varchar(250) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `STATUS` varchar(200) NOT NULL,
  `USED_BY` int(11) NOT NULL,
  `REF_KITCHEN` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_registers_activities`
--

DROP TABLE IF EXISTS `rompos_nexo_registers_activities`;
CREATE TABLE `rompos_nexo_registers_activities` (
  `ID` int(11) NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `BALANCE` float NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `NOTE` text DEFAULT NULL,
  `REF_REGISTER` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_appointments`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_appointments`;
CREATE TABLE `rompos_nexo_restaurant_appointments` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `notes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_areas`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_areas`;
CREATE TABLE `rompos_nexo_restaurant_areas` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `REF_ROOM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_kitchens`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_kitchens`;
CREATE TABLE `rompos_nexo_restaurant_kitchens` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `REF_CATEGORY` varchar(200) NOT NULL,
  `REF_ROOM` int(11) NOT NULL,
  `PRINTER` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rompos_nexo_restaurant_kitchens`
--

INSERT INTO `rompos_nexo_restaurant_kitchens` (`ID`, `NAME`, `DESCRIPTION`, `AUTHOR`, `DATE_CREATION`, `DATE_MOD`, `REF_CATEGORY`, `REF_ROOM`, `PRINTER`) VALUES
(1, 'Kitchen', '', 0, '2019-02-24 03:45:47', '2019-02-24 03:45:47', '', 0, 'PRN1'),
(2, 'Kitchen 2', '', 0, '2019-07-24 00:00:00', '2019-07-25 00:00:00', '', 0, 'PRN2');

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_modifiers`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_modifiers`;
CREATE TABLE `rompos_nexo_restaurant_modifiers` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `REF_CATEGORY` int(11) NOT NULL,
  `DEFAULT` tinyint(1) NOT NULL,
  `PRICE` float NOT NULL,
  `IMAGE` text NOT NULL,
  `SORT_ORDER` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_modifiers_categories`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_modifiers_categories`;
CREATE TABLE `rompos_nexo_restaurant_modifiers_categories` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `FORCED` tinyint(1) NOT NULL,
  `MULTISELECT` tinyint(1) NOT NULL,
  `UPDATE_PRICE` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_rooms`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_rooms`;
CREATE TABLE `rompos_nexo_restaurant_rooms` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_tables`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_tables`;
CREATE TABLE `rompos_nexo_restaurant_tables` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `MAX_SEATS` int(11) DEFAULT NULL,
  `CURRENT_SEATS_USED` int(11) DEFAULT NULL,
  `STATUS` varchar(200) DEFAULT NULL,
  `SINCE` datetime NOT NULL,
  `BOOKING_START` datetime NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MODIFICATION` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `REF_AREA` int(11) NOT NULL,
  `CURRENT_SESSION_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_tables_relation_orders`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_tables_relation_orders`;
CREATE TABLE `rompos_nexo_restaurant_tables_relation_orders` (
  `ID` int(11) NOT NULL,
  `REF_ORDER` int(11) NOT NULL,
  `REF_TABLE` int(11) NOT NULL,
  `REF_SESSION` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_restaurant_tables_sessions`
--

DROP TABLE IF EXISTS `rompos_nexo_restaurant_tables_sessions`;
CREATE TABLE `rompos_nexo_restaurant_tables_sessions` (
  `ID` int(11) NOT NULL,
  `REF_TABLE` int(11) NOT NULL,
  `SESSION_STARTS` datetime NOT NULL,
  `SESSION_ENDS` datetime NOT NULL,
  `AUTHOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_stock_transfert`
--

DROP TABLE IF EXISTS `rompos_nexo_stock_transfert`;
CREATE TABLE `rompos_nexo_stock_transfert` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `STATUS` varchar(200) NOT NULL,
  `APPROUVED_BY` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `DESTINATION_STORE` int(11) NOT NULL,
  `DEDUCT_FROM_SOURCE` varchar(200) NOT NULL,
  `FROM_STORE` int(11) NOT NULL,
  `FROM_RESPONSE` text NOT NULL,
  `TO_RESPONSE` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_stock_transfert_items`
--

DROP TABLE IF EXISTS `rompos_nexo_stock_transfert_items`;
CREATE TABLE `rompos_nexo_stock_transfert_items` (
  `ID` int(11) NOT NULL,
  `DESIGN` varchar(200) NOT NULL,
  `SKU` varchar(200) NOT NULL,
  `QUANTITY` float NOT NULL,
  `UNIT_PRICE` float NOT NULL,
  `TOTAL_PRICE` float NOT NULL,
  `REF_ITEM` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL,
  `REF_TRANSFER` int(11) NOT NULL,
  `BARCODE` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_stores`
--

DROP TABLE IF EXISTS `rompos_nexo_stores`;
CREATE TABLE `rompos_nexo_stores` (
  `ID` int(11) NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `STATUS` varchar(200) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `IMAGE` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_stores_activities`
--

DROP TABLE IF EXISTS `rompos_nexo_stores_activities`;
CREATE TABLE `rompos_nexo_stores_activities` (
  `ID` int(11) NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `REF_STORE` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_taxes`
--

DROP TABLE IF EXISTS `rompos_nexo_taxes`;
CREATE TABLE `rompos_nexo_taxes` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `RATE` float NOT NULL,
  `TYPE` varchar(200) NOT NULL,
  `FIXED` float NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `DATE_MOD` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_nexo_users_activities`
--

DROP TABLE IF EXISTS `rompos_nexo_users_activities`;
CREATE TABLE `rompos_nexo_users_activities` (
  `ID` int(11) NOT NULL,
  `AUTHOR` int(11) NOT NULL,
  `MESSAGE` text NOT NULL,
  `DATE_CREATION` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_options`
--

DROP TABLE IF EXISTS `rompos_options`;
CREATE TABLE `rompos_options` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(200) NOT NULL,
  `value` text DEFAULT NULL,
  `autoload` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `app` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rompos_options`
--

INSERT INTO `rompos_options` (`id`, `key`, `value`, `autoload`, `user`, `app`) VALUES
(1, 'rest_key', 'chuPsWJBSpbiphxgxF3ld9pSWQQIEj5dPEkbstj0', 1, 0, 'system'),
(2, 'actives_modules', '{\"0\":\"grocerycrud\",\"1\":\"nexo\",\"3\":\"nexo_sms\",\"4\":\"lang-switcher\",\"6\":\"nsam\",\"7\":\"perm_manager\",\"11\":\"nexo-updater\",\"15\":\"nexo_premium\",\"27\":\"stock-manager\",\"28\":\"intelagy-items-presentor\",\"30\":\"nexo-dejavoo-terminal\",\"31\":\"gastro\",\"32\":\"nexo-payments-gateway\",\"33\":\"nexo-globalonepay-gateway\",\"34\":\"self-ordering\"}', 1, 0, 'system'),
(4, 'nexo_sms_invoice_template', '{{site_name}} \\n Hello {{name}}, {{order_code}} order is ready. \\n total {{order_topay}} \\n thank you for your confidence. Follow us at https://facebook.com/IntelagyLLC', 1, 0, 'system'),
(5, 'database_version', '1.2', 1, 0, 'system'),
(6, 'site_name', 'rompos', 1, 0, 'system'),
(7, 'site_language', 'en_US', 1, 0, 'system'),
(8, 'nexo_installed', 'true', 1, 0, 'system'),
(9, 'nexo_display_select_client', 'enable', 1, 0, 'system'),
(10, 'nexo_display_payment_means', 'enable', 1, 0, 'system'),
(11, 'nexo_display_amount_received', 'enable', 1, 0, 'system'),
(12, 'nexo_display_discount', 'enable', 1, 0, 'system'),
(13, 'nexo_currency_position', 'before', 1, 0, 'system'),
(14, 'nexo_receipt_theme', 'default', 1, 0, 'system'),
(15, 'nexo_enable_autoprinting', 'no', 1, 0, 'system'),
(16, 'nexo_devis_expiration', '7', 1, 0, 'system'),
(17, 'nexo_shop_street', '', 1, 0, 'system'),
(18, 'nexo_shop_pobox', '', 1, 0, 'system'),
(19, 'nexo_shop_email', '', 1, 0, 'system'),
(20, 'how_many_before_discount', '', 1, 0, 'system'),
(21, 'nexo_products_labels', '5', 1, 0, 'system'),
(22, 'nexo_codebar_height', '100', 1, 0, 'system'),
(23, 'nexo_bar_width', '3', 1, 0, 'system'),
(24, 'nexo_soundfx', 'enable', 1, 0, 'system'),
(25, 'nexo_currency', '$', 1, 0, 'system'),
(26, 'nexo_vat_percent', '10', 1, 0, 'system'),
(27, 'nexo_enable_autoprint', 'yes', 1, 0, 'system'),
(28, 'nexo_enable_smsinvoice', 'no', 1, 0, 'system'),
(29, 'nexo_currency_iso', 'USD', 1, 0, 'system'),
(30, 'nexo_compact_enabled', 'yes', 1, 0, 'system'),
(31, 'nexo_enable_shadow_price', 'no', 1, 0, 'system'),
(32, 'nexo_enable_stripe', 'yes', 1, 0, 'system'),
(33, 'stock-manager-installed', 'true', 1, 0, 'system'),
(34, 'migration_gastro', NULL, 1, 0, 'system'),
(36, 'migration_self-ordering', NULL, 1, 0, 'system'),
(37, 'user_id', '2', 1, 0, 'system'),
(38, 'nexo_first_run', 'true', 1, 0, 'system'),
(39, 'daily-log', '2019-12-24 23:59:59', 1, 0, 'system'),
(40, 'migration_nexo-updater', NULL, 1, 0, 'system'),
(41, 'nexo_updater_modules_status', '{\"status\":\"failed\",\"message\":\"The request can\'t be handled, wrong tokens has been provided.\"}', 1, 0, 'system'),
(42, 'update_menu_notices', '0', 1, 0, 'system'),
(44, 'dashboard-sidebar', 'sidebar-expanded', 1, 2, 'system'),
(45, 'migration_nexo-globalonepay-gateway', NULL, 1, 0, 'system'),
(46, 'nexo_shop_address_1', '', 1, 0, 'system'),
(47, 'nexo_shop_address_2', '', 1, 0, 'system'),
(48, 'nexo_shop_city', '', 1, 0, 'system'),
(49, 'nexo_shop_phone', '', 1, 0, 'system'),
(50, 'nexo_shop_fax', '', 1, 0, 'system'),
(51, 'nexo_other_details', 'Follow us https://www.facebook.com/IntelagyLLC/', 1, 0, 'system'),
(52, 'nexo_disable_frontend', 'enable', 1, 0, 'system'),
(53, 'enable_group_discount', 'disable', 1, 0, 'system'),
(54, 'discount_type', 'disable', 1, 0, 'system'),
(55, 'discount_percent', '', 1, 0, 'system'),
(56, 'discount_amount', '', 1, 0, 'system'),
(57, 'default_compte_client', '1', 1, 0, 'system'),
(58, 'migration_intelagy-items-presentor', NULL, 1, 0, 'system'),
(59, 'order_code', '{\"1\":\"2LNUN1\",\"2\":\"O939DD\",\"3\":\"3E532I\",\"4\":\"UKM0QF\",\"6\":\"TSVZQI\",\"7\":\"A0XW0D\",\"8\":\"6KTFUM\",\"9\":\"QLWFW4\",\"10\":\"9YJAZ8\",\"11\":\"T9H064\",\"12\":\"KGLDWO\",\"13\":\"8IWEVL\",\"14\":\"GHAOWK\",\"16\":\"D00DTW\",\"17\":\"QQH547\",\"18\":\"LHEFWG\",\"20\":\"VFL2K0\",\"21\":\"PGPXG9\",\"22\":\"G4HURE\",\"23\":\"RCERJC\",\"24\":\"8BG9V4\",\"25\":\"H6AS84\",\"26\":\"05ZZHY\",\"27\":\"W7B66J\",\"28\":\"3QVQN4\",\"31\":\"87CETV\",\"32\":\"TZUAWN\",\"33\":\"XNXUSN\",\"34\":\"U5AHCS\",\"35\":\"U7ZL9P\",\"36\":\"BN7D71\",\"37\":\"UVD4T8\",\"38\":\"FJVUK5\",\"41\":\"REULMQ\",\"42\":\"2445DH\",\"43\":\"O07RA9\",\"44\":\"F9Y4NV\",\"45\":\"YP7Z0W\",\"46\":\"2XRMSJ\",\"47\":\"HSNC4W\",\"48\":\"FV2Y21\",\"49\":\"DVB4XO\",\"51\":\"WV013E\",\"52\":\"RP7TVI\",\"53\":\"8QZ848\",\"54\":\"M6Z7EZ\",\"55\":\"JCDNBC\",\"56\":\"FQ0EHJ\",\"57\":\"9O3SWJ\",\"59\":\"RM4VHI\",\"60\":\"IOQDKY\",\"61\":\"WDHO4C\",\"62\":\"SEZEWT\",\"63\":\"BK45Y4\",\"64\":\"9VOPSA\",\"65\":\"9N4SGG\",\"66\":\"NWQ5FX\",\"67\":\"VESJM4\",\"69\":\"K1B3ZV\",\"70\":\"8J98B4\",\"71\":\"URFI27\",\"72\":\"KUK2GH\",\"73\":\"TKWYXL\",\"74\":\"B742NT\",\"75\":\"JP5DNF\",\"77\":\"I35F22\",\"78\":\"CHS8M7\",\"79\":\"T59A27\",\"81\":\"LZG35D\",\"82\":\"CA76L6\",\"83\":\"XBFM66\",\"84\":\"Q6WQI2\",\"85\":\"ZKXN7T\",\"86\":\"4N7T16\",\"87\":\"7YZK0M\",\"88\":\"UL2KP0\",\"141\":\"LS9R0E\",\"142\":\"3PYSSY\",\"143\":\"F7RUGQ\",\"144\":\"8BOI8D\",\"145\":\"G18AOK\",\"146\":\"N9S5ME\",\"147\":\"YX9SOP\",\"148\":\"71GAY7\",\"149\":\"DL2S7J\",\"152\":\"TKQQAT\",\"153\":\"EGBUIK\",\"155\":\"68J894\",\"156\":\"HP8OF8\",\"162\":\"O70JDQ\",\"163\":\"ZXYCPT\",\"165\":\"KJX64D\",\"166\":\"6CY4TE\",\"167\":\"6QN3VI\",\"169\":\"OBPR5Y\",\"171\":\"SGB1D8\",\"172\":\"WWAVTU\",\"174\":\"11XPY0\",\"175\":\"VRC45I\",\"176\":\"JXJXAD\",\"178\":\"3NMGJL\",\"180\":\"EH3HNL\",\"181\":\"FZBA97\",\"182\":\"U0ICXF\",\"183\":\"TC0CHR\",\"184\":\"0BQZ1K\",\"185\":\"8AHBIU\",\"186\":\"FD0FG8\",\"187\":\"DEBPKV\",\"188\":\"CBBEHG\",\"189\":\"Q4OMNK\",\"190\":\"5XY75O\",\"191\":\"PVQSBU\",\"193\":\"6H7EW0\",\"195\":\"52PG07\",\"196\":\"STRXVC\",\"198\":\"K3EC3R\",\"200\":\"L4NPL3\",\"204\":\"KKF9W0\",\"205\":\"EU4FGR\",\"207\":\"LAN0B4\",\"215\":\"V7K2K2\",\"218\":\"GBIYPP\",\"219\":\"3RU0CS\",\"221\":\"51E766\",\"222\":\"CGAY2L\",\"223\":\"3IFTNX\",\"224\":\"KKX90L\",\"225\":\"C0DTOO\",\"226\":\"YBMGF9\",\"227\":\"IOGPDT\",\"228\":\"WWV6EG\",\"229\":\"QUMTKS\",\"230\":\"9UCR3E\",\"231\":\"OMSINA\",\"232\":\"ATGJ08\",\"233\":\"YEJN5H\",\"234\":\"FRP7WP\",\"235\":\"Y08H8V\",\"236\":\"4TDNGY\",\"237\":\"9S9K9H\",\"238\":\"TDUYMI\",\"239\":\"V9Q6I3\",\"240\":\"Z4NO5N\",\"241\":\"BQRZCE\",\"242\":\"TA9GD1\",\"243\":\"XZCASY\",\"244\":\"Z0QAUR\",\"245\":\"G1IDK7\",\"246\":\"QEL48N\",\"247\":\"ECK0V0\",\"248\":\"CTEKSZ\",\"249\":\"EUUOOS\",\"251\":\"P7T2UF\",\"252\":\"0SMRFF\",\"253\":\"EP7WSK\",\"254\":\"L71MGX\",\"255\":\"NMQ6PQ\",\"256\":\"71074C\",\"257\":\"SWJ48U\",\"258\":\"CS3K3C\",\"259\":\"LPMGNL\",\"260\":\"8OGW50\",\"261\":\"0KKPTO\",\"262\":\"FXMWTR\",\"263\":\"2YCT4P\",\"264\":\"SDPPC8\",\"265\":\"T48PY8\",\"266\":\"TEZ961\",\"267\":\"62KTWO\",\"268\":\"7FRSAE\",\"269\":\"U1QZJK\",\"270\":\"AWI0XG\",\"271\":\"98J992\",\"272\":\"IC7IRT\",\"273\":\"RW2OSH\",\"274\":\"PMEKJW\",\"275\":\"TXR0CD\",\"276\":\"HJIKLE\",\"278\":\"WZ9LBU\",\"279\":\"10EEMA\",\"280\":\"NKUG0G\",\"281\":\"YWAUZJ\",\"282\":\"84HWKG\",\"283\":\"73KJOF\",\"284\":\"ICLCD4\",\"285\":\"1RN3JQ\",\"286\":\"6PFO0T\",\"288\":\"9DBOJY\",\"289\":\"PAD6J8\",\"290\":\"1EY93Z\",\"291\":\"40NCGS\",\"292\":\"OPWNQA\",\"293\":\"0DJW02\",\"294\":\"U0AHP8\",\"295\":\"2VO8JO\",\"296\":\"GMRG2B\",\"298\":\"1QGLE2\",\"299\":\"B0DVZ3\",\"300\":\"NP31V7\",\"301\":\"3QGPKJ\",\"302\":\"B716U7\",\"303\":\"59HW5W\",\"304\":\"XNA33V\",\"305\":\"IPETFZ\",\"306\":\"K25KV2\",\"308\":\"06TYA9\",\"309\":\"0PEDEW\",\"310\":\"WCF9AN\",\"311\":\"ECL3O7\",\"312\":\"QCM5ZS\",\"313\":\"XD8S2M\",\"314\":\"73HS7I\",\"315\":\"YZ2C2I\",\"316\":\"5NJE66\",\"318\":\"MSL2NU\",\"319\":\"Z3D0N4\",\"320\":\"3FVQ33\",\"321\":\"X15SZW\",\"322\":\"TK2QI4\",\"323\":\"SYP9KZ\",\"324\":\"X7FDUQ\",\"325\":\"3J9P9W\",\"326\":\"AXGVU9\",\"328\":\"R3H19F\",\"330\":\"JPGHFI\",\"331\":\"IV67N6\",\"332\":\"4SWPX7\",\"334\":\"7JW807\",\"335\":\"6I86GN\",\"336\":\"94N7H1\",\"337\":\"7I2SML\",\"338\":\"YW9ZL0\",\"340\":\"EH0UZR\",\"342\":\"266D4S\",\"344\":\"GWTT5L\",\"345\":\"7SBKK3\",\"346\":\"L0DO49\",\"347\":\"OC3X4D\",\"348\":\"SC3EUV\",\"349\":\"RGA8UQ\",\"350\":\"CU5WWQ\",\"352\":\"AHYA06\",\"353\":\"6SZYF9\",\"354\":\"MPEQPZ\",\"355\":\"OOR2V9\",\"357\":\"3NM6V9\",\"358\":\"TTF8AT\",\"359\":\"IAHQ71\",\"360\":\"AFQIZP\",\"361\":\"BQKYMC\",\"362\":\"GT5LIK\",\"363\":\"FO85Z7\",\"364\":\"DXDUMP\",\"365\":\"8DNWW5\",\"367\":\"OFZ6CD\",\"368\":\"UVTW82\",\"369\":\"TAUR08\",\"370\":\"UDZACR\",\"371\":\"BNCUBZ\",\"372\":\"VZBR3E\",\"373\":\"Z202PJ\",\"374\":\"OX8Q93\",\"375\":\"SNEIFN\",\"377\":\"JRED3R\",\"378\":\"QDAEW2\",\"379\":\"UK57TH\",\"380\":\"IL3G9U\",\"381\":\"N5682R\",\"382\":\"PISY4Z\",\"383\":\"IP0IAC\",\"384\":\"Y7JV84\",\"385\":\"J92R84\",\"387\":\"17381A\",\"388\":\"9XXOSK\",\"389\":\"2TD3A3\",\"390\":\"V073SR\",\"391\":\"QDCW43\",\"392\":\"K18CS5\",\"393\":\"3RDW2S\",\"394\":\"YWI658\",\"395\":\"07VLWR\",\"397\":\"5ERP5D\",\"398\":\"81IAUO\",\"399\":\"0GZOA6\",\"400\":\"N35RQZ\",\"401\":\"6P74XN\",\"402\":\"CC49TC\",\"403\":\"K6QBF8\",\"404\":\"8L4DWY\",\"405\":\"EO7SWO\",\"407\":\"YFZZDF\",\"408\":\"0AKDWM\",\"409\":\"44V79T\",\"410\":\"Y4HWXR\",\"411\":\"I5DKU8\",\"412\":\"PD1646\",\"413\":\"62Z04P\",\"414\":\"IO7LJL\",\"415\":\"G2V18W\",\"417\":\"RPS406\",\"418\":\"RKB4I6\",\"419\":\"W91XHY\",\"420\":\"B33RGQ\",\"421\":\"3S7Y2S\",\"422\":\"DM4ZMT\",\"423\":\"IY9746\",\"424\":\"OLWANB\",\"425\":\"9K835J\",\"427\":\"9AXPW0\",\"428\":\"JKMJA9\",\"429\":\"8SYSFC\",\"430\":\"H5F5SG\",\"460\":\"BS70TW\",\"461\":\"LQNZ5E\",\"463\":\"2T94XF\",\"464\":\"YRFFLE\",\"465\":\"JLGC9H\",\"470\":\"I40NPU\",\"471\":\"7WMHZO\",\"474\":\"6V1V3P\",\"475\":\"2797W1\",\"478\":\"6SAG6O\",\"479\":\"CVO8YI\",\"480\":\"QH2PKA\",\"481\":\"6G989S\",\"482\":\"BHU4T3\",\"483\":\"SWSFZ7\",\"484\":\"JIL8E8\",\"485\":\"2RCP2D\",\"486\":\"QXL5EZ\",\"487\":\"UHB65V\",\"488\":\"TSMW3K\",\"489\":\"E8E65U\",\"490\":\"D2B3I6\",\"491\":\"AHNWXZ\",\"496\":\"3VAV0Z\",\"497\":\"UKAQBZ\",\"498\":\"D2VN4R\",\"499\":\"AXK2S5\",\"500\":\"KR1613\",\"501\":\"FJ19HA\",\"502\":\"F74UYD\",\"503\":\"40MTM5\",\"504\":\"C9GF5C\",\"505\":\"6UQH1F\",\"506\":\"UWFPMK\",\"507\":\"AFEA7D\",\"508\":\"A3Y4QU\",\"509\":\"QOCJEX\",\"510\":\"CG3SRG\",\"511\":\"9CBPZR\",\"512\":\"GHH26B\",\"513\":\"82DCQ4\",\"514\":\"24OSE6\",\"515\":\"L0HLLC\",\"516\":\"VES3HV\",\"517\":\"V7VFJQ\",\"518\":\"CTACL2\",\"519\":\"HXI3WA\",\"520\":\"5YZXBL\",\"521\":\"BSNMWM\",\"522\":\"5FPFDN\",\"523\":\"0CRSCX\",\"524\":\"8WQGUL\",\"525\":\"YV5N94\",\"526\":\"DVSGRZ\",\"527\":\"DUCAON\",\"528\":\"IXWGCY\",\"529\":\"0URVOU\",\"530\":\"HM1G22\",\"531\":\"YCAM2Z\",\"532\":\"V2XQMG\",\"533\":\"PIPY8S\",\"534\":\"3WN812\",\"535\":\"5F6J8J\",\"536\":\"4RWCI7\",\"537\":\"B9VN2J\",\"538\":\"O0H6L0\",\"539\":\"8IDIH9\",\"540\":\"LWLF1G\",\"541\":\"SMJMC4\",\"542\":\"3V6AQO\",\"543\":\"VYSBOW\",\"544\":\"ZFH7JZ\",\"545\":\"A5A9I9\",\"546\":\"J4M76D\",\"547\":\"QXP9QD\",\"548\":\"0G51BH\",\"549\":\"51Q8IN\",\"550\":\"8FL6NJ\",\"551\":\"XGH5MC\",\"552\":\"9248CH\",\"553\":\"1QLBDP\",\"554\":\"4VOHZA\",\"556\":\"J58BNT\",\"561\":\"41Q361\",\"562\":\"M5BDBN\",\"563\":\"4BKAVI\",\"564\":\"80DMBS\",\"565\":\"J3PMAQ\",\"566\":\"LKH6FX\",\"568\":\"RTEIVX\",\"569\":\"PIKTEI\",\"570\":\"ZI80V1\",\"571\":\"VSWQPO\",\"572\":\"F92TFM\",\"573\":\"IP10GA\",\"574\":\"GUCLI1\",\"575\":\"7SP7MX\",\"576\":\"C81V0G\",\"577\":\"EEB3LY\",\"578\":\"2HPQSO\",\"579\":\"V43Q6L\",\"580\":\"9W9JD7\",\"581\":\"8P8BAB\",\"582\":\"BHKVYX\",\"583\":\"29MY34\",\"584\":\"6EGZGB\",\"585\":\"EPYNRD\",\"586\":\"JKEJHJ\",\"587\":\"XPCC5T\",\"588\":\"6N8M83\",\"589\":\"KBNEAM\",\"590\":\"Q8KK6L\",\"591\":\"GQQXQO\",\"592\":\"ESU42E\",\"593\":\"GYNL1T\",\"594\":\"K24WKU\",\"595\":\"S2IO1E\",\"596\":\"SMIR0S\",\"597\":\"WUX05V\",\"598\":\"JYMVYJ\",\"599\":\"DOR0GB\",\"600\":\"DC1J21\",\"601\":\"V09QT7\",\"602\":\"IYXESW\",\"604\":\"UVRCD8\",\"605\":\"KQT85L\",\"607\":\"KPXNFJ\",\"608\":\"0ROUNQ\",\"610\":\"0FKIRA\",\"611\":\"PPYOHV\",\"612\":\"LJ60BY\",\"613\":\"8H3U0U\",\"614\":\"8X6LA0\",\"615\":\"P2O47U\",\"616\":\"QKPY87\",\"617\":\"MXCACO\",\"618\":\"3HBPPG\",\"619\":\"0ND72G\",\"620\":\"N8JNSO\",\"621\":\"89I8NY\",\"622\":\"72PMX8\",\"623\":\"DYXAJM\",\"624\":\"QJWNRS\",\"625\":\"2Z2UX7\",\"626\":\"XXUO34\",\"627\":\"X0PION\",\"628\":\"PAG9U7\",\"639\":\"5CL17N\",\"640\":\"9C22KU\",\"643\":\"H7WU36\",\"644\":\"JLK0RO\",\"645\":\"3Q60W5\",\"646\":\"19T5KI\",\"647\":\"HCMY0S\",\"648\":\"MAVLNI\",\"649\":\"0SDVE2\",\"650\":\"9RZ9ZX\",\"651\":\"I1TXWF\",\"652\":\"BP9K99\",\"653\":\"RXZ7NF\",\"654\":\"G71RLG\",\"655\":\"KCPVLB\",\"656\":\"HBYPX3\",\"657\":\"PFWC4D\",\"658\":\"UAH26L\",\"659\":\"ZWHNKP\",\"660\":\"DRAO00\",\"661\":\"TC3DX6\",\"662\":\"V5JDQI\",\"663\":\"EQ1U2S\",\"664\":\"UHJO6P\",\"665\":\"P5S1N4\",\"666\":\"COIZ0K\",\"667\":\"DD5JLP\",\"668\":\"GTXXA3\",\"669\":\"PTNN2D\",\"670\":\"37AI3T\",\"671\":\"LLRKBZ\",\"672\":\"BLOYPC\",\"673\":\"23E7MC\",\"674\":\"LJ3BUV\",\"676\":\"TAO2NR\",\"677\":\"GVE9BX\",\"678\":\"WF7OE7\",\"679\":\"TKFVBN\",\"680\":\"CGADJQ\",\"681\":\"N53QYG\",\"682\":\"266XTW\",\"683\":\"IIKSDC\",\"684\":\"V1I016\",\"685\":\"FJ1FTY\",\"686\":\"ZYTRNJ\",\"687\":\"HQERIM\",\"688\":\"M1P0PV\",\"689\":\"AZ1D05\",\"690\":\"ULQS53\",\"691\":\"KCP53Q\",\"692\":\"B3AGJN\",\"693\":\"TI80SJ\",\"694\":\"46BUGG\",\"695\":\"ZN2JOZ\",\"696\":\"E3MBPV\",\"697\":\"XZ2RJF\",\"698\":\"50FJBQ\",\"699\":\"ZKXPES\",\"700\":\"86B505\",\"701\":\"D43LIO\",\"702\":\"NXKEP8\",\"703\":\"9KV12U\",\"704\":\"6NSFNE\",\"705\":\"NWY4EU\",\"706\":\"07FQKD\",\"707\":\"MY673N\",\"708\":\"NSGWIO\",\"709\":\"GYCMN9\",\"710\":\"FPFPYQ\",\"711\":\"Z786D4\",\"712\":\"QWNNT7\",\"713\":\"IOVVOW\",\"714\":\"PLYSBZ\",\"715\":\"AAK09O\",\"716\":\"J1BR89\",\"717\":\"9NTF9A\",\"718\":\"O4T3QK\",\"719\":\"MV12ND\",\"720\":\"WV8GDX\",\"721\":\"N64BPS\",\"722\":\"2JIN26\",\"723\":\"QJJDJZ\",\"724\":\"486E36\",\"726\":\"D21XXZ\",\"727\":\"LA3S8M\",\"728\":\"EVNCHM\",\"729\":\"KFPIUU\",\"730\":\"6SL9D4\",\"731\":\"OZ9TZ2\",\"732\":\"8H0YVA\",\"733\":\"QDRTT3\",\"734\":\"RFJNAC\",\"735\":\"Q5FCKE\",\"736\":\"17L598\",\"737\":\"6OGRF9\",\"738\":\"5H3O8O\",\"739\":\"P4CWCO\",\"740\":\"BCYKRD\",\"741\":\"LHQN31\",\"742\":\"O9GQ1I\",\"743\":\"J4M9UB\",\"744\":\"GFB2ME\",\"745\":\"4M43ZJ\",\"746\":\"H9RAW5\",\"747\":\"U1CLOE\",\"748\":\"9X128O\",\"749\":\"YU0LHA\",\"750\":\"DNZCGZ\",\"751\":\"SKFLSW\",\"752\":\"NS1CUE\",\"753\":\"UC4HHG\",\"754\":\"QZMN0R\",\"755\":\"IDJK88\",\"756\":\"VIR9US\",\"757\":\"KG6XJE\",\"758\":\"BJ1QBJ\",\"759\":\"MA024M\",\"760\":\"NGZIQH\",\"761\":\"NK2TFC\",\"762\":\"HGBSTP\",\"763\":\"MGK365\",\"764\":\"0RUJBS\",\"765\":\"K2C53Y\",\"766\":\"2UIUUJ\",\"767\":\"HSAT4D\",\"768\":\"QVX1PK\",\"769\":\"1XDTVJ\",\"770\":\"Z1Q7GP\",\"771\":\"4J5CPB\",\"772\":\"6P69JX\",\"773\":\"D05UCS\",\"774\":\"CMO9X5\",\"775\":\"UPCD7A\",\"776\":\"PMYS3T\",\"777\":\"O3JHLN\",\"778\":\"9765TA\",\"779\":\"O93Q7X\",\"780\":\"LDLLAJ\",\"781\":\"V783LE\",\"782\":\"OTPD9U\",\"783\":\"SRT983\",\"784\":\"880DH2\",\"785\":\"U25WPG\",\"786\":\"2W3TNK\",\"787\":\"Z4ZYN4\",\"788\":\"MKWB09\",\"789\":\"I080GX\",\"790\":\"F590ZC\",\"791\":\"ZM8TWD\",\"792\":\"T1A6AB\",\"793\":\"VHDSDE\",\"794\":\"9QK7JL\",\"795\":\"IOKJB1\",\"796\":\"DF2P03\",\"797\":\"3JMIKH\",\"798\":\"H1C2ZT\",\"799\":\"GJ9IUJ\",\"800\":\"SLBS8Y\",\"801\":\"6X7YP3\",\"803\":\"3Z1BKA\",\"804\":\"JWRSB1\",\"805\":\"L56MAJ\",\"806\":\"V58AVA\",\"807\":\"QSWWQ4\",\"808\":\"HF5G1V\",\"809\":\"WF96GA\",\"810\":\"2TT7Z1\",\"811\":\"62CFQX\",\"812\":\"YSB6V8\",\"813\":\"8Y6E7Y\",\"814\":\"HP2JFB\",\"815\":\"ARMSUB\",\"816\":\"SMNK24\",\"817\":\"RTVY40\",\"818\":\"QH262C\",\"819\":\"P6J0XQ\",\"820\":\"2BZJR0\",\"821\":\"4S3DAZ\",\"822\":\"77U9OE\",\"823\":\"NM54X1\",\"824\":\"DA9VSV\",\"825\":\"8XLGYV\",\"826\":\"Z98DM4\",\"827\":\"7IXUJE\",\"828\":\"EITMFV\",\"829\":\"GW8AR2\",\"830\":\"9TYTNF\",\"831\":\"FHYI70\",\"832\":\"3I5FEU\",\"833\":\"FBMQJI\",\"834\":\"VIQT9P\",\"835\":\"JY57OW\",\"836\":\"QDRQWG\",\"837\":\"29EA43\",\"838\":\"NHKXA9\",\"839\":\"GUP2GD\",\"840\":\"QVPS7F\",\"841\":\"J5MH2U\",\"842\":\"IGOR2O\",\"843\":\"NQC74N\",\"844\":\"OKYQES\",\"845\":\"F8ZNXQ\",\"846\":\"3V8NH5\",\"847\":\"BX39IP\",\"848\":\"1RM345\",\"849\":\"YM5VKF\",\"850\":\"6SDAFE\",\"851\":\"N7CKOU\",\"852\":\"N71SKI\",\"853\":\"BZYOM0\",\"854\":\"7SDE8U\",\"855\":\"26YD2Q\",\"856\":\"6FCR6I\",\"857\":\"AO7AC8\",\"858\":\"TXZHD3\",\"859\":\"LJG0GR\",\"860\":\"UBHTMJ\",\"861\":\"BC4I1Y\",\"862\":\"Z1QMXD\",\"863\":\"JNRZ7V\",\"864\":\"0V7UX2\",\"865\":\"35QGZE\",\"866\":\"K4R14A\",\"867\":\"NIRTFK\",\"868\":\"8QCTOP\",\"869\":\"ZTF6OY\",\"870\":\"AMMP9P\",\"871\":\"B7R42G\",\"872\":\"T9BUGJ\",\"873\":\"51GEGW\",\"874\":\"S13721\",\"875\":\"D3D1OO\",\"876\":\"VQAX0S\",\"877\":\"025NBH\",\"878\":\"T6K62G\",\"879\":\"1RWC8K\",\"880\":\"GR2OGS\",\"881\":\"TIBTWX\",\"882\":\"PUQK1V\",\"883\":\"2O0RHX\",\"884\":\"2PKG1Q\",\"885\":\"CJ9808\",\"886\":\"W0OIXA\",\"887\":\"HCSFIL\",\"888\":\"2OIQ3L\",\"889\":\"KRL9M2\",\"890\":\"88SGU2\",\"891\":\"7TB24H\",\"892\":\"2RPNDS\",\"893\":\"HD1PB6\",\"894\":\"ZZEBFK\",\"895\":\"VHQ4C1\",\"896\":\"0NVGN3\",\"898\":\"OAB2LB\",\"899\":\"C4IYHP\",\"900\":\"DS57U7\",\"901\":\"CT85D7\",\"902\":\"Y5KCL9\",\"903\":\"KJSOAR\",\"904\":\"Z09XJL\",\"905\":\"1O202P\",\"906\":\"JEVK1A\",\"907\":\"4UA52I\",\"908\":\"8HR2C7\",\"909\":\"O2EW4K\",\"910\":\"S2JTNY\",\"911\":\"JZ06YR\",\"912\":\"HFQVZQ\",\"913\":\"3D3CO6\"}', 0, 0, 'system'),
(60, 'site_registration', '0', 1, 0, 'system'),
(61, 'require_validation', '0', 1, 0, 'system'),
(62, 'webdev_mode', '0', 1, 0, 'system'),
(63, 'so_default_author', '2', 1, 0, 'system'),
(64, 'so_timeout_before_closing_popup', '', 1, 0, 'system'),
(65, 'so_homepage_title', '', 1, 0, 'system'),
(66, 'so_nps_url', '', 1, 0, 'system'),
(67, 'so_maximum_seats', '', 1, 0, 'system'),
(68, 'so_allowed_order_status', 'show_all', 1, 0, 'system'),
(69, 'so_allow_table_selection', 'no', 1, 0, 'system'),
(70, 'fresh_order_min', '1', 1, 0, 'system'),
(71, 'late_order_min', '2', 1, 0, 'system'),
(72, 'too_late_order_min', '3', 1, 0, 'system'),
(73, 'fresh_order_color', 'box-default', 1, 0, 'system'),
(74, 'late_order_color', 'bg-warning box-warning', 1, 0, 'system'),
(75, 'too_late_order_color', 'bg-danger box-danger', 1, 0, 'system'),
(76, 'refreshing_seconds', '5', 1, 0, 'system'),
(77, 'disable_kitchen_screen', 'no', 1, 0, 'system'),
(78, 'disable_waiter_screen', 'no', 1, 0, 'system'),
(79, 'disable_pos_waiter', 'no', 1, 0, 'system'),
(80, 'disable_kitchen_print', 'yes', 1, 0, 'system'),
(81, 'disable_area_rooms', 'no', 1, 0, 'system'),
(82, 'disable_kitchens', '0', 1, 0, 'system'),
(83, 'so_default_table', '1', 1, 0, 'system'),
(84, 'takeaway_kitchen', '1', 1, 0, 'system'),
(85, 'enable_order_aging', 'yes', 1, 0, 'system'),
(86, 'nexo_code_type', 'order_code', 1, 0, 'system'),
(87, 'expiring_order_type', '', 1, 0, 'system'),
(88, 'expiration_time', '1', 1, 0, 'system'),
(89, 'site_description', '', 1, 0, 'system'),
(90, 'site_timezone', 'US/Eastern', 1, 0, 'system'),
(91, 'first-name', 'test', 1, 3, 'users'),
(92, 'last-name', 'test', 1, 3, 'users'),
(93, 'theme-skin', 'skin-blue', 1, 3, 'users'),
(94, 'first-name', 'test', 1, 4, 'users'),
(95, 'last-name', 'test', 1, 4, 'users'),
(96, 'theme-skin', 'skin-blue', 1, 4, 'users'),
(101, 'nexopos_store_access_key', 'chuPsWJBSpbiphxgxF3ld9pSWQQIEj5dPEkbstj0', 1, 0, 'system'),
(103, 'nexo_store', 'disabled', 1, 0, 'system'),
(104, 'first-name', 'Demo', 1, 2, 'users'),
(105, 'last-name', 'Account', 1, 2, 'users'),
(106, 'theme-skin', 'skin-blue-light', 1, 2, 'users'),
(107, 'first-name', 'Shop', 1, 5, 'users'),
(108, 'last-name', 'Manager', 1, 5, 'users'),
(109, 'theme-skin', 'skin-blue', 1, 5, 'users'),
(117, 'store_access_5_1', 'yes', 1, 5, 'system'),
(118, 'so_tax_status', 'enabled', 1, 0, 'system'),
(119, 'so_tax_value', '0.05', 1, 0, 'system'),
(120, 'so_allow_hidden_items', 'no', 1, 0, 'system'),
(121, 'so_hide_empty_cats', 'no', 1, 0, 'system'),
(122, 'item_name', 'only_primary', 1, 0, 'system'),
(123, 'url_to_logo', '/public/img/logo-bw.png', 1, 0, 'system'),
(124, 'logo_height', '', 1, 0, 'system'),
(125, 'logo_width', '', 1, 0, 'system'),
(126, 'receipt_col_1', '', 1, 0, 'system'),
(127, 'receipt_col_2', '', 1, 0, 'system'),
(128, 'nexo_bills_notices', 'Follow us  https://www.facebook.com/IntelagyLLC/\r\nhttps://intelagy.com\r\n', 1, 0, 'system'),
(129, 'transfert_column_1', '{provider_store_name}', 1, 0, 'system'),
(130, 'transfert_column_2', '{receiver_store_name}', 1, 0, 'system'),
(131, 'notification_receiver_email', '', 1, 0, 'system'),
(132, 'enable_email_notification', 'no', 1, 0, 'system'),
(133, 'deduct_from_store', 'yes', 1, 0, 'system'),
(141, 'nexo_print_gateway', '', 1, 0, 'system'),
(142, 'nexo_print_server_url', '', 1, 0, 'system'),
(143, 'nexo_pos_printer', '', 1, 0, 'system'),
(144, 'unit_price_changing', '', 1, 0, 'system'),
(145, 'show_item_taxes', '', 1, 0, 'system'),
(146, 'enable_quick_search', '', 1, 0, 'system'),
(147, 'auto_submit_barcode_entry', '', 1, 0, 'system'),
(148, 'nexo_enable_numpad', '', 1, 0, 'system'),
(149, 'keyshortcuts', '10|20|50|100', 1, 0, 'system'),
(150, 'disable_partial_order', 'yes', 1, 0, 'system'),
(151, 'takeaway_timer', '20', 1, 0, 'system'),
(152, 'delivery_timer', '30', 1, 0, 'system'),
(153, 'nexo_enable_globalonepay', 'yes', 1, 0, 'system'),
(154, 'nexo_globalonepay_endpoint', 'https://testpayments.globalone.me/merchant/xmlpayment', 1, 0, 'system'),
(155, 'nexo_globalonepay_terminal_id', '33001', 1, 0, 'system'),
(156, 'nexo_globalonepay_shared_secret', 'SandboxSecret001', 1, 0, 'system'),
(157, 'nexo_globalonepay_logs', 'yes', 1, 0, 'system'),
(158, 'nexo_enable_registers', 'non', 1, 0, 'system'),
(159, 'nexo_vat_type', '', 1, 0, 'system'),
(160, 'unit_item_discount_enabled', '', 1, 0, 'system'),
(161, 'hide_discount_button', '', 1, 0, 'system'),
(162, 'disable_coupon', '', 1, 0, 'system'),
(163, 'disable_shipping', 'yes', 1, 0, 'system'),
(164, 'disable_customer_creation', '', 1, 0, 'system'),
(165, 'disable_quick_item', '', 1, 0, 'system'),
(166, 'nexo_cashier_session_counted', '', 1, 0, 'system'),
(167, 'nexo_cashier_idle_after', '5', 1, 0, 'system'),
(168, 'base_url', 'http://api.rompos.intelagy.com/', 1, 0, 'system'),
(169, 'updater2_validated', '873', 1, 0, 'system'),
(170, 'oauth_api_url', '', 1, 0, 'system'),
(171, 'oauth_consumer_key', '', 1, 0, 'system'),
(172, 'oauth_consumer_secret', '', 1, 0, 'system'),
(173, 'oauth_token', '', 1, 0, 'system'),
(174, 'oauth_token_secret', '', 1, 0, 'system'),
(175, 'oauth_verifier', '', 1, 0, 'system'),
(176, 'nexo_premium_enable_history', 'yes', 1, 0, 'system'),
(177, 'nexo_logo_type', 'default', 1, 0, 'system'),
(178, 'nexo_logo_text', 'RomPOS', 1, 0, 'system'),
(179, 'nexo_logo_url', '', 1, 0, 'system'),
(180, 'nexo_logo_width', '', 1, 0, 'system'),
(181, 'nexo_logo_height', '', 1, 0, 'system'),
(182, 'nexo_footer_text', '', 1, 0, 'system'),
(183, 'nexo_date_format', '', 1, 0, 'system'),
(184, 'nexo_datetime_format', '', 1, 0, 'system'),
(185, 'nexo_js_datetime_format', '', 1, 0, 'system'),
(186, 'nexo_stripe_publishable_key', '123', 1, 0, 'system'),
(187, 'nexo_stripe_secret_key', '123', 1, 0, 'system'),
(188, 'nexo_saved_barcode', '[\"887565\",\"574039\",\"986019\",\"367994\",\"942678\",\"910350\",\"787091\",\"334848\",\"561218\",\"611713\",\"479366\",\"685167\",\"646809\",\"336957\",\"346031\",\"295796\",\"969732\",\"141204\",\"677637\",\"750951\",\"151208\",\"465105\",\"400097\",\"609513\",\"906035\",\"858096\",\"291394\",\"478849\",\"914384\",\"889062\",\"292897\",\"872570\",\"606518\",\"768505\",\"754605\",\"744492\",\"160953\",\"938701\",\"797592\",\"341290\",\"101985\",\"845637\",\"379817\",\"760019\",\"812016\",\"535692\",\"426478\",\"254031\",\"698720\",\"961376\",\"298226\",\"594158\",\"803764\",\"985869\",\"306537\"]', 1, 0, 'system'),
(189, 'first-name', 'John', 1, 6, 'users'),
(190, 'last-name', 'Doe', 1, 6, 'users'),
(191, 'theme-skin', 'skin-blue', 1, 6, 'users'),
(192, 'first-name', 'Chief', 1, 7, 'users'),
(193, 'last-name', '', 1, 7, 'users'),
(194, 'theme-skin', 'skin-blue', 1, 7, 'users'),
(195, 'gastro_print_gateway', 'nps', 1, 0, 'system'),
(196, 'printing_option', 'kitchen_printers', 1, 0, 'system'),
(197, 'restaurant_envato_licence', '', 1, 0, 'system'),
(198, 'printer_gpc_proxy', '', 1, 0, 'system'),
(199, 'enable_kitchen_synthesizer', '', 1, 0, 'system'),
(200, 'disable_takeaway', 'no', 1, 0, 'system'),
(201, 'disable_dinein', 'no', 1, 0, 'system'),
(202, 'disable_delivery', 'no', 1, 0, 'system'),
(203, 'disable_readyorders', '0', 1, 0, 'system'),
(204, 'disable_pendingorders', '0', 1, 0, 'system'),
(205, 'disable_saleslist', '0', 1, 0, 'system'),
(206, 'gastro_show_booked_at_kitchen', 'yes', 1, 0, 'system'),
(207, 'printer_kitchen_1', 'Star TSP100 Cutter (TSP143)', 1, 0, 'system'),
(208, 'printer_kitchen_2', 'Star TSP100 Cutter (TSP143)', 1, 0, 'system'),
(209, 'orders_sync_url', 'http://rompos.local', 1, 0, 'system'),
(379, 'nexo_receipt_header', 'yes', 1, 0, 'system'),
(566, 'empty_time_gap', '20', 1, 0, 'system'),
(567, 'enable_modifiers_suffixes', '0', 1, 0, 'system'),
(568, 'modifiers_suffix', '', 1, 0, 'system'),
(583, 'gastro_nexostore_split_print', 'yes', 1, 0, 'system'),
(584, 'gastro_cook_time_for_booked', '0', 1, 0, 'system'),
(585, 'printer_nexostore', 'Star TSP100 Cutter (TSP143)', 1, 0, 'system'),
(586, 'nexo_pos_printer_vendor', 'star', 1, 0, 'system'),
(587, 'printer_takeway', 'Star TSP100 Cutter (TSP143)', 1, 0, 'system'),
(588, 'printer_template_vendor', 'star', 1, 0, 'system'),
(589, 'order_status_shown', 'all', 1, 0, 'system'),
(590, 'gastro_kitchen_sort', 'old_to_new', 1, 0, 'system'),
(591, 'kitchen_card_title', 'h1', 1, 0, 'system'),
(592, 'kitchen_item_font', '11', 1, 0, 'system'),
(597, 'orders_client_id', '', 1, 0, 'system'),
(826, 'gastro_display_only_unpaid_ready_orders', '', 1, 0, 'system'),
(827, 'gastro_disable_orders_fetch', '', 1, 0, 'system'),
(828, 'gastro_freed_tables', '', 1, 0, 'system'),
(829, 'gastro_order_refresh_interval', '1000', 1, 0, 'system'),
(1026, 'orders_overwrite_id', '1', 1, 0, 'system'),
(1028, 'allow_one_click', '1', 1, 0, 'system'),
(1029, 'nexo_enable_dejavoo_terminal', 'yes', 1, 0, 'system'),
(1030, 'nexo_dejavoo_terminal_ip', '127.0.0.1', 1, 0, 'system'),
(1031, 'nexo_dejavoo_terminal_port', '8000', 1, 0, 'system'),
(1032, 'nexo_dejavoo_terminal_service_port', '8001', 1, 0, 'system'),
(1033, 'nexo_dejavoo_auth_key', '123123123123123123123', 1, 0, 'system'),
(1034, 'nexo_dejavoo_terminal_url', 'http://127.0.0.1', 1, 0, 'system'),
(1035, 'nexo_dejavoo_register_id', '5', 1, 0, 'system'),
(1036, 'nexo_dejavoo_logs', 'yes', 1, 0, 'system'),
(1044, 'nexo_sms_service', 'disable', 1, 0, 'system'),
(1045, 'nexo_twilio_account_sid', '', 1, 0, 'system'),
(1046, 'nexo_twilio_account_token', '', 1, 0, 'system'),
(1047, 'nexo_twilio_from_number', '', 1, 0, 'system'),
(1057, 'gastro_print_sale_receipt', '0', 1, 0, 'system'),
(1058, 'gastro_kitchen_autostatus', 'completed', 1, 0, 'system'),
(1061, 'nexo_use_cashdrawer', '', 1, 0, 'system'),
(1062, 'nexo_register_notices', 'disable', 1, 0, 'system'),
(1063, 'nexo_logo_mini_url', '', 1, 0, 'system');

-- --------------------------------------------------------

--
-- Table structure for table `rompos_reports_clients`
--

DROP TABLE IF EXISTS `rompos_reports_clients`;
CREATE TABLE `rompos_reports_clients` (
  `id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_reports_client_dates`
--

DROP TABLE IF EXISTS `rompos_reports_client_dates`;
CREATE TABLE `rompos_reports_client_dates` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_reports_customers`
--

DROP TABLE IF EXISTS `rompos_reports_customers`;
CREATE TABLE `rompos_reports_customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_reports_orders`
--

DROP TABLE IF EXISTS `rompos_reports_orders`;
CREATE TABLE `rompos_reports_orders` (
  `id` int(11) NOT NULL,
  `code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'takeaway, delivery, drive-in, etc',
  `subtype` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total` decimal(15,3) NOT NULL,
  `payment_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'cod,cash,creditcard,etc',
  `client_id` int(11) NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pending,completed,etc',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_reports_order_payments`
--

DROP TABLE IF EXISTS `rompos_reports_order_payments`;
CREATE TABLE `rompos_reports_order_payments` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,3) NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'cod,cash,creditcard,etc',
  `operation` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'incoming,outcoming',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_reports_order_products`
--

DROP TABLE IF EXISTS `rompos_reports_order_products`;
CREATE TABLE `rompos_reports_order_products` (
  `id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `codebar` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(15,3) NOT NULL,
  `sale_price` decimal(15,3) NOT NULL,
  `total` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_reports_product_modifiers`
--

DROP TABLE IF EXISTS `rompos_reports_product_modifiers`;
CREATE TABLE `rompos_reports_product_modifiers` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_restapi_keys`
--

DROP TABLE IF EXISTS `rompos_restapi_keys`;
CREATE TABLE `rompos_restapi_keys` (
  `id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `scopes` text DEFAULT NULL,
  `app_name` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `user` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rompos_restapi_keys`
--

INSERT INTO `rompos_restapi_keys` (`id`, `key`, `scopes`, `app_name`, `level`, `ignore_limits`, `user`, `date_created`, `expire`) VALUES
(1, 'chuPsWJBSpbiphxgxF3ld9pSWQQIEj5dPEkbstj0', 'core', 'ROMPos CMS', 0, 0, 2, '2018-12-11 21:05:34', '2023-12-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rompos_sync_orders`
--

DROP TABLE IF EXISTS `rompos_sync_orders`;
CREATE TABLE `rompos_sync_orders` (
  `tm` timestamp NOT NULL DEFAULT current_timestamp(),
  `latest` int(11) NOT NULL,
  `client_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rompos_system_sessions`
--

DROP TABLE IF EXISTS `rompos_system_sessions`;
CREATE TABLE `rompos_system_sessions` (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rompos_aauth_groups`
--
ALTER TABLE `rompos_aauth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rompos_aauth_perms`
--
ALTER TABLE `rompos_aauth_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rompos_aauth_perm_to_group`
--
ALTER TABLE `rompos_aauth_perm_to_group`
  ADD PRIMARY KEY (`perm_id`,`group_id`);

--
-- Indexes for table `rompos_aauth_perm_to_user`
--
ALTER TABLE `rompos_aauth_perm_to_user`
  ADD PRIMARY KEY (`perm_id`,`user_id`);

--
-- Indexes for table `rompos_aauth_pms`
--
ALTER TABLE `rompos_aauth_pms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `full_index` (`id`,`sender_id`,`receiver_id`,`read`);

--
-- Indexes for table `rompos_aauth_system_variables`
--
ALTER TABLE `rompos_aauth_system_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rompos_aauth_users`
--
ALTER TABLE `rompos_aauth_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rompos_aauth_user_to_group`
--
ALTER TABLE `rompos_aauth_user_to_group`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `rompos_aauth_user_variables`
--
ALTER TABLE `rompos_aauth_user_variables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_index` (`user_id`);

--
-- Indexes for table `rompos_nexo_arrivages`
--
ALTER TABLE `rompos_nexo_arrivages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_articles`
--
ALTER TABLE `rompos_nexo_articles`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `SKU` (`SKU`),
  ADD UNIQUE KEY `CODEBAR` (`CODEBAR`),
  ADD KEY `REF_PROVIDER` (`REF_PROVIDER`),
  ADD KEY `REF_CATEGORIE` (`REF_CATEGORIE`);

--
-- Indexes for table `rompos_nexo_articles_meta`
--
ALTER TABLE `rompos_nexo_articles_meta`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_ARTICLE` (`REF_ARTICLE`);

--
-- Indexes for table `rompos_nexo_articles_stock_flow`
--
ALTER TABLE `rompos_nexo_articles_stock_flow`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_COMMAND_CODE` (`REF_COMMAND_CODE`),
  ADD KEY `REF_SHIPPING` (`REF_SHIPPING`);

--
-- Indexes for table `rompos_nexo_articles_variations`
--
ALTER TABLE `rompos_nexo_articles_variations`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_ARTICLE` (`REF_ARTICLE`);

--
-- Indexes for table `rompos_nexo_categories`
--
ALTER TABLE `rompos_nexo_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_clients`
--
ALTER TABLE `rompos_nexo_clients`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_clients_address`
--
ALTER TABLE `rompos_nexo_clients_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_client` (`ref_client`);

--
-- Indexes for table `rompos_nexo_clients_groups`
--
ALTER TABLE `rompos_nexo_clients_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_clients_meta`
--
ALTER TABLE `rompos_nexo_clients_meta`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_CLIENT` (`REF_CLIENT`);

--
-- Indexes for table `rompos_nexo_commandes`
--
ALTER TABLE `rompos_nexo_commandes`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CODE` (`CODE`),
  ADD KEY `AUTHOR` (`AUTHOR`),
  ADD KEY `REF_CLIENT` (`REF_CLIENT`),
  ADD KEY `RESTAURANT_ORDER_STATUS` (`RESTAURANT_ORDER_STATUS`),
  ADD KEY `TYPE` (`TYPE`),
  ADD KEY `DATE_CREATION` (`DATE_CREATION`);

--
-- Indexes for table `rompos_nexo_commandes_coupons`
--
ALTER TABLE `rompos_nexo_commandes_coupons`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_COMMAND` (`REF_COMMAND`),
  ADD KEY `REF_COUPON` (`REF_COUPON`);

--
-- Indexes for table `rompos_nexo_commandes_meta`
--
ALTER TABLE `rompos_nexo_commandes_meta`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_ORDER_ID` (`REF_ORDER_ID`),
  ADD KEY `KEY` (`KEY`);

--
-- Indexes for table `rompos_nexo_commandes_paiements`
--
ALTER TABLE `rompos_nexo_commandes_paiements`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_COMMAND_CODE` (`REF_COMMAND_CODE`);

--
-- Indexes for table `rompos_nexo_commandes_produits`
--
ALTER TABLE `rompos_nexo_commandes_produits`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_COMMAND_CODE` (`REF_COMMAND_CODE`),
  ADD KEY `RESTAURANT_PRODUCT_REAL_BARCODE` (`RESTAURANT_PRODUCT_REAL_BARCODE`);

--
-- Indexes for table `rompos_nexo_commandes_produits_meta`
--
ALTER TABLE `rompos_nexo_commandes_produits_meta`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_COMMAND_PRODUCT` (`REF_COMMAND_PRODUCT`),
  ADD KEY `REF_COMMAND_CODE` (`REF_COMMAND_CODE`);

--
-- Indexes for table `rompos_nexo_commandes_refunds`
--
ALTER TABLE `rompos_nexo_commandes_refunds`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_ORDER` (`REF_ORDER`);

--
-- Indexes for table `rompos_nexo_commandes_refunds_products`
--
ALTER TABLE `rompos_nexo_commandes_refunds_products`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_ITEM` (`REF_ITEM`),
  ADD KEY `REF_REFUND` (`REF_REFUND`);

--
-- Indexes for table `rompos_nexo_commandes_shippings`
--
ALTER TABLE `rompos_nexo_commandes_shippings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_shipping` (`ref_shipping`),
  ADD KEY `ref_order` (`ref_order`);

--
-- Indexes for table `rompos_nexo_commandes_status`
--
ALTER TABLE `rompos_nexo_commandes_status`
  ADD KEY `tm` (`tm`);

--
-- Indexes for table `rompos_nexo_coupons`
--
ALTER TABLE `rompos_nexo_coupons`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_daily_log`
--
ALTER TABLE `rompos_nexo_daily_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_fournisseurs`
--
ALTER TABLE `rompos_nexo_fournisseurs`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_fournisseurs_history`
--
ALTER TABLE `rompos_nexo_fournisseurs_history`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_historique`
--
ALTER TABLE `rompos_nexo_historique`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_items_filters`
--
ALTER TABLE `rompos_nexo_items_filters`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_notices`
--
ALTER TABLE `rompos_nexo_notices`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_USER` (`REF_USER`);

--
-- Indexes for table `rompos_nexo_premium_backups`
--
ALTER TABLE `rompos_nexo_premium_backups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_premium_factures`
--
ALTER TABLE `rompos_nexo_premium_factures`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_premium_factures_categories`
--
ALTER TABLE `rompos_nexo_premium_factures_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_premium_factures_items`
--
ALTER TABLE `rompos_nexo_premium_factures_items`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_rayons`
--
ALTER TABLE `rompos_nexo_rayons`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_registers`
--
ALTER TABLE `rompos_nexo_registers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_registers_activities`
--
ALTER TABLE `rompos_nexo_registers_activities`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_restaurant_appointments`
--
ALTER TABLE `rompos_nexo_restaurant_appointments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `table_id` (`table_id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `start_date` (`start_date`);

--
-- Indexes for table `rompos_nexo_restaurant_areas`
--
ALTER TABLE `rompos_nexo_restaurant_areas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_restaurant_kitchens`
--
ALTER TABLE `rompos_nexo_restaurant_kitchens`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_CATEGORY` (`REF_CATEGORY`),
  ADD KEY `REF_ROOM` (`REF_ROOM`);

--
-- Indexes for table `rompos_nexo_restaurant_modifiers`
--
ALTER TABLE `rompos_nexo_restaurant_modifiers`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_CATEGORY` (`REF_CATEGORY`),
  ADD KEY `SORT_ORDER` (`SORT_ORDER`);

--
-- Indexes for table `rompos_nexo_restaurant_modifiers_categories`
--
ALTER TABLE `rompos_nexo_restaurant_modifiers_categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_restaurant_rooms`
--
ALTER TABLE `rompos_nexo_restaurant_rooms`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_restaurant_tables`
--
ALTER TABLE `rompos_nexo_restaurant_tables`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_restaurant_tables_relation_orders`
--
ALTER TABLE `rompos_nexo_restaurant_tables_relation_orders`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_restaurant_tables_sessions`
--
ALTER TABLE `rompos_nexo_restaurant_tables_sessions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_stock_transfert`
--
ALTER TABLE `rompos_nexo_stock_transfert`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_stock_transfert_items`
--
ALTER TABLE `rompos_nexo_stock_transfert_items`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_stores`
--
ALTER TABLE `rompos_nexo_stores`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_stores_activities`
--
ALTER TABLE `rompos_nexo_stores_activities`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `REF_STORE` (`REF_STORE`);

--
-- Indexes for table `rompos_nexo_taxes`
--
ALTER TABLE `rompos_nexo_taxes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_nexo_users_activities`
--
ALTER TABLE `rompos_nexo_users_activities`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rompos_options`
--
ALTER TABLE `rompos_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rompos_reports_clients`
--
ALTER TABLE `rompos_reports_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rompos_reports_client_dates`
--
ALTER TABLE `rompos_reports_client_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_date_id_fk` (`client_id`);

--
-- Indexes for table `rompos_reports_customers`
--
ALTER TABLE `rompos_reports_customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `rompos_reports_orders`
--
ALTER TABLE `rompos_reports_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id_fk` (`customer_id`),
  ADD KEY `client_id_fk` (`client_id`);

--
-- Indexes for table `rompos_reports_order_payments`
--
ALTER TABLE `rompos_reports_order_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_payment_id_fk` (`order_id`);

--
-- Indexes for table `rompos_reports_order_products`
--
ALTER TABLE `rompos_reports_order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id_fk` (`order_id`);

--
-- Indexes for table `rompos_reports_product_modifiers`
--
ALTER TABLE `rompos_reports_product_modifiers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id_fk` (`product_id`);

--
-- Indexes for table `rompos_restapi_keys`
--
ALTER TABLE `rompos_restapi_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rompos_sync_orders`
--
ALTER TABLE `rompos_sync_orders`
  ADD KEY `tm` (`tm`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rompos_aauth_groups`
--
ALTER TABLE `rompos_aauth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `rompos_aauth_perms`
--
ALTER TABLE `rompos_aauth_perms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `rompos_aauth_pms`
--
ALTER TABLE `rompos_aauth_pms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_aauth_system_variables`
--
ALTER TABLE `rompos_aauth_system_variables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_aauth_users`
--
ALTER TABLE `rompos_aauth_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rompos_aauth_user_variables`
--
ALTER TABLE `rompos_aauth_user_variables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_arrivages`
--
ALTER TABLE `rompos_nexo_arrivages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rompos_nexo_articles`
--
ALTER TABLE `rompos_nexo_articles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;

--
-- AUTO_INCREMENT for table `rompos_nexo_articles_meta`
--
ALTER TABLE `rompos_nexo_articles_meta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_articles_stock_flow`
--
ALTER TABLE `rompos_nexo_articles_stock_flow`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_articles_variations`
--
ALTER TABLE `rompos_nexo_articles_variations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_categories`
--
ALTER TABLE `rompos_nexo_categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `rompos_nexo_clients`
--
ALTER TABLE `rompos_nexo_clients`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `rompos_nexo_clients_address`
--
ALTER TABLE `rompos_nexo_clients_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rompos_nexo_clients_groups`
--
ALTER TABLE `rompos_nexo_clients_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rompos_nexo_clients_meta`
--
ALTER TABLE `rompos_nexo_clients_meta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes`
--
ALTER TABLE `rompos_nexo_commandes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=965;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_coupons`
--
ALTER TABLE `rompos_nexo_commandes_coupons`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_meta`
--
ALTER TABLE `rompos_nexo_commandes_meta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_paiements`
--
ALTER TABLE `rompos_nexo_commandes_paiements`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_produits`
--
ALTER TABLE `rompos_nexo_commandes_produits`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_produits_meta`
--
ALTER TABLE `rompos_nexo_commandes_produits_meta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_refunds`
--
ALTER TABLE `rompos_nexo_commandes_refunds`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_refunds_products`
--
ALTER TABLE `rompos_nexo_commandes_refunds_products`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_commandes_shippings`
--
ALTER TABLE `rompos_nexo_commandes_shippings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_coupons`
--
ALTER TABLE `rompos_nexo_coupons`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_daily_log`
--
ALTER TABLE `rompos_nexo_daily_log`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_fournisseurs`
--
ALTER TABLE `rompos_nexo_fournisseurs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rompos_nexo_fournisseurs_history`
--
ALTER TABLE `rompos_nexo_fournisseurs_history`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_historique`
--
ALTER TABLE `rompos_nexo_historique`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_items_filters`
--
ALTER TABLE `rompos_nexo_items_filters`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_notices`
--
ALTER TABLE `rompos_nexo_notices`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_premium_backups`
--
ALTER TABLE `rompos_nexo_premium_backups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_premium_factures`
--
ALTER TABLE `rompos_nexo_premium_factures`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_premium_factures_categories`
--
ALTER TABLE `rompos_nexo_premium_factures_categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_premium_factures_items`
--
ALTER TABLE `rompos_nexo_premium_factures_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_rayons`
--
ALTER TABLE `rompos_nexo_rayons`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_registers`
--
ALTER TABLE `rompos_nexo_registers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_registers_activities`
--
ALTER TABLE `rompos_nexo_registers_activities`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_appointments`
--
ALTER TABLE `rompos_nexo_restaurant_appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_areas`
--
ALTER TABLE `rompos_nexo_restaurant_areas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_kitchens`
--
ALTER TABLE `rompos_nexo_restaurant_kitchens`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_modifiers`
--
ALTER TABLE `rompos_nexo_restaurant_modifiers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_modifiers_categories`
--
ALTER TABLE `rompos_nexo_restaurant_modifiers_categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_rooms`
--
ALTER TABLE `rompos_nexo_restaurant_rooms`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_tables`
--
ALTER TABLE `rompos_nexo_restaurant_tables`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_tables_relation_orders`
--
ALTER TABLE `rompos_nexo_restaurant_tables_relation_orders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_restaurant_tables_sessions`
--
ALTER TABLE `rompos_nexo_restaurant_tables_sessions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_stock_transfert`
--
ALTER TABLE `rompos_nexo_stock_transfert`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_stock_transfert_items`
--
ALTER TABLE `rompos_nexo_stock_transfert_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_stores`
--
ALTER TABLE `rompos_nexo_stores`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_stores_activities`
--
ALTER TABLE `rompos_nexo_stores_activities`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_taxes`
--
ALTER TABLE `rompos_nexo_taxes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_nexo_users_activities`
--
ALTER TABLE `rompos_nexo_users_activities`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_options`
--
ALTER TABLE `rompos_options`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1066;

--
-- AUTO_INCREMENT for table `rompos_reports_clients`
--
ALTER TABLE `rompos_reports_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_reports_client_dates`
--
ALTER TABLE `rompos_reports_client_dates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_reports_customers`
--
ALTER TABLE `rompos_reports_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_reports_orders`
--
ALTER TABLE `rompos_reports_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_reports_order_payments`
--
ALTER TABLE `rompos_reports_order_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_reports_order_products`
--
ALTER TABLE `rompos_reports_order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_reports_product_modifiers`
--
ALTER TABLE `rompos_reports_product_modifiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rompos_restapi_keys`
--
ALTER TABLE `rompos_restapi_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rompos_reports_client_dates`
--
ALTER TABLE `rompos_reports_client_dates`
  ADD CONSTRAINT `client_date_id_fk` FOREIGN KEY (`client_id`) REFERENCES `rompos_reports_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rompos_reports_orders`
--
ALTER TABLE `rompos_reports_orders`
  ADD CONSTRAINT `client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `rompos_reports_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `rompos_reports_customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rompos_reports_order_payments`
--
ALTER TABLE `rompos_reports_order_payments`
  ADD CONSTRAINT `order_payment_id_fk` FOREIGN KEY (`order_id`) REFERENCES `rompos_reports_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rompos_reports_order_products`
--
ALTER TABLE `rompos_reports_order_products`
  ADD CONSTRAINT `order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `rompos_reports_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rompos_reports_product_modifiers`
--
ALTER TABLE `rompos_reports_product_modifiers`
  ADD CONSTRAINT `product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `rompos_reports_order_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
