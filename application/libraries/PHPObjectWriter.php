<?php

/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 11/27/19
 * Time: 12:15 PM
 */
class PHPObjectWriter
{
    protected $content = '';
    /**
     * Defines content-type for HTTP header
     *
     * @access  protected
     * @var     string
     */
    protected $content_type = 'application/octet-stream';

    /**
     * Defines file extension to be used when saving file
     *
     * @access  protected
     * @var     string
     */
    protected $file_extension = 'dat';

    /**
     * Get document content as string
     *
     * @return  string  Content of document
     */
    public function saveString(){
        return serialize($this->content);
    }

    public function setData($content){
        $this->content = $content;
    }

    public function saveFile($filename, $target = NULL){

        if (!isset($filename)) {
            $filename = date('YmdHis');
        }
        if (!isset($target)) {
            // write output to browser
            $target = 'php://output';
        }

        // set HTTP response header
        header('Content-Type: '.$this->content_type);
        header('Content-Disposition: attachment; filename='.$filename.'.'.$this->file_extension);

        $fp = fopen($target, 'w');
        fwrite($fp, $this->saveString());
        fclose($fp);

        if ($target == 'php://output') {
            // since there must be no data below
            exit();
        }
    }

}