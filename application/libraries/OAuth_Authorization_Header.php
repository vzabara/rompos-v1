<?php

class OAuth_Authorization_Header {

    private $keys;
    private $url;
    private $method;
    private $credentials;

    public function init($keys, $url, $method) {
        $this->url    = $url;
        $this->method = $method;
        $this->keys   = $keys;
        $this->set_credentials();
    }

    public function get_credentials()
    {
        return $this->credentials;

    }
    public function get_header() {

        $header = 'OAuth ';

        $oauth_params = array();

        foreach ( $this->credentials as $key => $value ) {
            $oauth_params[] = "$key=\"" . rawurlencode( $value ) . '"';
        }

        $header .= implode( ', ', $oauth_params );

        return $header;
    }

    private function set_credentials() {

        $credentials = array(
            'oauth_consumer_key'     => $this->keys['oauth_consumer_key'],
            'oauth_nonce'            => bin2hex( random_bytes(6) ),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token'            => $this->keys['oauth_token'],
            'oauth_timestamp'        => time(),
            'oauth_version'          => '1.0'
        );

        // For some reason, this matters!
        ksort( $credentials );

        $this->credentials = $credentials;

        $this->set_oauth_signature();
    }

    private function set_oauth_signature() {

        $string_params = array();

        foreach ( $this->credentials as $key => $value ) {
            $string_params[] = "$key=$value";
        }

        $signature = "$this->method&" . rawurlencode( $this->url ) . '&' . rawurlencode( implode( '&', $string_params ) );

        $hash_hmac_key = rawurlencode( $this->keys['oauth_consumer_secret'] ) . '&' . rawurlencode( $this->keys['oauth_token_secret'] );

        $oauth_signature = base64_encode( hash_hmac( 'sha1', $signature, $hash_hmac_key, true ) );

        $this->credentials['oauth_signature'] = $oauth_signature;
    }
}