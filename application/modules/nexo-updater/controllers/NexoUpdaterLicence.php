<?php
// Nexo Updater
include_once( MODULESPATH . 'nexo-updater/vendor/autoload.php' );

class NexoUpdaterLicence extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
        
        if ( ! User::in_group( 'master' ) ) {
            show_error(__( 'You`\'re not allowed to see that page', 'nexo-updater' ) );
        }

        $this->base     =   get_option( 'base_url' );
        $this->url      =   $this->base . 'api/';
//        $this->cache      =   new CI_Cache( array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'nexo_license_logger' ));
    }

    /**
     * Validation Index
     * @return void
     */
    public function index()
    {   
        $licences           =   [];
        if ( get_option( 'nexopos_store_access_key' ) ) {
            $apiRequest     =    Requests::get( $this->base . 'api/nexopos/envato-licences', [
                'X-API-TOKEN'   =>  get_option( 'nexopos_store_access_key' )
            ]);
            $licences       =    json_decode( $apiRequest->body, true );
//            $this->cache->save('envato-licences-url', true, $this->base . 'api/nexopos/envato-licences' );
//            $this->cache->save('envato-licences', true, $apiRequest->body );
        }

        $this->Gui->set_title( __( 'Licence Management', 'nexo-updater' ) );
        $this->load->module_view( 'nexo-updater', 'activate.gui', compact( 'licences' ) );
    }

    /**
     * Check the licnence
     */
    public function check() 
    {
        $apiRequest    =    Requests::post( $this->base . 'api/nexopos/use-licence', [
            'X-API-TOKEN'   =>  get_option( 'nexopos_store_access_key' )
        ], [
            'licence_id'    =>  $this->input->post( 'licence_id' )
        ]);

        $licence       =    json_decode( $apiRequest->body );
//        $this->cache->save('use-licence-url', true, $this->base . 'api/nexopos/use-licence' );
//        $this->cache->save('use-licence', true, $apiRequest->body );

        if ( ! is_object( $licence ) ) {
            return redirect([ 'dashboard', 'nexo', 'licence?notice=select-licence' ]);
        }

        if ( $licence->status === 'success' ) {
            set_option( 'updater2_validated', $licence->id );
//            set_option( 'updater2_validated', $this->input->post( 'licence_id' ) );
            $this->options->delete( 'always_ask' );
            return redirect([ 'dashboard', 'nexo', 'licence', 'activated' ]);
        } else {
            return show_error( sprintf( __( 'Unable to use the licence.', 'nexo-updater' ) ) );
        }
    }

    /**
     * Activated
     * @return view
     */
    public function activated()
    {
        $this->Gui->set_title( __( 'Thank You', 'nexo-updater' ) );
        $this->load->module_view( 'nexo-updater', 'activated.gui' );
    }

    /**
     * Auth to envato
     * @return void
     */
    public function auth()
    {
        return $this->check();
        $client_key     =   'IQtok7Iyp6lkdB1dok4SnN1Fd8SnNhtPph5JnySU';
        $client_secret  =   'be3zUChtAtrGeP08X2mzGaCXmWK4hGEWSLjfDbUe';
        redirect( $this->base . 'oauth?scopes=nexopos.envato-licences,nexopos.sync&client_secret=' . $client_secret . '&client_key=' . $client_key . '&new-comer=true&callback_url=' . urlencode( dashboard_url([ 'callback-auth' ]) ) . '&rest_key=' . get_option( 'rest_key' ) );
    }

    /***
     * Callback for the auth
     */
    public function callback()
    {
        if( @$_GET[ 'access_token' ] != null ) {
//            set_option( 'nexopos_store_access_key', $_GET[ 'access_token' ]);
//            $this->cache->save('access_token', true, $_GET[ 'access_token' ] );
            return redirect( dashboard_url([ 'licence' ]) );
        }
        return show_error( __( 'You don\'t have access to that page' ) );
    }

    /**
     * Revoke connexion
     * @return void
     */
    public function revoke()
    {
        return $this->revokeLicence();
//        $this->options->delete( 'nexopos_store_access_key' );
        return redirect( dashboard_url( [ 'licence' ] ) );
    }

    /**
     * revoke licence
     */
    public function revokeLicence()
    {
        $apiRequest    =    Requests::post( $this->base . 'api/nexopos/unuse-licence', [
            'X-API-TOKEN'   =>  get_option( 'nexopos_store_access_key' )
        ], [
            'licence_id'    =>  get_option( 'updater2_validated' )
        ]);

        $this->options->delete( 'updater2_validated' );
        set_option( 'always_ask', 'yes' );
        return redirect( dashboard_url([ 'licence' ]) );
    }

    /**
     * Update index
     * @return void
     */
    public function updateIndex()
    {
        if ( ! User::in_group([ 'admin', 'master' ]) ) {
            return show_error( __( 'Youre not allowed to access to that page.', 'nexo-updater' ) );
        }
        
        $this->Gui->set_title( __( 'Nexo Update Center', 'nexo-updater' ) );
        $this->load->module_view( 'nexo-updater', 'update.gui' );
    }
}