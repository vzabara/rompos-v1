<?php
global $Routes;

$Routes->match([ 'get', 'post' ], '/nexo/licence', 'NexoUpdaterLicence@index' )->name( 'nexo.licence' );
$Routes->match([ 'get' ], '/nexo/updates', 'NexoUpdaterLicence@updateIndex' )->name( 'nexo.update.index' );
$Routes->match([ 'post' ], '/nexo/check-licence', 'NexoUpdaterLicence@check' )->name( 'nexo.licence.check' );
$Routes->match([ 'get' ], '/nexo/revoke-connexion', 'NexoUpdaterLicence@revoke' )->name( 'nexo.licence.check' );
$Routes->match([ 'get' ], '/nexo/revoke-licence', 'NexoUpdaterLicence@revokeLicence' )->name( 'nexo.licence.check' );
$Routes->match([ 'get' ], '/nexo/auth', 'NexoUpdaterLicence@auth' )->name( 'nexo.licence.auth' );
$Routes->match([ 'get' ], '/nexo/callback-auth', 'NexoUpdaterLicence@callback' )->name( 'nexo.licence.callback' );
$Routes->match([ 'get', 'post' ], '/nexo/licence/activated', 'NexoUpdaterLicence@activated' )->name( 'nexo.licence' );