<div class="row">
     <?php if( get_option( 'updater2_validated', 'no' ) == 'no' ):?>
     <div class="col-md-6">
          <p><?php echo __( 'Thank you for choosing to use ROMPos. 
          We hope it will help you manage your business better. 
          To take advantage of priority support and updates, we invite you assign a licence to the current installation. 
          All licences are registered on Nexo Platform.', 'nexo-updater' );?></p>
          <form method="POST" ng-non-bindable action="<?php echo dashboard_url([ 'check-licence' ]);?>">
                <?php
                $csrf = array(
                    'name' => $this->security->get_csrf_token_name(),
                    'hash' => $this->security->get_csrf_hash()
                );
                ?>
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <?php if( ! get_option( 'nexopos_store_access_key', false ) ):?>
                    <a href="<?php echo dashboard_url([ 'auth' ]);?>" class="btn btn-primary" type="submit"><?php echo __( 'Authenticate with ROMPos', 'nexo' );?></a>
                <?php else:?>
                    <?php if ( empty( $licences ) ):?>
                        <?php echo tendoo_info( __( 'It seems like you no longer have any available licence on Nexo Platform. <strong><a href="https://api.rompos.com" target="__blank">Please consider adding new licences.</a></strong>', 'nexo-updater' ) );?>
                    <?php else:?>
                        <div class="form-group">
                            <label><?php echo __( 'Select the licence', 'nexo-updater' );?></label>
                            <select class="form-control" name="licence_id">
                                <option value="disable"><?php echo __( 'Select...', 'nexo-updater' );?></option>
                                <?php foreach( $licences as $licence ):?>
                                    <?php if ( $licence[ 'app_id' ] === '16195010' ):?>
                                    <option value="<?php echo $licence[ 'id' ];?>"><?php echo $licence[ 'app_name' ];?></option>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </select>
                            <p class="help-block"><?php echo __( 'You must define within your licence, which licence should be used for the current installation', 'nexo-updater' );?></p>
                        </div>
                        <button class="btn btn-primary select-licence" type="submit"><?php echo __( 'Validate the licence', 'nexo' );?></button>
                    <?php endif;?>
                    <a href="<?php echo dashboard_url([ 'revoke-connexion' ]);?>" class="btn btn-danger" type="submit"><?php echo __( 'Revoke the connexion', 'nexo' );?></a>
                <?php endif;?>
                <script>
                $( document ).ready( function(){
                    $( '.select-licence' ).bind( 'click', function(){
                        let $this   =   $( this );
                        NexoAPI.Bootbox().confirm( '<?php echo _s( 'If you set that licence as in use, you won\'t be able to use that again, unless you unregister it from the current installation.', 'nexo-updater' );?>', function( action ) {
                            if ( action ) {
                                $this.closest( 'form' ).submit();
                            }
                        });
                        return false;
                    })
                })
                </script>
          </form>
     </div>
     <?php else:?>
          <div class="container-fluid">
                <div class="jumbotron">
                    <h1><?php echo __( 'Unusing a licence', 'nexo' );?></h1>
                    <p><?php echo __( 'This installation of ROMPos is currently using a licence. 
                    If you would like to unuse the licence, you can proceed by clicking on the following button. 
                    Unusing a licence, will allow you to use the same licence for another installation. 
                    However, <strong>the current installation will be blocked</strong>.', 'nexo-updater' );?></p>
                    <p><a href="<?php echo dashboard_url([ 'revoke-licence' ]);?>" class="btn btn-danger" type="submit"><?php echo __( 'Unuse the licence', 'nexo' );?></a></p>
                </div>
          </div>
     <?php endif;?>
</div>