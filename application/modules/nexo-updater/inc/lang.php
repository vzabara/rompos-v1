<?php
$lang[ 'select-licence' ]   =   tendoo_warning( __( 'You need to select a licence.', 'nexo-updater' ) );
$lang[ 'nexo-updated' ]     =   tendoo_info( __( 'ROMPos has been successfully updated.', 'nexo-updater' ) );