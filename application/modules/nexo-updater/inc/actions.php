<?php
class Nexo_Updater_Actions extends Tendoo_Module
{
     public function __construct()
     {
          parent::__construct();
     }

     /**
      * Load Dashboard
      * 
     **/

     public function load_dashboard()
     {
        $this->load->module_config( 'nexo-updater', 'updater' );

        if( 
            $this->uri->uri_string() != 'dashboard/nexo/about' && 
            $this->uri->segment(5) != 'about' && 
            strpos( $this->uri->uri_string(), 'dashboard/modules' ) === false
        ) {
            if( 
                get_option( 'updater2_validated', 'no' ) === 'no' && 
                $this->uri->uri_string() !== 'dashboard/nexo/licence' &&
                $this->uri->uri_string() !== 'dashboard/nexo/check-licence' && 
                $this->uri->uri_string() !== 'dashboard/nexo/revoke-connexion' && 
                $this->uri->uri_string() !== 'dashboard/nexo/auth' &&
                $this->uri->uri_string() !== 'dashboard/nexo/callback-auth'
            ) {

                ob_start();
                ?>
                <?php if( ! get_option( 'nexopos_store_access_key', false ) ):?>
                    <a href="<?php echo dashboard_url([ 'auth' ]);?>" class="btn btn-lg btn-primary" type="submit"><?php echo __( 'Authenticate with ROMPos', 'nexo' );?></a>
                <?php else:?>
                    <a href="<?php echo dashboard_url([ 'licence' ]);?>" class="btn btn-lg btn-primary" type="submit"><?php echo __( 'Validate the installation', 'nexo' );?></a>
                    <a href="<?php echo dashboard_url([ 'revoke-connexion' ]);?>" class="btn btn-lg btn-danger" type="submit"><?php echo __( 'Revoke the connexion', 'nexo' );?></a>
                <?php endif;?>
                <?php
                $button     =   ob_get_clean();

                $this->notice->push_notice( 
                '<div class="container-fluid">
                    <div class="jumbotron">
                        <h1>' . __( 'Validate your licence', 'nexo' ) . '</h1>
                        <p>' . __( 'In order to remove this ugly notice, you should connect with ROMPos Platform and register your licence.', 'nexo' ) . '</p>
                        <p>' . $button . '</p>
                    </div>
                </div>' );

                /**
                 * if it has been requested to ask everytime
                 */
                if ( get_option( 'always_ask' ) === 'yes' ) {
                    redirect( dashboard_url([ 'licence' ]));
                }
            }
        }
     }

     /**
      * enable_module
      *
    **/

    public function do_enable_module( $namespace )
    {
        if( $namespace == 'nexo-updater' ) {
            return redirect([ 'dashboard', 'nexo', 'licence' ]);
        }
    }

    public function after_app_init()
    {
        $this->lang->load_lines( dirname( __FILE__ ) . '/lang.php' );
    }

    /**
     * Dashboard footer
     * @return void
     */
    public function dashboard_footer()
    {
//        $this->load->module_view( 'nexo-updater', 'update.cron' );
    }
}