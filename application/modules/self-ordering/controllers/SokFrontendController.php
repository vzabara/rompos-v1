<?php

use Carbon\Carbon;
use Rakit\Validation\Validator;

class SokFrontendController
{
    public function __construct()
    {
        // parent::__construct();
        $this->db       =   get_instance()->db;
        $this->load     =   get_instance()->load;
        $this->email    =   get_instance()->email;
        $this->cache    =   get_instance()->cache;
    }

    public function store()
    {
        get_instance()->load->module_view( 'self-ordering', 'frontend.home' );
    }

    /**
     * Login Endpoint
     * @return json of AsyncResponse
     */
    public function login()
    {
        $post           =   json_decode( file_get_contents( 'php://input' ), true );
        $customers       =   get_instance()->db->where([
            'username'  =>  $post[ 'username' ]
        ])  ->get( store_prefix() . 'nexo_clients' )
            ->result_array();

        $hasLogged      =   false;
        foreach( $customers as $customer ) {
            if( password_verify( $post[ 'password' ], $customer[ 'password' ] ) ) {
                $hasLogged  =   true;
            }
        }

        if( @$hasLogged ) {
            echo json_encode([
                'status'    =>  'success',
                'message'   =>  __( 'The user is logged', 'self-ordering' ),
                'user'      =>  $customers[0]
            ]);
            die;
        }

        echo json_encode([
            'status'    =>  'failed',
            'password'  =>  password_hash( $post[ 'password' ], PASSWORD_BCRYPT ),
            'message'   =>  __( 'Unable to login using the provided credentials. Please try again.', 'self-ordering' )
        ]);
        die;
    }

    /**
     * Categories
     * @return json of categories
     */
    public function categories()
    {
        $items          =   [];
            $id             =   get_instance()->uri->segment(3);
            $categories     =   get_instance()->db
            ->where( 'PARENT_REF_ID',  $id )
            ->get( store_prefix() . 'nexo_categories' )
            ->result_array();

            $type       =   count( $categories ) > 0 ? 'categories' : 'items';

            if ( count( $categories ) === 0 ) {
                $items  =   get_instance()->db
                    ->where( 'REF_CATEGORIE', $id )
                    ->get( store_prefix() . 'nexo_articles' )
                    ->result_array();
            } else {
                array_walk($categories, function(&$item) {
                    $item['DESCRIPTION'] = strip_tags($item['DESCRIPTION']);
                });
            }

            /**
             * prepare a return to
             */
            $category   =   get_instance()->db->where( 'ID', $id )
                ->get( store_prefix() . 'nexo_categories' )
                ->result_array();
            $return_to  =   empty( $category ) ? 0 : $category[0][ 'PARENT_REF_ID' ];
            $category   =   @$category[0];

            echo json_encode( compact( 'categories', 'items', 'type', 'return_to', 'category' ) );
        die;
    }

    /**
     * Modifiers
     * @return json of modifiers
     */
    public function modifiers()
    {
        $id     =   get_instance()->uri->segment(3);
        get_instance()->db->select( 
            store_prefix() . 'nexo_restaurant_modifiers.NAME as name,' .
            store_prefix() . 'nexo_restaurant_modifiers.DESCRIPTION as description,' . 
            store_prefix() . 'nexo_restaurant_modifiers.AUTHOR as author,' .      
            store_prefix() . 'nexo_restaurant_modifiers.REF_CATEGORY as category,' .    
            store_prefix() . 'nexo_restaurant_modifiers.DEFAULT as default,' .
            store_prefix() . 'nexo_restaurant_modifiers.PRICE as price,' .
            store_prefix() . 'nexo_restaurant_modifiers.IMAGE as image,' .
            store_prefix() . 'nexo_restaurant_modifiers_categories.FORCED as group_forced,' .
            store_prefix() . 'nexo_restaurant_modifiers_categories.NAME as group_name,' .
            store_prefix() . 'nexo_restaurant_modifiers_categories.UPDATE_PRICE as group_update_price,' .
            store_prefix() . 'nexo_restaurant_modifiers_categories.MULTISELECT as group_multiselect'  
        )
        ->from( store_prefix() . 'nexo_restaurant_modifiers' )
        ->join( 
            store_prefix() . 'nexo_restaurant_modifiers_categories', 
            store_prefix() . 'nexo_restaurant_modifiers_categories.ID = ' . store_prefix() . 'nexo_restaurant_modifiers.REF_CATEGORY' 
        );

        if( $id != null ) {
            get_instance()->db->where( store_prefix() . 'nexo_restaurant_modifiers.REF_CATEGORY', $id );
        }

        $query  =   get_instance()->db->get()->result();
        echo json_encode( $query );
        die;
    }

    /**
     * Orders
     * @return json of orders
     */
    public function orders()
    {
        $post   =   json_decode( file_get_contents( 'php://input' ), true );

            /**
             * Let's build items
             */
            $items          =   [];
            foreach( $post[ 'items' ] as $item ) {
                $item[ 'qte_added' ]                =   $item[ 'quantity' ];
                $item[ 'stock_enabled' ]            =   $item[ 'STOCK_ENABLED' ];
                $item[ 'inline' ]                   =   0;
                $item[ 'codebar' ]                  =   $item[ 'CODEBAR' ];
                $item[ 'sale_price' ]               =   $item[ 'PRIX_DE_VENTE_TTC' ];
                $item[ 'discount_type' ]            =   '';
                $item[ 'discount_amount' ]          =   0;
                $item[ 'discount_percent' ]         =   '';
                $item[ 'name' ]                     =   $item[ 'DESIGN' ];
                $item[ 'alternative_name' ]         =   $item[ 'ALTERNATIVE_NAME' ];

                $modifiers                          =   [];
                foreach( $item[ 'metas' ] as $index => $meta ) {
                    $modifiers[ $index ]    =   $meta;
                }

                $item[ 'metas' ]                    =   [
                    'restaurant_food_status'    =>  'not_ready',
                    'restaurant_note'           =>  '',
                    'modifiers'                 =>  $modifiers,
                    'restaurant_food_issue'     =>  ''
                ];
                $items[]                            =   $item;
            }

            /**
             * Define the fine title for the order
             */
            switch( $post[ 'order_type' ] ) {
                case 'dinein':      $title = __( 'Dine in', 'self-order' ); break;
                case 'takeaway':    $title = __( 'Take Away', 'self-order' ); break;
                case 'delivery':    $title = __( 'Delivery', 'self-order' ); break;
                default:            $title = __( 'Unknow Order Type', 'self-order' ); break;
            }

            get_instance()->load->model( 'Nexo_Checkout', 'checkout');
            $response   =   get_instance()->checkout->postOrder([
                'RISTOURNE'                 =>  0,
                'REMISE'                    =>  0,
                'REMISE_PERCENT'            =>  0,  
                'REMISE_TYPE'               =>  0,  
                'RABAIS'                    =>  0,
                'GROUP_DISCOUNT'            =>  0,        
                'TOTAL'                     =>  $post[ 'total' ],
                'REF_CLIENT'                =>  $post[ 'user' ][ 'ID' ],
                'TITRE'                     =>  $title,
                'DESCRIPTION'               =>  '',
                'DISCOUNT_TYPE'             =>  'disable',
                'HMB_DISCOUNT'              =>  '',
                'PAYMENT_TYPE'              =>  'cash',
                'TVA'                       =>  0,
                'REGISTER_ID'               =>  'default', // can be set from the settings
                'REF_TAX'                   =>  0,
                'SOMME_PERCU'               =>  0,
                'TYPE'                      =>  'nexo_order_devis',
                'ITEMS'                     =>  $items,
                'metas'                     =>  [
                    'order_real_type'       =>  'dinein',
                    'table_id'              =>  isset( $post[ 'selected_table' ][ 'TABLE_ID' ] ) ? $post[ 'selected_table' ][ 'TABLE_ID' ] : $post[ 'default_table' ], // let's choose from the setting the table used for these orders
                    'seat_used'             =>  $post[ 'seats' ]
                ],
                'shipping'                  =>  array_merge([
                    'id'                    =>  0
                ], $post[ 'shipping_address' ]),
                'RESTAURANT_ORDER_TYPE'     =>  $post[ 'order_type' ],
                'RESTAURANT_ORDER_STATUS'   =>  'pending'
            ], $post[ 'author' ]);

            echo json_encode( $response );
            die;
    }

    /**
     * Register Endpoint
     * @return json of AsyncResponse
     */
    public function register()
    {
        $post   =   json_decode( file_get_contents( 'php://input' ), true );
        get_instance()->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );

        $validator  =   new Validator;
        $validation =   $validator->make( $post, [
            'username'          =>  'required|min:5',
            'password'          =>  'required|min:6',
            'password_confirm'  =>  'same:password',
            'email'             =>  'required|email'
        ]);

        $validation->validate();

        if( $validation->fails() ) {
            $errors         =   [];
            foreach( $post as $name => $value ) {
                $errors[ $name ]    =   $validation->errors()->get( $name );  
            }
            echo json_encode([ 
                'status'        =>  'failed',
                'message'       =>  __( 'An error occured during the creation. The form has invalid data.', 'self-ordering' ),
                'errors_bag'    =>  $errors       
            ]);
            die;
        }

        $customerExists     =   get_instance()->customer_model->get( $post[ 'email' ], 'EMAIL' );
        if ( ! empty( $customerExists ) ) {
            echo json_encode([
                'status'    =>  'failed',
                'message'   =>  __( 'Unable to create the account. The email is already in use.', 'self-ordering' )
            ]);
            die;
        }

        /**
         * The customer doesn't exists, let's create 
         * the new customer then.
         */
        $data       =   [
            'username'          =>  $post[ 'username' ],
            'password'          =>  password_hash( $post[ 'password' ], PASSWORD_BCRYPT ),
            'EMAIL'             =>  $post[ 'email' ],
            'DATE_CREATION'     =>  date_now(),
            'NOM'               =>  $post[ 'username' ]
        ];

        get_instance()->db->insert( store_prefix() . 'nexo_clients', $data );

        echo json_encode([
            'status'        =>  'success',
            'message'       =>  __( 'Your account has been successfully created.', 'self-ordering' )
        ]);
        die;
    }

    /**
     * Loading Print Endpoint
     * @return json of AsyncResponse
     */
    public function local_print()
    {
        $order_id   =   get_instance()->uri->segment(3);
        if ( $order_id != null ) {    
            get_instance()->load->library('parser');
            get_instance()->load->model('Nexo_Checkout');
            get_instance()->load->model('Nexo_Misc');

            $data                		                    =   array();
            $data[ 'order' ]    		                    =   get_instance()->Nexo_Checkout->get_order_products($order_id, true);
            $data[ 'shipping' ]                             =   get_instance()->db->where( 'ref_order', $order_id )->get( store_prefix() . 'nexo_commandes_shippings' )->result_array();
            $data[ 'tax' ]                                  =   get_instance()->Nexo_Misc->get_taxes( $data[ 'order' ][ 'order' ][0][ 'REF_TAX' ] );
            $data[ 'template' ]						        =	array();
            $data[ 'template' ][ 'order_date' ]		        =	mdate( '%d/%m/%Y %g:%i %a', strtotime($data[ 'order' ][ 'order' ][0][ 'DATE_CREATION' ]));
            $data[ 'template' ][ 'order_updated' ]          =	mdate( '%d/%m/%Y %g:%i %a', strtotime($data[ 'order' ][ 'order' ][0][ 'DATE_MOD' ]));
            $data[ 'template' ][ 'order_code' ]		        =	$data[ 'order' ][ 'order' ][0][ 'CODE' ];
            $data[ 'template' ][ 'order_id' ]               =   $data[ 'order' ][ 'order' ][0][ 'ORDER_ID' ];
            $data[ 'template' ][ 'order_status' ]	        =	get_instance()->Nexo_Checkout->get_order_type($data[ 'order' ][ 'order' ][0][ 'TYPE' ]);
            $data[ 'template' ][ 'order_note' ]             =   $data[ 'order' ][ 'order' ][0][ 'DESCRIPTION' ];

            $data[ 'template' ][ 'order_cashier' ]	        =	User::pseudo( $data[ 'order' ][ 'order' ][0][ 'AUTHOR' ] );
            $data[ 'template' ][ 'shop_name' ]		        =	store_option( 'site_name' );
            $data[ 'template' ][ 'shop_pobox' ]		        =	store_option( 'nexo_shop_pobox' );
            $data[ 'template' ][ 'shop_fax' ]		        =	store_option( 'nexo_shop_fax' );
            $data[ 'template' ][ 'shop_email' ]             =	store_option( 'nexo_shop_email' );
            $data[ 'template' ][ 'shop_street' ]            =	store_option( 'nexo_shop_street' );
            $data[ 'template' ][ 'shop_phone' ]             =	store_option( 'nexo_shop_phone' );
            $data[ 'template' ][ 'customer_name' ]          =   $data[ 'order' ][ 'order' ][0][ 'customer_name' ];
            $data[ 'template' ][ 'customer_phone' ]         =   $data[ 'order' ][ 'order' ][0][ 'customer_phone' ];

            $data[ 'template' ][ 'delivery_address_1' ]     =   @$data[ 'shipping' ][0][ 'address_1' ];
            $data[ 'template' ][ 'delivery_address_2' ]     =   @$data[ 'shipping' ][0][ 'address_2' ];
            $data[ 'template' ][ 'city' ]                   =   @$data[ 'shipping' ][0][ 'city' ];
            $data[ 'template' ][ 'country' ]                =   @$data[ 'shipping' ][0][ 'country' ];
            $data[ 'template' ][ 'name' ]                   =   @$data[ 'shipping' ][0][ 'name' ];
            $data[ 'template' ][ 'surname' ]                =   @$data[ 'shipping' ][0][ 'surname' ];
            $data[ 'template' ][ 'state' ]                  =   @$data[ 'shipping' ][0][ 'surname' ];
            $data[ 'template' ][ 'delivery_cost' ]          =   @$data[ 'shipping' ][0][ 'price' ];

            $filtered   =   get_instance()->events->apply_filters( 'nexo_filter_receipt_template', [
                'template'          =>      $data[ 'template' ],
                'order'             =>      $data[ 'order' ][ 'order' ][0],
                'items'             =>      $data[ 'order' ][ 'products' ]
            ]);

            $data[ 'template' ]             =   $filtered[ 'template' ];
            $allowed_order_for_print	=	get_instance()->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );

            get_instance()->load->module_view( 'nexo', 'receipts.nps.basic', $data );
        } else {
            echo json_encode([
                'status'    =>  'failed',
                'message'   =>  __( 'Unable to locate the order', 'self-ordering' )
            ]);
        }
        die;
    }

    /**
     * Logout Endpoint
     * @return json of AsyncResponse
     */
    public function logout()
    {
        echo json_encode([
            'status'    =>  'success',
            'message'   =>  __( 'The user has been disconnected', 'self-ordering' )
        ]);
        die;
    }

    /**
     * Order Monitor
     */
    public function orderMonitor()
    {
        get_instance()->load->module_view( 'self-ordering', 'monitor.main' );
    }

    /**
     * Load orders
     * @return json of daily orders
     */
    public function loadOrders()
    {
        $dateStart      =   Carbon::parse( date_now() )->copy()->startOfDay();
        $dateEnd        =   Carbon::parse( date_now() )->copy()->endOfDay();

        $orders         =   $this->db->where( 'DATE_CREATION >=', $dateStart->toDateTimestring() )
            ->where( 'DATE_CREATION <=', $dateEnd->toDateTimeString() )
            ->order_by( 'DATE_CREATION', 'desc' )
            ->get( store_prefix() . 'nexo_commandes' )
            ->result_array();
        
        foreach( $orders as &$order ) {
            /**
             * Fill the customer
             */
            $customer   =   $this->db->where( 'ID', $order[ 'REF_CLIENT' ] )
                ->get( store_prefix() . 'nexo_clients' )
                ->result_array();
            $order[ 'customer' ]    =   $customer[0];
            
            /**
             * fill orders items
             */
            $items      =   $this->db->where( 'REF_COMMAND_CODE', $order[ 'CODE' ])
                ->get( store_prefix() . 'nexo_commandes_produits' )
                ->result_array();

            /**
             * fill items metas
             */
            foreach( $items as &$item ) {
                /**
                 * Construct the meta array
                 */
                $item[ 'metas' ]    =   [];

                /**
                 * Retrive the meta from the db
                 */
                $metas   =   $this->db->where( 'REF_COMMAND_PRODUCT', $item[ 'ID' ] )
                    ->get( store_prefix() . 'nexo_commandes_produits_meta' )
                    ->result_array();
                
                foreach( $metas as $meta ) {
                    $item[ 'metas' ][ $meta[ 'KEY' ] ]     =   $meta[ 'VALUE' ];
                }                
            }
            
            $order[ 'items' ]   =   $items;
        }

        echo json_encode( $orders );
        die;
    }

    /**
     * Reset Password.
     * This method actually send an email to the user
     * with reset details
     */
    public function requestPasswordReset()
    {
        /**
         * let's first seatch if such customers exists
         */
        
        $post       =   json_decode( file_get_contents( 'php://input' ), true );
        $customer   =   $this->db->where( 'EMAIL', $post[ 'email' ] )
            ->get( store_prefix() . 'nexo_clients' )
            ->result_array();
        
        if( $customer ) {
            $passwordCode   =   $customer[0][ 'ID' ] . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9);
            
            $this->cache->file->save( 'so_customers_' . $passwordCode, 
                [
                    'id'    =>  $customer[0][ 'ID' ],
                    'code'  =>  $passwordCode
                ], 7200 // save for 2 hours
            );

            $this->email->initialize([
                'mailtype'  =>  'html'
            ]);
            $this->email->from( store_option( 'so_from_email', 'contact@rompos.com' ) );
            $this->email->to( $customer[0][ 'EMAIL' ] );
            $this->email->subject( __( 'Password Recovery', 'self-ordering' ) );
            $this->email->message( 
                $this->load->module_view( 'self-ordering', 'emails.customer-password', compact( 'customer', 'passwordCode' ), true )
            );
            $this->email->send();

            echo json_encode([
                'status'    =>  'success',
                'message'   =>  __( 'The recovery code has been send to your email address. Please check it and fill your new password with the code send.', 'self-ordering' )
            ]);
            die;
        }

        echo json_encode([
            'status'    =>  'failed',
            'message'   =>  __( 'Unable to find the any account using this email.', 'self-ordering' )
        ]);
        die;
    }

    /**
     * Input a new password 
     * for the customer
     * @return json
     */
    public function changePassword()
    {
        $post       =   json_decode( file_get_contents( 'php://input' ), true );

        $validator  =   new Validator;
        $validation =   $validator->make( $post, [
            'password'          =>  'required|min:6',
            'password_confirm'  =>  'same:password'
        ]);

        $validation->validate();

        if( $validation->fails() ) {
            $errors         =   [];
            foreach( $post as $name => $value ) {
                if( in_array( $name, [ 'password', 'password_confirm' ] ) ) {
                    $errors[ $name ]    =   $validation->errors()->get( $name );  
                }
            }
            echo json_encode([ 
                'status'        =>  'failed',
                'message'       =>  __( 'An error has occured while updating the password.', 'self-ordering' ),
                'errors_bag'    =>  $errors       
            ]);
            die;
        }

        if( @$post[ 'reset_code' ] ) {
            $customer   =   $this->cache->file->get( 'so_customers_' . $post[ 'reset_code' ] );
            if( ! empty( $customer ) ) {
                $this->db->where( 'ID', $customer[ 'id' ] )->update( store_prefix() . 'nexo_clients', [
                    'password'  =>  password_hash( $post[ 'password' ], PASSWORD_BCRYPT )
                ]);
                echo json_encode([
                    'status'    =>  'success',
                    'message'   =>  __( 'The password has been successfully updated.', 'self-ordering' )
                ]);
                $this->cache->file->delete( 'so_customers_' . $post[ 'reset_code' ] );
                die;
            }
            echo json_encode([
                'status'    =>  'failed',
                'message'   =>  __( 'Wrong reset code has been provided or it has expired. Please try again.', 'self-ordering' )
            ]);
            die;
        }

        echo json_encode([
            'status'    =>  'failed',
            'message'   =>  __( 'Unvalid data has been submited', 'self-ordering' )
        ]);
        die;
    }

    /**
     * Login as Guest
     * @return json
     */
    public function logAsGuest()
    {
        $this->load->model( 'Nexo_Misc' );

        $customer   =   get_instance()->Nexo_Misc->get_customers( store_option( 'default_compte_client', null ) );

        if( count( $customer ) === 1 ) {
            echo json_encode([ 
                'status'    =>  'success',
                'user'      =>  $customer[0],
                'message'   =>  __( 'The user has been successfully logged.', 'self-ordering' )
            ]);
            die;
        }

        echo json_encode([ 
            'status'    =>  'failed',
            'message'   =>  __( 'The default user account has\'nt been set. Unable to log as guest.', 'self-ordering' )
        ]);
        die;

    }

    /**
     * Display a main store selection
     * @return void
     */
    public function mainStoreSelection()
    {
        if ( multistore_enabled() ) {
            
            $this->load->module_model( 'nexo', 'Nexo_Stores_Model', 'store_model' );
            
            $stores     =   get_instance()->store_model->getOpenedStores();
            $title      =   sprintf( __( 'Branch Selection &mdash; %s', 'self-ordering' ), get_option( 'site_name' ) );

            $this->load->module_view( 'self-ordering', 'frontend.store-selection', compact( 'stores', 'title' ) );

        } else {
            show_error( __( 'Unable to access to store selection for Self Ordering Kiosk. Please make sure the multistore is enabled on NeoxPOS', 'self-ordering' ) );
        }
    }
}