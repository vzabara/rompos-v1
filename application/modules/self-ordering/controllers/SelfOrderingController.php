<?php
class SelfOrderingController extends Tendoo_Module
{
    public function settings()
    {
        $this->Gui->set_title( store_title( __( 'Self Ordering Settings', 'self-ordering' ) ) );
        $this->load->module_view( 'self-ordering', 'settings.home' );
    }
}