<?php

use Rakit\Validation\Validator;

include_once( dirname( __FILE__ ) . '/SelfOrderingInstall.php' );

class SelfOrderingActions extends Tendoo_Module 
{
    public function __construct()
    {
        parent::__construct();
        $this->setup  =   new SelfOrderingInstall;
    }

    /**
     * Check if the application has yet been installed
     * @return void
     */
    public function enable_module( $module ) 
    {
        if( $module === 'self-ordering' ) {
            $this->setup->install();
        }
    }

    /**
     * Handle the frontend 
     * of the application for self ordering
     */
    public function load_frontend()
    {
        include_once( dirname( __FILE__ ) . '/../controllers/SokFrontendController.php' );
        
        $SokController  =   new SokFrontendController;

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === null ) {
            return $SokController->mainStoreSelection();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'store' ) {
            return $SokController->store();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'login' ) {
            return $SokController->login();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'categories' ) {
            return $SokController->categories();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'modifiers' ) {
            return $SokController->modifiers();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'order' ) {
            return $SokController->orders();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'register' ) {
            return $SokController->register();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'as-guest' ) {
            return $SokController->logAsGuest();
        }

        /**
         * Local Print
         * Print the order to the selected printer 
         * on the module SOK
         */
        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'local-print' ) {
            return $SokController->local_print();
        }

        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'logout' ) {
            return $SokController->logout();
        }  
        
        /**
         * Display the order monitor
         */
        if ( $this->uri->segment(1) === 'so' && $this->uri->segment(2) === 'orders-monitor' ) {
            return $SokController->orderMonitor();
        }       
        if ( 
            $this->uri->segment(1) === 'so' && 
            $this->uri->segment(2) === 'gastro' && 
            $this->uri->segment(3) === 'orders' ) {
            return $SokController->loadOrders();
        }       
        if ( 
            $this->uri->segment(1) === 'so' && 
            $this->uri->segment(2) === 'request-password-reset' ) {
            return $SokController->requestPasswordReset();
        }       
        if ( 
            $this->uri->segment(1) === 'so' && 
            $this->uri->segment(2) === 'change-password' ) {
            return $SokController->changePassword();
        }
    }

    /**
     * Init Customers
     * @return void
     */
    public function init_customers()
    {
        // $this->load->module_view( 'self-ordering', 'scripts.customers' );
    }
}