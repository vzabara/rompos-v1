<?php
class SelfOrderingInstall extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Update Database
     */
    public function install()
    {
        $this->load->model( 'Nexo_Stores' );

        $stores         =   $this->Nexo_Stores->get();

        array_unshift( $stores, [
            'ID'        =>  0
        ]);

        foreach( $stores as $store ) {
            $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
            $columns            =   $this->db->list_fields( $store_prefix . 'nexo_clients' );
            if( ! in_array( 'username', $columns ) ) {
                $this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_clients` 
                ADD `username` VARCHAR(200) NOT NULL AFTER `AUTHOR`;' );
            }
            if( ! in_array( 'password', $columns ) ) {
                $this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_clients` 
                ADD `password` VARCHAR(200) NOT NULL AFTER `AUTHOR`;' );
            }
        }
    }
}