<?php
class SelfOrderingFilters extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Build Administration Menus
     * @param array menus
     * @return array menus
     */
    public function admin_menus( $menus )
    {
        if( @$menus[ 'nexo_settings' ] ) {
            $menus[ 'nexo_settings' ][]     =   [
                'title'         =>      __( 'SOK Settings', 'self-ordering' ),
                'href'          =>      site_url([ 'dashboard', store_slug(), 'so', 'settings' ]),
                'permission' 		=>	'nexo.manage.settings'
            ];
        }

        if( is_multistore() || ! multistore_enabled() ) {
            $menus      =   array_insert_after( 'caisse', $menus, 'sok', [
                [
                    'title'         =>  __( 'Self Order Kiosk', 'self-ordering' ),
                    'icon'          =>  'fa fa-hand-pointer-o',
                    'href'          =>  '#',
                    'disable'       =>  true
                ], 
                [
                    'title'         =>  __( 'Frontend', 'self-ordering' ),
                    'icon'          =>  'fa fa-hand-pointer-o',
                    'href'          =>  site_url([ 'so', 'store', is_multistore() ? '?store_id=' . get_store_id() : '' ]),
                ], 
                [
                    'title'         =>  __( 'Orders Monitor', 'self-ordering' ),
                    'icon'          =>  'fa fa-screen',
                    'href'          =>  site_url([ 'so', 'orders-monitor', is_multistore() ? '?store_id=' . get_store_id() : '' ]),
                ]
            ]);
        }

        return $menus;
    }

    /**
     * Filter order 
     * to display only paid order at the kitchen
     */
    public function filter_orders( $orders ) {
        if ( store_option( 'so_allowed_order_status', 'show_paid' ) === 'show_paid' ) {
            return [ 'nexo_order_comptant' ];
        }
        return $orders;
    }

    /**
     * Customer Fields
     * @param array fields
     * @return array
     */
    public function customer_fields( $fields ) 
    {
        $fields[ 'username' ]   =   [
            'type'  =>  'string'
        ];

        $fields[ 'password' ]   =   [
            'type'  =>  'string'
        ];

        return $fields;
    }

    /**
     * Update fields type for customers
     * @param array fields
     * @return array fields
     */
    public function customers_field_type( $fields )
    {
        $fields[]   =   [
            'key'           =>      'username',
            'title'         =>  __( 'Username', 'self-ordering' ),
            'type'          =>  'text'
        ];

        $fields[]   =   [
            'key'           =>      'password',
            'title'         =>  __( 'Password', 'self-ordering' ),
            'type'          =>  'password'
        ];

        return $fields;
    }

    /**
     * Filter Customer Fields
     * While published
     * @param array fields
     * @return array
     */
    public function filter_put_fields( $fields, $object, $customer_id )
    {
        $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );
        
        $post                   =   json_decode( file_get_contents( 'php://input' ), true );

        /**
         * @todo
         * check if the username is already used
         */

        /**
         * Update the password only if it has changed
         */
        $customers      =   $this->customer_model->get( $customer_id );
        if( $customers ) {
            if( $customers[0][ 'password' ] !== $post[ 'password' ] ) {
                $fields[ 'password' ]   =   xss_clean( password_hash( $post[ 'password' ], PASSWORD_BCRYPT ) );
            }
        }
        
        $fields[ 'username' ]   =   xss_clean( $post[ 'username' ] );
        
        return $fields;
    }

    /**
     * Filter Post Fields
     * @param array fields
     * @return array
     */
    public function filter_post_fields( $fields )
    {
        $post                   =   json_decode( file_get_contents( 'php://input' ), true );
        $fields[ 'password' ]   =   xss_clean( password_hash( $post[ 'password' ], PASSWORD_BCRYPT ) );
        $fields[ 'username' ]   =   xss_clean( $post[ 'username' ] );
        return $fields;
    }
}