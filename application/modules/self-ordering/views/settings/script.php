<script>
function loadPrinters( select ) {
    const printerName   =   '[name="<?php echo store_prefix();?>so_nps_printer"]';
    if ( select.val() ) {
        $.ajax( select.val() + '/api/printers', {
            success    :   function( result ) {
                $( printerName ).html( '<option><?php echo __( 'Choisir une option', 'self-ordering' );?></option>' );
                result.forEach( printer => {
                    let selected    =  ( printer.name ==  '<?php echo store_option( 'so_nps_printer' );?>' ) ? 'selected="selected"' : null;
                    $( printerName ).append( `<option ${selected} value="${printer.name}">${printer.name}</option>` );
                });
                NexoAPI.Toast()( `<?php echo __( 'The connexion with the server has been made.', 'self-ordering' );?>` );
            },
            error   :   function() {
                NexoAPI.Notify().warning(
                    `<?php echo __( 'An error ocurred', 'self-ordering' );?>`,
                    `<?php echo __( 'Unable to access to the server using the url you\'ve provider.', 'self-ordering' );?>`,
                );
            }
        })
    }
}
$( document ).ready( function() {
    const fieldName     =   '[name="<?php echo store_prefix();?>so_nps_url"]';
    $( fieldName ).blur( function() {
        loadPrinters( $( this ) );
    });

    loadPrinters( $( fieldName ) );
});
</script>