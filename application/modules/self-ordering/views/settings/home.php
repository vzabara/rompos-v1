<?php


$this->Gui->col_width(1, 2);
$this->Gui->col_width(2, 2);

$this->Gui->add_meta(array(
    'namespace'         =>        'so_settings',
    'title'             =>       '',
    'col_id'            =>        1,
    'gui_saver'         =>        true,
    'footer'            =>        array(
        'submit'        =>        array(
            'label'     =>        __( 'Save Settings', 'self-ordering')
        )
    ),
    'use_namespace'     =>        false,
));

$authors            =   $this->auth->list_users();

$authorAsOptions    =   [];
foreach( $authors  as $author ) {
    $authorAsOptions[ $author->user_id ]    =   $author->user_name;
}

$this->Gui->add_item(array(
    'type'          =>  'select',
    'name'          =>  store_prefix() . 'so_default_author',
    'label'         =>  __( 'Order Author', 'nexo'),
    'description'   =>  __( 'All order placed from the self ordering module will be assigned to this author.', 'self-ordering' ),
    'options'       =>  $authorAsOptions
), 'so_settings', 1 );

$this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );

$rawTables          =   get_instance()->gastro_model->get_tables();
$tables             =   [];
foreach( $rawTables as $table ) {
    $tables[ $table[ 'ID' ] ]   =   $table[ 'NAME' ];
}

$this->Gui->add_item(array(
    'type'          =>  'select',
    'name'          =>  store_prefix() . 'so_default_table',
    'label'         =>  __( 'Default Table', 'self-ordering'),
    'description'   =>  __( 'All order needs to be assigned to a table. Please select the table used for self ordered orders.', 'self-ordering' ),
    'options'       =>  $tables
), 'so_settings', 1 );

$this->Gui->add_item([
    'type'          =>  'text',
    'name'          =>  store_prefix() . 'so_timeout_before_closing_popup',
    'label'         =>  __( 'Default Timeout', 'self-ordering' ),
    'description'   =>  __( 'After the order, the customer is invited to order again or his session is closed. 
    This invitation is valid for a defined number of seconds. You can then set for how many second this invitation is valid.', 'self-ordering' )
], 'so_settings', 1 );

$this->Gui->add_item([
    'type'          =>  'text',
    'name'          =>  store_prefix() . 'so_homepage_title',
    'label'         =>  __( 'HomePage Title', 'self-ordering' ),
    'description'   =>  __( 'Define the title of the home page.', 'self-ordering' )
], 'so_settings', 1 );

$this->Gui->add_item([
    'type'          =>  'text',
    'name'          =>  store_prefix() . 'so_nps_url',
    'label'         =>  __( 'RomPOS Agent URL', 'self-ordering' ),
    'description'   =>  __( 'Set the URL to access to RomPOS Agent in order to print a receipt of order placed by the customer.', 'self-ordering' )
], 'so_settings', 1 );

$this->Gui->add_item([
    'type'          =>  'text',
    'name'          =>  store_prefix() . 'so_maximum_seats',
    'label'         =>  __( 'Maximum Seats', 'self-ordering' ),
    'description'   =>  __( 'Define the maximum seats selectable from the dashboard.', 'self-ordering' )
], 'so_settings', 1 );

$this->Gui->add_item(array(
    'type'          =>  'select',
    'name'          =>  store_prefix() . 'so_nps_printer',
    'label'         =>  __( 'Printer', 'self-ordering'),
    'description'   =>  __( 'Define which printer will be used for order placed by the customer.', 'self-ordering' ),
    'options'       =>  []
), 'so_settings', 1 );

$this->Gui->add_item([
    'type'      =>  'dom',
    'content'   =>  $this->load->module_view( 'self-ordering', 'settings.script', [], true )
], 'so_settings', 1 );

$this->Gui->add_item(array(
    'type'          =>  'select',
    'name'          =>  store_prefix() . 'so_allowed_order_status',
    'label'         =>  __( 'Allowed Order Status', 'self-ordering'),
    'description'   =>  __( 'Choose the order allowed to appear at the kitchen (per status).', 'self-ordering' ),
    'options'       =>  [
        'show_paid'     =>  __( 'Only Paid Orders', 'self-ordering' ),
        'show_all'      =>  __( 'Show All Orders', 'self-ordering' ),
    ]
), 'so_settings', 1 );

$this->Gui->add_item(array(
    'type'          =>  'select',
    'name'          =>  store_prefix() . 'so_allow_table_selection',
    'label'         =>  __( 'Enable Table Selection', 'self-ordering'),
    'description'   =>  __( 'Define wether a table selection should be available on SOK.', 'self-ordering' ),
    'options'       =>  [
        'no'        =>  __( 'No', 'self-ordering' ),
        'yes'       =>  __( 'Yes', 'self-ordering' ),
    ]
), 'so_settings', 1 );

$this->Gui->output();