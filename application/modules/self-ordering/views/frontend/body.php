<div id="online-ordering" class="container-fluid h-100">
    <div class="row p-2 h-100" style="background: #e2e9ff; display:none">
        <div v-if="cartTabActive" class="flex-column d-flex px-0 pr-1" :class="{ 'col-md-6' : isComputer, 'col-md-12' : ! isComputer }">
            <div class="card d-block mb-2"  v-if="hasVisibleButtons( topLeftButtons ) || ! isComputer">
                <div class="card-header" v-if="! isComputer">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li v-for="tab in tabs" class="nav-item">
                            <a @click="setTabActive( tab )" class="nav-link" :class="{ 'active': tab.active }" href="javascript:void(0)" v-html="tab.title"></a>
                        </li>
                    </ul>
                </div>
                <div class="card-body p-1">
                    <div class="button-wrapper column-1">
                        <button @click="button.click()" v-for="button in topLeftButtons" v-show="button.show()" v-html="button.label" type="button" class="mr-1" :class="button.class"></button>
                    </div>
                </div>
            </div>
            <div class="card flex-fill cart-container">
                <div class="card-body p-0 d-flex flex-column">
                    <div class="cart-table">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th style="width: 350px" scope="col"><?php echo __( 'Name', 'self-ordering' );?></th>
                                    <th v-if="isComputer" class="text-left" style="width: 100px" scope="col"><?php echo __( 'Price', 'self-ordering' );?></th>
                                    <th style="width: 150px"  scope="col"><?php echo __( 'Quantity', 'self-ordering' );?></th>
                                    <th class="text-right" scope="col" style="width: 150px"><?php echo __( 'Total', 'self-ordering' );?></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="cart-table flex-fill" style="flex: 1 1 100%;overflow-y: scroll;">
                        <table class="table mb-0">
                            <tbody>
                                <tr v-for="( item, index ) in cartItems">
                                    <th style="width: 350px" scope="row">
                                        <span>{{ item.DESIGN }}</span> <span v-if="! isComputer">&mdash; {{ getSingleItemPrice( item ) | currency }}</span>
                                        <ul style="padding: 0px">
                                            <li v-for="meta in item.metas">&mdash; {{ meta.name }} : {{ meta.price | currency }}</li>
                                        </ul>
                                    </th>
                                    <td v-if="isComputer" class="text-left" style="width: 100px">{{ getSingleItemPrice( item ) | currency }}</td>
                                    <td style="width: 150px">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button @click="decreaseQuantity( item, 1, index )" class="btn btn-outline-secondary" type="button" id="button-addon1">-</button>
                                            </div>
                                            <input @change="refreshCart()" type="text" disabled v-model="item.quantity" class="form-control" placeholder="" aria-describedby="button-addon1">
                                            <div class="input-group-append">
                                                <button @click="increaseQuantity( item, 1, index )" class="btn btn-outline-secondary" type="button" id="button-addon1">+</button>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-right" style="width: 150px">{{ getTotalItemPrice( item ) | currency }}</td>
                                </tr>
                                <tr v-if="cartItems.length === 0">
                                    <th colspan="4" scope="row"><?php echo __( 'No items has been added to the cart...', 'self-ordering' );?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="cart-table">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr class="d-flex flex-row">
                                    <th scope="col" class="d-flex flex-row flex-fill justify-content-between">
                                        <span><?php echo __( 'Total Items', 'self-ordering' );?></span>
                                        <span>{{ totalItems }}</span>
                                    </th>
                                    <th scope="row" class="d-flex flex-row flex-fill justify-content-between">
                                        <span><?php echo __( 'Total', 'self-ordering' );?></span>
                                        <span>{{ totalPrice | currency }}</span>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="btn-group btn-group-lg" style="flex-shrink: 0;" role="group" aria-label="Basic example">
                        <button type="button" @click="toCheckout()" class="rounded-0 flex-fill btn btn-primary"><?php echo __( 'To Checkout', 'self-ordering' );?></button>
                        <button type="button" @click="confirmReset()" class="rounded-0 flex-fill btn btn-danger"><?php echo __( 'Cancel', 'self-ordering' );?></button>
                    </div>
                </div>
            </div>
        </div>
        <div v-if="gridTabActive" :class="{ 'col-md-6' : isComputer, 'col-md-12' : ! isComputer }" class="px-0 pl-1 d-flex flex-column">
            <div class="card d-block mb-2" v-if="hasVisibleButtons( topRightButtons )  || ! isComputer">
                <div class="card-header" v-if="! isComputer">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li v-for="tab in tabs" class="nav-item">
                            <a @click="setTabActive( tab )" class="nav-link" :class="{ 'active': tab.active }" href="javascript:void(0)" v-html="tab.title"></a>
                        </li>
                    </ul>
                </div>
                <div class="card-body p-1">
                    <div class="button-wrapper column-1">
                        <button @click="button.click()" class="mr-1" v-html="button.label"  v-for="button in topRightButtons" v-show="button.show()" type="button" :class="button.class"></button>
                    </div>
                </div>
            </div>
            
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-2" style="background: #FFF">
                    <li @click="setBreadIndexTo( index, bread )" v-for="( bread, index ) in breadcrumbs" class="breadcrumb-item"><a href="javascript:void(0)">{{ bread.name }}</a></li>
                </ol>
            </nav>

            <div class="flex-fill d-flex flex-column container-fluid" style="overflow-y: auto;background: #FFF;">
                <div class="row">
                    <div @click="goBackTo( returnTo )" v-if="returnTo !== 0" class="col-xs-6 col-sm-6 col-md-3 p-1 product-grid-item d-flex flex-row justify-content-center align-items-center">
                        <i style="font-size: 10em" class="fa fa-arrow-circle-left fa-6" aria-hidden="true"></i>
                    </div>
                    <div @click="loadCategories( category.ID )" v-if="loadType === 'categories'" v-for="category in rawCategories" class="col-xs-6 col-sm-6 col-md-3 p-1 product-grid-item" :style='{ background: `url(/thumb.php?src=/public/upload/categories/${category.THUMB}&h=${maxHeight}&zc=1) center center` }'>
                        <div class="product-item-details">
                            <p class="text-center mb-1">{{ category.DESCRIPTION }}</p>
                        </div>
                    </div>
                    <div @click="addToCart( item )" v-if="loadType === 'items'" v-for="item in rawItems" class="col-xs-6 col-sm-6 col-md-3 p-1 product-grid-item" :style='{ background: `url(/thumb.php?src=/public/upload/items-images/${item.APERCU}&h=${maxHeight}&zc=1) center center` }'>
                        <div class="product-item-details">
                            <p class="text-center mb-1">{{ item.DESIGN }}</p>
                            <p class="text-center">
                                <strong class="">{{ item.PRIX_DE_VENTE_TTC | currency }}</strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay-loader main-loader" style="background: #FFF">
        <div class="lds-roller" style="margin: auto"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div>
</div>
<style>
.modifiers-item {
    height: 7rem !important;
    margin-right: -1px;
    margin-bottom: -1px;
    padding: 1.5rem .5rem .5rem .5rem !important;
}
.cart-container {
    background: #FFF;
}
.product-item-details {
    position: absolute;
    bottom: 0;
    left: 0;
    height: 70px;
    width: 100%;
    background: #2ca2b8c7;
    color: #FFF;
    padding: 3px;
    margin: auto;
    font-size: 14px;
}
html, body {
    height:100%;
}
.product-grid-item {
    background: #FEFEFE;
    height:200px;
    border: 1px solid #EEE;
}
.product-grid-item.active:hover {
    box-shadow:inset 0px 0px 0px 4px #7a84fb;
}
.product-grid-item.active {
    box-shadow:inset 0px 0px 0px 4px #7a84fb;
}
.product-grid-item:hover {
    cursor: pointer;
    box-shadow: inset 0px 0px 2px #EEE;
}
@media screen and (max-width: 1200px) and (min-width: 901px) {
    .product-grid-item {
        height:150px;
    }
}
@media screen and (max-width: 900px) and (min-width: 766px) {
    .product-grid-item {
        height:100px;
    }
}
@media screen and (max-width: 765px) {
    .product-grid-item {
        height:200px;
    }
}
#modal-vue-modifier-modal .modal-body, 
#modal-vue-login-modal .modal-body,
#modal-vue-ask-person .modal-body, 
#modal-vue-registration-popup .modal-body, #modal-vue-ask-table .modal-body {
    display: flex;
    flex-direction: column;
    flex-basis: 0;
    overflow-y: auto;
}
.oy-auto {
    overflow-y: auto;
}
.busy-table {
    background: #d8d8d8;
}
div.selected-table, div.selected-table:hover {
    box-shadow: inset 0px 0px 0px 8px #8a75ff !important;
}
</style>
<script>
    const csrf                  =   <?php echo json_encode( array(
        'name' => $this->security->get_csrf_token_name(),
        'hash' => $this->security->get_csrf_hash()
    ) );?>;
    const storeOptions          =   <?php echo json_encode([
        'store_id'                  =>  $this->uri->segment(3),
        'url'                       =>  base_url(),
        'author'                    =>  store_option( 'so_default_author', 2 ), // who has posted the orders. A user should be created for that.
        'timerBeforeClosingPopup'   =>  store_option( 'so_timeout_before_closing_popup', 5000 ),
        'default_table'             =>  store_option( 'so_default_table', 1 ),
        'so_allow_table_selection'  =>  store_option( 'so_allow_table_selection', 'no' ),
        'printer'               =>  [
            'name'              =>  store_option( 'so_nps_printer', false ),
            'url'               =>  store_option( 'so_nps_url', false )
        ],
        'maxium_seats'          =>  store_option( 'so_maximum_seats', 30 ),
        'multistore'            =>  get_option( 'nexo_store', 'disabled' ),
    ]);?>;
    const user                  =   <?php echo json_encode( User::get() );?>;
    const currencyConfig        =   {
        symbol      :   '<?php echo store_option( 'nexo_currency' );?>',
        position    :   '<?php echo store_option( 'nexo_currency_position' );?>'
    };
    const textDomain                =   <?php echo json_encode([
        'goBackStores'              =>  __( 'Go to Branches', 'self-ordering' ),
        'dinein'                    =>  __( 'Dine In', 'self-ordering' ),
        'maxSeats'                  =>  __( 'Max Seats', 'self-ordering' ),
        'delivery'                  =>  __( 'Delivery', 'self-ordering' ),
        'busyTable'                 =>  __( 'This table is already in use !', 'self-ordering' ),
        'takeaway'                  =>  __( 'Take Away', 'self-ordering' ),
        'selectTable'               =>  __( 'Select Table', 'self-ordering' ),
        'selectTableShowDetails'    =>  __( 'Table : #', 'self-ordering' ),
        'mustSelectATable'          =>  __( 'You must select a table before proceeding', 'self-ordering' ),
        'whichTableIsUsed'          =>  __( 'Which table would you like to use', 'self-ordering' ),
        'fieldRequired'             =>  __( 'This field is required.', 'self-ordering' ),
        'NotDefined'                =>  __( 'Not defined', 'self-ordering' ),
        'shippingFormError'         =>  __( 'Unable to proceed the shipping form has an error.', 'self-ordering' ),
        'name'                      =>  __( 'First Name', 'self-ordering' ),
        'surname'                   =>  __( 'Second Name', 'self-ordering' ),
        'pobox'                     =>  __( 'Post Code', 'self-ordering' ),
        'phone'                     =>  __( 'Phone', 'self-ordering' ),
        'address_1'                 =>  __( 'Address 1', 'self-ordering' ),
        'address_2'                 =>  __( 'Address 2', 'self-ordering' ),
        'country'                   =>  __( 'Country', 'self-ordering' ),
        'city'                      =>  __( 'City', 'self-ordering' ),
        'orderTypeRequired'         =>  __( 'Selecting an order type is required', 'self-ordering' ),
        'orderType'                 =>  __( 'Type', 'self-ordering' ),
        'shippingAddress'           =>  __( 'Address', 'self-ordering' ),
        'defineShippingAddress'     =>  __( 'Define Shipping Address', 'self-ordering' ),
        'selectOrderType'           =>  __( 'Select Order type', 'self-ordering' ),
        'alreadyHasCode'            =>  __( 'Reset Password', 'self-ordering' ),
        'reset_code'                =>  __( 'Reset Code', 'self-ordering' ),
        'lostPassword'              =>  __( 'Lost Password ?', 'self-ordering' ),
        'unableToLogin'             =>  __( 'Request Password Recovery', 'self-ordering' ),
        'lostPasswordTitle'         =>  __( 'Lost Password', 'self-ordering' ),
        'enterRecoveryCode'         =>  __( 'Recovery Code', 'self-ordering' ),
        'lostPwdDetails'            =>  __( 'Please provide the account email you\'re attempting to request a password change.', 'self-ordering' ),
        'hasCodeDetails'            =>  __( 'Reset a password by using a code send to your inbox.', 'self-ordering' ),
        'passwordRecovery'          =>  __( 'Password Recovery', 'self-ordering' ),
        'closeSeconds'              =>  __( 'Closing within # second(s)', 'self-ordering' ),
        'welcome'                   =>  __( 'Welcome #', 'self-ordering' ),
        'logout'                    =>  __( 'Logout', 'self-ordering' ),
        'login'                     =>  __( 'Login', 'self-ordering' ),
        'register'                  =>  __( 'Create an account', 'self-ordering' ),
        'errorOccured'              =>  __( 'An unexpected error has occured during the operation.', 'self-ordering' ),
        'orderCreated'              =>  __( 'Your order has been send to the cashier. You can proceed to the payment from there.', 'self-ordering' ),
        'loginAlert'                =>  __( 'You need to login before proceeding', 'self-ordering' ),
        'username'                  =>  __( 'Username', 'self-ordering' ),
        'email'                     =>  __( 'Email', 'self-ordering' ),
        'password'                  =>  __( 'Password', 'self-ordering' ),
        'password_confirm'          =>  __( 'Confirm Password', 'self-ordering' ),
        'ok'                        =>  __( 'Ok', 'self-ordering' ),
        'cart'                      =>  __( 'Cart', 'self-ordering' ),
        'grid'                      =>  __( 'Products', 'self-ordering' ),
        'cancel'                    =>  __( 'Cancel', 'self-ordering' ),
        'confirm'                   =>  __( 'Would you like to confirm ?', 'self-ordering' ),
        'confirmOrderMessage'       =>  __( 'The order is ready to be send to the kitchen. Would you like to proceed ?', 'self-ordering' ),
        'warning'                   =>  __( 'Warning', 'self-ordering' ),
        'loginWarning'              =>  __( 'You need to provide the username & the password.', 'self-ordering' ),
        'emptyCart'                 =>  __( 'You cannot proceed if the cart is empty', 'self-ordering' ),
        'home'                      =>  __( 'Home', 'self-ordering' ),
        'stockExausted'             =>  __( 'Stock Exhausted ! Unable to add this item to the cart.', 'self-odering' ),
        'itemAdded'                 =>  __( 'The item has been added to the cart.', 'self-ordering' ),
        'selectModifier'            =>  __( 'Select a modifier', 'self-ordering' ),
        'modifierGroup'             =>  __( 'Modifier Group', 'self-ordering' ),
        'modifierRequired'          =>  __( 'You need to select a modifier before proceeding.', 'self-ordering' ),
        'modifierAdded'             =>  __( 'The modifier has been added.', 'self-ordering' ),
        'noModifierAvailable'       =>  __( 'No modifier availabel for this group', 'self-ordering' ),
        'userConnected'             =>  __( 'The user is connected.', 'self-ordering' ),
        'userNotConnected'          =>  __( 'Please login before proceeding.', 'self-ordering' ),
        'howManyJoinTheParty'       =>  __( 'How many people join the party ?', 'self-ordering' ),
        'seatsRequired'             =>  __( 'You need to define how many seat will be used !', 'self-ordering' ),
        'seatHasBeenSelected'       =>  __( 'The seats has been selected.', 'self-ordering' ),
        'usernameRequired'          =>  __( 'You need to input a username', 'self-ordering' ),
        'orderAgainDetails'         =>  __( 'Would you like to order something else ? If not you\'ll be automatically disconnected...', 'self-ordering' ),
        'orderAgain'                =>  __( 'Proceed Another Order ?', 'self-ordering' ),
        'loginAsGuest'              =>  __( 'Login as guest', 'self-ordering' ),
        'unexpectedErrorOccured'    =>  __( 'An unexpected error occured during the authentication', 'self-ordering' ),
        'resetCartConfirmMessage'   =>  __( 'Would you like to reset the cart ? The current user will be disconnected !', 'self-ordering' )
    ]);?>;
    const BreakPointCSS     =   
        `<style id="modal-vue-style-{namespace}">
        /* 
        ##Device = Desktops
        ##Screen = 1281px to higher resolution desktops
        */

        @media (min-width: 1281px) {
        
            {elementSelector} .modal-dialog {
                /*xlheight*/
                /*xlwidth*/
            }
        
        }

        /* 
        ##Device = Laptops, Desktops
        ##Screen = B/w 1025px to 1280px
        */

        @media (min-width: 1025px) and (max-width: 1280px) {
        
            {elementSelector} .modal-dialog {
                /*lgheight*/
                /*lgwidth*/
            }
        
        }

        /* 
        ##Device = Tablets, Ipads (portrait)
        ##Screen = B/w 768px to 1024px
        */

        @media (min-width: 768px) and (max-width: 1024px) {
        
            {elementSelector} .modal-dialog {
                /*mdheight*/
                /*mdwidth*/
            }
        
        }

        /* 
        ##Device = Tablets, Ipads (landscape)
        ##Screen = B/w 768px to 1024px
        */

        @media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
        
            {elementSelector} .modal-dialog {
                /*smheight*/
                /*smwidth*/
            }
        
        }

        /* 
        ##Device = Low Resolution Tablets, Mobiles (Landscape)
        ##Screen = B/w 360px to 767px
        */

        @media (min-width: 360px) and (max-width: 767px) {
        
            {elementSelector} .modal-dialog {
                /*xsheight*/
                /*xswidth*/
                margin: 1.75rem auto;
            }
        
        }
    </style>`;
</script>
<script src="<?php echo module_url( 'nexo' ) . 'js/responsive.js';?>"></script>
<script src="<?php echo module_url( 'nexo' ) . 'js/modal.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.order-type.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.lost-password.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.login.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.registration.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.modifier.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.select-table.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.select-seats.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/pos.shipping-address.vue.js';?>"></script>
<script src="<?php echo module_url( 'self-ordering' ) . 'js/frontend.vue.js';?>"></script>
<link rel="stylesheet" href="<?php echo module_url( 'nexo' ) . '/css/loader-style.css';?>"/>
