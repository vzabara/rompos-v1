<div class="container-fluid h-100 d-flex flex-row" style="background: #EEE;">
    <div class="flex-fill d-flex align-items-center">
        <div class="row flex-fill">
            <div class="col-md-12">
                <h1 class="display-4 text-center"><?php echo get_option( 'site_name' );?></h1>
                <p class="text-center"><?php echo __( 'Please choose the branch where you want to place your order.', 'self-ordering' );?></p>
                <br>
            </div>
            <div class="col-md-6 offset-md-3 d-flex">
                <div class="row align-items-center flex-fill">
                    <?php foreach( $stores as $store ):?>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="card">
                                <img style="max-height:120px" class="card-img-top" src="<?php echo ! empty( $store[ 'IMAGE' ] ) ? upload_url() . '/stores/' . $store[ 'IMAGE' ] : module_url( 'self-ordering' ) . '/images/store-with-no-image.jpg';?>" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $store[ 'NAME' ];?></h5>
                                    <p class="card-text"><?php echo $store[ 'DESCRIPTION' ] ?: __( 'No description provided for this store', 'self-ordering' );?></p>
                                    <a href="<?php echo site_url([ 'so', 'store', $store[ 'ID' ]] );?>" class="btn btn-primary"><?php echo __( 'Choose', 'self-ordering' );?></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
                <?php if ( count( $stores ) === 0 ):?>
                    <div class="alert alert-info" role="alert">
                        <strong><?php echo __( 'It seems like there is not store created on the system. Please consider creating a new store.', 'self-ordering' );?></strong>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>