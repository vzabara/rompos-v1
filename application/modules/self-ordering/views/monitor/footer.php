    <script>
        const textDomain    =   <?php echo json_encode([
            'processingOrder'   =>  __( 'Processing the order, #', 'self-ordering' ),
            'orderReady'        =>  __( 'the order, #, is ready.', 'self-ordering' )
        ]);?>;
    </script>
    <script src="<?php echo module_url( 'self-ordering' ) . 'js/modal.vue.js';?>"></script>
    <script src="<?php echo module_url( 'nexo' ) . 'bower_components/moment/min/moment.min.js';?>"></script>    
    <script src="<?php echo module_url( 'self-ordering' ) . 'js/order-monitor.js';?>"></script>    
</html>