<body>
    <div class="container-fluid h-100 d-flex flex-column" id="monitor-vue">
        <div class="row py-3 flex-fill">
            <div class="col-md-6 d-flex flex-column">
                <div class="card d-block rounded-0 border-bottom-0">
                    <div class="card-header">
                        <?php echo __( 'Ready Orders', 'self-ordering' );?>
                    </div>
                    <div class="card-body">
                        <h1 v-if="!lastReadyOrder"class="display-3 text-center"><?php echo __( 'N/A', 'self-ordering' );?></h1>
                        <h1 v-if="lastReadyOrder" class="display-3 text-center">{{ lastReadyOrder.CODE }}</h1>
                    </div>
                    <hr class="border-top m-0">
                    <div class="card-body">
                        <h1 v-if="!beforeLastReadyOrder" class="display-3 text-center"><?php echo __( 'N/A', 'self-ordering' );?></h1>
                        <h1 v-if="beforeLastReadyOrder"  class="display-3 text-center">{{ beforeLastReadyOrder.CODE }}</h1>
                    </div>
                </div>
                <div class="card rounded-0 flex-fill">
                    <div class="card-body p-0 d-flex flex-column">
                        <div class="order-header">
                            <table class="table m-0">
                                <thead class="thead-dark">
                                    <tr>
                                        <th width="400" scope="col">
                                            <?php echo __( 'Order ID', 'self-ordering' );?>
                                        </th>
                                        <th class="text-left" width="200" scope="col">
                                            <?php echo __( 'Time', 'self-ordering' );?>
                                        </th>
                                        <th class="text-left" width="300" scope="col">
                                            <?php echo __( 'Guest Name', 'self-ordering' );?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="orders-container flex-fill" style="overflow-y:auto">
                            <table class="table m-0">
                                <tbody>
                                    <tr v-for="order in readyOrders" >
                                        <th width="400" scope="row">{{ order.CODE }}</th>
                                        <td class="text-left" width="200">{{ showHours( order.DATE_MOD  ) }}</td>
                                        <td class="text-left" width="300">{{ order.customer.NOM }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-flex flex-column">
                <div class="card flex-fill">
                    <div class="card-header">
                        <?php echo __( 'Processing Orders', 'self-ordering' );?>
                    </div>
                    <div class="card-body p-0 d-flex flex-column">
                        <div class="ongoing-order-header">
                            <table class="table m-0">
                                <thead class="thead-dark">
                                    <tr>
                                        <th width="400" scope="col">
                                            <?php echo __( 'Order ID', 'self-ordering' );?>
                                        </th>
                                        <th class="text-left" width="200" scope="col">
                                            <?php echo __( 'Time', 'self-ordering' );?>
                                        </th>
                                        <th class="text-left" width="300" scope="col">
                                            <?php echo __( 'Guest Name', 'self-ordering' );?>
                                        </th>
                                        <th class="text-right" width="150" scope="col">
                                            <?php echo __( 'Meals', 'self-ordering' );?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="ongoing-orders-container flex-fill" style="overflow-y:auto">
                            <table class="table m-0">
                                <tbody>
                                    <tr v-for="order in ongoingOrders">
                                        <th class="text-left" width="400">{{ order.CODE }}</th>
                                        <th width="200" class="text-left" width="100">{{ showHours( order.DATE_CREATION ) }}</th>
                                        <th width="300" class="text-left" scope="row">{{ order.customer.NOM }}</th>
                                        <th width="150" class="text-right" width="100">{{ getMealStatus( order.items ) }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<style>
    html,
    body {
        height: 100%;
    }
    #monitor-vue {
        background: #EEE;
    }
</style>