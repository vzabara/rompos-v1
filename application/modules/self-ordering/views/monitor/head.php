<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo module_url( 'self-ordering' ) . '/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo module_url( 'nexo' ) . '/../../../bower_components/sweetalert2/dist/sweetalert2.min.css';?>">
    <link rel="stylesheet" href="<?php echo module_url( 'nexo' ) . '/../../../css/font-awesome.min.css';?>">
    <script src="<?php echo module_url( 'nexo' ) . '/bower_components/axios/dist/axios.min.js';?>"></script>
    <script src="<?php echo module_url( 'nexo' ) . '/bower_components/vue/dist/vue.min.js';?>"></script>
    <script src="<?php echo module_url( 'nexo' ) . '/bower_components/jquery/dist/jquery.min.js';?>"></script>
    <script src="<?php echo module_url( 'nexo' ) . '/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js';?>"></script>
    <script src="<?php echo module_url( 'nexo' ) . '/../../../bower_components/sweetalert2/dist/sweetalert2.min.js';?>"></script>
    <title><?php echo __( 'Gastro Orders Monitor', 'gastro' );?></title>
    <?php include_once( MODULESPATH . 'nexo/views/exposed-http-request.php' );?>
</head>