<?php

include_once( dirname( __FILE__ ) . '/inc/SelfOrderingActions.php' );
include_once( dirname( __FILE__ ) . '/inc/SelfOrderingFilters.php' );
include_once( dirname( __FILE__ ) . '/vendor/autoload.php' );

class SelfOrderingModule extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();

        $this->filters  =   new SelfOrderingFilters;
        $this->actions  =   new SelfOrderingActions;

        $this->events->add_action( 'load_frontend', [ $this->actions, 'load_frontend' ]);
        $this->events->add_action( 'do_enable_module', [ $this->actions, 'enable_module' ]);
        $this->events->add_filter( 'admin_menus', [ $this->filters, 'admin_menus' ], 20 );
        $this->events->add_filter( 'gastro_kitchen_orders', [ $this->filters, 'filter_orders' ]);
        // $this->events->add_action( 'nexo_customers_init', [ $this->actions, 'init_customers' ]);
        $this->events->add_filter( 'customer_basic_fields_properties', [ $this->filters, 'customer_fields' ]);
        $this->events->add_filter( 'nexo_customers_basic_fields', [ $this->filters, 'customers_field_type' ]);
        $this->events->add_filter( 'nexo_filters_customers_put_fields', [ $this->filters, 'filter_put_fields' ], 10, 3 );
        $this->events->add_filter( 'nexo_filters_customers_post_fields', [ $this->filters, 'filter_post_fields' ], 10, 2 );
    }
}
new SelfOrderingModule();