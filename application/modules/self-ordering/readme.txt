Thank you for having purchased Self Order Kiosk
===============================================
We've created a documentation online at the following 
url : 

- https://rompos.com/divi-nexopos/documentation/self-order-kiosk/

Support : 
=========
You can request a support by contacting use at : contact@rompos.com, 
or by joining the WhatsApp community : 

https://rompos.com/divi-nexopos/join-the-community/

Regards.