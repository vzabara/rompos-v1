<?php
$Routes->post( 'so/login', 'SoApiController@login' );
$Routes->post( 'so/logout', 'SoApiController@logout' );
$Routes->post( 'so/order', 'SoApiController@order' );
$Routes->get( 'so/categories/{id?}', 'SoApiController@categories' );