<?php
class SoApiController extends Tendoo_Api
{
    /**
     * Logout the logged user
     * @return json
     */
    public function logout() 
    {
        return $this->response([
            'status'    =>  'success',
            'message'   =>  __( 'The user is logged out !', 'self-ordering' ),
        ]);
    }
}