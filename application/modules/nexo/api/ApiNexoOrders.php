<?php
use Carbon\Carbon;

class ApiNexoOrders extends Tendoo_Api
{
    public function full_order($order_id)
    {
        $this->load->model('Nexo_Checkout');
        $this->load->module_model('nexo', 'NexoCustomersModel', 'customerModel');
        $this->load->module_model('nexo', 'Nexo_Orders_Model', 'orderModel');

        $data = $this->events->apply_filters(
            'loaded_order',
            $this->Nexo_Checkout->get_order_products($order_id, true)
        );

        if ($data) {
            // load shippings
            /**
             * get shippings linked to that order
             * @since 3.1
             **/

            foreach (( array )$data['order'] as $index => $_order) {
                $shippings = $this->db->where('ref_order', $_order['ID'])
                                      ->get(store_prefix() . 'nexo_commandes_shippings')
                                      ->result_array();

                if ($shippings) {
                    $data['order'][$index]['shipping'] = $shippings[0];
                }

            }

            $data['order'] = $data['order'][0];

            // load metas
            $metaData = $this->db->where([
                'REF_ORDER_ID' => $order_id
            ])
                                 ->get(store_prefix() . 'nexo_commandes_meta')
                                 ->result_array();
            // use woocommerce order id instead of CODE
//            foreach ($metaData as $meta) {
//                if ( $meta['KEY'] == 'woocommerce_order_id' ) {
//                    $data[ 'order' ]['CODE'] = $meta['VALUE'];
//                }
//            }
            $data['order']['metas'] = $metaData;


            /**
             * load customer informations
             */
            $base = $this->customerModel->get($_order['REF_CLIENT']);

            $data['customer'] = [
                'informations' => $base[0],
                'address'      => $this->customerModel->get_informations($_order['REF_CLIENT']),
            ];

            /**
             * include refund made on this orders
             * @var array
             */
            $refunds = $this->db->where('REF_ORDER', $_order['ID'])
                                ->get(store_prefix() . 'nexo_commandes_refunds')
                                ->result_array();

            /**
             * Fill refunds with refunded items
             * but sometime they might not
             * be provided
             * @var array
             */
            $refunds = array_map(function (&$refund) {
                $items = $this->db->where('REF_REFUND', $refund['ID'])
                                  ->get(store_prefix() . 'nexo_commandes_refunds_products')
                                  ->result_array();

                $refund['items'] = $items;

                return $refund;
            }, $refunds);

            $data['refunds'] = $refunds;

            $this->response($data, 200);
        }

        $this->__empty();
    }

    /**
     * get Orders
     * @return json
     */
    public function orders()
    {
        $orders = $this->db->get(store_prefix() . 'nexo_commandes')
                           ->result_array();

        return $this->response($orders);
    }

    /**
     * Change order status
     *
     * @param int order id
     *
     * @return json
     */
    public function setOrderStatus($order_id)
    {
        $this->load->module_model('nexo', 'NexoLogModel', 'history');
        $this->load->model('Nexo_Checkout');
        $status = $this->config->item('nexo_orders_status');

        $order = $this->Nexo_Checkout->get_order($order_id);

        if ($order[0]['STATUS'] === $this->post('status')) {
            return $this->response([
                'status'  => 'failed',
                'message' => __('No changes to save!')
            ]);
        }

        /**
         * If the order has been found, let's yet register his state.
         */
        if ($order) {
            $this->history->log(
                __('Mise à jour d\'une commande', 'nexo'),
                sprintf(
                    __('Le statut de la commande <strong>%s</strong> a changé de <strong>%s</strong> à <strong>%s</strong> par <strong>%s</strong>',
                        'nexo'),
                    $order[0]['CODE'],
                    $status[$order[0]['STATUS']],
                    $status[$this->post('status')],
                    User::pseudo()
                )
            );
        }

        $this->db->where('ID', $order_id)
                 ->update(store_prefix() . 'nexo_commandes', [
                     'STATUS'   => $this->post('status'),
                     'DATE_MOD' => date_now()
                 ]);

        // check if we need to notify wooCommerce
        $metaData = $this->db->where([
            'REF_ORDER_ID' => $order_id,
            'KEY'          => 'woocommerce_order_id',
        ])
                             ->get(store_prefix() . 'nexo_commandes_meta')
                             ->result_array();
        if ( ! empty($metaData[0]['VALUE'])) {
            // set woocommerce compatibel status
            $status = in_array($this->post('status'), [
                'completed',
                'shipped',
                'delivered',
            ]) ? 'completed' : $this->post('status');
            // send request to wooCommerce
            $oauth_verifier = store_option(store_prefix() . 'oauth_verifier');
            $data           = [
                'oauth_verifier' => $oauth_verifier,
                'order'          => $metaData[0]['VALUE'],
                'code'           => $order[0]['CODE'],
                'status'         => $status,
            ];
            // save to local table
            $this->db->insert(store_prefix() . 'nexo_commandes_status', array(
                'id'             => $order[0]['ID'],
                'code'           => $order[0]['CODE'],
                'woocommerce_id' => $metaData[0]['VALUE'],
                'status'         => $status,
            ));
            $url = store_option(store_prefix() . 'oauth_api_url');
            if ( ! empty($url)) {
                $url .= '/wp-json/rompos/v1/status';
                // more info at https://www.datafeedr.com/using-oauth-1-0-wordpress-api-custom-endpoints/
                $key   = array(
                    'oauth_consumer_key'    => store_option(store_prefix() . 'oauth_consumer_key'),
                    //'L1rrOgwKG3UA',
                    'oauth_consumer_secret' => store_option(store_prefix() . 'oauth_consumer_secret'),
                    //'S5VsBTnE8Hr2usegXDxHAO7a3ihbpHCTzg3KQL07sa3tPmy4',
                    'oauth_token'           => store_option(store_prefix() . 'oauth_token'),
                    //'FYRZWQMiiZjBLvtCdAOk0CrG',
                    'oauth_token_secret'    => store_option(store_prefix() . 'oauth_token_secret'),
                    //'jJIHTtdO2eqwrYpFzzcosbCTFde2n1d73Jy0Z6PpNeb1tNsv',
                );
                $oauth = new OAuth_Authorization_Header();
                $oauth->init($key, $url, 'POST');
                $curl   = new Curl\Curl();
                $header = $oauth->get_header();
                $curl->setOpt(CURLOPT_HTTPHEADER, ['Authorization: ', $header]);
                $curl->post($url, $data);
                if ($curl->error) {
                    return [
                        'status'  => 'failed',
                        'message' => $curl->errorMessage
                    ];
                }
                // remove processed status
                $this->db->delete(store_prefix() . 'nexo_commandes_status', array(
                    'id'     => $order[0]['ID'],
                    'status' => $status,
                ));
            }
        }

        return $this->response([
            'status'  => 'success',
            'message' => __('Le statut de la commande a été mis à jour', 'nexo')
        ]);
    }

    /**
     * Proceed to a payment of an order
     * @return json
     */
    public function payment($order_id)
    {

        $this->load->module_model('nexo', 'Nexo_Orders_Model', 'orderModel');

        $response = $this->orderModel->addPayment($order_id, $this->post('amount'), $this->post('namespace'));

        if ($response['status'] === 'failed') {
            return $this->response($response, 403);
        }

        return $this->response($response);
    }

    /**
     * Return a json object of
     * payment made for an order
     * @return json
     */
    public function payments($order_id)
    {
        $this->load->module_model('nexo', 'Nexo_Orders_Model', 'orderModel');

        $response = $this->orderModel->getPayments($order_id);

        if (@$response['status'] === 'failed') {
            return $this->response($response, 403);
        }

        return $this->response($response);
    }

    /**
     * Make a partial refund
     *
     * @param int order id
     *
     * @return json
     */
    public function refund($order_id)
    {
        $this->load->module_model('nexo', 'Nexo_Orders_Model', 'orderModel');
        $total                = $this->post('total');
        $sub_total            = $this->post('sub_total');
        $shipping_fees        = $this->post('shipping_fees');
        $type                 = $this->post('type');
        $description          = $this->post('description');
        $payment_type         = $this->post('payment_type');
        $products             = $this->post('products');
        $refund_shipping_fees = $this->post('refund_shipping_fees');

        $response = $this->orderModel->refund(compact(
            'shipping_fees',
            'refund_shipping_fees',
            'total',
            'sub_total',
            'order_id',
            'type',
            'description',
            'payment_type',
            'products'
        ));

        return $this->response($response);
    }

    /**
     * Display the refund history registered for a specific order
     *
     * @param int order id
     *
     * @return json
     */
    public function refundHistory($order_id)
    {
        $this->load->module_model('nexo', 'Nexo_Orders_Model', 'orderModel');

        return $this->response($this->orderModel->order_refunds($order_id));
    }
}