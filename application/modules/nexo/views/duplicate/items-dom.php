<?php
    global $Options;
    $this->config->load( 'rest' )
?>
	<form enctype="multipart/form-data" method="POST" ng-controller="duplicateController">
		<?php echo tendoo_info( __( 'Here you can duplicate items from one store into another', 'nexo' ));?>
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();
?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="source">
							<?php echo __( 'Source', 'nexo' );?>
						</label>
						<select class="form-control" name="source" ng-model="selectedSource">
							<option value="0" selected>
								<?php echo __( 'Main', 'nexo' );?>
							</option>
							<?php foreach( $sources as $source ):?>
							<option value="<?php echo $source[ 'ID' ];?>">
								<?php echo $source[ 'NAME' ];?>
							</option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="form-group">
						<label for="target">
							<?php echo __( 'Target', 'nexo' );?>
						</label>
						<select class="form-control" name="target" ng-model="selectedTarget">
							<option value="0" selected>
								<?php echo __( 'Main', 'nexo' );?>
							</option>
							<?php foreach( $targets as $target ):?>
							<option value="<?php echo $target[ 'ID' ];?>">
								<?php echo $target[ 'NAME' ];?>
							</option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="form-group">
						<label for="target">
							<?php echo __( 'Clear target before duplicating', 'nexo' );?>
						</label>
						<select class="form-control" name="clear" ng-model="selectedClear">
							<option value="1" selected>
								<?php echo __( 'Yes', 'nexo' );?>
							</option>
							<option value="0">
								<?php echo __( 'No', 'nexo' );?>
							</option>
						</select>
					</div>
				</div>
			</div>

			<button type="submit" class="btn btn-primary" ng-class="{ 'disabled' : selectedSource==selectedTarget }" ng-click="submitDumplicate()">
				<?php echo __( 'Duplicate', 'nexo' );?>
			</button>
			<br>
			<br>
			<div ng-show="notices.length > 0">
				<p ng-repeat="notice in notices">{{ notice.msg }}</p>
			</div>
	</form>