<?php
global $Options;
?>
<?php include_once(MODULESPATH . '/nexo/inc/angular/services/textHelper.php'); ?>
<script type="text/javascript">
    "use strict";

    tendooApp.controller('duplicateController', ['$scope', '$http', 'textHelper', function ($scope, $http, textHelper) {

        $scope.selectedSource = '0';
        $scope.selectedTarget = '0';
        $scope.selectedClear = '1';

        $scope.submitDumplicate = function () {

            if ($scope.selectedSource == $scope.selectedTarget) {
                NexoAPI.Notify().warning('<?php echo _s('Source and target cannot be the same', 'nexo');?>');
                return;
            }

            $scope.notices = new Array;

            $scope.notices.push({
                msg: '<?php echo _s('Duplicating tables...', 'nexo');?>',
                type: 'info'
            });

            $http.post('<?php echo site_url(array('rest', 'nexo', 'duplicate', store_get_param('?')));?>', {
                'source': $scope.selectedSource,
                'target': $scope.selectedTarget,
                'clear': $scope.selectedClear,
            }, {
                headers: {
                    '<?php echo $this->config->item('rest_key_name');?>': '<?php echo @$Options['rest_key'];?>'
                }
            }).then(function (returned) {
                if (returned.data.status == 'success') {
                    $scope.notices.push({
                        msg: '<?php echo _s('Duplicating is completed', 'nexo');?>',
                        type: 'info'
                    });
                } else {
                    $scope.notices.push({
                        msg: '<?php echo _s('Error', 'nexo');?>',
                        type: 'warning'
                    });
                }

            }, function () {
                $scope.notices.push({
                    msg: '<?php echo _s('Error', 'nexo');?>',
                    type: 'warning'
                });
            });
        }
    }]);
</script>
