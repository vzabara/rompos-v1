<?php
$this->Gui->col_width(1, 2);

$this->Gui->add_meta(array(
     'type'			=>    'unwrapped',
     'col_id'		=>    1,
     'namespace'	=>    'nexo_orders',
     'gui_saver'    =>   true,
     'footer'       =>   [
          'submit'  =>   [
               'label'   =>   __( 'Enregistrer', 'nexo' )
          ]
     ]
));

$this->Gui->add_item( array(
     'type' =>    'select',
     'options'      =>   [ 
          ''        =>   __( 'Choisissez une option', 'nexo' ),
          'yes'     =>   __( 'Oui', 'nexo' ),
          'no'      =>   __( 'Non', 'nexo' )
     ],
     'name' =>	store_prefix() . 'enable_order_aging',
     'label' =>   __( 'Expiration des commandes', 'nexo' ),
     'description' =>   __( 'Activer une date d\'expiration des commandes qui ne sont pas totalement réglées', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'options'      =>   [ 
        ''        =>   __( 'Choisissez une option', 'nexo' ),
        'order_code'     =>   __( 'Code Aléatoire', 'nexo' ),
        'date_code'      =>   __( 'Code à Date', 'nexo' )
    ],
    'name' =>	store_prefix() . 'nexo_code_type',
    'label' =>   __( 'Type des codes', 'nexo' ),
    'description' =>   __( 'Modifier le type de code que vous souhaitez appliquer au code d\'une commande', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
     'type'                   =>    'select',
     'options'                =>   [ 
          ''                  =>   __( 'Choisissez une option', 'nexo' ),
          'quotes'            =>   __( 'Devis', 'nexo' ),
          'incompletes'       =>   __( 'Incomplètes', 'nexo' ),
          'both'              =>   __( 'Devis & Incomplètes', 'nexo' )
     ],
     'name' =>	store_prefix() . 'expiring_order_type',
     'label' =>   __( 'Commandes concernées', 'nexo' ),
     'description' =>   __( 'Définir les commandes qui sont susceptibles d\'expirer.', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
     'type' =>    'text',
     'name' =>	store_prefix() . 'expiration_time',
     'label' =>   __( 'Expiration (Jours)', 'nexo' ),
     'description' =>   __( 'Définir après combien de temps une commande expire.', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
    'type' =>    'text',
    'name' =>	store_prefix() . 'orders_sync_url',
    'label' =>   __( 'Orders sync URL', 'nexo' ),
    'description' =>   __( 'Remote ROMPOS instance where orders are collected.', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
    'type'                   =>    'select',
    'options'                =>   [
        ''                  =>   __( 'Choisissez une option', 'nexo' ),
        '1'            =>   __( 'Yes', 'nexo' ),
        '0'       =>   __( 'No', 'nexo' ),
    ],
    'name' =>	store_prefix() . 'orders_overwrite_id',
    'label' =>   __( 'Overwrite ID', 'nexo' ),
    'description' =>   __( 'Allow to use WooCommerce IDs as order CODE', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
    'type' =>    'text',
    'name' =>	store_prefix() . 'orders_client_id',
    'label' =>   __( 'Client ID', 'nexo' ),
    'description' =>   __( 'Unique client ID to process its own orders for particular store.', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
    'type' =>    'text',
    'name' =>	store_prefix() . 'oauth_api_url',
    'label' =>   __( 'OAuth API URL', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
     'type' =>    'text',
     'name' =>	store_prefix() . 'oauth_consumer_key',
     'label' =>   __( 'OAuth Consumer Key', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
     'type' =>    'text',
     'name' =>	store_prefix() . 'oauth_consumer_secret',
     'label' =>   __( 'OAuth Consumer Secret', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
     'type' =>    'text',
     'name' =>	store_prefix() . 'oauth_token',
     'label' =>   __( 'OAuth Token', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
     'type' =>    'text',
     'name' =>	store_prefix() . 'oauth_token_secret',
     'label' =>   __( 'OAuth Token Secret', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->add_item( array(
     'type' =>    'text',
     'name' =>	store_prefix() . 'oauth_verifier',
     'label' =>   __( 'OAuth Verify Token', 'nexo' ),
), 'nexo_orders', 1 );

$this->Gui->output();