<?php
/**
 * Starting Cache
 * Cache should be manually restarted
**/

use Carbon\Carbon;

if (! $order_cache = $cache->get($order[ 'order' ][0][ 'ID' ]) || @$_GET[ 'refresh' ] == 'true') {
    ob_start();
}
$print_header = store_option( 'nexo_receipt_header' );

?>
<?php if( @$_GET[ 'ignore_header' ] != 'true' ):?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo sprintf(__('Order ID : %s &mdash; Nexo Shop Receipt', 'nexo'), $order[ 'order' ][0][ 'CODE' ]);?></title>
<link rel="stylesheet" media="all" href="<?php echo css_url('nexo') . '/bootstrap.min.css';?>" />
<link rel="stylesheet" media="all" href="<?php echo module_url( 'nexo' ) . 'fonts/receipt-stylesheet.css';?>" />
</head>

<body>
<?php endif;?>

<?php global $Options;?>
<?php if (@$order[ 'order' ][0][ 'CODE' ] != null):?>
<div class="container-fluid">
    <div class="row">
        <div class="well col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="order-details">
                <div class="row">
                    <?php if($print_header=='yes'): ?>
                    <?php if( store_option( 'url_to_logo' ) != null ):?>
                        <div class="text-center">
                            <img src="<?php echo store_option( 'url_to_logo' );?>" style="display:inline-block;<?php echo store_option( 'logo_height' ) != null ? 'height:' . store_option( 'logo_height' ) . 'px' : '';?>;<?php echo store_option( 'logo_width' ) != null ? 'width:' . store_option( 'logo_width' ) . 'px' : '';?>"/>
                        </div>
                    <?php else:?>
                        <h2 class="text-center"><?php echo @$Options[ store_prefix() . 'site_name' ];?></h2>
                    <?php endif;?>
                    <?php endif; ?>
                </div>
                <?php ob_start();?>
                <div class="row shop-details">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <?php echo xss_clean( @$Options[ store_prefix() . 'receipt_col_1' ] );?>
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-right">
                        <?php echo xss_clean( @$Options[ store_prefix() . 'receipt_col_2' ] );?>
                    </div>
                </div>
            </div>
            <?php if( @$_GET[ 'is-pdf' ] ):?>
            <br>
            <br>
            <?php endif;?>
            <?php
            $string_to_parse	=	ob_get_clean();
            echo $this->parser->parse_string( $string_to_parse, $template , true );
            ?>
            <div class="row">
                <div class="text-center">
                    <h4><?php _e('Reçu de vente', 'nexo');?>
                            <?php
                            echo $this->Nexo_Misc->cmoney_format(
                                number_format(
                                    (
                                        (
                                            floatval( $order[ 'order' ][0][ 'TOTAL' ])
                                        )
                                    ), 2
                                )
                            );
                            ;?>
                    </h4>
                </div>
            </div>
            <div class="row line">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 timeline1">
                    <div class="col-xs-6 text-left"><?php echo date('F j, Y'); ?></div>
                    <div class="col-xs-6 text-right"><?php echo date('g:i a'); ?></div>
                </div>
            </div>
            <?php
            if ($order[ 'order' ][0]['RESTAURANT_BOOKED_FOR'] != null && $order[ 'order' ][0]['RESTAURANT_BOOKED_FOR'] != '0000-00-00 00:00:00'):
            ?>
            <div class="row line">

                <div class="timeline2 text-center">
                    <?php
                        if ($order['order'][0]['RESTAURANT_ORDER_TYPE'] == 'takeaway') {
                            echo "Takeaway: " . date('g:i:a', strtotime($order['order'][0]['RESTAURANT_BOOKED_FOR']));
                        } elseif ($order['order'][0]['RESTAURANT_ORDER_TYPE'] == 'delivery') {
                            echo "Delivery: " . date('g:i:a', strtotime($order['order'][0]['RESTAURANT_BOOKED_FOR']));
                        }
                    ?>
                </div>
            </div>
            <?php endif; ?>
            <div class="row line">
                    	<?php
                        $total_global    =    0;
                        $total_unitaire    =    0;
                        $total_quantite    =    0;
						$total_discount		=	0;
                        $products       =   $this->events->apply_filters( 'receipt_items', $order[ 'products' ] );

                        if( $products ) {
                            foreach ( $products as $_produit) {

                                // $total_global        +=    __floatval($_produit[ 'PRIX_TOTAL' ]);
                                $total_unitaire      	+=    __floatval($_produit[ 'PRIX_BRUT' ]);
                                $total_quantite   	 	+=    __floatval($_produit[ 'QUANTITE' ]);
                                $total_global        	+=    ( __floatval($_produit[ 'PRIX_TOTAL' ]) );

                                if (!empty($_produit['metas']['customer'])) {
                                    echo '<div class="divider modifiers">To: ' . $_produit['metas']['customer']. '</div>';
                                } else {
                                    echo '<div class="divider"></div>';
                                }
                                ?>

                                <div class="text-bold">
                                        <?php echo $_produit[ 'QUANTITE' ]; ?> &times;
                                        <?php if ( store_option( 'item_name' ) == 'use_both' ):?>
                                            <?php echo empty( $_produit[ 'DESIGN' ] ) ? $_produit[ 'NAME' ] : $_produit[ 'DESIGN' ];?><br>
                                            <?php echo $_produit[ 'ALTERNATIVE_NAME' ];?>
                                        <?php elseif ( store_option( 'item_name' ) == 'only_secondary' ):?>
                                            <?php echo $_produit[ 'ALTERNATIVE_NAME' ];?>
                                        <?php else: // use_primary?>
                                            <?php echo empty( $_produit[ 'DESIGN' ] ) ? $_produit[ 'NAME' ] : $_produit[ 'DESIGN' ];?>
                                        <?php endif;?>
                                </div>
                                <?php
                                if (!empty($_produit['metas']['modifiers'])) {
                                    foreach ($_produit['metas']['modifiers'] as $modifier) {
                                        echo '<div class="modifiers"> + ' . $modifier->name . '</div>';
                                    }
                                }
                                if (!empty($_produit['metas']['restaurant_note'])) {
                                    echo '<div class="modifiers">Notes: ' . $_produit['metas']['restaurant_note'] . '</div>';
                                }
                                echo '<div class="divider line"></div>';
                            }

                            if (!empty($order[ 'order' ][0]['metas']['customer_note'])) {
                                echo '<div class="notes">Order notes: ' . $order[ 'order' ][0]['metas']['customer_note'] . '</div>';

                            }
                        }
                        ?>
                <div class="divider"></div>
                <?php include_once( dirname( __FILE__ ) . '/barcode.php' );?>
                <div class="divider"></div>
                <p class="text-center"><?php echo xss_clean( $order[ 'order' ][0][ 'DESCRIPTION' ] );?></p>
				<p class="text-center"><?php echo xss_clean( $this->parser->parse_string( @$Options[ store_prefix() . 'nexo_bills_notices' ], $template , true ) );?></p>
                <?php if( @$_GET[ 'is-pdf' ] == null ):?>
                <div class="container-fluid hideOnPrint">
                    <div class="row hideOnPrint">
                        <div class="col-lg-12">
                        <a href="<?php echo dashboard_url([ 'orders' ]);?>" class="btn btn-success btn-lg btn-block"><?php _e('Revenir à la liste des commandes', 'nexo');?></a>
                        </div>
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
<?php else:?>
<div class="container-fluid"><?php echo tendoo_error(__('Une erreur s\'est produite durant l\'affichage de ce reçu. La commande concernée semble ne pas être valide ou ne dispose d\'aucun produit.', 'nexo'));?></div>
<div class="container-fluid hideOnPrint">
    <div class="row hideOnPrint">
        <div class="col-lg-12">
        <a href="<?php echo dashboard_url([ 'orders' ]);?>" class="btn btn-success btn-lg btn-block"><?php _e('Revenir à la liste des commandes', 'nexo');?></a>
        </div>
    </div>
</div>
<?php endif;?>
<style>
@media print
{
        @page
    {
        size: auto;   /* auto is the initial value */

        /* this affects the margin in the printer settings */
        margin: 0mm 1mm 0mm 1mm;
    }
    html, body {
        background: #FFF;
        overflow:visible;
    }
    .shop-details {
        font-size: 10pt;
    }
}
    body
    {
        /* this affects the margin on the content before sending to printer */
        margin: 0px;
    }
    .shop-details {
        font-size: 6vw;
    }
    .hideOnPrint {
		display:none !important;
	}
	.order-details {
		font-size: 9vw;
	}
    p {
		font-size: 5vw;
	}
	.order-details h2 {
		font-size: 7vw;
	}
	h3 {
        font-size: 6vw;
        font-weight: bold;
	}
	h4 {
        font-size: 9vw;
        font-weight: bold;
	}
    .timeline1 {
        font-size: 4vw;
    }
    .timeline2, .text-bold, .modifiers {
        font-size: 6vw;
    }
    .text-bold {
        font-weight: bold;
    }
    .modifiers {
        padding-left: 5vw;
    }
    .divider {
        padding: 4vw;
    }
    .line {
        padding: 2vw 0;
        border-top: 1px solid #000;
    }
    .notes {
        font-size: 4vw;
    }
</style>
<?php include( dirname( __FILE__ ) . '/receipt-footer.php' );?>
<?php if( @$_GET[ 'ignore_header' ] != 'true' ):?>
</body>
</html>
<?php endif;?>

<?php
if (! $cache->get($order[ 'order' ][0][ 'ID' ]) || @$_GET[ 'refresh' ] == 'true') {
    $cache->save($order[ 'order' ][0][ 'ID' ], ob_get_contents(), 999999999); // long time
}
