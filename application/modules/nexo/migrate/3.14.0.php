<?php
/**
 * Introduce multiple taxes
 * per orders
 * @since 3.14.0
 */
$this->load->model( 'Nexo_Stores' );

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';

    $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_taxes` (
        `ID` int(11) NOT NULL AUTO_INCREMENT,
        `NAME` varchar(200) NOT NULL, 
        `TYPE` varchar(200) NOT NULL,
        `VALUE` float(11) NOT NULL,
        `DATE_CREATION` datetime NOT NULL,
        `AUTHOR` int(11) NOT NULL
        `REF_ORDER` int(11) NOT NULL,
        PRIMARY KEY (`ID`)
    )');

    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_commandes' );
    if( ! in_array( 'TOTAL_TAXES', $columns ) ) {
        $this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` 
        ADD `TOTAL_TAXES` float(11) NOT NULL AFTER `REF_SHIPPING_ADDRESS`' );
    }
    
    if ( in_array( 'REF_TAX', $column ) ) {
        $this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` 
        DROP COLUMN `REF_TAX`' );
    }

    $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_status` (
        `id` int(11) NOT NULL,
        `code` varchar(200) NOT NULL, 
        `woocommerce_id` int(11) NOT NULL,
        `status` varchar(10) NOT NULL, 
        `tm` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        KEY `tm` (`tm`)
    )');

}
$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'sync_orders` (
        `tm` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `latest` int(11) NOT NULL,
        `client_id` int(11) NOT NULL,
        KEY `tm` (`tm`)
    )');

$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_clients_meta` ADD INDEX (`REF_CLIENT`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_clients_address` ADD INDEX (`ref_client`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_shippings` ADD INDEX (`ref_shipping`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_shippings` ADD INDEX (`ref_order`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_produits` ADD INDEX (`REF_COMMAND_CODE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_produits_meta` ADD INDEX (`REF_COMMAND_PRODUCT`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_produits_meta` ADD INDEX (`REF_COMMAND_CODE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_paiements` ADD INDEX (`REF_COMMAND_CODE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_meta` ADD INDEX (`REF_ORDER_ID`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_coupons` ADD INDEX (`REF_COMMAND`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_coupons` ADD INDEX (`REF_COUPON`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_refunds` ADD INDEX (`REF_ORDER`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_refunds_products` ADD INDEX (`REF_ITEM`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_refunds_products` ADD INDEX (`REF_REFUND`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_articles` ADD INDEX (`REF_PROVIDER`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_articles_meta` ADD INDEX (`REF_ARTICLE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_articles_variations` ADD INDEX (`REF_ARTICLE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_articles_stock_flow` ADD INDEX (`REF_COMMAND_CODE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_articles_stock_flow` ADD INDEX (`REF_SHIPPING`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_notices` ADD INDEX (`REF_USER`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_shippings` CHANGE `enterprise` `enterprise` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . 'nexo_stores_activities` ADD INDEX (`REF_STORE`);' );

$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` ADD INDEX (`AUTHOR`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` ADD INDEX (`REF_CLIENT`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` ADD INDEX (`RESTAURANT_ORDER_STATUS`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` ADD INDEX (`TYPE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes` ADD INDEX (`DATE_CREATION`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_meta` ADD INDEX (`KEY`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_articles` ADD INDEX (`REF_CATEGORIE`);' );
$this->db->query( 'ALTER TABLE `' . $this->db->dbprefix . $store_prefix . 'nexo_commandes_produits` ADD INDEX (`RESTAURANT_PRODUCT_REAL_BARCODE`);' );


// Sync Reports tables

$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'reports_clients` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
        PRIMARY KEY (`id`)
)');
$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'rompos_reports_client_dates` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `client_id` int(11) NOT NULL,
         `date` date NOT NULL,
         PRIMARY KEY (`id`),
         KEY `client_date_id_fk` (`client_id`),
         CONSTRAINT `client_date_id_fk` FOREIGN KEY (`client_id`) REFERENCES `rompos_reports_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)');
$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'reports_customers` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
         `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
         `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
         `created` datetime NOT NULL,
         PRIMARY KEY (`id`),
         UNIQUE KEY `email` (`email`)
)');
$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'reports_orders` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
         `customer_id` int(11) NOT NULL,
         `type` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT \'takeaway, delivery, drive-in, etc\',
         `subtype` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
         `total` decimal(15,3) NOT NULL,
         `payment_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT \'cod,cash,creditcard,etc\',
         `client_id` int(11) NOT NULL,
         `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT \'pending,completed,etc\',
         `created` datetime NOT NULL,
         PRIMARY KEY (`id`),
         KEY `customer_id_fk` (`customer_id`),
         KEY `client_id_fk` (`client_id`),
         CONSTRAINT `client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `rompos_reports_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
         CONSTRAINT `customer_id_fk` FOREIGN KEY (`customer_id`) REFERENCES `rompos_reports_customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)');
$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'reports_order_payments` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `order_id` int(11) NOT NULL,
         `amount` decimal(15,3) NOT NULL,
         `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT \'cod,cash,creditcard,etc\',
         `operation` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT \'incoming,outcoming\',
         `created` datetime NOT NULL,
         PRIMARY KEY (`id`),
         KEY `order_payment_id_fk` (`order_id`),
         CONSTRAINT `order_payment_id_fk` FOREIGN KEY (`order_id`) REFERENCES `rompos_reports_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)');
$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'reports_order_products` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
         `codebar` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
         `order_id` int(11) NOT NULL,
         `quantity` int(11) NOT NULL,
         `price` decimal(15,3) NOT NULL,
         `sale_price` decimal(15,3) NOT NULL,
         `total` decimal(15,3) NOT NULL,
         PRIMARY KEY (`id`),
         KEY `order_id_fk` (`order_id`),
         CONSTRAINT `order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `rompos_reports_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)');
$this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $this->db->dbprefix . 'reports_product_modifiers` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `product_id` int(11) NOT NULL,
         `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
         `price` decimal(15,3) NOT NULL,
         PRIMARY KEY (`id`),
         KEY `product_id_fk` (`product_id`),
         CONSTRAINT `product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `rompos_reports_order_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)');
