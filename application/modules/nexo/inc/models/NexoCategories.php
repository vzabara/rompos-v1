<?php
class NexoCategories extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        return $this->db
            ->get( store_prefix() . 'nexo_categories' )
            ->result_array();
    }
}