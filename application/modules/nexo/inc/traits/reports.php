<?php

use Carbon\Carbon;

trait Nexo_reports
{
    public function reports_export_post()
    {
        $start_date = $this->post('start');
        $end_date   = $this->post('end');
        $client_id  = get_option(store_prefix() . 'orders_client_id');
        if (Carbon::parse($start_date)->getTimestamp() > Carbon::parse($end_date)->getTimestamp()) {
            $tmp        = $start_date;
            $start_date = $end_date;
            $end_date   = $tmp;
        }
        $date_before = date('Y-m-d', strtotime($end_date . ' +1 day'));
        $date_after  = date('Y-m-d', strtotime($start_date . ' -1 day'));

        $prefix = $this->db->dbprefix . store_prefix();
        $rows   = $this->db->query("SELECT * FROM {$prefix}nexo_commandes WHERE `DATE_CREATION` BETWEEN ? AND ?", [
            $date_after,
            $date_before,
        ])->result_array();

        $orders = [];
        foreach ($rows as $row) {
            $order = [
                'code'         => $row['CODE'],
                'type'         => $row['RESTAURANT_ORDER_TYPE'],
                'subtype'      => '',
                'total'        => $row['SOMME_PERCU'],
                'payment_type' => $row['PAYMENT_TYPE'],
                'client_id'    => $client_id,
                'status'       => $row['STATUS'],
                'created'      => $row['DATE_CREATION'],
            ];

            // find group order info
            $res = $this->db->query("SELECT `VALUE` FROM {$prefix}nexo_commandes_meta WHERE `REF_ORDER_ID`=? AND `KEY`=?", [
                $row['ID'],
                'group_order',
            ])->result_array();
            if ( ! empty($res)) {
                $order['subtype'] = 'group';
            }

            // collect customer info
            $customer_data = $this->db->query("SELECT * FROM {$prefix}nexo_clients WHERE `ID`=?", [
                $row['REF_CLIENT'],
            ])->result_array();
            $customer      = [];
            if ( ! empty($customer_data)) {
                $customer = [
                    'name'    => trim($customer_data[0]['NOM'] . ' ' . $customer_data[0]['PRENOM']),
                    'email'   => $customer_data[0]['EMAIL'],
                    'phone'   => $customer_data[0]['TEL'],
                    'created' => $customer_data[0]['DATE_CREATION'],
                ];
            }
            $order['customer'] = $customer;

            // collect products
            $rows2    = $this->db->query("SELECT * FROM {$prefix}nexo_commandes_produits WHERE `REF_COMMAND_CODE`=?", [
                $row['CODE'],
            ])->result_array();
            $products = [];
            foreach ($rows2 as $product) {
                $prod = [
                    'title'      => $product['NAME'],
                    'codebar'    => $product['REF_PRODUCT_CODEBAR'],
                    'quantity'   => $product['QUANTITE'],
                    'price'      => $product['PRIX'],
                    'sale_price' => $product['PRIX_BRUT'],
                    'total'      => $product['PRIX_TOTAL'],
                ];

                // collect products meta
                $res  = $this->db->query("SELECT `VALUE` FROM {$prefix}nexo_commandes_produits_meta WHERE `REF_COMMAND_PRODUCT`=? AND `KEY`=?",
                    [
                        $product['ID'],
                        'modifiers',
                    ])->result_array();
                $meta = [];
                if ( ! empty($res)) {
                    $productMeta = $res[0]['VALUE'];
                    if ( ! empty($productMeta)) {
                        $modifiers = json_decode($productMeta);
                        if ( ! empty($modifiers)) {
                            foreach ($modifiers as $modifier) {
                                $meta[] = [
                                    'name'  => $modifier->name,
                                    'price' => $modifier->price,
                                ];
                            }
                        }
                    }
                }
                $prod['modifiers'] = $meta;
                $products[]        = $prod;
            }
            $order['products'] = $products;


            // collect payments
            $rows4 = $this->db->query("SELECT * FROM {$prefix}nexo_commandes_paiements WHERE `REF_COMMAND_CODE`=?", [
                $row['CODE'],
            ])->result_array();;
            $payments = [];
            foreach ($rows4 as $payment) {
                $payments[] = [
                    'amount'    => $payment['MONTANT'],
                    'type'      => $payment['PAYMENT_TYPE'],
                    'operation' => $payment['OPERATION'],
                    'created'   => $payment['DATE_CREATION'],
                ];
            }
            $order['payments'] = $payments;

            $orders[] = $order;
        }

        $this->response([
            'success'  => true,
            'data'     => serialize($orders),
            'filename' => 'client-' . $client_id . '-report-' . str_replace('-', '_', $start_date) . '-' . str_replace('-', '_', $end_date) . '.data',
            'type'     => 'application/octet-stream',
        ]);
    }

    public function reports_import_post()
    {
        $saved = [];
        $orders = unserialize($this->post('data'));
        if ( ! empty($orders)) {
            foreach ($orders as $order) {
                $customer = $order['customer'];
                $products = $order['products'];
                $payments = $order['payments'];
                unset($order['customer'], $order['products'], $order['payments']);
                // search customer
                $customer_id = 0;
                $users = $this->db->where('email', $customer['email'] ?? '')->get('reports_customers')->result_array();
                if (empty($users)) {
                    // insert new user
                    if ($this->db->insert('reports_customers', $customer)) {
                        $customer_id = $this->db->insert_id();
                    } else {
                        // TODO: report error?
                    }
                } else {
                    $customer_id = $users[0]['id'];
                }
                $order['customer_id'] = $customer_id;
                // insert new order
                if ($this->db->insert('reports_orders', $order)) {
                    $order_id = $this->db->insert_id();

                    // save payments
                    foreach ($payments as $payment) {
                        $payment['order_id'] = $order_id;
                        $this->db->insert('reports_order_payments', $payment);
                    }

                    // save products
                    foreach ($products as $product) {
                        $product['order_id'] = $order_id;
                        $modifiers = $product['modifiers'];
                        unset($product['modifiers']);
                        if ($this->db->insert('reports_order_products', $product)) {
                            $product_id = $this->db->insert_id();

                            // save product meta
                            foreach ($modifiers as $modifier) {
                                $modifier['product_id'] = $product_id;
                                $this->db->insert('reports_product_modifiers', $modifier);
                            }
                        } else {
                            // TODO: report error?
                        }
                    }

                    // save date
                    $saved[Carbon::parse($order['created'])->format('Y-m-d')] = $order['client_id'];
                }
            }

            $to_save = [];
            foreach ($saved as $date => $client_id) {
                $to_save[] = [
                    'client_id' => $client_id,
                    'date' => $date,
                ] ;
            }
            $this->db->insert_batch('reports_client_dates', $to_save);

            $this->response([
                'success'  => true,
            ]);
        }
        $this->response([
            'success'  => false,
        ]);
    }

    public function reports_clear_post()
    {
        $res1 = $this->db->empty_table('reports_product_modifiers');
        $res2 = $this->db->empty_table('reports_order_products');
        $res3 = $this->db->empty_table('reports_order_payments');
        $res4 = $this->db->empty_table('reports_orders');
        $res5 = $this->db->empty_table('reports_customers');
        $res6 = $this->db->empty_table('reports_client_dates');

        if ($res1 && $res2 && $res3 && $res4 && $res5 && $res6) {
            $this->response([
                'success'  => true,
            ]);
        } else {
            $this->response([
                'success'  => false,
            ]);
        }
    }
}
