<?php

//$Routes->post( 'nexopos/reports/daily-sync', 'ApiNexoReports@daily_sync' );
$Routes->post( 'nexopos/reports/monthly-sales', 'ApiNexoReports@monthly_sales' );
$Routes->get( 'nexopos/full-order/{order_id}', 'ApiNexoOrders@full_order' );
$Routes->get( 'nexopos/orders', 'ApiNexoOrders@orders' );
$Routes->post( 'nexopos/order-status/{order_id}', 'ApiNexoOrders@setOrderStatus' );
$Routes->post( 'nexopos/physicals-and-digitals/', 'ApiNexoItems@physicals_and_digitals' );
$Routes->post( 'nexopos/post-grouped', 'ApiNexoItems@post_grouped' );
$Routes->post( 'nexopos/put-grouped/{id}', 'ApiNexoItems@put_grouped' );
$Routes->post( 'nexopos/reporst-by-email', 'ApiNexoReports@sendByEmail' );
$Routes->post( 'nexopos/supplies', 'ApiNexoItems@createSupply' );
$Routes->get( 'nexopos/system-details', 'ApiNexoSystem@details' );
// $Routes->get( 'nexopos/orders', 'ApiNexoSystem@orders' );
$Routes->get( 'nexopos/registers', 'ApiNexoRegisters@getAll' );
$Routes->get( 'nexopos/registers/idle/{id}', 'ApiNexoRegisters@idleRegister' );
$Routes->get( 'nexopos/registers/active/{id}', 'ApiNexoRegisters@activeRegister' );
$Routes->get( 'nexopos/customers', 'ApiNexoSystem@customers' );
$Routes->get( 'nexopos/products', 'ApiNexoSystem@products' );
$Routes->get( 'nexopos/categories', 'ApiNexoSystem@categories' );
$Routes->get( 'nexopos/options', 'ApiNexoSystem@options' );
$Routes->post( 'nexopos/options', 'ApiNexoSystem@setOptions' );
$Routes->post( 'nexopos/cashiers/week-sales/{cashier_id}', 'ApiNexoReports@cashierWeekReport' );
$Routes->post( 'nexopos/cashiers/card/{cashier_id}', 'ApiNexoReports@cashierCard' );
$Routes->post( 'nexopos/cashiers/register-history/{cashier_id}', 'ApiNexoReports@cashierRegisterHistory' );
$Routes->post( 'nexopos/orders/payment/{order_id}', 'ApiNexoOrders@payment' );
$Routes->get( 'nexopos/orders/payments/{order_id}', 'ApiNexoOrders@payments' );
$Routes->post( 'nexopos/orders/refund/{order_id}', 'ApiNexoOrders@refund' );
$Routes->get( 'nexopos/orders/refund-history/{order_id}', 'ApiNexoOrders@refundHistory' );

/**
 * Sync Down
 */
$Routes->post( 'nexopos/woo_product', 'ApiNexoSystem@syncDownSingleProduct' );
$Routes->delete( 'nexopos/woo_product', 'ApiNexoSystem@syncDownDeleteSingleProduct' );
$Routes->post( 'nexopos/woo_products', 'ApiNexoSystem@syncDownProducts' );
$Routes->post( 'nexopos/woo_category', 'ApiNexoSystem@syncDownSingleCategory' );
$Routes->post( 'nexopos/woo_categories', 'ApiWooCommerce@syncDownCategories' );
$Routes->post( 'nexopos/woo_order', 'ApiWooCommerce@syncDownSingleOrder' );
$Routes->post( 'nexopos/woo_customers', 'ApiWooCommerce@syncDownCustomers' );

/**
 * Sync Up
 */
$Routes->post( 'nexopos/order', 'ApiNexoSystem@syncUpOrder' ); // new rompos method
$Routes->post( 'nexopos/orders', 'ApiNexoSystem@syncUpOrders' ); // new rompos method
$Routes->post( 'nexopos/product', 'ApiNexoSystem@syncUpSingleProduct' );
$Routes->post( 'nexopos/category', 'ApiNexoSystem@syncUpSingleCategory' );

$Routes->post( 'nexopos/import/customers', 'ApiNexoCustomers@importCSV' );
$Routes->get( 'nexopos/history/{page?}', 'ApiNexoSystem@history' );
$Routes->post( 'nexopos/delete_history', 'ApiNexoSystem@deleteSelectedHistory' );

$Routes->get( 'nexopos/local-print/{order_id}', function($order_id) {
    return (new NexoPrintController())->printResult($order_id);
});
