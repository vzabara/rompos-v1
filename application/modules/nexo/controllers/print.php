<?php
use Dompdf\Dompdf;

class NexoPrintController extends CI_Model
{
    public function defaults()
    {
        show_404();
    }

    public function order_receipt($order_id = null)
    {
        if ($order_id != null) {
            $this->cache        =    new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'nexo_order_' . store_prefix() ));

            if ($order_cache = $this->cache->get($order_id) && @$_GET[ 'refresh' ] != 'true') {
                echo $this->cache->get($order_id);
                return;
            }

			$this->load->library('parser');
            $this->load->model('Nexo_Checkout');

            global $Options;

            $data                		=   array();
            $data[ 'order' ]    		=   $this->Nexo_Checkout->get_order_products($order_id, true);
            $data[ 'cache' ]    		=   $this->cache;
            $data[ 'shipping' ]         =   $this->db->where( 'ref_order', $order_id )->get( store_prefix() . 'nexo_commandes_shippings' )->result_array();
            $data[ 'tax' ]              =   $this->Nexo_Misc->get_taxes( $data[ 'order' ][ 'order' ][0][ 'REF_TAX' ] );
            $allowed_order_for_print	=	$this->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );


            // Allow only cash order to be printed
            // if ( ! in_array( $data[ 'order' ]['order'][0][ 'TYPE' ], $allowed_order_for_print ) ) {
            //     redirect(array( 'dashboard', 'nexo', 'orders', '?notice=print_disabled' ));
            // }

            if (count($data[ 'order' ]) == 0) {
                return show_error(sprintf(__('Impossible d\'afficher le ticket de caisse. Cette commande ne possède aucun article &mdash; <a href="%s">Retour en arrière</a>', 'nexo'), $_SERVER['HTTP_REFERER']));
            }

            $freshOrder = $data[ 'order' ][ 'order' ][0];
			// @since 2.7.9
			$data[ 'template' ]						=	array();
            $data[ 'template' ][ 'order_date' ]		=	mdate( '%d/%m/%Y %g:%i %a', strtotime($freshOrder[ 'DATE_CREATION' ]));
            $data[ 'template' ][ 'order_updated' ]  =	mdate( '%d/%m/%Y %g:%i %a', strtotime($freshOrder[ 'DATE_MOD' ]));
			$data[ 'template' ][ 'order_code' ]		=	$freshOrder[ 'CODE' ];
            $data[ 'template' ][ 'order_id' ]       =   $freshOrder[ 'ORDER_ID' ];
			$data[ 'template' ][ 'order_status' ]	=	$this->Nexo_Checkout->get_order_type($freshOrder[ 'TYPE' ]);
            $data[ 'template' ][ 'order_note' ]     =   $freshOrder[ 'DESCRIPTION' ];

			$data[ 'template' ][ 'order_cashier' ]	=	User::pseudo( $freshOrder[ 'AUTHOR' ] );
			$data[ 'template' ][ 'shop_name' ]		=	@$Options[ store_prefix() . 'site_name' ];
			$data[ 'template' ][ 'shop_pobox' ]		=	@$Options[ store_prefix() . 'nexo_shop_pobox' ];
			$data[ 'template' ][ 'shop_fax' ]		=	@$Options[ store_prefix() . 'nexo_shop_fax' ];
			$data[ 'template' ][ 'shop_email' ]     =	@$Options[ store_prefix() . 'nexo_shop_email' ];
			$data[ 'template' ][ 'shop_street' ]    =	@$Options[ store_prefix() . 'nexo_shop_street' ];
			$data[ 'template' ][ 'shop_phone' ]     =	@$Options[ store_prefix() . 'nexo_shop_phone' ];
            $data[ 'template' ][ 'customer_name' ]  =   html_entity_decode($freshOrder[ 'CUSTOMER_NAME' ]);
            $data[ 'template' ][ 'customer_phone' ]  =   $freshOrder[ 'customer_phone' ];

            $data[ 'template' ][ 'delivery_address_1' ]     =   html_entity_decode(@$data[ 'shipping' ][0][ 'address_1' ]);
            $data[ 'template' ][ 'delivery_address_2' ]     =   html_entity_decode(@$data[ 'shipping' ][0][ 'address_2' ]);
            $data[ 'template' ][ 'city' ]               =   @$data[ 'shipping' ][0][ 'city' ];
            $data[ 'template' ][ 'country' ]            =   @$data[ 'shipping' ][0][ 'country' ];
            $data[ 'template' ][ 'name' ]               =   html_entity_decode(@$data[ 'shipping' ][0][ 'name' ]);
            $data[ 'template' ][ 'surname' ]            =   html_entity_decode(@$data[ 'shipping' ][0][ 'surname' ]);
            $data[ 'template' ][ 'state' ]              =   @$data[ 'shipping' ][0][ 'surname' ];
            $data[ 'template' ][ 'delivery_cost' ]      =   @$data[ 'shipping' ][0][ 'price' ];

            // rompos variables
            $data[ 'request' ] = [];
            if (@$_GET[ 'ignore_header' ]) {
                $data[ 'request' ][ 'ignore_header' ]	=	true;
            }
            if (@$_GET[ 'autoprint' ]) {
                $data[ 'request' ][ 'autoprint' ]		=	 true;
            }
            if (@$_GET[ 'is-pdf' ]) {
                $data[ 'request' ][ 'is_pdf' ]		    =	true;
            }
            $data[ 'template' ][ 'page_title' ]		    =	sprintf(__('Order ID : %s &mdash; Nexo Shop Receipt', 'nexo'), $freshOrder[ 'CODE' ]);
            $data[ 'template' ][ 'css_url' ]		=	css_url('nexo');
            $data[ 'template' ][ 'module_url' ]		=	module_url( 'nexo' );
//            $data[ 'template' ][ 'title' ]		    =	__('Reçu de vente', 'nexo') . ' ' . $this->Nexo_Misc->cmoney_format(
//                    number_format(floatval( $freshOrder[ 'TOTAL' ]), 2)
//                );
            $data[ 'template' ][ 'title' ]		    = 'Order #'. (!empty($freshOrder['metas']['woocommerce_order_id']) ? $freshOrder['metas']['woocommerce_order_id'] : $freshOrder['CODE']);
            $data[ 'template' ][ 'date' ]		    = date( 'F j, Y', strtotime($freshOrder[ 'DATE_CREATION' ]));
            $data[ 'template' ][ 'time' ]		    = date( 'g:i a', strtotime($freshOrder[ 'DATE_CREATION' ]));
            $data[ 'template' ][ 'print_header']    = store_option( 'nexo_receipt_header' );
            $data[ 'template' ][ 'site_name']       = @$Options[ store_prefix() . 'site_name' ];
            $data[ 'template' ][ 'logo' ]    = [];
            $data[ 'template' ][ 'logo' ]['url']     = store_option( 'url_to_logo' );
            $data[ 'template' ][ 'logo' ]['width']   = store_option( 'logo_width' ) ? 'width:'.store_option( 'logo_width' ).'px' : '';
            $data[ 'template' ][ 'logo' ]['height']  = store_option( 'logo_height' ) ? 'height:'.store_option( 'logo_height' ).'px' : '';

            $data[ 'template' ][ 'headers' ] = [];
            foreach( buildingLines(
                $this->parser->parse_string( store_option( 'receipt_col_1' ), $data[ 'template' ] , true ),
                $this->parser->parse_string( store_option( 'receipt_col_2' ), $data[ 'template' ] , true )
            ) as $line ) {
                $data[ 'template' ]['headers'][] = nexting( $line );
            }
            if ($freshOrder['RESTAURANT_BOOKED_FOR'] != null && $freshOrder['RESTAURANT_BOOKED_FOR'] != '0000-00-00 00:00:00') {
                if (date('Y-m-d', strtotime($freshOrder['RESTAURANT_BOOKED_FOR'])) != date('Y-m-d')) {
                    $data[ 'template' ]['delivery_date'] = __('Booked For: ', 'nexo') . date('M j, Y', strtotime($freshOrder['RESTAURANT_BOOKED_FOR']));
                }
                if ($data[ 'order' ]['order'][0]['RESTAURANT_ORDER_TYPE'] == 'delivery') {
                    $data[ 'template' ]['delivery_title'] = 'Delivery Time: ' . date('g:i a', strtotime($data[ 'order' ]['order'][0]['RESTAURANT_BOOKED_FOR']));
                } elseif (!empty($data[ 'order' ]['order'][0]['metas']['woocommerce_order_id'])) {
                    $data[ 'template' ]['delivery_title'] =  'Pick Up Time: ' . date('g:i a', strtotime($data[ 'order' ]['order'][0]['RESTAURANT_BOOKED_FOR']));
                } elseif ($data[ 'order' ]['order'][0]['RESTAURANT_ORDER_TYPE'] == 'takeaway') {
                    $data[ 'template' ]['delivery_title'] =  'Take Away Time: ' . date('g:i a', strtotime($data[ 'order' ]['order'][0]['RESTAURANT_BOOKED_FOR']));
                }
            }
            $data[ 'template' ][ 'error' ] = empty($order[ 'order' ][0][ 'CODE' ]) ? tendoo_error(__('Une erreur s\'est produite durant l\'affichage de ce reçu. La commande concernée semble ne pas être valide ou ne dispose d\'aucun produit.', 'nexo')) : '';
            $data[ 'template' ][ 'dashboard_url_orders' ] = dashboard_url([ 'orders' ]);
            $data[ 'template' ][ 'dashboard_url_orders_title' ] = __('Revenir à la liste des commandes', 'nexo');
            $data[ 'template' ][ 'nexo_bills_notices' ] = xss_clean( $this->parser->parse_string( @$Options[ store_prefix() . 'nexo_bills_notices' ], $data[ 'template' ] , true ) );
            $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
            $data[ 'template' ][ 'barcode' ] = base64_encode($generator->getBarcode( $freshOrder[ 'CODE' ], $generator::TYPE_CODE_128));

            $data[ 'template' ][ 'sub_total_title' ] = __( 'Sous-Total', 'nexo' );
            $data[ 'template' ][ 'tax_title' ] = __( 'TAX', 'nexo' );
            $data[ 'template' ][ 'delivery_cost_title' ] = __( 'Livraison', 'nexo' );
            $data[ 'template' ][ 'grand_total_title' ] = __( 'Total:', 'nexo' );
            $data[ 'template' ][ 'collected_title' ] = __( 'Collected:', 'nexo' );
            $data[ 'template' ][ 'due_title' ] = __floatval( $freshOrder[ 'SOMME_PERCU' ] ) >= floatval( $freshOrder[ 'TOTAL' ] ) ? __('à rendre :', 'nexo') : __('&Agrave; percevoir :', 'nexo');
            $subTotal = 0;
            $taxTotal = 0;
            foreach ($data[ 'order' ][ 'products' ] as &$product) {
                $product['total_price'] = $this->Nexo_Misc->cmoney_format(
                    number_format(floatval( $product['PRIX'] * $product['QUANTITE']), 2)
                );
                $subTotal += $product['PRIX'] * $product['QUANTITE'];
                $taxTotal += $product['TOTAL_TAXES'];
                if (!empty($product['metas']['restaurant_note'])) {
                    $product['metas']['restaurant_note'] = html_entity_decode($product['metas']['restaurant_note']);
                }
            }
            $data[ 'template' ][ 'sub_total' ] = $this->Nexo_Misc->cmoney_format(
                number_format(floatval($subTotal), 2)
            );
            $data[ 'template' ][ 'tax_total' ] = $this->Nexo_Misc->cmoney_format(
                number_format(floatval($taxTotal), 2)
            );

//            $order_payments         =   $this->Nexo_Misc->order_payments( $order[ 'order' ][0][ 'CODE' ] );
//            $payment_types          =   $this->events->apply_filters( 'nexo_payments_types', $this->config->item( 'nexo_payments_types' ) );
//
//            foreach( $order_payments as $payment ) {
//                if ( $payment[ 'OPERATION' ] === 'incoming' ) {
//                    $build['payments'][] = nexting([
//                        @$payment_types[ $payment[ 'PAYMENT_TYPE' ] ] == null ? __( 'Type de paiement inconnu', 'nexo' ) : @$payment_types[ $payment[ 'PAYMENT_TYPE' ] ],
//                        $this->Nexo_Misc->cmoney_format( __floatval( $payment[ 'MONTANT' ] ) )
//                    ]);
//                } else {
//                    $build['payments'][] = nexting([
//                        @$payment_types[ $payment[ 'PAYMENT_TYPE' ] ] == null ? __( 'Type de paiement inconnu', 'nexo' ) : @$payment_types[ $payment[ 'PAYMENT_TYPE' ] ],
//                        $this->Nexo_Misc->cmoney_format( - __floatval( $payment[ 'MONTANT' ] ) )
//                    ]);
//                }
//            }
//
            $shipping_total = $data[ 'template' ][ 'delivery_cost' ];
            if ( $shipping_total > 0 ) {
                $data[ 'template' ]['delivery'] =
                    $this->Nexo_Misc->cmoney_format(
                        number_format(
                            (floatval($shipping_total)),
                            2
                        )
                    );
            }

            // process custom fields
            $fees_total = 0;
            $fees = [];
            foreach ( $freshOrder['metas'] as $key => $value ) {
                if ( stripos( $key, '_wccf_cf_value_' ) === 0 ) {
                    $slug = substr( $key, strlen('_wccf_cf_value_' ) );
                    if ( empty( $fees[$slug] ) ) {
                        $fees[$slug] = [];
                    }
                    $fees[$slug]['value'] = $value;
                } else if ( stripos( $key, '_wccf_cf_label_' ) === 0 ) {
                    $slug = substr( $key, strlen('_wccf_cf_value_' ) );
                    if ( empty( $fees[$slug] ) ) {
                        $fees[$slug] = [];
                    }
                    $fees[$slug]['label'] = $value;
                }
            }

            if ( count( $fees ) ) {
                $data[ 'template' ]['fees'] = [];
                foreach ( $fees as $fee ) {
                    if (isset($fee['label']) && isset($fee['value'])) {
                        $fees_total += floatval($fee['value']);
                        $data[ 'template' ]['fees'][] = [
                            'label' => $fee['label'],
                            'value' => $this->Nexo_Misc->cmoney_format(
                                number_format(
                                    (floatval($fee['value'])),
                                    2
                                )
                            )
                        ];
                    }
                }
            }

            $discount_amount = 0;
            if ( floatval( $freshOrder[ 'REMISE' ] ) > 0.0) {
                if ( $freshOrder[ 'REMISE_TYPE' ] == 'percentage' ) {
                    $discount_amount		=	( nexoCartGrossValue( $data['order']['products'] ) * floatval( $freshOrder[ 'REMISE_PERCENT' ] ) ) / 100;
                } else  {
                    $discount_amount		=	floatval( $freshOrder[ 'REMISE' ] );
                }
            }
            if ( $discount_amount > 0.0 ) {
                $data[ 'template' ]['fees'][] = [
                    'label' => __( 'Discount', 'nexo' ),
                    'value' => $this->Nexo_Misc->cmoney_format(
                        number_format(
                            $discount_amount,
                            2
                        )
                    )
                ];
            }

            if ( in_array( $freshOrder[ 'TYPE' ], [ 'nexo_order_partially_refunded', 'nexo_order_refunded' ] ) ) {
                /**
                 * handling refunds
                 */
                $refunds    =   $this->orderModel->order_refunds( $freshOrder[ 'ID' ] );
                $totalRefunds   =   array_sum( array_map( function( $refund ) {
                    return floatval( $refund[ 'TOTAL' ] );
                }, $refunds ) );
                $data[ 'template' ]['refund'] = nexting([
                    __( 'Remboursement', 'nexo' ),
                    $this->Nexo_Misc->cmoney_format(
                        number_format(
                            - $totalRefunds, 2
                        )
                    )
                ]);
            } else {
                $totalRefunds     =   0;
            }

            $data[ 'template' ][ 'grand_total' ] = $this->Nexo_Misc->cmoney_format(
                number_format(floatval($subTotal + $shipping_total + $fees_total + $taxTotal - $discount_amount), 2)
            );

            $data[ 'template' ][ 'collected' ] = nexting([
                $this->Nexo_Misc->cmoney_format(
                    number_format(
                        ( floatval( $freshOrder[ 'SOMME_PERCU' ] ) - $totalRefunds ),
                        2
                    )
                )
            ]);

            $data[ 'template' ]['due'] =
                            $this->Nexo_Misc->cmoney_format(
                                number_format(
                                    abs(
                                        (
                                            floatval( $freshOrder[ 'TOTAL' ]) -
                                            floatval( $freshOrder[ 'SOMME_PERCU' ])
                                        )
                                        + floatval( $totalRefunds )
                                    ), 2
                                )
                            );

            $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template', [
                'request'           =>      $data[ 'request' ],
                'template'          =>      $data[ 'template' ],
                'order'             =>      $freshOrder,
                'items'             =>      $data[ 'order' ][ 'products' ]
            ]);
            if (!empty($filtered['order']['metas']['customer_note'])) {
                $filtered['order']['metas']['customer_note'] = html_entity_decode($filtered['order']['metas']['customer_note']);
            }

            $loader        = new \Twig\Loader\FilesystemLoader(TWIGPATH);
            $twig          = new \Twig\Environment($loader);
            $template = $twig->load('receipt-default.tpl');
            echo $template->render($filtered);

//            $data[ 'template' ]             =   $filtered[ 'template' ];
//            $theme                          =	@$Options[ store_prefix() . 'nexo_receipt_theme' ] ? @$Options[ store_prefix() . 'nexo_receipt_theme' ] : 'default';
//            $path                           =   '../modules/nexo/views/receipts/' . $theme . '.php';
//
//            $this->load->view(
//                $this->events->apply_filters( 'nexo_receipt_theme_path', $path ),
//                $data,
//                $theme
//            );

        } else {
            die(__('Cette commande est introuvable.', 'nexo'));
        }
    }

    public function order_refund( $order_id = null )
    {
        // if ($order_cache = $this->cache->get($order_id) && @$_GET[ 'refresh' ] != 'true') {
        //     echo $this->cache->get($order_id);
        //     return;
        // }

        $this->load->library('parser');
        $this->load->model('Nexo_Checkout');

        global $Options;

        $data                		=   array();
        // $data[ 'order' ]    		=   $this->Nexo_Checkout->get_order_products($order_id, true);
        // $data[ 'stock' ]            =   $this->Nexo_Checkout->get_order_with_item_stock( $order_id );
        // $data[ 'cache' ]    		=   $this->cache;

        // if (count($data[ 'order' ]) == 0) {
        //     die(sprintf(__('Impossible d\'afficher le reçu de remboursement. Cette commande ne possède aucun article &mdash; <a href="%s">Retour en arrière</a>', 'nexo'), $_SERVER['HTTP_REFERER']));
        // }

        // @since 2.7.9
        $data[ 'template' ]						=	array();
        $data[ 'template' ][ 'order_date' ]		=	':orderDate'; // mdate( '%d/%m/%Y %g:%i %a', strtotime($data[ 'order' ][ 'order' ][0][ 'DATE_CREATION' ]));
        $data[ 'template' ][ 'order_updated' ]  =   ':orderUpdated'; // just to show the date when the order has been update
        $data[ 'template' ][ 'order_code' ]		=	':orderCode'; // $data[ 'order' ][ 'order' ][0][ 'CODE' ];
        $data[ 'template' ][ 'order_id' ]       =   ':orderId'; // $data[ 'order' ][ 'order' ][0][ 'ID' ];
        $data[ 'template' ][ 'order_status' ]	=	':orderStatus'; // $this->Nexo_Checkout->get_order_type($data[ 'order' ][ 'order' ][0][ 'TYPE' ]);
        $data[ 'template' ][ 'order_note' ]     =   ':orderNote'; // $data[ 'order' ][ 'order' ][0][ 'DESCRIPTION' ];            
        $data[ 'template' ][ 'order_cashier' ]	=	':orderCashier'; // User::pseudo( $data[ 'order' ][ 'order' ][0][ 'AUTHOR' ] );

        $data[ 'template' ][ 'shop_name' ]		=	@$Options[ store_prefix() . 'site_name' ];
        $data[ 'template' ][ 'shop_pobox' ]		=	@$Options[ store_prefix() . 'nexo_shop_pobox' ];
        $data[ 'template' ][ 'shop_fax' ]		=	@$Options[ store_prefix() . 'nexo_shop_fax' ];
        $data[ 'template' ][ 'shop_email' ]		=	@$Options[ store_prefix() . 'nexo_shop_email' ];
        $data[ 'template' ][ 'shop_street' ]    =	@$Options[ store_prefix() . 'nexo_shop_street' ];
        $data[ 'template' ][ 'shop_phone' ]	    =	@$Options[ store_prefix() . 'nexo_shop_phone' ];

        $theme                                  =	@$Options[ store_prefix() . 'nexo_refund_theme' ] ? @$Options[ store_prefix() . 'nexo_refund_theme' ] : 'default';

        $path   =   '../modules/nexo/views/refund/' . $theme . '.php';

        $this->load->view(
            $this->events->apply_filters( 'nexo_refund_theme_path', $path ),
            $data,
            $theme
        );
    }

    /**
     * Gestion des impressions des étiquettes des produits
    **/

    public function shipping_item_codebar($shipping_id = null)
    {
        if ($shipping_id  == null) {
            show_error(__('Arrivage non définie.', 'nexo'));
        }

        $this->cache        =    new CI_Cache(array('adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'nexo_products_labels_' . store_prefix() ));

        if ($products_labels = $this->cache->get($shipping_id) && @$_GET[ 'refresh' ] != 'true') {
            echo $this->cache->get( $shipping_id );
            return;
        }

        $this->load->model('Nexo_Products');
        $this->load->model('Nexo_Shipping');

        global $Options;
        $pp_row                    =    ! empty($Options[ store_prefix() . 'nexo_products_labels' ]) ? @$Options[ store_prefix() . 'nexo_products_labels' ] : 4;

        $data                    =    array();
        $data[ 'shipping_id' ]    =    $shipping_id;
        $data[ 'pp_row'    ]        =    $pp_row;
        $data[ 'cache' ]    =    $this->cache;

        if (isset($_GET[ 'products_ids' ])) {
            $get        =    str_replace('%2C', ',', $_GET[ 'products_ids' ]);
            $ids        =    explode(',', $get);
            $products    =    array();
            foreach ($ids as $id) {
                // $unique_product        =    $this->Nexo_Products->get( store_prefix() . 'nexo_articles', $id, 'ID');
                $unique_product             =   $this->db->select( '*' )
                ->from( store_prefix() . 'nexo_arrivages' )
                ->join( store_prefix() . 'nexo_articles_stock_flow', store_prefix() . 'nexo_articles_stock_flow.REF_SHIPPING = ' . store_prefix() . 'nexo_arrivages.ID' )
                ->join( store_prefix() . 'nexo_fournisseurs', store_prefix() . 'nexo_fournisseurs.ID = ' . store_prefix() . 'nexo_articles_stock_flow.REF_PROVIDER' )
                ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_articles_stock_flow.REF_ARTICLE_BARCODE' )
                ->where( store_prefix() . 'nexo_arrivages.ID', $shipping_id /*$delivery_id*/ )
                ->get()->result_array();
                
                // Si le produit existe
                if (count($unique_product) > 0) {
                    $products[]            =    $unique_product[0];
                }
            }
            // var_dump( $products );
            $data[ 'products' ]        =    $products;
        } else {
            $data[ 'products' ]        =    $this->Nexo_Products->get_products_by_shipping($shipping_id);
        }

        $this->load->view('../modules/nexo/views/products-labels/default.php', $data);
    }

    /**
     *  Return a PDF document with current order receipt
     *  @param int order id
     *  @return PDF document
    **/

    public function order_pdf( $order_id )
    {
        ob_start();
        $this->order_receipt( $order_id );
        $content    =   ob_get_clean();
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml( $content );

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();

    }

    /**
     * Order Invoice
     * @param int order id
     * @return view
     */
    public function invoice( $order_id ) 
    {
        $this->load->library('parser');
        $this->load->model('Nexo_Checkout');
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orderModel' );

        global $Options;

        $data                		=   array();
        $data[ 'order' ]    		=   $this->Nexo_Checkout->get_order_products($order_id, true);
        $data[ 'refunds' ]          =   $this->orderModel->order_refunds( $order_id );
        $data[ 'tax' ]              =   $this->Nexo_Misc->get_taxes( $data[ 'order' ][ 'order' ][0][ 'REF_TAX' ] );

        /**
         * We need to make sure that
         * the order use the customer shipping 
         * information or not.
         */
        $data[ 'shipping' ]         =   $this->Nexo_Checkout->getOrderShippingInformations( $order_id );
        $data[ 'billing' ]          =   $this->db->where( 'ref_client', $data[ 'order' ][ 'order' ][0][ 'REF_CLIENT' ] )->where( 'type', 'billing' )->get( store_prefix() . 'nexo_clients_address' )->result_array();

        $allowed_order_for_print	=	$this->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );

        // Allow only cash order to be printed
        // if ( ! in_array( $data[ 'order' ]['order'][0][ 'TYPE' ], $allowed_order_for_print ) ) {
        //     redirect(array( 'dashboard', 'nexo', 'orders', '?notice=print_disabled' ));
        // }

        if (count($data[ 'order' ]) == 0) {
            return show_error(sprintf(__('Impossible d\'afficher la facture. Cette commande ne possède aucun article &mdash; <a href="%s">Retour en arrière</a>', 'nexo'), $_SERVER['HTTP_REFERER']));
        }

        $data['taxes'] = $data[ 'order' ][ 'order' ][0]['TOTAL_TAXES'] ?? 0;

        $fees_total = 0;
        $fees = [];
        foreach ( $data[ 'order' ][ 'order' ][0]['metas'] as $key => $value ) {
            if ( stripos( $key, '_wccf_cf_value_' ) === 0 ) {
                $slug = substr( $key, strlen('_wccf_cf_value_' ) );
                if ( empty( $fees[$slug] ) ) {
                    $fees[$slug] = [];
                }
                $fees[$slug]['value'] = $value;
            } else if ( stripos( $key, '_wccf_cf_label_' ) === 0 ) {
                $slug = substr( $key, strlen('_wccf_cf_value_' ) );
                if ( empty( $fees[$slug] ) ) {
                    $fees[$slug] = [];
                }
                $fees[$slug]['label'] = $value;
            }
        }
        if ( count( $fees ) ) {
            $data['fees'] = [];
            foreach ( $fees as $fee ) {
                if (isset($fee['label']) && isset($fee['value'])) {
                    $fees_total += $fee['value'];
                    $data[ 'order' ][ 'order' ][0]['fees'][] = [
                        'label' => $fee['label'],
                        'value' => floatval($fee['value']),
                    ];
                }
            }
        }
        $data[ 'order' ][ 'order' ][0]['fees_total'] = $fees_total;

        $this->events->add_action( 'dashboard_footer', function() use ( $data ) {
            get_instance()->load->module_view( 'nexo', 'invoices.default-script', $data );
        });

        $this->Gui->set_title( store_title( __( 'Facture', 'nexo' ) ) );
        return $this->load->module_view( 'nexo', 'invoices.default', $data, true );
    }

    /**
     * Print Result
     *
     * @param $order_id
     * @param bool $prices
     *
     * @return view
     * @internal param order $int id
     */
    public function printResult( $order_id, $prices = true )
    {
        $this->load->library('parser');
        $this->load->model('Nexo_Checkout');
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orderModel' );
        $this->load->model( 'Nexo_Misc' );

        $data                		            =   [];
        
        $data[ 'order' ]    		            =   $this->Nexo_Checkout->get_order_products($order_id, true);
        $data[ 'cache' ]    		            =   $this->cache;
        $data[ 'shipping' ]                     =   $this->db->where( 'ref_order', $order_id )->get( store_prefix() . 'nexo_commandes_shippings' )->result_array();
        $data[ 'tax' ]                          =   $this->Nexo_Misc->get_taxes( $data[ 'order' ][ 'order' ][0][ 'REF_TAX' ] );

        $data[ 'template' ]						=	[];
        $data[ 'template' ][ 'order_date' ]		=	mdate( '%d/%m/%Y %g:%i %a', strtotime($data[ 'order' ][ 'order' ][0][ 'DATE_CREATION' ]));
        $data[ 'template' ][ 'order_updated' ]  =	mdate( '%d/%m/%Y %g:%i %a', strtotime($data[ 'order' ][ 'order' ][0][ 'DATE_MOD' ]));
        $data[ 'template' ][ 'order_code' ]		=	!empty($data[ 'order' ][ 'order' ][0][ 'metas' ]['woocommerce_order_id']) ? $data[ 'order' ][ 'order' ][0][ 'metas' ]['woocommerce_order_id'] : $data[ 'order' ][ 'order' ][0][ 'CODE' ];
        $data[ 'template' ][ 'order_id' ]       =   $data[ 'order' ][ 'order' ][0][ 'ORDER_ID' ];
        $data[ 'template' ][ 'order_status' ]	=	$this->Nexo_Checkout->get_order_type( $data[ 'order' ][ 'order' ][0][ 'TYPE' ] );
        $data[ 'template' ][ 'order_note' ]     =   $data[ 'order' ][ 'order' ][0][ 'DESCRIPTION' ];

        $data[ 'template' ][ 'order_cashier' ]	=	User::pseudo( $data[ 'order' ][ 'order' ][0][ 'AUTHOR' ] );
        $data[ 'template' ][ 'shop_name' ]		=	store_option( 'site_name' );
        $data[ 'template' ][ 'shop_pobox' ]		=	store_option( 'nexo_shop_pobox' );
        $data[ 'template' ][ 'shop_fax' ]		=	store_option( 'nexo_shop_fax' );
        $data[ 'template' ][ 'shop_email' ]     =	store_option( 'nexo_shop_email' );
        $data[ 'template' ][ 'shop_street' ]    =	store_option( 'nexo_shop_street' );
        $data[ 'template' ][ 'shop_phone' ]     =	store_option( 'nexo_shop_phone' );
        $data[ 'template' ][ 'customer_name' ]  =   $data[ 'order' ][ 'order' ][0][ 'CUSTOMER_NAME' ];
        $data[ 'template' ][ 'customer_phone' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_phone' ];

        $data[ 'template' ][ 'delivery_address_1' ]     =   htmlentities(@$data[ 'shipping' ][0][ 'address_1' ]);
        $data[ 'template' ][ 'delivery_address_2' ]     =   htmlentities(@$data[ 'shipping' ][0][ 'address_2' ]);
        $data[ 'template' ][ 'city' ]               =   @$data[ 'shipping' ][0][ 'city' ];
        $data[ 'template' ][ 'country' ]            =   @$data[ 'shipping' ][0][ 'country' ];
        $data[ 'template' ][ 'name' ]               =   htmlentities(@$data[ 'shipping' ][0][ 'name' ]);
        $data[ 'template' ][ 'surname' ]            =   htmlentities(@$data[ 'shipping' ][0][ 'surname' ]);
        $data[ 'template' ][ 'state' ]              =   @$data[ 'shipping' ][0][ 'state' ];
        $data[ 'template' ][ 'delivery_cost' ]      =   @$data[ 'shipping' ][0][ 'price' ];
        $data[ 'template' ][ 'title' ]		    = 'Order #'. (!empty($data[ 'order' ][ 'order' ][0]['metas']['woocommerce_order_id']) ? $data[ 'order' ][ 'order' ][0]['metas']['woocommerce_order_id'] : $data[ 'order' ][ 'order' ][0]['CODE']);

        /**
         * Get order metas
         */
        $freshOrder     =   $data[ 'order' ][ 'order' ][0];
        $freshOrder[ 'metas' ]  =   $this->Nexo_Checkout->getOrderMetas( $order_id );

        /**
         * allow modification of data 
         * used on the receipts
         */
        $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template', [
            'template'          =>      $data[ 'template' ],
            'order'             =>      $freshOrder,
            'items'             =>      $data[ 'order' ][ 'products' ],
            'tax'               =>      $data[ 'tax' ],
            'shipping'          =>      $data[ 'shipping' ]
        ]);

        $allowed_order_for_print	    =	$this->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );

        // new template system
        $printer_template_vendor =   store_option( 'printer_template_vendor' );
        if (! empty( $printer_template_vendor ) ) {

            $loader        = new \Twig\Loader\FilesystemLoader(TWIGPATH);
            $twig          = new \Twig\Environment($loader);
            $template = $twig->load($printer_template_vendor . '/basic.tpl');


            // fill data
            $build = [
                'labels' => __( 'Produits', 'nexo' ),
                'template'  => $data[ 'template' ],
                'wildcards' => nexting([], '*'),
                'dashes' => nexting([], '-'),
                'payments' => [],
                'order' => $freshOrder,
                'shipping'  => $data[ 'shipping' ][0] ?? [],
            ];

            $logo_type = store_option( 'nps_logo_type' );
            if (!empty($logo_type)) {
                switch( $logo_type ) {
                    case 'nps-logo':
                        $build['title'] = store_option( 'nps_logo' );
                        break;
                    case 'store-name':
                        $build['title'] = store_option('site_name');
                        break;
                }
            }
            $other_details = store_option( 'nexo_other_details' );
            if (!empty($other_details)) {
                $build['other_details'] = $other_details;
            }
            $build['headers'] = [];
            foreach( buildingLines(
                $this->parser->parse_string( store_option( 'receipt_col_1' ), $data[ 'template' ] , true ),
                $this->parser->parse_string( store_option( 'receipt_col_2' ), $data[ 'template' ] , true )
            ) as $line ) {
                $build['headers'][] = nexting( $line );
            }

            $build['print_prices'] = $prices;
            $subTotal      = 0;
            $discountTotal = 0;
            foreach ($data['order']['products'] as $item) {
                $product   = nexting([
                    $item['NAME'] . ' (x' . $item['QUANTITE'] . ')',
                    $this->Nexo_Misc->cmoney_format($item['PRIX_TOTAL'])
                ]);
                $modifiers = [];
                if (is_string(@$item['metas']['modifiers'])) {
                    $item['metas']['modifiers'] = json_decode(@$item['metas']['modifiers'], true);
                }
                if ( ! empty(@$item['metas']['modifiers'])) {
                    foreach (@$item['metas']['modifiers'] as $modifier) {
                        $modifiers[] = $modifier->name . ' (' . trim($this->Nexo_Misc->cmoney_format($modifier->price)) . ')';
                    }
                }

                $subTotal += floatval($item['PRIX_TOTAL']);
                $build['items'][] = [
                    'product'   => $product,
                    'modifiers' => $modifiers,
                    'metas'     => @$item['metas'], // all metas
                ];
            }
            if ($prices) {
                $discount_amount = 0;

                if ( floatval( $freshOrder[ 'REMISE' ] ) > 0.0) {
                    if ( $freshOrder[ 'REMISE_TYPE' ] == 'percentage' ) {
                        $discount_amount		=	( nexoCartGrossValue( $data['order']['products'] ) * floatval( $freshOrder[ 'REMISE_PERCENT' ] ) ) / 100;
                        // echo $this->Nexo_Misc->cmoney_format( __floatval( $discount_amount ) );
                    } else  {
                        $discount_amount		=	floatval( $freshOrder[ 'REMISE' ] );
                        // echo $this->Nexo_Misc->cmoney_format( __floatval( $discount_amount ) );
                    }
                }

                $discountTotal += $discount_amount;

                $build['subtotal']  = nexting([
                    __('Sous Total', 'nexo'),
                    $this->Nexo_Misc->cmoney_format($subTotal)
                ]);
                $build['deduction'] = nexting([
                    __('Remises', 'nexo'),
                    $this->Nexo_Misc->cmoney_format($discountTotal)
                ]);

                if ( $freshOrder['TOTAL_TAXES'] > 0 ) {
                    $build['taxes'] = nexting([
                        __('Tax', 'nexo'),
                        $this->Nexo_Misc->cmoney_format($freshOrder['TOTAL_TAXES'])
                    ]);
                } else if ($data['tax'] || store_option('nexo_vat_type') == 'fixed') {
                    if ($data['tax'] && store_option('nexo_vat_type') == 'variable') {
                        $build['taxes'] = nexting([
                            sprintf(__('%s (%s%%)', 'nexo'), $data['tax'][0]['NAME'], $data['tax'][0]['RATE']),
                            $this->Nexo_Misc->cmoney_format($freshOrder['TVA'])
                        ]);
                    } elseif (store_option('nexo_vat_type') == 'fixed') {
                        $build['taxes'] = nexting([
                            __('TVA', 'nexo') . ' (' . store_option('nexo_vat_percent') . '%)',
                            $this->Nexo_Misc->cmoney_format($freshOrder['TVA'])
                        ]);
                    }
                }

                $build['total'] = nexting([
                    __('Total', 'nexo'),
                    $this->Nexo_Misc->cmoney_format($freshOrder['TOTAL'])
                ]);

                $order_payments = $this->Nexo_Misc->order_payments($freshOrder['CODE']);
                $payment_types  = $this->events->apply_filters('nexo_payments_types',
                    $this->config->item('nexo_payments_types'));

                foreach ($order_payments as $payment) {
                    if ($payment['OPERATION'] === 'incoming') {
                        $build['payments'][] = nexting([
                            @$payment_types[$payment['PAYMENT_TYPE']] == null ? __('Type de paiement inconnu',
                                'nexo') : @$payment_types[$payment['PAYMENT_TYPE']],
                            $this->Nexo_Misc->cmoney_format(__floatval($payment['MONTANT']))
                        ]);
                    } else {
                        $build['payments'][] = nexting([
                            @$payment_types[$payment['PAYMENT_TYPE']] == null ? __('Type de paiement inconnu',
                                'nexo') : @$payment_types[$payment['PAYMENT_TYPE']],
                            $this->Nexo_Misc->cmoney_format(-__floatval($payment['MONTANT']))
                        ]);
                    }
                }

                if (in_array($freshOrder['TYPE'], ['nexo_order_partially_refunded', 'nexo_order_refunded'])) {
                    /**
                     * handling refunds
                     */
                    $refunds         = $this->orderModel->order_refunds($freshOrder['ID']);
                    $totalRefunds    = array_sum(array_map(function ($refund) {
                        return floatval($refund['TOTAL']);
                    }, $refunds));
                    $build['refund'] = nexting([
                        __('Remboursement', 'nexo'),
                        $this->Nexo_Misc->cmoney_format(
                            number_format(
                                -$totalRefunds, 2
                            )
                        )
                    ]);
                } else {
                    $totalRefunds = 0;
                }

                $build['collected'] = nexting([
                    __('Somme Perçue', 'nexo'),
                    $this->Nexo_Misc->cmoney_format(
                        number_format(
                            (floatval($freshOrder['SOMME_PERCU']) - $totalRefunds),
                            2
                        )
                    )
                ]);

                if ( $data[ 'template' ][ 'delivery_cost' ] > 0 ) {
                    $build['delivery'] = nexting([
                        __('Delivery', 'nexo'),
                        $this->Nexo_Misc->cmoney_format(
                            number_format(
                                (floatval($data[ 'template' ][ 'delivery_cost' ])),
                                2
                            )
                        )
                    ]);
                }

                // process custom fields
                $fees = [];
                foreach ( $freshOrder['metas'] as $key => $value ) {
                    if ( stripos( $key, '_wccf_cf_value_' ) === 0 ) {
                        $slug = substr( $key, strlen('_wccf_cf_value_' ) );
                        if ( empty( $fees[$slug] ) ) {
                            $fees[$slug] = [];
                        }
                        $fees[$slug]['value'] = $value;
                    } else if ( stripos( $key, '_wccf_cf_label_' ) === 0 ) {
                        $slug = substr( $key, strlen('_wccf_cf_value_' ) );
                        if ( empty( $fees[$slug] ) ) {
                            $fees[$slug] = [];
                        }
                        $fees[$slug]['label'] = $value;
                    }
                }
                if ( count( $fees ) ) {
                    $build['fees'] = [];
                    foreach ( $fees as $fee ) {
                        if (isset($fee['label']) && isset($fee['value'])) {
                            $build['fees'][] = nexting([
                                $fee['label'],
                                $this->Nexo_Misc->cmoney_format(
                                    number_format(
                                        (floatval($fee['value'])),
                                        2
                                    )
                                )
                            ]);
                        }
                    }
                }

                $terme = __floatval($freshOrder['SOMME_PERCU']) >= floatval($freshOrder['TOTAL']) ? __('à rendre :',
                    'nexo') : __('&Agrave; percevoir :', 'nexo');

                $build['due'] = $terme . ' ' .
                                $this->Nexo_Misc->cmoney_format(
                                    number_format(
                                        abs(
                                            (
                                                floatval($freshOrder['TOTAL']) -
                                                floatval($freshOrder['SOMME_PERCU'])
                                            )
                                            + floatval($totalRefunds)
                                        ), 2
                                    )
                                );
            }

            $build['customer']  = @$freshOrder['customer_name'] ? nexting([
                __('Customer :', 'gastro'),
                @$freshOrder['customer_name']
            ]) : null;
            $build['phone']  = @$freshOrder['phone'] ? nexting([
                __('Phone :', 'gastro'),
                @$freshOrder['phone']
            ]) : null;
            $build['email']  = @$freshOrder['email'] ? nexting([
                __('Email :', 'gastro'),
                @$freshOrder['email']
            ]) : null;

            if ( $freshOrder['RESTAURANT_BOOKED_FOR'] != '0000-00-00 00:00:00' ) {
                if (date('Y-m-d', strtotime($freshOrder['RESTAURANT_BOOKED_FOR'])) != date('Y-m-d')) {
                    $build['date'] = __('Booked For: ', 'nexo') . date('M j, Y', strtotime($freshOrder['RESTAURANT_BOOKED_FOR']));
                }
                if ($freshOrder['RESTAURANT_ORDER_TYPE'] == 'delivery') {
                    $build['time'] = __('Delivery Time: ', 'nexo') . date('g:i a', strtotime($freshOrder['RESTAURANT_BOOKED_FOR']));
                } elseif (!empty($freshOrder['metas']['woocommerce_order_id'])) {
                    $build['time'] =  __('Pick Up Time: ', 'nexo') . date('g:i a', strtotime($freshOrder['RESTAURANT_BOOKED_FOR']));
                } elseif ($freshOrder['RESTAURANT_ORDER_TYPE'] == 'takeaway') {
                    $build['time'] =  __('Take Away Time: ', 'nexo') . date('g:i a', strtotime($freshOrder['RESTAURANT_BOOKED_FOR']));
                }

//                $build['date'] = $this->getTitle(@$freshOrder['RESTAURANT_ORDER_TYPE']) . ' ' . __('Time : ',
//                        'gastro') . date('g:i a', strtotime(@$freshOrder['RESTAURANT_BOOKED_FOR']));
            }

            $build['order_date'] = nexting([
                    date( 'F j, Y', strtotime($freshOrder[ 'DATE_CREATION' ])),
                    date( 'g:i a', strtotime($freshOrder[ 'DATE_CREATION' ])),
            ]);

            return $template->render($build);
        }
        return $this->load->view(
            '../modules/' . $this->events->apply_filters( 'nps_receipt_path', 'nexo/views/receipts/nps/basic' ), 
            $filtered, 
            true 
        );
    }

    public function getTitle( $type )
    {
        switch( $type ) {
            case 'dinein'       :   return __( 'Dine in', 'gastro' ); break;
            case 'takeaway'     :   return __( 'Take Away', 'gastro' ); break;
            case 'delivery'     :   return __( 'Delivery', 'gastro' ); break;
            default             :   return __( 'Unknown Type', 'gastro' ); break;
        }
    }

    /**
     * return a refund receipt of a spific order
     * @param int refund id
     * @return html
     */
    public function refundReceipt( $refund_id )
    {
        $this->load->library( 'parser' );
        $this->load->model( 'Nexo_Misc' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orderModel' );

        $data                                   =   [];
        $data[ 'refund' ]                       =   $this->orderModel->get_refund( $refund_id );

        if ( $data[ 'refund' ] ) {

            $data[ 'template' ]						=	[];
            $data[ 'template' ][ 'order_date' ]		=	mdate( '%d/%m/%Y %g:%i %a', strtotime( $data[ 'refund' ][ 'DATE_CREATION' ] ));
            $data[ 'template' ][ 'shop_name' ]		=	store_option( 'site_name' );
            $data[ 'template' ][ 'shop_pobox' ]		=	store_option( 'nexo_shop_pobox' );
            $data[ 'template' ][ 'shop_fax' ]		=	store_option( 'nexo_shop_fax' );
            $data[ 'template' ][ 'shop_email' ]     =	store_option( 'nexo_shop_email' );
            $data[ 'template' ][ 'shop_street' ]    =	store_option( 'nexo_shop_street' );
            $data[ 'template' ][ 'shop_phone' ]     =	store_option( 'nexo_shop_phone' );   
            $data[ 'template' ][ 'refund_author' ]  =   $data[ 'refund' ][ 'author' ][ 'name' ];
            $data[ 'template' ][ 'refund_date' ]    =   $data[ 'refund' ][ 'DATE_CREATION' ];
            $data[ 'template' ][ 'refund_type' ]    =   $data[ 'refund' ][ 'TYPE' ] === 'withstock' ? __( 'Avec retour de stock', 'nexo' )  : __( 'Sans retour de stock', 'nexo' );
    
            $this->load->module_view( 'nexo', 'receipts.nps.refund', $data );
        }
    }
}