<?php
global $Options;
?>
<?php include_once(MODULESPATH . '/nexo/inc/angular/services/textHelper.php'); ?>
<script type="text/javascript">
    "use strict";

    tendooApp.directive('fileReader', function() {
        return {
            scope: {
                fileReader:"=",
                fileExtension : "="
            },
            link: function(scope, element) {
                $(element).on('change', function(changeEvent) {
                    var files = changeEvent.target.files;

                    if (files.length) {
                        var r = new FileReader();
                        r.onload = function(e) {
                            var contents = e.target.result;

                            scope.$apply(function () {
                                scope.fileReader    =   contents;
                            });
                        };

                        r.readAsText(files[0]);
                    }
                });
            }
        };
    });

    tendooApp.controller('manageReportsController', ['$scope', '$http', '$document', '$timeout', function ($scope, $http, $document, $timeout) {

        $scope.selectedStart = false;
        $scope.selectedEnd = false;
        $scope.enabledButton = false;
        $scope.fileContent = '';

        $scope.enableButton = function () {
            $scope.enabledButton = $scope.selectedStart !== false && $scope.selectedEnd !== false;
        };

        $scope.submitExport = function () {

            if (!$scope.selectedStart || !$scope.selectedEnd) {
                return;
            }
            $scope.notices = [];

            $scope.notices.push({
                msg: '<?php echo _s('Generating data...', 'nexo_premium');?>',
                type: 'info'
            });

            $http.post('<?php echo site_url(array('rest', 'nexo', 'reports_export', store_get_param('?')));?>', {
                'start': $scope.selectedStart,
                'end': $scope.selectedEnd
            }, {
                headers: {
                    '<?php echo $this->config->item('rest_key_name');?>': '<?php echo @$Options['rest_key'];?>'
                }
            }).then(function (returned) {
                if (returned.data.success) {
                    $scope.downloadResponseAsFile(returned.data);
                    $scope.notices.push({
                        msg: '<?php echo _s('Export is completed', 'nexo_premium');?>',
                        type: 'info'
                    });
                } else {
                    $scope.notices.push({
                        msg: '<?php echo _s('Error', 'nexo_premium');?>',
                        type: 'warning'
                    });
                }

            }, function () {
                $scope.notices.push({
                    msg: '<?php echo _s('Error', 'nexo_premium');?>',
                    type: 'warning'
                });
            });
        };

        $scope.downloadResponseAsFile = function (response) {
            var blob = new Blob([response.data], {
                type: response.type
            });

            if (window.navigator.msSaveOrOpenBlob) {
                navigator.msSaveBlob(blob, response.filename); // @untested
            } else {
                var downloadContainer = angular.element('<div data-tap-disabled="true"><a></a></div>');
                var downloadLink = angular.element(downloadContainer.children()[0]);
                downloadLink.attr('href', window.URL.createObjectURL(blob));
                downloadLink.attr('download', response.filename);
                downloadLink.attr('target', '_blank');

                $document.find('body').append(downloadContainer);

                $timeout(function () {
                    downloadLink[0].click();
                    downloadLink.remove();
                }, null);
            }
        };

        $scope.submitImport = function () {

            if( $scope.fileContent.length == 0 ) {
                return false;
            }

            NexoAPI.Bootbox().confirm({
                message 		:	'<div class="meal-selection">Are you sure to import the new data?</div>',
                title          :	'<?php echo _s( 'Import Reports Data', 'nexo_premium' );?>',
                buttons: {
                    cancel: {
                        label: '<?php echo _s( 'Close', 'nexo_premium' );?>',
                        className: 'btn-default'
                    }
                },
                callback		:	function( action ) {
                    if (action) {
                        $scope.notices = [];

                        $scope.notices.push({
                            msg: '<?php echo _s('Importing data...', 'nexo_premium');?>',
                            type: 'info'
                        });

                        $http.post('<?php echo site_url(array(
                            'rest',
                            'nexo',
                            'reports_import',
                            store_get_param('?')
                        ));?>', {
                            'data': $scope.fileContent
                        }, {
                            headers: {
                                '<?php echo $this->config->item('rest_key_name');?>': '<?php echo @$Options['rest_key'];?>'
                            }
                        }).then(function (returned) {
                            if (returned.data.success) {
                                $scope.notices.push({
                                    msg: '<?php echo _s('Import is completed', 'nexo_premium');?>',
                                    type: 'info'
                                });
                            } else {
                                $scope.notices.push({
                                    msg: '<?php echo _s('Error', 'nexo_premium');?>',
                                    type: 'warning'
                                });
                            }

                        }, function () {
                            $scope.notices.push({
                                msg: '<?php echo _s('Error', 'nexo_premium');?>',
                                type: 'warning'
                            });
                        });
                    }
                }
            });
        };

        $scope.submitClear = function () {
            NexoAPI.Bootbox().confirm({
                message: '<div class="meal-selection">Are you sure to clear report data?</div>',
                title: '<?php echo _s('Clear Reports Data', 'nexo_premium');?>',
                buttons: {
                    cancel: {
                        label: '<?php echo _s('Close', 'nexo_premium');?>',
                        className: 'btn-default'
                    }
                },
                callback: function (action) {
                    if (action) {
                        NexoAPI.Bootbox().confirm({
                            message: '<div class="meal-selection">Are you ABSOLUTELY sure to clear your data?</div>',
                            title: '<?php echo _s('Clear All Reports Data', 'nexo_premium');?>',
                            buttons: {
                                cancel: {
                                    label: '<?php echo _s('Close', 'nexo_premium');?>',
                                    className: 'btn-default'
                                }
                            },
                            callback: function (action) {
                                if (action) {
                                    $scope.notices = [];

                                    $scope.notices.push({
                                        msg: '<?php echo _s('Clearing data...', 'nexo_premium');?>',
                                        type: 'info'
                                    });

                                    $http.post('<?php echo site_url(array(
                                        'rest',
                                        'nexo',
                                        'reports_clear',
                                        store_get_param('?')
                                    ));?>', {
                                        'data': $scope.fileContent
                                    }, {
                                        headers: {
                                            '<?php echo $this->config->item('rest_key_name');?>': '<?php echo @$Options['rest_key'];?>'
                                        }
                                    }).then(function (returned) {
                                        if (returned.data.success) {
                                            $scope.notices.push({
                                                msg: '<?php echo _s('Reports data have been removed', 'nexo_premium');?>',
                                                type: 'info'
                                            });
                                        } else {
                                            $scope.notices.push({
                                                msg: '<?php echo _s('Error', 'nexo_premium');?>',
                                                type: 'warning'
                                            });
                                        }

                                    }, function () {
                                        $scope.notices.push({
                                            msg: '<?php echo _s('Error', 'nexo_premium');?>',
                                            type: 'warning'
                                        });
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }

    }]);

    // Date Picker
    $(function () {
        var reportsFormScope = angular.element($('#manageReportsForm')).scope();
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD'
        }).on('dp.change', function (e) {
            reportsFormScope.selectedStart = e.date.format('YYYY-MM-DD');
            reportsFormScope.enableButton();
        });
        $('#datetimepicker2').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'YYYY-MM-DD'
        }).on('dp.change', function (e) {
            reportsFormScope.selectedEnd = e.date.format('YYYY-MM-DD');
            reportsFormScope.enableButton();
        });
    });
</script>
