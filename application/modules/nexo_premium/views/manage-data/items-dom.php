<?php
global $Options;
$this->config->load('rest')
?>
<form enctype="multipart/form-data" method="POST" ng-controller="manageReportsController" id="manageReportsForm">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>">
    <div class="row">
        <div class="col-md-6">
            <h3>Export data</h3>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <span class="input-group-addon"><?php _e('Period start date', 'nexo_premium'); ?></span>
                    <input type='text' class="form-control" name="start" value=""/>
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>
                </div>
            </div>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                    <span class="input-group-addon"><?php _e('Period end date', 'nexo_premium'); ?></span>
                    <input type='text' class="form-control" name="end" value=""/>
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary" ng-click="submitExport()">
                <?php echo __('Export', 'nexo_premium'); ?>
            </button>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-6">
            <h3>Import data</h3>

            <div class="form-group">
                <label for="exampleInputFile">
                    <?php _e('Upload File', 'nexo'); ?>
                </label>
                <input type="file" class="form-control-file" name="data_file" file-reader="fileContent"
                       file-extension="data">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary" ng-click="submitImport()">
                <?php echo __('Import', 'nexo_premium'); ?>
            </button>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-6">
            <h3>Clear data</h3>

            <button type="submit" class="btn btn-primary" ng-click="submitClear()">
                <?php echo __('Clear', 'nexo_premium'); ?>
            </button>
        </div>
    </div>
    <br>
    <br>
    <div ng-show="notices.length > 0">
        <p ng-repeat="notice in notices">{{ notice.msg }}</p>
    </div>
</form>