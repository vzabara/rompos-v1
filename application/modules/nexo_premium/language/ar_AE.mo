��    Y      �     �      �     �     �  &   �  �   �  	   x     �     �     �     �  
   �     �      �     �     	  
   	  �   	     �	  	   [
     e
     w
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
            (        ?     S     _     h     m  m   s  t   �  �   V     �     �     �     �       a        m     s     w     }     �     �     �     �     �     �     �  
   �     �     �     �  	             ,     J     S     g     y     �     �     �  	   �  C   �       $        7     N     _     e     k     o     �     �     �     �  V   �  $     �  C  
   =  
   H     S  �   q  
     
        (     5  
   O  
   Z     e  ,   �     �     �     �    �  �   �  ,   �     �     �     �          (     9  
   F  
   Q     \     p     �     �     �     �  +   �     �     �                 �      ~   �  �      
   �  
   �     �  
   �  
   �  �        �     �     �     �     �     �     �  
                  '     4     C     J     \     t  (   }     �     �  
   �  !   �               #  
   2     =  \   J     �  )   �     �       
        $  "   5  $   X  $   }     �     �  -   �  Z   �  F   T     ,       <   1         D   !   9       3       5   /       :               I       B   U      4   2                                   >   M       X           -   V      C         0           L   P             "   '           O       S           $   (       N          *       E      G                    ;      
   K   .       +   @       8           T   F           W   6          Y   Q   =            ?              H   7   	         J   )   A   R         &       #   %       Annuler Aout Assigner la dépense à une categorie. Assigner une dépense à un fournisseur. Assurez-vous <a href="%s">d'avoir assigné la bonne catégorie</a> pour les comptes créditeurs des fournisseurs. Attention Aujourd'hui Auteur Autres Statistiques Avril Bénéfice Bénéfices et Pertes Bénéfices et Pertes &mdash; %s Caisse Enregistreuse Caissier Catégorie Ce rapport est expérimentale. Ses données ne devrait être utilisées qu'a des fins de tests. Vous pouvez donc partager vos avis, en envoyant un email à notre addresse : contact@rompos.com Ce rapport vous permet d'afficher le flux du stock entrant et sortant pendant une période précise avec la valeur de celle-ci. Créances Date de création Date de départ Date de fin Date de modification Dates Decembre Description Dimanche Durée d'inactivitée Durée de session Dépenses Entrée Erreur Factures Fiche de suivi de stock &mdash; Nexo POS Flux de trésorerie Fournisseur Février Hier Image Impossible d'afficher ce rapport sur une durée qui excède 31 jours. Veuillez réduire l'intervale de temps. Impossible d'afficher le rapport pour une caisse actuellement ouverte. Veuillez fermer la caisse avant de continuer. Impossible d'afficher le rapport. Nous n'avons pas été en mesure de localiser la caisse. Veuillez rafraichir le rapport et essayer à nouveau. Imprimer Janvier Jeudi Juillet Juin La date mentionnée est invalide. Le rapport sollicité ne peut se faire pour les jours à venir. Lundi Mai Mardi Mars Mercredi Montant à l'ouverture Montant à la fermeture Name Nom Novembre Octobre Opération Oui Par catégorie Prix de la facture Quantité Rapport des Ventes Annuelles Rapport journalier détaillé Rapports Revenir en arrière Revenu journalier Récapitulatif des dépenses Référence Références Samedi Septembre Si la dépense à une valeur, vous pouvez le définir sur ce champ. Stock Sortant Suivi de l'inventaire par catégorie Supprimer l'historique Temps de travail Title Total UGS Valeur Stock Entrant  Valeur Stock Sortant Vendredi Ventes de la semaine Vider le cache Vous devez choisir un caissier et une caisse enregistreuse avant d'afficher le rapport Vous n'avez pas accès à cette page Project-Id-Version: 
POT-Creation-Date: 2018-09-03 21:27+0100
PO-Revision-Date: 2018-09-03 21:28+0100
Last-Translator: 
Language-Team: 
Language: ar_AE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=6; plural=(n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_s;_e
X-Poedit-SearchPath-0: .
 إلغاء أغسطس تعيين حساب لفئة. تعيين حساب للمورد. تأكد من <a href="%s"> تعيين الفئة الصحيحة </a> لحسابات البائع المستحقة الدفع. انتبه اليوم الكاتب إحصائيات أخرى أبريل الربح الفوائد والخسائر الأرباح والخسائر & mdash ; %s سجل النقدية امين الصندوق فئة هذا التقرير تجريبي. ولا ينبغي استخدام بياناته الا لأغراض الاختبار. يمكنك بالتالي مشاركه آرائك ، عن طريق إرسال بريد الكتروني إلى عنواننا: contact@rompos.com يسمح لك هذا التقرير بعرض تدفق المخزون الوارد والصادر لفتره زمنيه محدده بقيمه المخزون. المبالغ المستحقّة القبض تاريخ الإنشاء تاريخ المغادرة تاريخ الإنجاز تاريخ التعديل التواريخ ديسمبر الوصف الأحد وقت الخمول طول الجلسة النفقات دخول خطأ الفواتير ورقة تتبع المخزون - Nexo POS التدفق النقدي مزود فبراير أمس صورة لا يمكن عرض هذا التقرير لمده تتجاوز 31 يوما. الرجاء تقليل الفاصل الزمني. يتعذر عرض التقرير لمربع مفتوح حاليا. الرجاء إغلاق القفص قبل المتابعة. تعذر عرض التقرير. لم نكن قادرين علي تحديد مكان الصراف الرجاء تحديث التقرير والمحاولة مره أخرى. طباعة يناير الخميس يوليو يونيو التاريخ المذكور غير صحيح. لا يمكن أن يتم التقرير المطلوب للأيام القادمة. الاثنين مايو الثلاثاء مارس الأربعاء المبلغ الافتتاحي المبلغ الختامي الإسم اسم نوفمبر أكتوبر العمليه نعم حسب الفئة سعر الفاتورة كمية تقرير المبيعات السنوي تقرير يومي مفصل العلاقات إرجاع الإيرادات اليومية موجز النفقات: مرجع المراجع السبت سبتمبر إذا كان للمبلغ قيمة ، فيمكنك تعيينها على هذا الحقل. المخزون الصادر تتبع المخزون حسب الفئة مسح المحفوظات وقت العمل اللقب الإجمالي وحده أداره المخزون قيمه الأسهم الواردة قيمه المخزون الصادر الجمعة مبيعات الاسبوع مسح ذاكرة التخزين المؤقت يجب عليك اختيار صراف وسجل النقدية قبل عرض التقرير ليس لديك إمكانية الدخول إلى هذه الصفحة 