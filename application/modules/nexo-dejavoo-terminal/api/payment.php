<?php

require __DIR__ . '/../../../vendor/autoload.php';

use \Curl\Curl;

class DejavooTerminal extends Tendoo_Api
{
    private $logFile = '';
    private $requestData = [];

    public function pay()
    {
        global $Options;

        $this->logFile = APPPATH . 'logs/dejavoo-terminal-' . date('Y-m-d') . '.log';

        $url = '';
        $port = '';
        $authKey = '';
        $registerId = '';

        if (@$Options[store_prefix() . 'nexo_enable_dejavoo_terminal'] != 'no') {
            $url = $Options[store_prefix() . 'nexo_dejavoo_terminal_url'];
            $port = $Options[store_prefix() . 'nexo_dejavoo_terminal_port'];
            $authKey = $Options[store_prefix() . 'nexo_dejavoo_auth_key'];
            $registerId = $Options[store_prefix() . 'nexo_dejavoo_register_id'];
        }

        $orderId = $this->post('id');
        $orderCode = $this->post('code');
        $amount = $this->post('amount');
        $paymentType = $this->post('payment_type');
        $transType = $this->post('trans_type');
        $print = $this->post('print');
        $author = $this->post( 'author' );

        if (empty($url) || empty($port) || empty($authKey)) {
            $this->response([
                'error' => [
                    'message' => 'Incorrect Dejavoo Terminal Settings',
                ]
            ], 500);

            return;
        }

        $xmlRequest = "<request><PaymentType>{$paymentType}</PaymentType><TransType>{$transType}</TransType><Amount>{$amount}</Amount><InvNum>{$orderId}</InvNum><RefId>{$orderCode}</RefId><AuthKey>{$authKey}</AuthKey><RegisterId>{$registerId}</RegisterId><PrintReceipt>{$print}</PrintReceipt></request>";

        $parts = parse_url($url);
        $parts['port'] = $port;
        //$parts['path'] = 'transaction';
        $url = http_build_url($parts);


        $this->requestData = [
            'RefId' => $orderCode,
            'InvNum' => $orderId,
            'RegisterId' => $registerId,
            'Amount' => $amount,
            'PaymentType' => $paymentType,
            'TransType' => $transType,
            'PrintReceipt' => $print,
            'Author' => $author,
        ];

        $curl = new Curl();
        $curl->error(function() {
            $this->response([
                'status' => 'try_later'
            ], 200);
        });
        $response = $curl->get($url, array(
            'TerminalTransaction' => $xmlRequest,
        ));

        $data = simplexml_load_string((string) $response);
        if ($data === false) {
            if (@$Options[store_prefix() . 'nexo_dejavoo_logs'] != 'no') {
                $this->log([
                    'request' => $this->requestData,
                    'response' => (string) $response,
                ]);
            }
            $this->response([
                'error' => [
                    'message' => 'Error parsing response',
                ]
            ], 500);
        }
        if ($data->response->Message == 'Approved') {
            // update order payment status
            $this->setPaid($this->requestData);
            $this->response([
                'status' => 'payment_success'
            ], 200);
        } else {
            if (@$Options[store_prefix() . 'nexo_dejavoo_logs'] != 'no') {
                $this->log($this->requestData);
                $this->log($data->response);
            }
            $this->response([
                'error' => [
                    'message' => (string) $data->response->RespMSG,
                ]
            ], 500);
        }
    }

    protected function setPaid($requestData)
    {
        $this->db
            ->set('TYPE', 'nexo_order_comptant')
            ->set('SOMME_PERCU', $requestData['Amount'])
            ->where('ID', $requestData['InvNum'])
            ->update(store_prefix() . 'nexo_commandes');

        $this->db->insert( store_prefix() . 'nexo_commandes_paiements', array(
            'REF_COMMAND_CODE'		=>	$requestData['RefId'],
            'AUTHOR'				=>	$requestData['Author'],
            'DATE_CREATION'			=>	date_now(),
            'PAYMENT_TYPE'			=>	'dejavoo',
            'OPERATION'             =>  'incoming',
            'MONTANT'				=>	$requestData['Amount']
        ) );
    }

    private function log($var)
    {
        flog($var, $this->logFile);
    }
}