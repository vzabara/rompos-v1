<?php
class DejavooTerminal_Controller
{
	/**
	 * Dejavoo Terminal Settings
	**/

	public static function settings()
	{
		$core	=	get_instance();
		$core->Gui->set_title( 'Dejavoo Terminal Settings' );
		$core->load->module_view( 'nexo-dejavoo-terminal', 'dejavoo-settings' );
	}
}
