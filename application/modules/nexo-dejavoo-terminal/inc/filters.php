<?php
class Nexo_DejavooTeminal_Filters
{
	public static function payment_gateway( $gateway )
	{
		global $Options;
		if (@$Options[ store_prefix() . 'nexo_enable_dejavoo_terminal' ] != 'no'):
			$gateway[ 'dejavoo' ]	=	__( 'Dejavoo Terminal', 'nexo-dejavoo-terminal' );
		endif;

		return $gateway;
	}

	/**
	 * Admin Menu
	**/

	public static function admin_menus( $menus )
	{
		$menus[]		=	array(
			'title'		=>		__( 'Dejavoo Terminal', 'nexo-dejavoo-terminal' ),
			'href'		=>		dashboard_url([ 'settings', 'dejavoo' ])
		);

		return $menus;
	}

	/**
	 * PayBox dependency
	 * register Windows_Splash
	**/

	public static function paybox_dependencies( $dependencies )
	{
		return array_merge( $dependencies, array( '__windowSplash' ) );
	}
}
