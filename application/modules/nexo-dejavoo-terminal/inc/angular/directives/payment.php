<script>
tendooApp.directive( 'dejavooPayment', function(){

	HTML.body.add( 'angular-cache' );

	HTML.query( 'angular-cache' )
	.add( 'h3.text-center' )
	.each( 'style', 'margin:0px;margin-bottom:10px;' )
	.textContent	=	'<?php echo _s( 'Dejavoo Terminal', 'nexo-dejavoo-terminal' );?>';

    HTML.query( 'angular-cache' )
	.add( 'p.text-center.cards-wrapper>button.btn.bigLabel.cardType.creditCardType' )
	.each( 'ng-click', 'setCardType("credit")' )
	.each( 'ng-disabled', 'addPaymentDisabled' )
	.textContent	=	'<?php echo _s( 'Credit ', 'nexo-dejavoo-terminal' );?>';

    HTML.query( '.cards-wrapper' )
	.add( 'button.btn.bigLabel.cardType.debitCardType' )
	.each( 'ng-click', 'setCardType("debit")' )
	.each( 'ng-disabled', 'addPaymentDisabled' )
	.textContent	=	'<?php echo _s( 'Debit ', 'nexo-dejavoo-terminal' );?>';

    HTML.query( '.cards-wrapper' )
	.add( 'button.btn.bigLabel.cardType.ebtCardType' )
	.each( 'ng-click', 'setCardType("ebt")' )
	.each( 'ng-disabled', 'addPaymentDisabled' )
	.textContent	=	'<?php echo _s( 'EBT ', 'nexo-dejavoo-terminal' );?>';

    HTML.query( 'angular-cache' )
        .add( 'p.text-center>button.btn.btn-success.addPaymentButton.bigLabel.disabled' )
        .each( 'ng-click', 'openDejavooTerminal()' )
        .textContent	=	'<?php echo _s( 'Pay ', 'nexo-dejavoo-terminal' );?> ' + NexoAPI.DisplayMoney(NexoAPI.Format(v2Checkout.CartValue));

    var DOM		=	angular.element( 'angular-cache' ).html();

	angular.element( 'angular-cache' ).remove();

	return {
		template 	:	DOM
	}
});
</script>
