<?php
global $Options;
?>
<script>

<?php
if (@$Options[ store_prefix() . 'nexo_enable_dejavoo_terminal' ] != 'no'):
?>

NexoAPI.events.addFilter( 'nexo_payments_types_object', function( object ) {

	object		=	_.extend( object, _.object( [ 'dejavoo' ], [{
		text		:	'<?php echo _s( 'Dejavoo Terminal', 'nexo-dejavoo-terminal' );?>',
		active		:	false,
		isCustom	:	true
	}] ) );

	return object;

});

<?php
endif;
?>

var	previous_text	=	null;

NexoAPI.events.addAction( 'pos_select_payment', function( data ) {

	if( previous_text == null ) {
		previous_text	=	data[0].defaultAddPaymentText;
	}

	if( data[1] == 'dejavoo' ) {
		// Disable payment for Dejavoo Terminal
		data[0].defaultAddPaymentText	=	'<?php echo _s( 'Connect Dejavoo Terminal', 'nexo-dejavoo-terminal' );?>';
	} else {
		data[0].defaultAddPaymentText	=	previous_text;
	}

});

// Disable payment edition for Dejavoo Terminal
NexoAPI.events.addFilter( 'allow_payment_edition', function( data ) {
	if( data[1] == 'dejavoo' ) {
		NexoAPI.Notify().warning( '<?php echo _s( 'Attention', 'nexo-dejavoo-terminal' );?>', '<?php echo _s( 'Vous ne pouvez pas modifier un paiement déjà effectué, car une carte a déjà été débitée.', 'nexo-dejavoo-terminal' );?>' );

		return [ false, data[1] ];
	}

	return data;
});
</script>
