<?php
global $Options;

if (@$Options[ store_prefix() . 'nexo_enable_dejavoo_terminal' ] != 'no'):
?>
$scope.cardType = '';
$scope.setCardType = function(type) {
    $.each($('button.cardType'), function() {
        $(this).removeClass('active');
    });
    $('button.cardType.'+type+'CardType').addClass('active');
    switch (type) {
        case 'credit':
            $scope.cardType = 'Credit';
            break;
        case 'debit':
            $scope.cardType = 'Debit';
            break;
        case 'ebt':
            $scope.cardType = 'EBT';
            break;
    }
    $('button.addPaymentButton').removeClass('disabled');
};

/**
 * Load Dejavoo Terminal Payment
**/
$scope.openDejavooTerminal	=	function(){

    $scope.orderId = 0;
    $scope.card = new Object;
    $scope.card.id = 0;
    $scope.card.code = '';
    $scope.card.amount = NexoAPI.Format( v2Checkout.CartValue, '0.00' );
    $scope.card.payment_type = $scope.cardType;
    $scope.card.trans_type = 'Sale';
    $scope.card.print = 'Both';

    NexoAPI.events.addFilter( 'process_data', function( ) {
        if ($scope.orderId > 0) {
            v2Checkout.CartType = 'nexo_order_devis';
            return {
                url			:	'/rest/nexo/order/<?php echo User::id();?>/' + $scope.orderId + '?store_id=<?php echo get_store_id();?>',
                type		:	'PUT'
            };
        } else {
            return {
                url			:	v2Checkout.ProcessURL,
                type		:	v2Checkout.ProcessType
            };
        }
    });

    if ($scope.orderId == 0 ) {
       v2Checkout.dejavooSaveOrder();
    } else {
        $scope.card.id = $scope.orderId;
        NexoAPI.events.doAction( 'dejavoo_charged', $scope.card );
    }

<!--    $('.dejavoo-wrapper').html($compile($('.dejavoo-wrapper').html())($scope));-->
};

// Register events
NexoAPI.events.addFilter( 'check_payment_mean', function( object ) {
    object[0] = object[1] == "dejavoo";

    return object;
});

NexoAPI.events.addFilter( 'payment_mean_checked', function( object ) {
    if (object[1] == 'dejavoo') {
        object[0].PAYMENT_TYPE = object[1];
        object[0].SOMME_PERCU = NexoAPI.ParseFloat( v2Checkout.CartToPay );
    }

    return object;
});

NexoAPI.events.addFilter( 'dejavoo_after_order_save', function( object ) {
    if (object[1] == 'dejavoo') {
        $scope.card.id = object[0].order_id;
        $scope.card.code = object[0].order_code;
        $scope.card.author = '<?php echo User::id();?>';
        NexoAPI.events.doAction( 'dejavoo_charged', $scope.card );
    }
    return object;
});

NexoAPI.events.addFilter( 'nexo_payments_types_object', function( object ) {
    object = _.extend( object, _.object( [ 'dejavoo' ], [{
        text            :       '<?php echo _s( 'Dejavoo Terminal', 'nexo-dejavoo-terminal' );?>',
        active          :       false,
        isCustom        :       true
    }] ) );
    return object;
});

NexoAPI.events.addAction( 'dejavoo_charged', function( data ) {

    // clear cart before payment
    v2Checkout.resetCart();
	$.ajax( '<?php echo site_url(array( 'api', 'nexopos', 'dejavoo', store_get_param( '?' ) ) );?>', {
        beforeSend : 	function(){
            v2Checkout.paymentWindow.showSplash();
            NexoAPI.Notify().success( '<?php echo _s('Veuillez patienter', 'nexo-dejavoo-terminal');?>', '<?php echo _s('Paiement en cours...', 'nexo-dejavoo-terminal');?>' );
        },
        type		:	'POST',
        dataType	:	"json",
        data		:	data,
        success		: 	function( data ) {
            if( data.status == 'payment_success' ) {
                v2Checkout.paymentWindow.close();
				NexoAPI.Notify().success( '<?php echo _s('Paiement effectué', 'nexo-dejavoo-terminal');?>', '<?php echo _s('Le paiement a été effectué.', 'nexo-dejavoo-terminal');?>' );
            } else if( data.status == 'try_later' ) {
                v2Checkout.paymentWindow.close();
				NexoAPI.Notify().success( '<?php echo _s('Try later', 'nexo-dejavoo-terminal');?>', '<?php echo _s('Order was placed to Hold list', 'nexo-dejavoo-terminal');?>' );
            } else if ( typeof data.error != 'undefined' ) {
                NexoAPI.Notify().error( '<?php echo _s('Error', 'nexo-dejavoo-terminal');?>', '<?php echo _s('Dejavoo Terminal response is : <br><strong>', 'nexo-dejavoo-terminal');?>' + data.error.message );
                v2Checkout.paymentWindow.hideSplash();
            }
        },
        error		:	function( data ){
            data			=	$.parseJSON( data.responseText );
            if( typeof data.error != 'undefined' ) {
                var message		=	data.error.message;
            } else if( typeof data.httpBody != 'undefined' ) {
                var message		=	data.jsonBody.error.message;
            } else {
                var message		=	data;
            }
            v2Checkout.paymentWindow.hideSplash();
            NexoAPI.Notify().error( '<?php echo _s('Error', 'nexo-dejavoo-terminal');?>', '<?php echo _s('Dejavoo Terminal response is : <br><strong>', 'nexo-dejavoo-terminal');?>' + message );
        }
    });
});
<?php endif; ?>
