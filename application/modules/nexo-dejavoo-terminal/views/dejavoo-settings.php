<?php
/**
 * Add support for Multi Store
 * @since 2.8
**/

global $store_id, $CurrentStore, $Options;

$option_prefix		=	'';

if( $store_id != null ) {
	$option_prefix	=	'store_' . $store_id . '_' ;
}

$this->Gui->col_width(1, 2);

$this->Gui->add_meta(array( 
	'namespace'    =>    'dejavoo_settings',
	'title'        =>    __('Réglages Dejavoo Terminal', 'nexo-dejavoo-terminal'),
	'col_id'    =>    1,
	'type'        =>    'box',
	'gui_saver'    =>    true,
	'use_namespace'    =>    false,
	'footer'        =>        array(
		'submit'    =>        array(
			'label'    =>        __('Sauvegarder les réglages', 'nexo-dejavoo-terminal')
		)
	)
));

$this->Gui->add_item(array(
	'type'        =>    'select',
	'name'        =>    $option_prefix	. 'nexo_enable_dejavoo_terminal',
	'label'        =>    __('Activer Dejavoo Terminal', 'nexo-dejavoo-terminal'),
	'options'    =>    array(
		'no'    =>    __('Non', 'nexo'),
		'yes'    =>    __('Oui', 'nexo')
	),
	'description'    =>    __('Désactiver Dejavoo Terminal empêchera au ressource de ce dernier de se charger dans l\'interface de la caisse.', 'nexo-dejavoo-terminal')
), 'dejavoo_settings', 1);

// Dejavoo Terminal URL
$this->Gui->add_item(array(
	'type'        =>    'text',
	'name'        =>    $option_prefix	. 'nexo_dejavoo_terminal_url',
	'label'        =>    __('Dejavoo Terminal URL', 'nexo-dejavoo-terminal'),
	'description'    =>  __('Contact dejavoosystems.com integration team to get correct URL.', 'nexo-dejavoo-terminal')
), 'dejavoo_settings', 1);

// Dejavoo Terminal Port
$this->Gui->add_item(array(
	'type'        =>    'text',
	'name'        =>    $option_prefix	. 'nexo_dejavoo_terminal_port',
	'label'        =>    __('Dejavoo Terminal Port', 'nexo-dejavoo-terminal'),
	'description'    =>  __('Contact dejavoosystems.com integration team to get correct port number.', 'nexo-dejavoo-terminal')
), 'dejavoo_settings', 1);

// Dejavoo Terminal Service Port
$this->Gui->add_item(array(
	'type'        =>    'text',
	'name'        =>    $option_prefix	. 'nexo_dejavoo_terminal_service_port',
	'label'        =>    __('Dejavoo Terminal Service Port', 'nexo-dejavoo-terminal'),
	'description'    =>  __('Contact dejavoosystems.com integration team to get correct service port number.', 'nexo-dejavoo-terminal')
), 'dejavoo_settings', 1);

// Register ID
$this->Gui->add_item(array(
	'type'        =>    'text',
	'name'        =>    $option_prefix	. 'nexo_dejavoo_register_id',
	'label'        =>    __('Register ID', 'nexo-dejavoo-terminal'),
	'description'    =>  __('Specifies the register ID.', 'nexo-dejavoo-terminal')
), 'dejavoo_settings', 1);

// Auth Key
$this->Gui->add_item(array(
	'type'        =>    'text',
	'name'        =>    $option_prefix	. 'nexo_dejavoo_auth_key',
	'label'        =>    __('Auth Key', 'nexo-dejavoo-terminal'),
	'description'    =>  __('Specifies the Auth-Key, which authorizes Host system to use the SPin Web Service.', 'nexo-dejavoo-terminal')
), 'dejavoo_settings', 1);

$this->Gui->add_item(array(
    'type'        =>    'select',
    'name'        =>    $option_prefix	. 'nexo_dejavoo_logs',
    'label'        =>    __('Enable Logs', 'nexo-dejavoo-terminal'),
    'options'    =>    array(
        'no'    =>    __('Non', 'nexo'),
        'yes'    =>    __('Oui', 'nexo')
    ),
    'description'    =>    __('Error log files are \'application/logs/dejavoo-terminal-XXXXXX.log\'.', 'nexo-dejavoo-terminal')
), 'dejavoo_settings', 1);


$this->Gui->output();