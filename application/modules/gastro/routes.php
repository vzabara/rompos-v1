<?php
global $Routes;

/**
 * To avoid inserting twice
 */
if ( ! is_multistore() ) {
    include( dirname( __FILE__ ) . '/route-config.php' );
}