<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nexo_Restaurant_Install extends Tendoo_Module
{
    /**
     * create gastro roles and permission
     * @param void
     * @return void
     */
    public function create_permissions()
    {
        Group::create(
            'gastro.chief',
            __( 'Chief', 'gastro' ),
            true,
            __( 'This role can manage the kitchen', 'gastro' )
        );
        
        Group::create(
            'gastro.waiter',
            __( 'Waiter', 'gastro' ),
            true,
            __( 'This role can manage the order', 'gastro' )
        );
        
        $permissions        =   [];
        $permissions[ 'gastro.view.tokitchen' ]         =   __( 'View the button "to kitchen"', 'gastro' );
        $permissions[ 'gastro.view.paybutton' ]         =   __( 'View the button "Pay"', 'gastro' );
        $permissions[ 'gastro.view.discountbutton' ]    =   __( 'View the discount button', 'gastro' );
        $permissions[ 'gastro.use.waiter-screen' ]      =   __( 'Use the waiter screen', 'gastro' );
        $permissions[ 'gastro.use.kitchen-screen' ]     =   __( 'Use the kitchen screen', 'gastro' );
        $permissions[ 'gastro.use.merge-meal' ]         =   __( 'Use the merging meal feature', 'gastro' );
        $permissions[ 'gastro.view.cancel-button' ]     =   __( 'View the item cancel button.', 'gastro' );
        
        foreach( $permissions as $namespace => $perm ) {
            $this->auth->create_perm( 
                $namespace,
                $perm
            );
            
            $this->auth->allow_group( 'master', $namespace );
            $this->auth->allow_group( 'admin', $namespace );
            $this->auth->allow_group( 'store.manager', $namespace );
        }
        
        $this->auth->allow_group( 'store.cashier', 'gastro.view.paybutton' );
        $this->auth->allow_group( 'store.cashier', 'gastro.view.tokitchen' );
        $this->auth->allow_group( 'store.cashier', 'gastro.view.cancel-button' );
        $this->auth->allow_group( 'gastro.chief', 'gastro.use.kitchen-screen' );
        $this->auth->allow_group( 'gastro.chief', 'nexo.enter.stores' );
        $this->auth->allow_group( 'gastro.waiter', 'gastro.use.waiter-screen' );
        $this->auth->allow_group( 'gastro.waiter', 'gastro.view.tokitchen' );
        $this->auth->allow_group( 'gastro.waiter', 'gastro.use.merge-meal' );
        $this->auth->allow_group( 'gastro.waiter', 'gastro.view.discountbutton' );
        $this->auth->allow_group( 'gastro.waiter', 'nexo.enter.stores' );
    }

    /**
    *  create tables
    *  @param string table prefix
    *  @return void
    **/
    public function create_tables( $prefix = '' )
    {
        // @deprecated
        $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $prefix . 'nexo_restaurant_rooms` (
           `ID` int(11) NOT NULL AUTO_INCREMENT,
           `NAME` varchar(200) NOT NULL,
           `DESCRIPTION` text NOT NULL,
           `DATE_CREATION` datetime NOT NULL,
           `DATE_MODIFICATION` datetime NOT NULL,
           `AUTHOR` int(11),
           PRIMARY KEY (`ID`)
         )' );

        $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $prefix . 'nexo_restaurant_tables` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `NAME` varchar( 200 )  NOT NULL,
            `DESCRIPTION` text NOT NULL,
            `MAX_SEATS` int( 11 ),
            `CURRENT_SEATS_USED` int(11),
            `STATUS` varchar(200),
            `SINCE` datetime not null,
            `BOOKING_START` datetime not null,
            `DATE_CREATION` datetime not null,
            `DATE_MODIFICATION` datetime not null,
            `AUTHOR` int(11) NOT NULL,
            `REF_AREA` int(11) NOT NULL,
            `CURRENT_SESSION_ID` int(11) NOT NULL,
            PRIMARY KEY (`ID`)
        )' );
                
        $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $prefix . 'nexo_restaurant_tables_sessions` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `REF_TABLE` int(11) NOT NULL,
            `SESSION_STARTS` datetime NOT NULL,
            `SESSION_ENDS` datetime NOT NULL,
            `AUTHOR` int(11) NOT NULL,
            PRIMARY KEY (`ID`)
        )' );
                    
        $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $prefix . 'nexo_restaurant_tables_relation_orders` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `REF_ORDER` int(11) NOT NULL,
            `REF_TABLE` int(11) NOT NULL,
            `REF_SESSION` int(11) NOT NULL,
            PRIMARY KEY (`ID`)
        )' );
                        
        $this->db->query( 'CREATE TABLE IF NOT EXISTS `' . $prefix . 'nexo_restaurant_areas` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `NAME` varchar(200) NOT NULL,
            `DESCRIPTION` text NOT NULL,
            `DATE_CREATION` datetime NOT NULL,
            `DATE_MODIFICATION` datetime NOT NULL,
            `AUTHOR` int(11) NOT NULL,
            `REF_ROOM` int(11) NOT NULL,
            PRIMARY KEY (`ID`)
        )' );

        $this->db->query('CREATE TABLE IF NOT EXISTS `'. $prefix .'nexo_restaurant_kitchens` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `NAME` varchar(200) NOT NULL,
            `DESCRIPTION` text NOT NULL,
            `AUTHOR` int(11) NOT NULL,
            `DATE_CREATION` datetime NOT NULL,
            `DATE_MOD` datetime NOT NULL,
            `REF_CATEGORY` varchar(200) NOT NULL,
            `REF_ROOM` int NOT NULL,
            `PRINTER` text NOT NULL,
            PRIMARY KEY (`ID`)
        )');

        $this->db->query('CREATE TABLE IF NOT EXISTS `'. $prefix .'nexo_restaurant_modifiers` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `NAME` varchar(200) NOT NULL,
            `DESCRIPTION` text NOT NULL,
            `AUTHOR` int(11) NOT NULL,
            `DATE_CREATION` datetime NOT NULL,
            `DATE_MODIFICATION` datetime NOT NULL,
            `REF_CATEGORY` int(11) NOT NULL,
            `DEFAULT` boolean NOT NULL,
            `PRICE` float(11) NOT NULL,
            `IMAGE` text NOT NULL,
            `SORT_ORDER` int(11) NOT NULL DEFAULT 0,
            PRIMARY KEY (`ID`)
        )');

        $this->db->query('CREATE TABLE IF NOT EXISTS `'. $prefix .'nexo_restaurant_modifiers_categories` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `NAME` varchar(200) NOT NULL,
            `DESCRIPTION` text NOT NULL,
            `AUTHOR` int(11) NOT NULL,
            `UPDATE_PRICE` boolean NOT NULL,
            `DATE_CREATION` datetime NOT NULL,
            `DATE_MODIFICATION` datetime NOT NULL,
            `FORCED` boolean NOT NULL,
            `MULTISELECT` boolean NOT NULL,
            PRIMARY KEY (`ID`)
        )');
                                        
        $table_fields   = $this->db->list_fields( $prefix . 'nexo_registers' );
        if( ! in_array( 'REF_KITCHEN', $table_fields ) ) {
            $this->db->query( 'ALTER TABLE `'. $prefix .'nexo_registers` ADD `REF_KITCHEN` INT NOT NULL AFTER `USED_BY`;');
        }
        
        $table_fields   = $this->db->list_fields( $prefix . 'nexo_articles' );
        if( ! in_array( 'REF_MODIFIERS_GROUP', $table_fields ) ) {
            $this->db->query( 'ALTER TABLE `'. $prefix .'nexo_articles` ADD `REF_MODIFIERS_GROUP` varchar(200) NOT NULL AFTER `BARCODE_TYPE`;');
        }
        
        $table_group        =   $this->db->list_fields( $prefix . 'nexo_commandes' );
        if( ! in_array( 'RESTAURANT_ORDER_TYPE', $table_group ) ) {
            $this->db->query( 'ALTER TABLE `'. $prefix .'nexo_commandes` ADD `RESTAURANT_ORDER_TYPE` varchar(200) NOT NULL AFTER `TYPE`;');
            $this->db->query( 'ALTER TABLE `'. $prefix .'nexo_commandes` ADD `RESTAURANT_ORDER_STATUS` varchar(200) NOT NULL AFTER `RESTAURANT_ORDER_TYPE`;');
            $this->db->query( 'ALTER TABLE `'. $prefix .'nexo_commandes` ADD `RESTAURANT_BOOKED_FOR` datetime NOT NULL AFTER `RESTAURANT_ORDER_STATUS`;');
        }
        
        $table_group        =   $this->db->list_fields( $prefix . 'nexo_commandes_produits' );
        if( ! in_array( 'RESTAURANT_PRODUCT_REAL_BARCODE', $table_group ) ) {
            $this->db->query( 'ALTER TABLE `'. $prefix .'nexo_commandes_produits` ADD `RESTAURANT_PRODUCT_REAL_BARCODE` varchar(200) NOT NULL AFTER `REF_PRODUCT_CODEBAR`;');
        }
        // $this->db->query( 'ALTER TABLE `'. $prefix .'nexo_commandes` ADD `REF_BOOKING` INT NOT NULL AFTER `BARCODE_TYPE`;');
        
        Modules::enable( 'gastro' );

        $this->options->set( 'nexo_restaurant_installed', true, true );
    }
    
    /**
    *  Delete Tables
    *  @param string table prefix
    *  @return void
    **/
    public function delete_tables( $table_prefix = '' )
    {
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_rooms`;');
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_tables`;');
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_areas`;');
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_kitchens`;');
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_modifiers`;');
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_table_sessions`;');
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_modifiers_categories`;');
        $this->db->query('DROP TABLE IF EXISTS `' . $table_prefix . 'nexo_restaurant_tables_relation_orders`;');
        
//        $table_fields   = $this->db->list_fields( $prefix . 'nexo_registers' );
//        if( $this->db->table_exists( $table_prefix . 'nexo_registers' ) && in_array( 'REF_KITCHEN', $table_fields ) ) {
//            $this->db->query( 'ALTER TABLE `' . $table_prefix . 'nexo_registers` DROP `REF_KITCHEN`;' );
//        }
//
//        $table_fields   = $this->db->list_fields( $prefix . 'nexo_articles' );
//        if( $this->db->table_exists( $table_prefix . 'nexo_articles' ) && in_array( 'REF_MODIFIERS_GROUP', $table_fields ) ) {
//            $this->db->query( 'ALTER TABLE `' . $table_prefix . 'nexo_articles` DROP `REF_MODIFIERS_GROUP`;' );
//        }
//
//        $table_fields   = $this->db->list_fields( $prefix . 'nexo_commandes' );
//        if( $this->db->table_exists( $table_prefix . 'nexo_commandes' ) && in_array( 'RESTAURANT_ORDER_TYPE', $table_fields ) ) {
//            $this->db->query( 'ALTER TABLE `' . $table_prefix . 'nexo_commandes` DROP `RESTAURANT_ORDER_TYPE`;' );
//        }
//
//        if( $this->db->table_exists( $table_prefix . 'nexo_commandes' ) && in_array( 'RESTAURANT_ORDER_STATUS', $table_fields ) ) {
//            $this->db->query( 'ALTER TABLE `' . $table_prefix . 'nexo_commandes` DROP `RESTAURANT_ORDER_STATUS`;' );
//        }
//
//        if( $this->db->table_exists( $table_prefix . 'nexo_commandes_produits' ) && in_array( 'RESTAURANT_PRODUCT_REAL_BARCODE', $table_fields ) ) {
//            $this->db->query( 'ALTER TABLE `' . $table_prefix . 'nexo_commandes_produits` DROP `RESTAURANT_PRODUCT_REAL_BARCODE`;' );
//        }
        
        $this->options->delete( 'nexo_restaurant_installed' );
    }
}
                                
