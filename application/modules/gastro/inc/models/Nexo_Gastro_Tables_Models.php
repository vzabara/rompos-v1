<?php
class Nexo_Gastro_Tables_Models extends CI_Model
{
    /**
     * Return Table used by a specific order
     * @param numeric order id
     * @return order
     */
    public function get_table_used( $order_id )
    {
        $tables     =   $this->db->select( '*,
        ' . store_prefix() . 'nexo_restaurant_tables.ID as TABLE_ID,
        ' . store_prefix() . 'nexo_restaurant_tables.ID as ID,
        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID' )
        ->from( store_prefix() . 'nexo_commandes' )
        ->join( store_prefix() . 'nexo_restaurant_tables_relation_orders', store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER = ' . store_prefix() . 'nexo_commandes.ID' )
        ->join( store_prefix() . 'nexo_restaurant_tables', store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE = ' . store_prefix() . 'nexo_restaurant_tables.ID' )
        ->where( store_prefix() . 'nexo_commandes.ID', $order_id )
        ->get()->result_array();
        return $tables;
    }

    /**
     * Get Item modifier
     * @param number commande_order id
     * @return void
     */
    public function get_modifiers( $item_id, $command_code ) 
    {
        $modifiers      =   $this->db
        ->where( 'REF_COMMAND_CODE', $command_code )
        ->where( 'REF_COMMAND_PRODUCT', $item_id )
        ->where( 'KEY', 'modifiers' )
        ->get( store_prefix() . 'nexo_commandes_produits_meta' )
        ->result_array();
        return @$modifiers[0][ 'VALUE' ] ? $modifiers[0][ 'VALUE' ] : '[]';
    }

    /**
     * Get Table area
     * @param int table id
     * @return array
     */
    public function get_table_area( $table_id ) 
    {
        $table      =   $this->get_table( $table_id );
        $this->db->select( '*, ' . store_prefix() . 'nexo_restaurant_areas.ID as AREA_ID' );
        $area       =   $this->db->where( 'ID', $table[ 'REF_AREA' ] )
        ->get( store_prefix() . 'nexo_restaurant_areas' )
        ->result_array();

        return @$area[0] ? $area[0] : [];
    }

    /**
     * Get Table
     * @param int table_id
     * @return array
     */
    public function get_table( $table_id )
    {
        $table      =   $this->db->where( 'ID', $table_id )
        ->get( store_prefix() . 'nexo_restaurant_tables' )
        ->result_array();
        return @$table[0] ? $table[0] : [];
    }

    /**
     * Get All tables
     * @return array of tables
     */
    public function get_tables()
    {
        $tables      =   $this->db
        ->get( store_prefix() . 'nexo_restaurant_tables' )
        ->result_array();
        return $tables;
    }

    /**
     * Change Table Statuts
     */
    public function table_status( $options ) 
    {
        $order      =   $this->db->where( 'ID', $options[ 'ORDER_ID' ] )
            ->get( store_prefix() . 'nexo_commandes' )->result_array();

        $table_id       =   $options[ 'TABLE_ID' ];

        // current table
        $table      =   $this->db->where( 'ID', $table_id )
            ->get( store_prefix() . 'nexo_restaurant_tables' )
            ->result_array();

        if( ! empty( $table ) ) {
            $data       =   [
                'CURRENT_SEATS_USED'    =>  $options[ 'CURRENT_SEATS_USED' ],
                'STATUS'                =>  $options[ 'STATUS' ]
            ];
    
            if( $data[ 'STATUS' ] === 'in_use' ) {
                // if the current order status is in_use, we assume the table has been opened before
                
                tendoo_debug( $table, 'nexo-gastro-tables-models-' . __LINE__ . '.json' );
                
                if( $table[0][ 'STATUS' ]   === 'in_use' ) {
                    $current_session_id     =   $table[0][ 'CURRENT_SESSION_ID' ];
                } else {
                    // we're placing order for the first time
                    // create session
                    $this->db->insert( store_prefix() . 'nexo_restaurant_tables_sessions', [
                        'REF_TABLE'         =>  $table_id,
                        'SESSION_STARTS'    =>  $options[ 'SESSION_STARTS' ] ?? date_now(),
                        'AUTHOR'            =>  User::id(),
                    ]);

                    // save last session id
                    $data[ 'CURRENT_SESSION_ID' ]       =   $this->db->insert_id();
                    // the table is placed for the first time
                    $data[ 'SINCE' ]        =   $options[ 'SINCE' ] ?? @$order[0][ 'DATE_MOD' ];
                    $current_session_id     =   $data[ 'CURRENT_SESSION_ID' ];
                }   
                
                // add table relation to order
                $this->db->insert( store_prefix() . 'nexo_restaurant_tables_relation_orders', [
                    'REF_ORDER'     =>  $options[ 'ORDER_ID' ],
                    'REF_TABLE'     =>  $table_id,
                    'REF_SESSION'   =>  $current_session_id
                ]);
            } else {
                // close table session
                $this->db->where( 'ID', $table[0][ 'CURRENT_SESSION_ID' ])
                    ->update( store_prefix() . 'nexo_restaurant_tables_sessions', [
                        'SESSION_ENDS'      =>  date_now(),
                        'AUTHOR'            =>  User::id(),
                    ]);

                // remove last session id
                $data[ 'CURRENT_SESSION_ID' ]       =   0; // reset last session id
                $data[ 'SINCE' ]                    =   $options[ 'SINCE' ] ?? '0000-00-00 00:00:00';
            }        
                
            $this->db->where( 'ID', $table_id )
                ->update( store_prefix() . 'nexo_restaurant_tables', $data );
            
            // return new status
            $table      =   $this->db->where( 'ID', $table_id )
                ->get( store_prefix() . 'nexo_restaurant_tables' )
                ->result_array();

            return $table[0];
        }
        return [];
    }

    /**
     * Remove Order from table history
     * @param int order id
     * @return Active Record Object
     */
    public function remove_from_history( $order_id, $table_id ) 
    {
        return $this->db->where( 'REF_TABLE', $table_id )
        ->where( 'REF_ORDER', $order_id )
        ->delete( store_prefix() . 'nexo_restaurant_tables_relation_orders' );
    }

    /**
     * Unbind order from any history
     * @param int order id
     * @return Active Record Object
     */
    public function unbind_order( $order_id, $table_id ) 
    {
        return $this->db
            ->where( 'REF_ORDER', $order_id )
            ->delete( store_prefix() . 'nexo_restaurant_tables_relation_orders' );
    }

    /**
     * Push Order to a table
     * @param int table id
     * @param array of orders id
     * @return void
     */
    public function push_order( $table_id, $orders_id )
    {        
        $last_session   =   $this->last_table_session( $table_id );

        $datas           =   [];

        if ( is_array( $orders_id ) ) {
            foreach( $orders_id as $order_id ) {
                $datas[]     =   [
                    'REF_TABLE'     =>  $table_id,
                    'REF_SESSION'   =>  $last_session[0][ 'ID' ],
                    'REF_ORDER'     =>  $order_id
                ];
            }
        } else {
            $datas[]     =   [
                'REF_TABLE'     =>  $table_id,
                'REF_SESSION'   =>  $last_session[0][ 'ID' ],
                'REF_ORDER'     =>  $orders_id
            ];
        }

        $this->db
            ->insert_batch( store_prefix() . 'nexo_restaurant_tables_relation_orders', $datas );
    }

    /**
     * bind order to table
     * @param int order id
     * @param int table id
     * @return void
     */
    public function bind_order( $order_id, $table_id, $seat_used )
    {
        $table  =   $this->get_table( $table_id );

        if ( $table ) {
            /**
             * if the table is currently in use, then we only have to change the relation with that order
             */
            if ( $table[ 'STATUS' ] == 'in_use' ) {

                /**
                 * If a session is defined
                 */
                $last_session   =   $this->last_table_session( $table_id );

                if ( $last_session ) {
                    $this->db->where( 'REF_ORDER', $order_id )->update( store_prefix() . 'nexo_restaurant_tables_relation_orders', [
                        'REF_TABLE'     =>  $table_id,
                        'REF_SESSION'   =>  $last_session[0][ 'ID' ]
                    ]);
                }
            } else if ( $table[ 'STATUS' ] == 'available' ) {

                /**
                 * if the table is not in use
                 * well crete a session for it
                 */
                $this->db->insert( store_prefix() . 'nexo_restaurant_tables_sessions', [
                    'REF_TABLE'         =>  $table_id,
                    'SESSION_STARTS'    =>  date_now(),
                    'AUTHOR'            =>  User::id()
                ]);

                $session_id         =   $this->db->insert_id();

                /**
                 * Let's create a relation with that table
                 */
                $this->db->insert( store_prefix() . 'nexo_restaurant_tables_relation_orders', [
                    'REF_ORDER'     =>  $order_id,
                    'REF_TABLE'     =>  $table_id,
                    'REF_SESSION'   =>  $session_id
                ]);

                /**
                 * update table status
                 */
                $this->db->where( 'ID', $table_id )->update( store_prefix() . 'nexo_restaurant_tables', [
                    'STATUS'                =>  'in_use',
                    'CURRENT_SEATS_USED'    =>  $seat_used,
                    'SINCE'                 =>  date_now(),
                ]);

                /**
                 * Update relation
                 */
                $this->db->where( 'REF_ORDER', $order_id )->update( store_prefix() . 'nexo_restaurant_tables_relation_orders', [
                    'REF_TABLE'     =>  $table_id,
                    'REF_SESSION'   =>  $session_id
                ]);
            }
            return true;
        }
        return false;
    }

    /**
     * Get table settings id
     * @return array of table session
     */
    public function last_table_session( $table_id )
    {
        $table_sessions     =   $this->db->where( 'REF_TABLE', $table_id )
            ->order_by( 'ID', 'desc' )
            ->limit(1)
            ->get( store_prefix() . 'nexo_restaurant_tables_sessions' )
            ->result_array();
        
        return $table_sessions;    
    }

    /**
     * delete last table session
     * @param int table id
     * @return true
     */
    public function delete_table_session( $table_id ) 
    {
        $lastSession    =   $this->last_table_session( $table_id );

        if ( $lastSession ) {
            $this->free_table( $table_id );
    
            /**
             * Delete all relation to that session
             */
            $this->db->where( 'REF_SESSION', $lastSession[0][ 'ID' ] )
                ->delete( store_prefix() . 'nexo_restaurant_tables_relation_orders' );
    
            /**
             * Delete the session for this table
             */
            $this->db->where( 'REF_TABLE', $table_id )
                ->order_by( 'ID', 'desc' )
                ->limit(1)
                ->delete( store_prefix() . 'nexo_restaurant_tables_sessions' );
        }
        return true;
    }

    /**
     * delete session
     * @param int session id
     * @return void
     */
    public function delete_session( $session_id )
    {
        /**
         * might need to delete a session as well
         */
        $this->db->where( 'ID', $session_id )
            ->delete( store_prefix() . 'nexo_restaurant_tables_sessions' );
        $this->db->where( 'REF_SESSION', $session_id )
            ->delete( store_prefix() . 'nexo_restaurant_tables_relation_orders' );
    }

    /**
     * Free a table by closing his session
     * @param int table id
     * @return void
     */
    public function free_table( $table_id )
    {
        /**
        * If a session is defined
        */
        $this->close_session( $table_id );

        /**
         * Closing the usage of the table as well
         */
        $this->db->where( 'ID', $table_id )->update( store_prefix() . 'nexo_restaurant_tables', [
            'SINCE'                 =>  '0000-00-00 00:00:00',
            'CURRENT_SEATS_USED'    =>  0,
            'STATUS'                =>  'available'
        ]);
    }

    /**
     * close table session
     * @param int session id
     * @return void
     */
    public function close_session( $table_id )
    {
        /**
        * If a session is defined
        */
        $last_session   =   $this->last_table_session( $table_id );

        // close table session
        $this->db->where( 'ID', $last_session[0][ 'ID' ] )
        ->update( store_prefix() . 'nexo_restaurant_tables_sessions', [
            'SESSION_ENDS'      =>  date_now(),
            'AUTHOR'            =>  User::id(),
        ]);
    }

    /**
     * get table session
     * @param int table session id
     * @return array
     */
    public function get_table_session( $id )
    {
        return $table_sessions     =   $this->db
            ->where( 'ID', $id )
            ->get( store_prefix() . 'nexo_restaurant_tables_sessions' )
            ->result_array();
        
        return $table_sessions;    
    }

    /**
     * get settings orders
     * @param int session id
     * @return array
     */
    public function get_session_orders( $session_id )
    {
        $session    =   $this->get_table_session( $session_id );

        if ( $session ) {
            $ordersRelation     =   $this->db->where( 'REF_SESSION', $session_id )
                ->get( store_prefix() . 'nexo_restaurant_tables_relation_orders' )
                ->result_array();

            if ( $ordersRelation ) {
                $orders     =   [];
                foreach( $ordersRelation as $relation ) {
                    $orders[]   =   $this->db->where( 'ID', $relation[ 'REF_ORDER' ] )
                        ->get( store_prefix() . 'nexo_commandes' )
                        ->result_array();
                }

                return $orders;
            }
        }  
        return [];
    }
}