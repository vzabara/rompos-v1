<?php
class Nexo_Gastro_Appointment_model extends grocery_CRUD_model
{
    /**
     * Get appointment
     * @param int $appointment_id
     * @return array
     */
    public function get_appointment( $appointment_id )
    {
        $appointments      =   $this->db->where( 'id', $appointment_id )
        ->get( store_prefix() . 'nexo_restaurant_appointments' )
        ->result_array();
        return @$appointments[0] ? $appointments[0] : [];
    }

    /**
     * Get All appointments
     * @return array of appointments
     */
    public function get_appointments()
    {
        $appointments      =   $this->db
        ->get( store_prefix() . 'nexo_restaurant_appointments' )
        ->result_array();
        return $appointments;
    }
}