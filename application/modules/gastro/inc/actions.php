<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once( dirname( __FILE__ ) . '/install.php' );
include_once( dirname( __FILE__ ) . '/controller.php' );

class Nexo_Restaurant_Actions extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();

        $this->install      =   new Nexo_Restaurant_Install;
    }

    public function create_permissions()
    {
        $this->install->create_permissions();
    }

    /**
     *  Enable module
     *  @param string module namespace
     *  @return void
    **/
    public function enable_module( $module )
    {
        if( $module == 'gastro' ) {
            global $Options;

            // if module is not yet installed
            if( @$Options[ 'nexo_restaurant_installed' ] == null ) {

                $this->options->set( 'nexo_restaurant_installed', true, true );
                $this->install->create_permissions();
                $this->load->model( 'Nexo_Stores' );
                $stores         =   $this->Nexo_Stores->get();

                array_unshift( $stores, [
                    'ID'        =>  0
                ]);

                foreach( $stores as $store ) {
                    $store_prefix       =   $this->db->dbprefix . ( $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_' );
                    $this->install->create_tables( $store_prefix );
                }
                                
            }
        }
    }

    /**
     * Load frontend
     * @since 2.3.13
     */
    public function load_frontend( $segments, $uri )
    {
        switch( $uri ) {
            case 'cron/reset-gastro-demo':
                $this->load->model( 'Nexo_Misc' );
                get_instance()->Nexo_Misc->enable_demo( 'gastro' );
                echo json_encode([
                    'status'    =>  'success',
                    'message'   =>  __( 'The demo has been reset.', 'gastro' )
                ]);
            break;
        }
    }

    /**
     *  Load dashboard
     *  @param void
     *  @return void
    **/

    public function load_dashboard()
    {
        $this->load->module_config( 'gastro' );
        $nexo_item_tabs         =  $this->config->item( 'nexo_item_stock_group' );
        $nexo_item_tabs[]       = 'REF_MODIFIERS_GROUP';

        $this->config->set_item( 'nexo_item_stock_group', $nexo_item_tabs );

        /**
         * Let's check if a multistore is enabled 
         * and if Gastro is correctly installed on this system
         */
        if ( is_multistore() ) {
            if ( store_option( 'gastro_installed', null ) === null ) {
                $this->notice->push_notice( tendoo_info( 
                    sprintf( 
                        __( 'It seems like Gastro tables aren\'t installed on this store. <a href="%s">Click here to install it</a>?', 'gastro' ),
                        site_url([ 'dashboard', store_slug(), 'gastro', 'install-tables' ])
                    )
                ) );
            }
        }

        // enqueue styles
        $this->enqueue->css_namespace( 'dashboard_header' );
        $this->enqueue->css( 'bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min', module_url( 'gastro' ) );
        
        $this->enqueue->js_namespace( 'dashboard_footer' );
        $this->enqueue->js( 'bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min', module_url( 'gastro' ) );
        $this->enqueue->js( 'js/masonry.pkg', module_url( 'gastro' ) );
        // $this->enqueue->js( 'js/imagesloaded.pkgd.min', module_url( 'gastro' ) );
        // $this->enqueue->js( 'js/angular-masonry', module_url( 'gastro' ) );
        switch( get_option( 'site_language' ) ) {
            case 'fr_FR':
            $this->enqueue->js( 'bower_components/moment/locale/fr', module_url( 'gastro' ) );
            break;
            case 'en_US':
            $this->enqueue->js( 'bower_components/moment/locale/en-gb', module_url( 'gastro' ) );
            break;
            case 'tr_TR':
            $this->enqueue->js( 'bower_components/moment/locale/tr', module_url( 'gastro' ) );
            break;
            case 'es_ES':
            $this->enqueue->js( 'bower_components/moment/locale/es', module_url( 'gastro' ) );
            break;
            case 'de_DE':
            $this->enqueue->js( 'bower_components/moment/locale/de', module_url( 'gastro' ) );
            break;
        }
        // hold order datepicker
        $this->enqueue->css('../modules/nexo/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min');
        $this->enqueue->js( '../modules/nexo/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min' );
    }

    /**
     *  dashboard footer
     *  @param void
     *  @return void
    **/

    public function dashboard_footer()
    {
        global $PageNow;

        if( $PageNow == 'nexo/registers/__use') {
            $this->load->module_view( 'gastro', 'directives.order-type' );
            $this->load->module_view( 'gastro', 'directives.table-history' );
            $this->load->module_view( 'gastro', 'directives.booking-ui' );
            $this->load->module_view( 'gastro', 'directives.table-status' );
            $this->load->module_view( 'gastro', 'directives.restaurant-rooms' );
            $this->load->module_view( 'gastro', 'register-footer' );
            $this->load->module_view( 'gastro', 'combo/combo-script' ); // rename it to modifier directive
            $this->load->module_view( 'gastro', 'waiters.screen' );
            $this->load->module_view( 'gastro', 'ready-items.script' );
            $this->load->module_view( 'gastro', 'merging.script' );
            $this->load->module_view( 'gastro', 'splitting.script' );
        }
    }

    /**
     *  Store Install Tables
     *  @param void
     *  @return void
    **/

    public function store_install_tables( $table_prefix )
    {
        $this->install->create_tables( $table_prefix );
    }

    /**
     *  Store Delete Table
     *  @param void
     *  @return void
    **/

    public function store_delete_tables( $store_prefix )
    {
        $this->install->delete_tables( $store_prefix );
    }

    /**
     *  Remove Module
     *  @param string module namespace
     *  @return void
    **/

    public function remove_module( $module_namespace )
    {
        if ( $module_namespace === 'gastro') {

            $this->load->model( 'Nexo_Stores' );

            $stores         =   $this->Nexo_Stores->get();

            array_unshift( $stores, [
                'ID'        =>  0
            ]);

            foreach( $stores as $store ) {
                $store_prefix       =   $this->db->dbprefix . ( $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_' );
                $this->install->delete_tables( $store_prefix );
            }
        }
    }

    /**
     *  enable demo
     *  @param string demo name
     *  @return void
    **/

    public function enable_demo( $demo )
    {
        if( $demo == 'gastro' ) {
            $this->load->module_view( 'gastro', 'demo' );
        }        
    }

    /**
     *  Empty Shop
     *  @param void
     *  @return void
    **/

    public function empty_shop()
    {
        $table_prefix   =   $this->db->dbprefix . store_prefix();

        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_rooms' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_rooms`;');
        }
        
        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_modifiers' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_modifiers`;');
        }
        
        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_modifiers_categories' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_modifiers_categories`;');
        }

        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_tables' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_tables`;');
        }

        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_areas' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_areas`;');
        }

        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_kitchens' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_kitchens`;');
        }

        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_tables_relation_orders' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_tables_relation_orders`;');
        }

        if( $this->db->table_exists( $table_prefix . 'nexo_restaurant_tables_sessions' ) ) {
            $this->db->query('TRUNCATE `' . $table_prefix . 'nexo_restaurant_tables_sessions`;');
        }
    }

    /**
     * Inject Data on v2Checkout
     * @param array order details
     * @return string vue
     */
    public function edit_loaded_order( $order ) 
    {
        $this->load->module_view( 'gastro', 'register.edit-loaded-order', [ 'order' => $order[ 'order' ][0] ]);
    }

    /**
     * Add modifier on the receipt for an item
     * @since 2.3.19
     * @return void
     */
    public function nps_after_item_line( $product )
    {
        return $this->load->module_view( 'gastro', 'print.nps-receipt-modifiers', compact( 'product' ) );
    }
}
