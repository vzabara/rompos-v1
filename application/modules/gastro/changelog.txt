v2.3.21
=======
- Added : Kitchen name on RomPOS Agent receipt
- Fixed : using the same category for more than 1 kitchen didn't allow the item to be printed on each kitchen
v2.3.12
======
- Fixed : changing table when areas are disabled.
v2.3.4
======
- Added : Split order features
- Some bug fixes
- Improving the workflow
v2.1.1
======
* Added : include assets
v2.1
======
* Fix : Multiple modifier window when internet is slow.
* Fix : Remove RomPOS core files
v.2.0
======
* Refactored Script
v1.4.4
======
* Added : Collect meal individually
v1.4.3
======
* Fix : kitchen view when single kitchen is enabled
* Fix : Hidding kitchen when it's disabled
* Added : Hide waiter screen.