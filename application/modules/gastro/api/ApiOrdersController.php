<?php

use Curl\Curl;

class ApiOrdersController extends Tendoo_Api
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get daily orders
     * @return json
     */
    public function daily( $type = null, $status = null, $restaurant_type = null )
    {
        $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );
        $this->db->select( '*' );

        if( $type != null ) {
            switch( $type ) {
                case 'unpaid'   :   
                    $this->db->where( store_prefix() . 'nexo_commandes.TYPE', 'nexo_order_devis' );
                break;
            }            
        }

        if( @$_GET[ 'restaurant_type' ] != null ) {
            $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE', $this->input->get( 'restaurant_type' ) );
        }

        if( $status != null ) {
            $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', $status );
        }

        $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
        
        $orders     =   $this->db->get( store_prefix() . 'nexo_commandes' )
        ->result_array();

        /**
         * Adding items on the process
         */
        foreach( $orders as &$order ) {
            $order[ 'items' ]   =   $this->db->where( 'REF_COMMAND_CODE', $order[ 'CODE' ] )
                ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' )
                ->get( store_prefix() . 'nexo_commandes_produits' )
                ->result_array();

            /**
             * get orders table
             */
            $relation                   =    $this->db->where( 'REF_ORDER', $order[ 'ID' ] )
                ->order_by( 'ID', 'desc' )
                ->get( store_prefix() . 'nexo_restaurant_tables_relation_orders' )
                ->result_array();

            $order[ 'table_relation' ]      =  @$relation[0];
            
            /**
             * assign table to a relation
             */
            $table                      =   $this->db->where( 'ID', $order[ 'table_relation' ][ 'REF_TABLE' ] )
                ->get( store_prefix() . 'nexo_restaurant_tables' )
                ->result_array();
                
            $order[ 'table' ]        =   @$table[0];

            /**
             * join customer
             */
            $customer               =   $this->customer_model->get( $order[ 'REF_CLIENT' ]);
            $order[ 'customer' ]    =   @$customer[0];
        }

        return $this->response( $orders );
    }

    /**
     * Move order from one table to another table
     * @return json response
     */
    public function moveOrder()
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $this->load->module_model( 'nexo', 'NexoLogModel', 'log_model' );
        
        $order_id   =   $this->post( 'order_id' );
        $table_id   =   $this->post( 'table_id' );
        $seat_used  =   $this->post( 'seat_used' );
        $lastTable  =   $this->gastro_model->get_table_used( $order_id );

        /**
         * Perform binding
         */
        if ( $this->gastro_model->bind_order( $order_id, $table_id, $seat_used ) ) {

            /**
             * delete last table session
             * if the table doesn't have any other orders on the session
             */
            $session            =   $this->gastro_model->last_table_session( $lastTable[0][ 'ID' ] );
            $sessionOrders      =   $this->gastro_model->get_session_orders( $session[0][ 'ID' ] );

            /**
             * if the session has'nt any order
             */
            tendoo_debug( $session, 'table-session.json' );
            tendoo_debug( $sessionOrders, 'session-orders.json' );

            if( count( $sessionOrders ) === 0 ) {
                $this->gastro_model->delete_table_session( $lastTable[0][ 'ID' ] );
            }
        
            $this->log_model->log( 
                __( 'Gastro &mdash; Move Orders', 'gastro' ), 
                sprintf( 
                    __( 'The order with id %s has been moved from %s to table with ID : %s by %s' ),
                    $order_id,
                    $lastTable[0][ 'NAME' ],
                    $table_id,
                    User::pseudo()
                )
            );
            
            return $this->response([
                'status'    =>  'success',
                'message'   =>  __( 'The order was successfully bound to the table', 'gastro' )
            ]);
        }

        /**
         * We were unable to find the table
         */
        return $this->response([
            'status'    =>  'failed',
            'message'   =>  __( 'Unable to find that order', 'gastro' )
        ]);
    }

    /**
     * Split order
     * @return json
     */
    public function split()
    {
        $order  =   $this->post( 'order' );
        $this->load->config( 'rest' );
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_model( 'nexo', 'NexoLogModel', 'log_model' );
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'order_model' );

        /**
         * get previous order details
         */
        $table      =   $this->gastro_model->get_table_used( $order[ 'ID' ] );

        /**
         * Delete order with items
         */
        $this->Nexo_Checkout->commandes_delete( $order[ 'ID' ] );  

        /**
         * Since Nexo_Checkout::commandes_delete doesn't delete the order him self
         */
        $this->db->where( 'ID', $order[ 'ID' ])->delete( store_prefix() . 'nexo_commandes' );    


        /**
         * Let's create new orders 
         * first
         */
        $response       =   [];

        foreach( $this->post( 'parts' ) as $partOrder ) {

            foreach( $partOrder[ 'items' ] as &$item ) {
                $item[ 'codebar' ]              =   $item[ 'RESTAURANT_PRODUCT_REAL_BARCODE' ];
                $item[ 'qte_added' ]            =   $item[ 'QTE_ADDED' ];
                $item[ 'sale_price' ]           =   $item[ 'PRIX' ];
                $item[ 'stock_enabled' ]        =   $item[ 'STOCK_ENABLED' ];
                $item[ 'discount_type' ]        =   @$item[ 'DISCOUNT_TYPE' ] ? $item[ 'DISCOUNT_TYPE' ] : '';
                $item[ 'discount_amount' ]      =   @$item[ 'DISCOUNT_AMOUNT' ] ? $item[ 'DISCOUNT_AMOUNT' ] : 0;
                $item[ 'discount_percent' ]     =   @$item[ 'DISCOUNT_PERCENT' ] ? $item[ 'DISCOUNT_PERCENT' ] : '';
                $item[ 'name' ]                 =   @$item[ 'NAME' ] ? $item[ 'NAME' ] : '';
                $item[ 'inline' ]               =   @$item[ 'INLINE' ] ? $item[ 'INLINE' ] : '';
                $item[ 'alternative_name' ]     =   @$item[ 'ALTERNATIVE_NAME' ] ? $item[ 'ALTERNATIVE_NAME' ] : '';
            }

            $_response   =   $this->Nexo_Checkout->postOrder( array(
                'RISTOURNE'             =>  0,
                'REMISE'                =>  0,
                'REMISE_PERCENT'        =>  0,
                'REMISE_TYPE'           =>  0,
                'RABAIS'                =>  0,
                'GROUP_DISCOUNT'        =>  0,
                'AUTHOR'                =>  0,
                'PAYMENT_TYPE'          =>  0,
                'TVA'                   =>  0,
                'DISCOUNT_TYPE'         =>  'disable', // by default discount are disabled when we're splitting orders
                'SHIPPING_AMOUNT'       =>  0,
                'REF_SHIPPING_ADDRESS'  =>  0,
                'REF_TAX'               =>  0,
                'TOTAL'                 =>  $partOrder[ 'total' ],
                'SOMME_PERCU'           =>  0,
                'TYPE'                  =>  $order[ 'TYPE' ],
                'REF_CLIENT'            =>  $partOrder[ 'customer' ],
                'HMB_DISCOUNT'          =>  0,
                'ITEMS'                 =>  $partOrder[ 'items' ],
                'payments'              =>  [],
                'shipping'              =>  [
                    'id'                =>  $order[ 'REF_SHIPPING_ADDRESS' ],
                    'price'             =>  0
                ],
                'metas'                 =>  [
                    'table_id'          =>  $table[0][ 'ID' ],
                    'room_id'           =>  0,
                    'area_id'           =>  $table[0][ 'REF_AREA' ],
                    'seat_used'         =>  $table[0][ 'CURRENT_SEATS_USED' ] ?? 0,
                    'order_real_type'   =>  'dinein' // hardcoded for the meantime
                ],
                'DESCRIPTION'           =>  $order[ 'DESCRIPTION' ] ? $order[ 'DESCRIPTION' ] : '',
                'REGISTER_ID'           =>  $order[ 'REF_REGISTER' ],
                'REF_TAX'               =>  $order[ 'REF_TAX' ],
                'TITRE'                 =>  sprintf( __( 'Splitted Dine in on %s'), $table[0][ 'NAME' ] ),
                'RESTAURANT_ORDER_STATUS'   =>  'pending',
                'RESTAURANT_ORDER_TYPE'   =>  @$order[ 'RESTAURANT_ORDER_TYPE' ] ? $order[ 'RESTAURANT_ORDER_TYPE' ] : 'dinein',
            ), User::id() );

            /**
             * let's recalculate the order 
             * to make the tax calculated accordingly
             */
            $this->order_model->recalculateOrder( $_response[ 'current_order' ][0][ 'ID' ], []);

            /**
             * Pushing Response to the resonse array
             */
            $response[]   =   $_response;
        }
        
        /**
         * Deleting previous order from the table session
         */
        $this->db->where( 'REF_ORDER', $order[ 'ID' ] )
            ->delete( store_prefix() . 'nexo_restaurant_tables_relation_orders' );

        $this->log_model->log( 
            __( 'Gastro &mdash; Order Splitted', 'gastro' ), 
            sprintf( 
                __( 'The order %s has been splitted in %s parts by %s' ),
                $order[ 'CODE' ],
                count( $this->post( 'parts' ) ),
                User::pseudo()
            )
        );

        return $this->response( $response );
    }

    /**
     * Queue bookend order when the
     * time reached. Set the order to pending
     * @return json
     */
    public function queueBooked()
    {
        $order  =   $this->post( 'order' );

        $this->db->where( 'ID', $order[ 'ORDER_ID' ] )
            ->update( store_prefix() . 'nexo_commandes', [
                'RESTAURANT_ORDER_STATUS'   =>  'pending'
            ]);

        return $this->response([
            'status'    =>  'success',
            'message'   =>  __( 'The order has been placed to the pending order queue.', 'gastro' )
        ]);
    }
}