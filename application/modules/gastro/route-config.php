<?php
if ( is_multistore() && multistore_enabled() ) {
    global $StoreRoutes;
    $Routes     =   $StoreRoutes;
} else {
    global $Routes;
}

$Routes->get( '/gastro/revoke', 'RestaurantController@revoke' );
$Routes->get( '/gastro/callback', 'RestaurantController@callback' );
$Routes->get( '/gastro/settings', 'RestaurantController@settings' );
$Routes->get( '/gastro/templates/{name}', 'RestaurantController@templates' );
$Routes->get( '/gastro/install-tables', 'RestaurantController@installTables' );

$Routes->match([ 'get', 'post' ], '/gastro/mod-groups/{params?}/{id?}', 'ModifiersController@modifiers_groups', ['defaultParameterRegex' => '[\w|.|-]+']);
$Routes->match([ 'get', 'post' ], '/gastro/modifiers/{param?}/{id?}/{filename?}', 'ModifiersController@modifiers', ['defaultParameterRegex' => '[\w|.|-]+']);

$Routes->match([ 'get', 'post' ], '/gastro/kitchen-screen/{id?}', 'KitchensController@kitchenScreen' );
$Routes->match([ 'get', 'post' ], '/gastro/kitchens/{param?}/{id?}', 'KitchensController@kitchens' );
$Routes->match([ 'get', 'post' ], '/gastro/waiters-screen/{id?}', 'KitchensController@waiterScreen' );
$Routes->match([ 'get', 'post' ], '/gastro/appointments/{param?}/{id?}', 'AppointmentsController@lists' );
$Routes->match([ 'get', 'post' ], '/gastro/tables/{param?}/{id?}', 'TablesController@tables' );
$Routes->match([ 'get', 'post' ], '/gastro/areas/{param?}/{id?}', 'AreasController@areas' );
