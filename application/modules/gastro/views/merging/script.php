<script>
tendooApp.controller( 'mergingCTRL', [ '$rootScope', '$scope', '$timeout', '$compile', '$http', '$filter', 
    function( $rootScope, $scope, $timeout, $compile, $http, $filter ){
    
    $scope.orders               =   [];
    $scope.showTable            =   false;
    $scope.showSeats            =   false;
    $scope.selectedSeat         =   0;
    $scope.selectedCustomer     =   0;
    $scope.showCustomers        =   false;
    $scope.customers            =   gastroSplitData.customers;

    $scope.orderTypes       =   [{
        namespace   :   'dinein',
        icon        :   'fa fa-cutlery',
        label       :   '<?php echo _s( 'Dine in', 'gastro' );?>'
    },{
        namespace   :   'takeaway',
        icon        :   'fa fa-shopping-bag',
        label       :   '<?php echo _s( 'Take Away', 'gastro' );?>'
    },{
        namespace   :   'delivery',
        icon        :   'fa fa-truck',
        label       :   '<?php echo _s( 'Delivery', 'gastro' );?>'
    }];

    $scope.selectedTable    =       false;
    $scope.tables           =   <?php echo json_encode( 
        $this->db->get( store_prefix() . 'nexo_restaurant_tables' )
        ->result_array()
    );?>

    /**
     * Open merging order
     * @return void
     */
    $scope.openMergingOrder     =   function(){
        NexoAPI.Bootbox().dialog({
            title     :   '<?php echo __( 'Merging Order', 'gastro' );?>',
            message       :   `
            <div class="row merging-wrapper" style="
                    display: flex;
                    flex-grow: 1;
                    margin:0;
                ">
                
            </div>
            `,
            buttons     :   {
                ok          :   {
                    label       :   '<?php echo _s( 'Close', 'gastro' );?>',
                    className   :   'btn btn-default'
                }
            },
            animate         :   false,
            className       :   'merging-box'
        });

        $scope.windowHeight				=	window.innerHeight;
        $scope.wrapperHeight			=	$scope.windowHeight - ( ( 56 * 2 ) + 30 );

        let content         =   `
        <div class="col-md-8 col-sm-6 col-xs-6 no-padding" style="height: 100%;display: flex;flex-grow: initial;flex-direction: row;">
            <div class="row" style="height:100%;background: #ecebeb;width: 100%;margin:0;display:flex;flex-direction:column">
                <div class="col-md-12 search-order-field" style="padding:15px;">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button ng-click="loadOrders()" class="btn btn-default" type="button">
                                <i class="fa fa-refresh"></i> 
                            </button>
                        </span>
                        <span class="input-group-addon" id="basic-addon1"><?php echo __( 'Search Order', 'gastro' );?></span>
                        <input ng-model="searchOrder" type="text" class="form-control" placeholder="<?php echo __( 'Order Code', 'gastro' );?>" aria-describedby="basic-addon1">
                        
                    </div>
                </div>
                <div class="col-md-12 no-padding" style="display:flex;flex-grow:1;overflow-y:scroll;flex-direction:column;flex-basis: 0;">
                    <div class="row order-container" style="margin:0;padding-top:5px;">
                        <div class="col-md-4" ng-repeat="order in orders | filter: searchOrder">
                            <div class="box" ng-click="selectOrder( order )" ng-class="{ 'order-selected' : order.selected }">
                                <div class="box-header with-border">
                                    <strong>{{ order.CODE }} <span class="pull-right">{{ getType( order ) }}</span></strong>
                                </div>
                                <div class="box-body no-padding">
                                    <ul class="list-group" style="margin-bottom: 0px;">
                                        <li class="list-group-item no-border-lr"><?php echo __( 'Customer', 'gastro' );?> <span class="pull-right">{{ order.customer.NOM }}</li>
                                        <li class="list-group-item no-border-lr"><?php echo __( 'Total', 'gastro' );?> <span class="pull-right">{{ order.TOTAL | moneyFormat }}</span></li>
                                        <li class="list-group-item no-border-lr"><?php echo __( 'Table', 'gastro' );?> <span class="pull-right">{{ order.table.NAME == null ? '---' : order.table.NAME }}</span></li>
                                        <li class="list-group-item no-border-lr"><?php echo __( 'Created', 'gastro' );?> <span class="pull-right">{{ order.DATE_CREATION | date }}</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div ng-show="orders.length == 0" class="col-md-12 text-center">
                            <?php echo tendoo_info( __( 'No orders available', 'gastro' ) );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 no-padding">
            <div>
                <h3 class="text-center"><?php echo __( 'Merging Selected Orders : {{ countSelected() }}', 'gastro' );?></h3>
                <div style="padding:15px" ng-show="countSelected() > 1">
                    <h4 class="text-center" ng-show="! showTable && ! showSeats && ! showCustomers"><?php echo __( 'Which order type should be used ?', 'gastro' );?></h4>
                    <div class="row">
                        <div class="col-md-6" ng-show="! showTable && ! showSeats && ! showCustomers" ng-repeat="orderType in orderTypes" ng-click="selectType( orderType )">    
                            <div class="box">
                                <div class="box-body text-center" ng-class="{
                                    'type-selected'     :   orderType.selected,
                                    'type-unselected'    :   ! orderType.selected
                                }">
                                    <h3>
                                        <i ng-class="orderType.icon"></i>
                                    </h3>
                                    <p>
                                        {{ orderType.label }} 
                                        <span ng-show="orderType.namespace == 'dinein' && selectedTable != false">
                                            <?php echo sprintf( __( 'on %s', 'gastro' ), '<strong>{{ selectedTable.NAME }}</strong>[{{ selectedSeat }}]' );?>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" ng-show="showTable">
                            <h4 class="text-center"><?php echo __( 'Assign orders to which table ?', 'gastro' );?></h4>
                            <div class="row">
                                <div class="col-md-3" ng-repeat="table in tables">
                                    <div ng-click="selectTable( table )" class="box text-center" ng-class="{
                                        'type-selected'     :   table.selected,
                                        'type-unselected'    :   ! table.selected
                                    }">
                                        <div class="box-body">{{ table.NAME }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" ng-show="showSeats">
                            <h4 class="text-center"><?php echo __( 'How many people joined the party ?', 'gastro' );?></h4>
                            <div>
                                <button ng-click="selectSeat( seat )" class="btn btn-default keyboard-button" ng-repeat="seat in getSeat( selectedTable )">{{ seat }}</button>
                            </div>
                            <br>
                        </div>
                        <div class="col-md-12" ng-show="showCustomers">
                            <h4 class="text-center"><?php echo __( 'Assign to which customer ?', 'gastro' );?></h4>
                            <div>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><?php echo __( 'Customer', 'gastro' );?></span>
                                    <select ng-model="selectedCustomer" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
                                        <option ng-repeat="customer in customers" value="{{ customer.ID }}">{{ customer.NOM }} {{ customer.PRENOM }} - {{ customer.EMAIL }}</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                        <div class="btn-group" role="group" ng-show="! showTable && ! showSeats && getSelectedType().namespace && ! showCustomers">
                            <button ng-click="mergeOrder()" class="btn btn-primary"><?php echo __( 'Merge', 'gastro' );?></button>
                        </div>
                        <div class="btn-group" role="group" ng-show="! showTable && ! showSeats && ! showCustomers">
                            <button ng-click="cancelMerging()" class="btn btn-default"><?php echo __( 'Cancel', 'gastro' );?></button>
                        </div>
                        <div class="btn-group" role="group" ng-show="showTable || showSeats || showCustomers">
                            <button ng-click="selectCustomer()" class="btn btn-primary"><?php echo __( 'Finish', 'gastro' );?></button>
                        </div>
                        <div class="btn-group" role="group" ng-show="showTable || showSeats || showCustomers">
                            <button ng-click="cancelTableSelection()" class="btn btn-default"><?php echo __( 'Cancel', 'gastro' );?></button>
                        </div>
                    </div>  
                </div>
            </div>
        </div>`;

        $timeout( function(){
            angular.element( '.merging-box .modal-dialog' ).css( 'width', '98%' );
            angular.element( '.merging-box .modal-body' ).css( 'height', $scope.wrapperHeight );
            angular.element( '.merging-box .bootbox-body' ).css({
                'display': 'flex',
                'flex-grow': 'inherit'
            });
            angular.element( '.merging-box .modal-body' ).css( 'overflow-x', 'hidden' );
            angular.element( '.merging-box .modal-body' ).css({
                'display': 'flex',
                'width': '100%',
                'flex-grow': '1',
                'flex-direction' : 'column'
            });
            angular.element( '.merging-box .modal-body' ).addClass( 'no-padding' );
            angular.element( '.merging-box .order-container' ).height( $( '.modal-body' ).height() - 5 - $( '.search-order-field' ).outerHeight() );
            angular.element( '.merging-box .order-container' ).css({
                'overflow-y' : 'scroll'
            });

            $( '.merging-wrapper' ).html( content );
            $( '.merging-box .modal-footer' ).append( '<a ng-click="printSelectedOrderReceipt()" ng-show="countSelected() == 1" href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-print"></i> <?php echo _s( 'Print', 'gastro' );?></a>' )
            $( '.merging-box .modal-footer' ).append( '<a ng-show="getSelectedOrders()[0].TYPE == \'nexo_order_devis\' && countSelected() == 1" ng-click="checkoutOrder()" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-shopping-cart"></i> <?php echo _s( 'Checkout', 'gastro' );?></a>' )       
            $( '.merging-box' ).html( $compile( $( '.merging-box').html() )( $scope ) );
            
            $scope.loadOrders();
        }, 100 );

    }

    /**
     * Select A customer
     * @return void
     */
    $scope.selectCustomer       =   function(){
        if( $scope.selectedCustomer == 0 ){
            return NexoAPI.Toast()( '<?php echo _s( 'Please select a customer', 'gastro' );?>' );
        }
        $scope.showCustomers    =   false;
    }

    /**
     * Checkout Order
     * Only checkout the first selected order
     * @return void
     */
    $scope.checkoutOrder            =   function(){
        $scope.openOrderDetails( $scope.getSelectedOrders()[0].ID );
    }

    /**
     * Get Selected Order
     * @return object
     */
    $scope.getSelectedOrders         =   function(){
        let orders   =   [];
        _.each( $scope.orders, ( order ) => {
            if( order.selected ) {
                orders.push( order );
            }
        });
        return orders;
    }

    /**
     * Open Order Details
     * @return void
     */
    $scope.openOrderDetails			=	function( order_id ) {
		$http.get( '<?php echo site_url( array( 'api', 'nexopos', 'full-order' ) );?>' + '/' + order_id + '?<?php echo store_get_param( null );?>', {
			headers			:	{
				[ tendoo.rest.key ]     :   tendoo.rest.value
			}
		}).then(function( returned ){
            let details     =   returned.data;
            let { order, products, customer, refund }    =   details;
            
            v2Checkout.From 				=	'readerOrdersCTRL.runPayment';
            v2Checkout.emptyCartItemTable();
            v2Checkout.CartItems 			=	products;

            _.each( v2Checkout.CartItems, function( value, key ) {
                value.QTE_ADDED		=	parseFloat( value.QTE_ADDED );
            });

            // @added CartRemisePercent
            // @since 2.9.6

            if( order.REMISE_TYPE != '' ) {
                v2Checkout.CartRemiseType			    =	order.REMISE_TYPE;
                v2Checkout.CartRemise				    =	NexoAPI.ParseFloat( order.REMISE );
                v2Checkout.CartRemisePercent			=	NexoAPI.ParseFloat( order.REMISE_PERCENT );
                v2Checkout.CartRemiseEnabled			=	true;
            }

            if( parseFloat( order.GROUP_DISCOUNT ) > 0 ) {
                v2Checkout.CartGroupDiscount 			=	parseFloat( order.GROUP_DISCOUNT ); // final amount
                v2Checkout.CartGroupDiscountAmount 	    =	parseFloat( order.GROUP_DISCOUNT ); // Amount set on each group
                v2Checkout.CartGroupDiscountType 		=	'amount'; // Discount type
                v2Checkout.CartGroupDiscountEnabled 	=	true;
            }

            v2Checkout.CartCustomerID 			=	order.REF_CLIENT;
            // @since 2.7.3
            v2Checkout.CartNote 				=	order.DESCRIPTION;
            v2Checkout.CartTitle 				=	order.TITRE;

            // @since 3.1.2
            v2Checkout.CartShipping 				=	parseFloat( order.SHIPPING_AMOUNT );
            $scope.price 						    =	v2Checkout.CartShipping; // for shipping directive
            $( '.cart-shipping-amount' ).html( $filter( 'moneyFormat' )( $scope.price ) );

            // Restore Custom Ristourne
            v2Checkout.restoreCustomRistourne();

            // Refresh Cart
            // Reset Cart state
            v2Checkout.buildCartItemTable();
            v2Checkout.refreshCart();
            v2Checkout.refreshCartValues();
            v2Checkout.ProcessURL				=	"<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>" + '/' + order.ORDER_ID + "?store_id=<?php echo get_store_id();?>";
            v2Checkout.ProcessType				=	'PUT';
            

            // // Restore Shipping
            // // @since 3.1
            _.each( order.shipping, ( value, key ) => {
                $scope[ key ] 	=	value;
            });

            $scope.wasOpenFromTableHistory      =   true;
            $rootScope.$broadcast( 'filter-selected-order-type', {
                namespace 	:	order.RESTAURANT_ORDER_TYPE
            });
            $rootScope.$emit( 'payBox.openPayBox' );
		});
	};

    /**
    * Quick shortcut to print orders
    * @param object order details
    */
    $scope.printSelectedOrderReceipt 		=	function(){
        $( '#receipt-wrapper' ).remove();
        $( 'body' ).append( '<iframe id="receipt-wrapper" style="visibility:hidden;height:0px;width:0px;position:absolute;top:0;" src="<?php echo dashboard_url([ 'orders', 'receipt' ]);?>/' + $scope.pluckComponentID( $scope.orders, 'ID' )[0] + '?refresh=true&autoprint=true"></iframe>' );
    }

    /**
     * Get Selected Type
     * @return object
     */
    $scope.getSelectedType  =   function(){
        let selected;
        _.each( $scope.orderTypes, ( type )=> {
            if( type.selected ) {
                selected    =   type;
            }
        });

        return selected;
    }

    /**
     * Load Orders
     * @return void
     */
    $scope.loadOrders       =   function(){
        $http.get( '<?php echo site_url([ 'api', 'gastro', 'orders', 'daily', 'unpaid', store_get_param( '?' ) ]);?>', {
            headers     :   {
                [ tendoo.rest.key ]     :   tendoo.rest.value 
            }
        }).then( result => {
            $scope.orders   =   result.data;
        })
    }

    /**
     * Select Order
     * @param  object order
     * @return void
     */
    $scope.selectOrder          =   function( order ) {

        if( $scope.countSelected() >= 5 && [ false, undefined ].indexOf( order.selected ) >= 0 ) {
            return NexoAPI.Notify().warning( 
                '<?php echo __( 'Warning', 'gastro' );?>',
                '<?php echo _s( 'You can\'t merge more that 5 orders', 'gastro' );?>'
            );
        }

        if( typeof order.selected == 'undefined' ) {
            order.selected  =   true;
        } else {
            order.selected  =  !order.selected;
        }
    }

    $scope.countSelected            =   function(){
        let selected        =   0;
        _.each( $scope.orders, function( order ) {
            if( order.selected ) {
                selected++;
            }
        });

        return selected;
    }

    /**
     * cancelTableSelection
     * @return void
     */
    $scope.cancelTableSelection     =   function(){
        _.each( $scope.tables, function( table ) {
            table.selected  =   false;
        });
        $scope.showTable        =   false;
        $scope.showSeats        =   false;
        $scope.selectedTable    =   false;
        $scope.cancelOrderTypeSelection();
        $scope.selectedSeat     =   0;
        $scope.showSeats        =   false;
        $scope.showCustomers    =   false;
    }

    /**
     * Get Type
     * @param object order
     * @return string
     */
    $scope.getType          =   function( order ){
        switch( order.RESTAURANT_ORDER_TYPE ) {
            case 'dinein'   :  
                return '<?php echo _s( 'Dine in', 'gastro' );?>'; 
            break;
            case 'delivery'   :  
                return '<?php echo _s( 'Delivery', 'gastro' );?>'; 
            break;
            case 'takeaway'   :  
                return '<?php echo _s( 'Take Away', 'gastro' );?>'; 
            break;
        }
    }

    /**
     * Get Seat for the selected Table
     * @return array of seats
     */
    $scope.getSeat          =   function( selectedTable ) {
        let seat    =   parseInt( selectedTable.MAX_SEATS );
        let seatsArray      =   [];
        for( let i = 1; i <= seat; i++ ) {
            seatsArray.push( i );
        }
        return seatsArray;
    }

    /**
     * Merge order action
     * @param void
     * @return void
     */
    $scope.mergeOrder       =   function(){
        NexoAPI.showConfirm({
            message     :   '<?php echo _s( 'The selected orders will be merged. This action can\'t be undone.', 'gastro' );?>'
        }).then( ( action ) => {
            if( action.value ) {
                v2Checkout.paymentWindow.showSplash();
                $http.post('<?php echo site_url([ 'api', 'gastro', 'tables', 'orders-merging', store_get_param( '?' ) ]);?>', {
                    'orders'    :   $scope.pluckComponentID( $scope.orders, 'ID' ),
                    'table'     :   $scope.pluckComponentID( $scope.tables, 'ID' )[0], // we only need one table
                    'seats'     :   $scope.selectedSeat,
                    'type'      :   $scope.getSelectedType().namespace,
                    'customer'  :   $scope.selectedCustomer
                }, {
                    headers     :   {
                        [ tendoo.rest.key ]     :   tendoo.rest.value
                    }
                }).then( result => {
                    v2Checkout.paymentWindow.hideSplash();
                    $scope.cancelMerging();
                    $scope.loadOrders();
                }, ( result ) => {
                    NexoAPI.Toast()( '<?php echo __( 'An error occured', 'gastro' );?>' );
                    v2Checkout.paymentWindow.hideSplash();
                    $scope.cancelMerging();
                })
            }
        });
    }

    /**
     * Pluck Selected Order ID
     * @return array of ID
     */
    $scope.pluckComponentID     =   function( components, filter ){
        let ids     =   [];
        _.each( components, ( order ) => {
            if( order.selected ) {
                ids.push( order[ filter ] );
            }
        });
        return ids;
    }

    /**
     * Select Type
     * @param object
     * @return object
     */
    $scope.selectType       =   function( orderType ) {
        _.each( $scope.orderTypes, ( type ) => {
            type.selected   =   false;
        });
        orderType.selected       =   true;

        // cancel table selection
        if( orderType.namespace == 'dinein' ) {
            $scope.selectedTable    =   false;
            $scope.showTable        =   true;
            $scope.showSeats        =   false;
            $scope.showCustomers    =   false; // since table and seats need to be selected
        } else {
            $scope.showTable        =   false;
            $scope.showSeats        =   false;
            $scope.showCustomers    =   true;
        }
    }

    /**
     * Cancel Mering
     * @return void
     */
    $scope.cancelMerging        =   function() {
        $scope.cancelOrderTypeSelection();

        _.each( $scope.orders, ( order ) => {
            order.selected   =   false;
        });
        $scope.selectedTable    =   false;
        $scope.showTable        =   false;
        $scope.showCustomers    =   false;
    }

    /**
     * Cancel Table Selection
     * @return void
     */
    $scope.cancelOrderTypeSelection     =   function(){
        _.each( $scope.orderTypes, ( type ) => {
            type.selected   =   false;
        });
    }

    /**
     * Select Table
     * @return void
     */
    $scope.selectTable          =   function( table ) {
        _.each( $scope.tables, ( table ) => {
            table.selected   =   false;
        });
        table.selected          =   true;
        $scope.selectedTable    =   table;
        $scope.showTable        =   false;
        $scope.showSeats        =   true;
        $scope.showCustomers    =   false;
    }

    /**
     * Select Seat
     * @return void
     */
    $scope.selectSeat           =   function( seat ) {
        $scope.selectedSeat     =   seat;
        $scope.showTable        =   false;
        $scope.showSeats        =   false;
        $scope.showCustomers    =   true;
    }
}])
</script>
<style>
.no-border-lr.list-group-item:first-child {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-top: solid 0px;
}
.order-selected {
    box-shadow: 0px 0px 0px 3Px #9f90ff;
}
.order-error {
    box-shadow: 0px 0px 0px 3Px #ff9090;
    background: #ff9090;
}
.no-border-lr {
    border-left: solid 0px;
    border-right: solid 0px;
}
.type-unselected {
    background: #f9f9f9;
    box-shadow: inset 0px 0px 0px 1px #333;
}
.type-selected {
    background: #deeff9;
    box-shadow: inset 0px 0px 0px 4px #3c8dbc;
}
.keyboard-button {
    padding: 20px 25px;
    margin-right: 5px;
}
</style>