<?php if( User::can( 'gastro.view.tokitchen' ) ):?>
<div class="btn-group sendToKitchenButtonWrapper" role="group">
     <button type="button" class="btn btn-default btn-lg sendToKitchenButton" ng-click="sendToKitchen()" style="margin-bottom:0px;"> <i class="fa fa-cutlery"></i>
          <span class="hidden-xs"><?php _e('To Kitchen', 'gastro');?></span>
     </button>
</div>
<?php endif;?>