<?php
global $Options;

defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->library( 'Curl' );

$this->Gui->col_width(1, 4);
// $this->Gui->col_width(2, 2);

$this->Gui->add_meta( array(
    'col_id'    =>  1,
    'namespace' =>  'gastro-settings',
    'type'      =>  'unwrapped',
    'gui_saver' =>  true,
    'footer'    =>  [
        'submit'  =>  [
            'label' =>  __( 'Save Settings', 'gastro' )
        ]
    ]
) );

$this->Gui->add_item([
    'type'      =>  'dom',
    'content'   =>  $this->load->module_view( 'gastro', 'settings.gastro.tab-wrapper', [
        'tabs'  =>  [
            'printing-settings'     =>  __( 'Print Settings', 'gastro' ),
            'pos'                   =>  __( 'POS Settings', 'gastro' ),
            'waiter-screen'         =>  __( 'Waiter Screen', 'gastro' ),
            'features'              =>  __( 'Features', 'gastro' ),
            'kitchens'              =>  __( 'Kitchens', 'gastro' ),
            'timing'                =>  __( 'Timing', 'gastro' ),
            'nexo-store'            =>  __( 'RomPOS Store', 'gastro' ),
            'speech-synthetizer'    =>  __( 'Speech Synthesizer', 'gastro' ),
            'misc'                  =>  __( 'Misc', 'gastro' ),
        ],
        'activeTab'     =>  isset( $_GET[ 'tab' ] ) ? xss_clean( $_GET[ 'tab' ] ) : 'printing-settings'
    ], true )
], 'gastro-settings', 1 );

$this->Gui->output();
