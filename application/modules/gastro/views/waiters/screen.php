<?php global $Options, $current_register;?>
<script>
tendooApp.directive( 'readyOrders', function(){
	return {
		templateUrl         :    '<?php echo site_url([ 'dashboard', store_slug(), 'gastro', 'templates', 'ready_orders' ] );?>',
		restrict            :   'E'
	}
});

tendooApp.controller( 'readerOrdersCTRL', [ 
'$scope', '$timeout', '$compile', '$rootScope', '$interval', '$http', '$filter', 
function( $scope, $timeout, $compile, $rootScope, $interval, $http, $filter ){
	$scope.$watch( 'newOrders', function( previous, current ){
		if( $scope.newOrders > 0 && previous < current ){
			NexoAPI.Toast()( '<?php echo _s( 'A new order is ready', 'gastro' );?>' );
		}
	});

	// $( '.new-orders-button' ).append( '<span class="badge badge-warning">42</span>' );
	$scope.newOrders         	=    0;  

	$scope.selectedOrder 		=	null;
	
	/**
	* Open Waiter Screen
	* @return void
	**/
	
	$scope.openReadyOrders       =    function(){
		NexoAPI.Bootbox().alert({
			message 		:	'<div class="ready-orders"><ready-orders></ready-orders></div>',
			title          :	'<?php echo _s( 'Ready orders', 'gastro' );?>',
			buttons 		:	{
				ok: {
					label: '<span ><?php echo _s( 'Close', 'gastro' );?></span></span>',
					className: 'btn-default'
				},
			}, 
			callback 		:	function( action ) {
				// $rootScope.$broadcast( 'open-select-order-type' );
			},
			className 	:	'ready-orders-box col-md-8 col-lg-12'
		});
		
		$scope.windowHeight           =	window.innerHeight;
		$scope.wrapperHeight          =	$scope.windowHeight - ( ( 56 * 2 ) + 30 );

		let printAttr;
		switch( '<?php echo store_option( 'nexo_print_gateway' );?>' ) {
			case 'normal_print': 
				printAttr	=	'ng-click="printReceipt()"'; 
			break;
			case 'nexo_print_server': 
				printAttr	=	'ng-click="npsPrint()"'; 
			break;
			case 'register_nps': 
				printAttr	=	'ng-click="registerPrint()"'; 
			break;
		}
		
		
		$timeout( function(){
			angular.element( '.ready-orders-box .modal-dialog' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-top', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-bottom', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-left', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-right', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'height', $scope.wrapperHeight );
			angular.element( '.ready-orders-box .modal-body' ).css( 'overflow-x', 'hidden' );
		}, 150 );
		
		$( '.ready-orders-box' ).last().find( '.modal-footer' )
		.prepend( '<a ng-show="selectedOrder != null && selectedOrder.TYPE != \'nexo_order_comptant\'" ng-click="runPayment()" class="btn btn-primary"><?php echo _s( 'Pay that order', 'nexo-restarant' );?></a>' );
		$( '.ready-orders-box' ).last().find( '.modal-footer' )
		.prepend( '<a ng-show="selectedOrder != null && ( selectedOrder.REAL_TYPE == \'delivery\' || selectedOrder.REAL_TYPE == \'takeaway\' || selectedOrder.REAL_TYPE == \'dinein\' )" ' + printAttr + ' class="btn btn-primary"><?php echo _s( 'Print Receipt', 'nexo-restarant' );?></a>' );
		$( '.ready-orders-box' ).last().find( '.modal-footer' )
		.prepend( '<a ng-show="selectedOrder.STATUS == \'ready\'" ng-click="setAsServed()" class="btn btn-primary"><?php echo _s( 'Complete', 'nexo-restarant' );?></a>' );
		
		$( '.ready-orders' ).html( 
			$compile( $( '.ready-orders').html() )( $scope ) 
		);
		$( '.ready-orders-box' )
		.find( '.modal-footer' )
		.html( $compile( $( '.ready-orders-box' ).find( '.modal-footer' ).html() )( $scope ) );
		$scope.fetchOrders();
	}

	$scope.setAsServed = function() {
		var order_id = $scope.selectedOrder.ID;
		$http.post( '<?php echo site_url([ 'api', 'gastro', 'tables', 'serve', store_get_param( '?' ) ]);?>', {
			order_id
		}, {
			headers			:	{
				'<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo @$Options[ 'rest_key' ];?>'
			}
		}).then(function( returned ) {
			$rootScope.$broadcast( 'food-served', returned.data );

			if( $( '.table-selection-box' ).length > 1 ) {
				$scope.showHistory      =   false;
				// to force refresh
				$timeout(function(){
					$scope.showHistory  =   true;
				}, 100 );
			}
		}, function( returned ){
			$rootScope.$broadcast( 'food-no-served', returned.data );
		});

	}

	$scope.printReceipt 		=	function(){
		$( 'body' ).append( '<iframe id="receipt-wrapper" style="visibility:hidden;height:0px;width:0px;position:absolute;top:0;" src="<?php echo dashboard_url([ 'orders', 'receipt' ]);?>/' + $scope.selectedOrder.ID + '?refresh=true&autoprint=true"></iframe>' );
	}

	/**
		* RomPOS Agent
		* @return void
		*/
	$scope.npsPrint         =   function() {
		$.ajax( '<?php echo dashboard_url([ 'local-print'  ]);?>' + '/' + $scope.selectedOrder.ID, {
			success 	:	function( printResult ) {
				$.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
					type  	:	'POST',
					data 	:	{
						'content' 	:	printResult,
						'printer'	:	'<?php echo store_option( 'nexo_pos_printer' );?>',
                        'debug'     :   "<?php echo defined('ENABLE_DEBUG') ? 'true' : '';?>"
					},
					dataType 	:	'json',
					success 	:	function( result ) {
						console.log( result );
					}
				});
			}
		});
	}

	/**
		* Register Print NPS
		* @return void
		*/
	$scope.registerPrint    =   function() {
		<?php if ( empty( $current_register[ 'ASSIGNED_PRINTER' ] ) || ! filter_var( $current_register[ 'NPS_URL' ], FILTER_VALIDATE_URL ) ):?>
			NexoAPI.Notify().warning(
				`<?php echo __( 'Unable to print', 'gastro' );?>`,
				`<?php echo __( 'No printer is assigned to the cash register or the server URL is incorrect.', 'gastro' );?>`
			);
		<?php else:?>
		$.ajax( '<?php echo dashboard_url([ 'local-print' ]);?>' + '/' + $scope.selectedOrder.ID, {
			success 	:	function( printResult ) {
				$.ajax( '<?php echo $current_register[ 'NPS_URL' ];?>/api/print', {
					type  	:	'POST',
					data 	:	{
						'content' 	:	printResult,
						'printer'	:	'<?php echo $current_register[ 'ASSIGNED_PRINTER' ];?>',
                        'debug'     :   "<?php echo defined('ENABLE_DEBUG') ? 'true' : '';?>"
					},
					dataType 	:	'json',
					success 	:	function( result ) {
						NexoAPI.Toast()( `<?php echo __( 'Print job submitted', 'gastro' );?>` );
					},
					error		:	() => {
						NexoAPI.Notify().warning(
							`<?php echo __( 'Unable to print', 'gastro' );?>`,
							`<?php echo __( 'ROMPos has not been able to connect to the Print Server or this latest has returned an unexpected error.', 'gastro' );?>`
						);
					}
				});
			}
		});
		<?php endif;?>
	}

	/**
	* Payement
	* @return void
	**/
	
	$scope.runPayment             =    function(){
		
		v2Checkout.From 				=	'readerOrdersCTRL.runPayment';
		v2Checkout.emptyCartItemTable();
		// since by default the query don't return the meta. We can the force meta when the order is being open
		// on the ready order screen.
		// the modifier will be available on the cart
		$scope.selectedOrder.items.forEach( ( item ) => {
			item.metas 								=	new Object;
			if ( item.MOIDIFIERS !== undefined ) {
				item.metas[ 'modifiers' ] 				=	JSON.parse( item.MODIFIERS );
			}
			item.metas[ 'restaurant_food_issue' ] 	=	item.FOOD_ISSUE;
			item.metas[ 'restaurant_food_status' ] 	=	item.FOOD_STATUS;
			item.metas[ 'restaurant_note' ]			=	item.NOTE;
		});

		console.log( $scope.selectedOrder );

		v2Checkout.CartItems 			=	$scope.selectedOrder.items;

		if( parseFloat($scope.selectedOrder.REMISE) > 0.0 /*$scope.selectedOrder.REMISE_TYPE != ''*/ ) {
			v2Checkout.CartRemiseType				=	$scope.selectedOrder.REMISE_TYPE == 'percentage' ? 'percentage' : 'flat';
			v2Checkout.CartRemise					=	NexoAPI.ParseFloat( $scope.selectedOrder.REMISE );
			v2Checkout.CartRemisePercent			=	NexoAPI.ParseFloat( $scope.selectedOrder.REMISE_PERCENT );
			v2Checkout.CartRemiseEnabled			=	true;
		}

		if( parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ) > 0 ) {
			v2Checkout.CartGroupDiscount 			=	parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ); // final amount
			v2Checkout.CartGroupDiscountAmount 		=	parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ); // Amount set on each group
			v2Checkout.CartGroupDiscountType 		=	'amount'; // Discount type
			v2Checkout.CartGroupDiscountEnabled 	=	true;
		}

		v2Checkout.CartCustomerID 			=	$scope.selectedOrder.REF_CLIENT;
		// @since 2.7.3
		v2Checkout.CartNote 				=	$scope.selectedOrder.DESCRIPTION;
		v2Checkout.CartTitle 				=	$scope.selectedOrder.TITRE;

		// @since 3.1.2
		v2Checkout.CartShipping 			=	parseFloat( $scope.selectedOrder.SHIPPING_AMOUNT );
		$scope.price 						=	v2Checkout.CartShipping; // for shipping directive
		$( '.cart-shipping-amount' ).html( $filter( 'moneyFormat' )( $scope.price ) );

		// Restore Custom Ristourne
		v2Checkout.restoreCustomRistourne();

		v2Checkout.woocommerce_fees = {};
		var fees = 0;
		for ( var meta in $scope.selectedOrder.metas) {
			if ( /^_wccf_cf_label_/.test(meta) ) {
				var slug = meta.substring("_wccf_cf_label_".length);
				if ( typeof(v2Checkout.woocommerce_fees[slug]) == 'undefined' ) {
					v2Checkout.woocommerce_fees[slug] = {}
				}
				v2Checkout.woocommerce_fees[slug]['label'] = $scope.selectedOrder.metas[meta];
			} else if ( /^_wccf_cf_value_/.test(meta) ) {
				var slug = meta.substring("_wccf_cf_value_".length);
				if ( typeof(v2Checkout.woocommerce_fees[slug]) == 'undefined' ) {
					v2Checkout.woocommerce_fees[slug] = {}
				}
				v2Checkout.woocommerce_fees[slug]['value'] = $scope.selectedOrder.metas[meta];
				fees++;
			}
		}
		console.log(v2Checkout.woocommerce_fees);

		// Refresh Cart
		// Reset Cart state
		v2Checkout.buildCartItemTable();
		v2Checkout.refreshCart();
		if ( fees ) {
			v2Checkout.CartValue += parseFloat( $scope.selectedOrder.TOTAL_TAXES );
			for ( var item in v2Checkout.woocommerce_fees)  {
				v2Checkout.CartValue += parseFloat( v2Checkout.woocommerce_fees[item].value );
			}
			v2Checkout.refreshCartValues( true );
		} else {
			v2Checkout.refreshCartValues();
		}

		v2Checkout.ProcessURL				=	"<?php echo site_url(array( 'rest', 'nexo', 'order' ));?>" + '/<?php echo User::id();?>/' + $scope.selectedOrder.ID + "?store_id=<?php echo get_store_id();?>&from-pos-waiter-screen=true";
		v2Checkout.ProcessType				=	'PUT';
		// convert type to terminated
		// if( _.indexOf( [ 'dinein', 'takeaway', 'delivery', 'booking' ], $scope.selectedOrder.REAL_TYPE ) != -1 ) {
		// 	v2Checkout.CartType 				=	'nexo_order_' + $scope.selectedOrder.REAL_TYPE + '_paid';
		// } else {
		// 	// $scope.openReadyOrders();
		// 	v2Checkout.resetCart();
		// 	v2Checkout.ProcessURL 		=	"<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>?store_id=<?php echo get_store_id();?>";
		// 	v2Checkout.ProcessType 		=	'POST';
		// 	return NexoAPI.Toast()( '<?php echo _s( 'The order type is not supported.', 'gastro' );?>' );
		// }

		// Restore Shipping
		// @since 3.1
		_.each( $scope.selectedOrder.shipping, ( value, key ) => {
			$scope[ key ] 	=	value;
		});
		
		$rootScope.$broadcast( 'filter-selected-order-type', {
			namespace 	:	$scope.selectedOrder.RESTAURANT_ORDER_TYPE || $scope.selectedOrder.REAL_TYPE
		});
		$rootScope.$emit( 'payBox.openPayBox' );
	}

	/**
	 * Unselect all orders
	**/

	$scope.unselectAllOrders 	=	function(){
		_.each( $scope.orders, function( _order ) {
			_order.active       =    false;
		});
	}
	
	/***
	* Select Order
	* @return void
	**/
	
	$scope.selectOrder            =    function( order ) {
		// unselect all orders
		$scope.unselectAllOrders();
		
		order.active             =    true;
		$scope.selectedOrder 	=	order;
	}

	/**
	 * Resume Orders
	 * @return string
	**/

	$scope.resumeItems 		=	function( items ) {
		let itemNames 		=	[];
		_.each( items, function( item ){
			itemNames.push( item.DESIGN );
		});
		return itemNames.toString();
	}

	$scope.fetchOrders 		=	function() {
		$http.get( '<?php echo site_url([ 'api', 'gastro', 'tables', 'orders', store_get_param( '?' ) ]);?>', {
                headers			:	{
                    '<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo get_option( 'rest_key' );?>'
                }
            })
		.then( function( returned ){

			// filter only ready items
			let indexToRemove 		=	[];
			let dineInOrders 		=	[];
			returned.data.forEach( ( order, index ) => {
				// if order is not ready or collected
				if( _.indexOf([ 'ready', 'collected' ], order.STATUS ) == -1 ) {
					indexToRemove.push( index );
				}

				if( order.REAL_TYPE == 'dinein' ) {
					dineInOrders.push( order );
				}
			});

			// Just emit what the server returns
			$rootScope.$emit( 'getOrders.withMetas', angular.copy( dineInOrders ) );

			// Clear all unused index
			indexToRemove.forEach( index => {
				returned.data.splice( index, 1 );
			});

			$scope.rawOrders		=	[];
			$scope.rawOrdersCodes 	=	[];
			
			_.each( returned.data, function( order ) {
				if( order.TYPE != 'nexo_order_comptants' ) {
					$scope.rawOrders.unshift( order );
					$scope.rawOrdersCodes.unshift( order.CODE );
				}
			});

			let indexToKill 		=	[];
			let indexCodeToKill 	=	[];
			// remove order which aren't not more listed
			_.each( $scope.orders, ( order, index ) => {
				if( _.indexOf( $scope.rawOrdersCodes, order.CODE ) == -1 ) {
					indexToKill.push( index );
					indexCodeToKill.push( _.indexOf( $scope.ordersCodes, order.CODE ) );
				}
			});

			_.each( $scope.rawOrdersCodes, function( rawCode, index ){
				if( _.indexOf( $scope.ordersCodes, rawCode ) == -1 ) {
					$scope.orders.unshift( $scope.rawOrders[ index ] );
					$scope.ordersCodes.unshift( rawCode );
					$scope.newOrders++;
				}
			});

			if( indexToKill.length > 0 ){
				// kill indexes
				indexToKill.forEach( index => {
					$scope.orders.splice( index, 1 );
					if( $scope.newOrders > 0 ) {
						$scope.newOrders--;
					}
				});
			}

			if( indexCodeToKill.length > 0 ) {
				indexCodeToKill.forEach( index => {
					$scope.ordersCodes.splice( index, 1 );
					if( $scope.newOrders > 0 ) {
						$scope.newOrders--;
					}
				});
			}

		});
	}

	/**
	 * Time SPan
	 * @param object order
	 * @return string
	**/

	$scope.timeSpan 		=	function( order ){
		return moment( order.DATE_CREATION ).from( tendoo.date );
	}

	$scope.rawOrders 			=	[];
	$scope.rawOrdersCodes 		=	[];
	$scope.orders 				=	[];
	$scope.ordersCodes  		=	[];
	
	<?php if( store_option( 'gastro_disable_orders_fetch', 'no' ) === 'no' ):?>
	$interval( function(){
		$scope.fetchOrders();
	}, <?php echo store_option( 'gastro_order_refresh_interval', 8000 );?> );
	<?php endif;?>

	NexoAPI.events.addAction( 'close_paybox', function(){
		// if current order is a modification
		// we can cancel that.
		if( v2Checkout.ProcessType == 'PUT' && v2Checkout.From == 'readerOrdersCTRL.runPayment' ) {
			// $scope.openReadyOrders();
			v2Checkout.resetCart();
			v2Checkout.ProcessURL 		=	"<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>?store_id=<?php echo get_store_id();?>";
			v2Checkout.ProcessType 		=	'POST';
		}
		$scope.selectedOrder 	=	null;
		$scope.unselectAllOrders();
	});

	/**
	 * Remove only served order from the list
	 * What even happen, when an order is proceeced, the selectedOrder is reset.
	**/

	NexoAPI.events.addFilter( 'test_order_type', function( data ){
		$scope.selectedOrder 	=	null;
		return data;
	});

	/**
	 * Watch Events
	**/

	$scope.$on( 'open-ready-orders', function(){
		$scope.openReadyOrders();
	});
}]);
</script>