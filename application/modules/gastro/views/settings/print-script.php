<script>
$( document ).ready(function(){
    $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/printers', {
        success    :   function( result ) {
            result.forEach( printer => {
                let selected    =   printer.name ==  '<?php echo store_option( $option );?>' ? 'selected="selected"' : null;
                $( '[name="<?php echo store_prefix() . $option;?>"]' ).append( `<option ${selected} value="${printer.name}">${printer.name}</option>` );
            });
        }
    })
})
</script>