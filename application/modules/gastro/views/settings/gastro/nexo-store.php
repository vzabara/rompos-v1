<?php

$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h3>' . __( 'RomPOS Store Settings', 'gastro' ) . '</h3>' .
    '<p>' . __( 'The settings helps Gastro to handle better online booked orders.', 'gastro' ) . '</p>'
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_show_booked_at_kitchen',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Show Booked Orders', 'gastro' ),
    'description' =>   __( 'Help you to display booked orders (from RomPOS Store) at the kitchens so that the preparation can start. Default : No', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_nexostore_split_print',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Enable Split Print for Booked Orders', 'gastro' ),
    'description' =>   __( 'Allow you to enable the split printing for all booked orders. Default : No', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_cook_time_for_booked',
    'options'       =>  [
        '0'         =>      __( 'Display directly', 'gastro' ),
        '5'         =>      __( '5 minutes', 'gastro' ),
        '10'        =>      __( '10 minutes', 'gastro' ),
        '20'        =>      __( '20 minutes', 'gastro' ),
        '30'        =>      __( '30 minutes', 'gastro' ),
        '45'        =>      __( '45 minutes', 'gastro' ),
        '60'        =>      __( '60 minutes', 'gastro' ),
        '90'        =>      __( '90 minutes', 'gastro' ),
        '120'       =>      __( '120 minutes', 'gastro' )
    ],    
    'label' =>   __( 'Cook Time for booked Orders', 'gastro' ),
    'description' =>   __( 'Define how many time before the customer expected time, the order should appear at the kitchen.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type'          =>  'select',
    'name'          =>	store_prefix() . 'printer_nexostore',
    'label'         =>  __( 'RomPOS Store Printer', 'gastro' ),
    'description'   =>  __( 'Select a printer used for all booked orders. Make sure NPS url is set on the checkout settings.', 'gastro' ),
    'options'       =>  [
        ''          =>  __( 'Choose a printer', 'gastro' )
    ]
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    $this->load->module_view( 'gastro', 'settings.print-script', [
        'option'    =>    'printer_nexostore' // // no need to add store_prefix() since we're already calling the store_option function
    ], true )
), 'gastro-settings', 1 );