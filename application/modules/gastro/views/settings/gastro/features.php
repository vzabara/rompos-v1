<?php

$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h3>' . __( 'Features Control', 'gastro' ) . '</h3>'
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_takeaway',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Take Away', 'gastro' ),
    'description' =>   __( 'If the Take away order type is not in use, you can disable it.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_dinein',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Dine in', 'gastro' ),
    'description' =>   __( 'If the Dine In order type is not in use, you can disable it.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_delivery',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Delivery', 'gastro' ),
    'description' =>   __( 'If the Delivery order type is not in use, you can disable it.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_readyorders',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Ready Order Button', 'gastro' ),
    'description' =>   __( 'This option just let you disable the ready orders button form the new operation popup.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_print_sale_receipt',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Print Unpaid Receipt', 'gastro' ),
    'description' =>   __( 'This enable the option to print a sale receipt while sending an unpaid order to the kitchen.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_pendingorders',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Pending Orders Button', 'gastro' ),
    'description' =>   __( 'This option let you disable the pending orders from the new operation popup.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_saleslist',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Sale List button', 'gastro' ),
    'description' =>   __( 'Hi the sales list button on the new operation popup.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h4>' . __( 'Feature List', 'gastro' ) . '</h4>'
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_kitchen_screen',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Kitchen Screen', 'gastro' ),
    'description' =>   __( 'You can disable the kitchen screen. This will disable food status.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_kitchen_print',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Kitchen Print', 'gastro' ),
    'description' =>   __( 'All order proceeded from the POS system is send by default to the kitchen. You can disale this feature from here.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_area_rooms',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Areas', 'gastro' ),
    'description' =>   __( 'If you want to make the restaurant management easier, you can disable the area for the tables.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_kitchens',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Multiple Kitchens', 'gastro' ),
    'description' =>   __( 'This feature will let you manage all orders from one single kitchen.', 'gastro' )
), 'gastro-settings', 1 );