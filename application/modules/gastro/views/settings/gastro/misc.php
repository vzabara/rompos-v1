<?php

$this->Gui->add_item( array(
    'type' =>    'text',
    'name' =>	store_prefix() . 'takeaway_timer',
    'label' =>   __( 'Take Away Timer', 'gastro' ),
    'description' =>   __( 'Take away timer in minutes', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'text',
    'name' =>	store_prefix() . 'delivery_timer',
    'label' =>   __( 'Delivery Timer', 'gastro' ),
    'description' =>   __( 'Delivery timer in minutes', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'text',
    'name' =>	store_prefix() . 'empty_time_gap',
    'label' =>   __( 'Time gap', 'gastro' ),
    'description' =>   __( 'Time gap for orders with empty takeaway time in minutes', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'enable_modifiers_suffixes',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],
    'label' =>   __( 'Enable Modifiers Suffixes', 'gastro' ),
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'text',
    'name' =>	store_prefix() . 'modifiers_suffix',
    'label' =>   __( 'Add text to modifier label', 'gastro' ),
    'description' =>   __( 'Add this text to every modifier label in online order displayed at kitchen screen.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'allow_one_click',
    'options'     =>  [
        0    =>  __( 'No', 'gastro' ),
        1    =>     __( 'Yes', 'gastro' ),
    ],
    'label' =>   __( 'Allow one click on kitchen screen', 'gastro' ),
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_kitchen_autostatus',
    'options'     =>  [
        ''             =>  __( '', 'gastro' ),
        'completed'    =>  __( 'Completed', 'gastro' ),
        'shipped'      =>  __( 'Shipped', 'gastro' ),
        'delivered'    =>  __( 'Delivered', 'gastro' ),
    ],
    'label' =>   __( 'Automatically set order status when all items are ready ', 'gastro' ),
), 'gastro-settings', 1 );

