<?php
$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'disable_pos_waiter',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'    =>     __( 'Yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Waiter Screen on POS', 'gastro' ),
    'description' =>   __( 'Disable the waiter screen feature on POS.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'        =>    'select',
    'label'        =>    __('Disable Waiter Screen', 'gastro'),
    'name'        =>    store_prefix() . 'disable_waiter_screen',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'yes'       =>  __( 'Yes', 'gastro' ),
        'no'        =>  __( 'No', 'gastro' )
    ],   
    'description'    =>    __( 'Disable the waiter screen feature. The meal won\'t have the status "collected", but "ready" instead', 'gastro' )
), 'gastro-settings', 1 );

if ( store_option( 'disable_waiter_screen', 'no' ) === 'no' ) {
    $this->Gui->add_item(array(
        'type'        =>    'select',
        'label'        =>    __('Refresh Interval', 'gastro'),
        'name'        =>    store_prefix() . 'waiter_refresh_interval',
        'options'     =>  [
            '5000'     =>     __( '5 seconds', 'gastro' ),
            '10000'    =>  __( '10 seconds', 'gastro' ),
            '15000'    =>  __( '15 seconds', 'gastro' ),
            '20000'    =>  __( '20 seconds', 'gastro' ),
            '25000'    =>  __( '25 seconds', 'gastro' ),
            '30000'    =>  __( '30 seconds', 'gastro' ),
        ],   
        'description'    =>    __( 'Helps you to determine how many often the waiter screen should refresh the data. A more high interval could reduce CPU overload.', 'gastro' )
    ), 'gastro-settings', 1 );
}