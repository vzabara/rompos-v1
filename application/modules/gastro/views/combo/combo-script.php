<?php
global $Options;
$this->load->config( 'rest' );
?>
<script>
    tendooApp.directive( 'addmodifiers', function() {
        return {
            restrict            :   'E',
            template            :   <?php echo json_encode( $this->load->module_view( 'gastro', 'modifiers.dom2', null, true ) );?>,
            controller          :      [ '$scope', '$attrs', '$http', '$compile', '$rootScope', function( $scope, $attrs, $http, $compile, $rootScope ){
                // override confirm button
                $( '[data-bb-handler="confirm"]' ).hide();
                $( '.modal-footer' ).append( $compile( `
                <a type="button" class="btn btn-success btn-lg ng-scope" ng-click="addModifier()"><?php echo __( 'Add Modifier', 'gastro' );?></a>
                ` )( $scope ) );    
                $( '.modal' ).css({
                    background  :   'rgba(136, 136, 136, 0.56)'
                });    

                // reset modifiers
                $scope.modifiers            =       [];

                /**
                 * Get Unique ID
                 * @param void
                 * @return void
                **/
                
                $scope.get_unique_id 	= function(){
                    let uniqueId
                    let debug       =   0;
                    
                    do {
                        uniqueId   =   Math.random().toString(36).substring(7);
                        debug++;
                        if( debug == 1000 ) {
                            return;
                        }
                    } while( _.indexOf( $scope.savedIDS, uniqueId ) != -1 );

                    return uniqueId;
                }

                /**
                 * Add Modifiers
                **/

                $scope.addModifier          =   function() {
                    let atLeastOneSelected  =   false;
                    let modifiersPrice       =   0;
                    let modifiersArray      =   [];

                    _.each( $scope.modifiers, ( modifier ) => {
                        if( parseInt( modifier.default ) == 1 ) {
                            atLeastOneSelected  =   true;
                            modifiersPrice  +=   parseFloat( modifier.price );
                            modifiersArray.push( modifier );
                        }
                    });

                    if( parseInt( $scope.modifiers[0].group_forced ) == 1 && atLeastOneSelected == false ) {
                        return NexoAPI.Toast()( '<?php echo _s( 'You must select at least one modifier.', 'gastro' );?>' );
                    }

                    if( ! $scope.currentItem.modifiersArray ) {
                        $scope.currentItem.modifiersArray   =   [];
                        $scope.currentItem.modifiersPrice   =   0;
                    }
                    
                    if( modifiersArray.length > 0 ){
                        $scope.currentItem.modifiersArray       =   _.union( 
                            $scope.currentItem.modifiersArray,
                            modifiersArray 
                        );
                    }

                    $scope.currentItem.modifiersPrice       +=   modifiersPrice;

//                    if( $attrs.modifierGroupsLength > parseInt( $attrs.modifierIndex ) + 1 ) {
//                        return $( '[data-bb-handler="confirm"]' ).trigger( 'click' );
//                    }
                    
                    // add new item with his modifier
                    let item                =   $scope.currentItem
                    
                    /**
                     * Update the item price
                     */
                    if ( $scope.modifiers[0].group_update_price === '1' ) {
                        item.PRIX_DE_VENTE      =  $scope.currentItem.modifiersPrice;
                        item.PRIX_DE_VENTE_TTC  =  $scope.currentItem.modifiersPrice;
                    } else {
                        item.PRIX_DE_VENTE      =  parseFloat( item.PRIX_DE_VENTE ) + $scope.currentItem.modifiersPrice;
                        item.PRIX_DE_VENTE_TTC  =  parseFloat( item.PRIX_DE_VENTE_TTC ) + $scope.currentItem.modifiersPrice;
                    }

                    item.INLINE             =   true; // this item become inline since it's should be singular
                    item.CODEBAR            =   $scope.get_unique_id() + '-barcode-' + item.CODEBAR; // it definitely has to be 

                    // if meta is not set, then we'll set a default value
                    if( typeof item.metas == 'undefined' ) {
                        item.metas      =   new Object;
                    }

                    item.metas.modifiers    =   $scope.currentItem.modifiersArray;

                    // $scope.modifiers
                    // loop modifier price
                    v2Checkout.addToCart({
                        item, 
                        index       :   $attrs.index,
                        increase    :   false
                    });
                    
                    // v2Checkout.addOnCart([item], $attrs.barcode, $attrs.qte, $attrs.increase == 'true' ? true : false );
                    // 'cancel' instead of 'confirm' to close popup immediately
                    $( '[data-bb-handler="cancel"]' ).trigger( 'click' );
                }

                $scope.get_modifiers        =   function( group_id ) {
                    $http.get(
                        '<?php echo site_url( array( 'api', 'gastro', 'allmodifiers' ) );?>',
                    {
                        headers			:	{
                            '<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo @$Options[ 'rest_key' ];?>'
                        }
                    }).then(function( response ){
                        $scope.modifiers    =   response.data;
                        if ( $scope.modifiers.length < 1 ) {
                            NexoAPI.Toast()( '<?php echo _s( 'No modifiers available for this group', 'gastro' );?>' );
                        } else {
                            $('.available-modifier-groups').append('<div class="modifier-link"><a href="javascript:showAllGroups()"><?php echo _s( 'Show All', 'gastro' );?></a>');
                            let categories = [];
                            // collect modifiers groups and create links
                            _.each( $scope.modifiers, ( modifier ) => {
                                if (typeof(categories[modifier.group_name]) == 'undefined') {
                                    categories[modifier.group_name] = parseInt( modifier.category );
                                    $('.available-modifier-groups').append('<div class="modifier-link"><a href="javascript:showGroup('+modifier.category+')">'+modifier.group_name+'</a>');
                                }
                            });
                        }
                    });
                }

                /**
                 * Select Modifier
                **/

                $scope.select       =   ( modifier ) => {

                    if( modifier.default == '0' ) {
                        // check if modifier group allow multiple selection
                        if( modifier.group_multiselect == '0' ) {
                            // if the group doesn't allow multi select, just disable it
                            _.each( $scope.modifiers, ( _modifier ) => {
                                _modifier.default   =   '0';
                            });
                        }

                        modifier.default    =   '1';
                    } else {
                        modifier.default    =   '0';
                    }
                    
                }

                $scope.get_modifiers( $attrs.item );

                $rootScope.$on( 'close.modifierBox', ( scope, { ids, index, modifierIndex, increase, quantity, proceed, item } ) => {
                    delete v2Checkout.tempModifiedItem;
                });
            }]
        }
    });

    tendooApp.directive( 'editmodifiers', function() {
        return {
            restrict            :   'E',
            template            :   <?php echo json_encode( $this->load->module_view( 'gastro', 'modifiers.dom2', null, true ) );?>,
            controller          :      [ '$scope', '$attrs', '$http', '$compile', '$rootScope', function( $scope, $attrs, $http, $compile, $rootScope ){
                // override confirm button
                $( '[data-bb-handler="confirm"]' ).hide();
                $( '.modal-footer' ).append( $compile( `
                <a type="button" class="btn btn-success btn-lg ng-scope" ng-click="editModifier()"><?php echo __( 'Save Modifier', 'gastro' );?></a>
                ` )( $scope ) );
                $( '.modal' ).css({
                    background  :   'rgba(136, 136, 136, 0.56)'
                });

                $scope.selected_modifiers = [];

                /**
                 * Edit Modifiers
                **/

                $scope.editModifier          =   function() {

                    let atLeastOneSelected  =   false;
                    let modifiersPrice       =   0;

                    _.each( $scope.selected_modifiers, ( modifier ) => {
//                        if( parseInt( modifier.default ) == 1 ) {
                            atLeastOneSelected  =   true;
                            modifiersPrice  +=   parseFloat( modifier.price );
//                        }
                    });

//                    if( parseInt( $scope.modifiers[0].group_forced ) == 1 && atLeastOneSelected == false ) {
//                        return NexoAPI.Toast()( '<?php //echo _s( 'You must select at least one modifier.', 'gastro' );?>//' );
//                    }

                    let item                =  $scope.currentItem;
                    item.modifiersPrice     =  modifiersPrice;
                    item.PRIX_DE_VENTE      =  parseFloat( item.PRIX ) + $scope.currentItem.modifiersPrice;
                    item.PRIX_DE_VENTE_TTC  =  parseFloat( item.PRIX_TTC ) + $scope.currentItem.modifiersPrice;
                    item.metas.modifiers    =  $scope.selected_modifiers;;
                    item.modifiersArray     =  $scope.selected_modifiers;
                    $scope.currentItem = item;
//console.log(item);
                    v2Checkout.CartItems[ $scope.currentItem.LOOP_INDEX ] = item;

                    // 'cancel' instead of 'confirm' to close popup immediately
                    $( '[data-bb-handler="cancel"]' ).trigger( 'click' );

                    // re-draw order
                    v2Checkout.buildCartItemTable();
                }

                /**
                 * Select Modifier
                **/

                $scope.select       =   ( modifier ) => {

                    if( modifier.default == '0' ) {
                        // check if modifier group allow multiple selection
                        if( modifier.group_multiselect == '0' ) {
                            // if the group doesn't allow multi select, just disable it
                            _.each( $scope.modifiers, ( _modifier ) => {
                                _modifier.default   =   '0';
                            });
                        }

                        modifier.default    =   '1';
                    } else {
                        modifier.default    =   '0';
                    }

                    $scope.selected_modifiers = [];
                    _.each( $scope.modifiers, ( modifier ) => {
                        if ( modifier.default == '1' ) {
                            $scope.selected_modifiers.push(modifier);
                        }
                    });
                }

                $scope.get_modifiers        =   function( ) {
                    if ( $scope.modifiers.length < 1 ) {
                        NexoAPI.Toast()( '<?php echo _s( 'No modifiers available for this group', 'gastro' );?>' );
                    } else {
                        $('.available-modifier-groups').append('<div class="modifier-link"><a href="javascript:showAllGroups()"><?php echo _s( 'Show All', 'gastro' );?></a>');
                        let categories = [];
                        // collect modifiers groups and create links
                        _.each( $scope.modifiers, ( modifier ) => {
                            // reset modifier
                            modifier.default = '0';
                            if (typeof(categories[modifier.group_name]) == 'undefined') {
                                categories[modifier.group_name] = parseInt( modifier.category );
                                $('.available-modifier-groups').append('<div class="modifier-link"><a href="javascript:showGroup('+modifier.category+')">'+modifier.group_name+'</a>');
                            }
                        });
                        // select modifiers from order item
                        _.each( $scope.currentItem.metas.modifiers, ( current_modifier ) => {
                            _.each( $scope.modifiers, ( modifier ) => {
                                if ( current_modifier.id == modifier.id ) {
                                    modifier.default = '1';
                                    $scope.selected_modifiers.push(modifier);
                                }
                            });
                        });
                    }
                }

                $scope.get_modifiers( );

                $rootScope.$on( 'close.modifierBox', ( scope, { ids, index, modifierIndex, increase, quantity, proceed, item } ) => {
                    delete v2Checkout.tempModifiedItem;
                });
            }]
        }
    });

    function showAllGroups() {
        $('.all-modifiers div').each(function() {
            $(this).show();
        });
    }

    function showGroup( category ) {
        $('.all-modifiers div').each(function() {
            if ($(this).data('category') == category) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }
</script>
<style>
.modifier-link {
    font-size: 1.5rem;
    margin-right: -1px;
    padding: 0.7rem 0;
}
.modifier-link:hover {
    cursor: pointer;
}
.combo-item {
    display:block;
    font-size: 12px;
    border-bottom:solid 1px #EEE;
}
.modifiers-item {
    border: solid 1px #d2d2d2;
    height: 7rem;
    margin-right: -1px;
    margin-bottom: -1px;
}

.modifiers-item:hover {
    box-shadow: inset 0px 0px 60px 0px #EEE;
    cursor: pointer;
}

.modifiers-item.active {
    box-shadow: inset 0px 0px 60px 0px #c1d3fd; 
}

.modifier-name {
    text-align: center;
    font-weight: 600;
    margin: 0;
}
.modifier-price {
    text-align: center;
    margin: 0;
}
.modifier-image {
    max-height: 90px;
    width: 100%;
    margin: 10px 0 5px;
    border-radius: 10px;
}

.modifiers-box {
  text-align: center;
  padding: 0!important;
}

.modifiers-box:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modifiers-box .modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>