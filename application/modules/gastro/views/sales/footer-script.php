<?php $this->load->module_view( 'gastro', 'footer-swal' );?>
<script type="text/javascript">
$( document ).ready( function(){
    
    let bindSendToKitchen = function()  {
        setTimeout(() => {
        $( '.print-to-kitchen' ).bind( 'click', function(){
            $this   =   $( this );
                NexoAPI.showConfirm({
                    title       :   '<?php echo _s( 'Send this order to the kitchen ?', 'gastro' );?>',
                    message     :   '<?php echo _s( 'By proceeding to this will send all items again to the kitchen, even if those items are ready', 'gastro' );?>'
                }).then( function( action ){
                    if( action.value ) {
                        $.ajax({
                            url     :   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'print' ]);?>/' + $this.attr( 'data-id' ) + '?caching=false&app_code=<?php echo store_option( 'nexopos_app_code' ) . store_get_param( '&' );?>',
                            success     :   function( data ){
                                <?php if ( store_option( 'gastro_print_gateway', 'gcp' ) == 'nps' ):?>
                                $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
                                    type  	:	'POST',
                                    data 	:	{
                                        'content' 	:	data,
                                        'printer'	:	'<?php echo store_option( 'printer_takeway' );?>',
                                        'debug'     :   "<?php echo defined('ENABLE_DEBUG') ? 'true' : '';?>"
                                    },
                                    dataType 	:	'json',
                                    success 	:	function( result ) {
                                        console.log( result );
                                    }
                                });
                                <?php endif;?>
                            }, 
                            headers     :   {
                                [ tendoo.rest.key ]     :   tendoo.rest.value
                            }
                        })
                    }
                });
            })        
        }, 3000 );
    }

    $( document ).ajaxComplete( function() {
        bindSendToKitchen();
    });

    bindSendToKitchen();
});
</script>