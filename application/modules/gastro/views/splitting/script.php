<?php $this->load->model( 'Nexo_Misc' );?>
<script type="text/javascript">
"use strict";
window.textDomain      =   {
    boxTitle                        :   `<?php echo _s( 'Split Orders', 'gastro' );?>`,
    closeBox                        :   `<?php echo _s( 'Close', 'gastro' );?>`,
    checkout                        :   `<?php echo _s( 'Checkout', 'gastro' );?>`,
    splitOrder                      :   `<?php echo _s( 'Split Order', 'gastro' );?>`,
    proceedCheckout                 :   `<?php echo _s( 'Would you like to proceed ? All unsaved orders will be lost', 'gastro' );?>`,
    searchOrder                     :   `<?php echo _s( 'Search Order', 'gastro' );?>`,
    customer                        :   `<?php echo _s( 'Customer', 'gastro' );?>`,
    total                           :   `<?php echo _s( 'Total', 'gastro' );?>`,
    splitSuccess                    :   `<?php echo _s( 'The split was successful !', 'gastro' );?>`,
    table                           :   `<?php echo _s( 'Table', 'gastro' );?>`,
    hasSomeValid                    :   `<?php echo _s( 'The split will be made ? orders with no items, will be ignored.', 'gastro' );?>`,
    noItemsError                    :   `<?php echo _s( 'You must add at least one item before proceeding.', 'gastro' );?>`,
    proceedSplitting                :   `<?php echo _s( 'Split the order', 'gastro' );?>`,
    cantSplitSingleItemOrders       :   `<?php echo _s( 'You cannot split an order with only one item', 'gastro' );?>`,
    noMoreQuantity                  :   `<?php echo _s( 'There is no more quantity available', 'gastro' );?>`,
    selectOrderFirst                :   `<?php echo _s( 'You need to define in how many part the order should be splitted and select in to which part you would like to move available items.', 'gastro' );?>`,
    order                           :   `<?php echo _s( 'Order', 'gastro' );?>`,
    created                         :   `<?php echo _s( 'Created', 'gastro' );?>`,
    cancel                          :   `<?php echo _s( 'Cancel', 'gastro' );?>`,
    changeOrderNumber               :   `<?php echo _s( 'Would you like to change the number of parts ? that will reset all the current order created so far.', 'gastro' );?>`,
    noItemAdded                     :   `<?php echo _s( 'No item added here', 'gastro' );?>`,
    spliceInHowManyParts            :   `<?php echo _s( 'Split in how many parts ?', 'gastro' );?>`,
    piecesAlert                     :   `<?php echo _s( 'The available pieces matches the number of items including their quantity on the order.', 'gastro' );?>`,
    noOrderAvailable                :   `<?php echo _s( 'No order are available', 'gastro' );?>`,
    itemsRemainToMove               :   `<?php echo _s( "Unable to proceed, some items aren't assigned to an order. Please make sure to add all the available items to the available orders parts.", 'gastro' );?>`,
    selectCustomer                  :   `<?php echo _s( 'Assign to a customer', 'gastro' );?>`,
    chooseAnOption                  :   `<?php echo _s( 'Choose an option', 'gastro' );?>`,
    customerError                   :   `<?php echo _s( 'Unable to proceed. Some orders aren\'t assigned to any customer.', 'nexo' );?>`
}
window.urls             =   {
    submitOrder         :   '<?php echo site_url([ 'api', 'gastro', 'orders', 'split' . store_get_param( '?' ) ]);?>',
    loadOrders          :   '<?php echo site_url([ 'api', 'gastro', 'orders', 'daily', 'unpaid', store_get_param( '?' ) . '&restaurant_type=dinein' ]);?>',
    fullOrder           :   '<?php echo site_url( array( 'api', 'nexopos', 'full-order' ) );?>',
    storeParam          :   '?<?php echo store_get_param( null );?>',
    processURL          :   '<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>',
    processEnd          :   "?store_id=<?php echo get_store_id();?>&from-pos-waiter-screen=true"
}
window.gastroSplitData      =   {
    customers           :   <?php echo json_encode( $this->Nexo_Misc->get_customers_minimum() );?>
}
</script>
<style type="text/css">
.list-group-item-flex {
    padding: 0px;
    display: flex;
    flex-direction: row;
}
.list-group-item-flex .item-text {
    padding: 10px 15px;
    display: flex;
    flex-grow: 1;
}
.list-group-item-flex .item-button {
    display: flex;
    padding: 10px;
    border-left: solid 1px #ddd;
}
.list-group-item-flex .item-button:hover {
    background: #d1d7ff;
    color: #FFF;
}
</style>
<script src="<?php echo module_url( 'gastro' ) . '/js/split-order.ctrl.js';?>"></script>