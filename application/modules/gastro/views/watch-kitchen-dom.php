<div class="row" ng-controller="watchRestaurantCTRL" ng-cloak="">
    <div class="<?php echo store_option( 'gastro_show_booked_at_kitchen', 'no' ) === 'yes' ? 'col-md-8 col-lg-9 col-sm-6  col-xs-12' : 'col-md-12 col-sm-12 col-xs-12';?>">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12 col-lg-3 kitchen-box" ng-repeat="( column_index, column ) in columns">
                <div class="box box-solid {{ testOrderWaitingTime( order ) }}" ng-repeat="( order_index, order ) in column track by $index" ng-hide="hideOrder( order )">
                    <div class="box-header with-border">
                        <<?php echo store_option( 'kitchen_card_title', 'strong' );?> style="margin:0">
                            <span style="float:left">#{{ order.metas.woocommerce_order_id ? order.metas.woocommerce_order_id : order.CODE}}</span>
                            <span style="float:right" ng-show="order.REAL_TYPE =='dinein'">
                                <?php echo __( 'Dine In', 'gastro' );?>
                            </span>
                            <span style="float:right" ng-show="order.REAL_TYPE == 'takeaway'">
                                {{ order.metas.woocommerce_order_id ? 'Pick Up' : 'Take Away' }}
                            </span>
                            <span style="float:right" ng-show="order.REAL_TYPE == 'delivery'">
                                <?php echo __( 'Delivery', 'gastro' );?>
                            </span>
                    </<?php echo store_option( 'kitchen_card_title', 'strong' );?>>
                    </div>
                    <div class="box-header with-border">
                        <strong><?php echo sprintf( __( 'By %s', 'gastro' ), '{{ ucwords( order.AUTHOR_NAME ) }}' );?></strong> (<span am-time-ago="order.DATE_CREATION  | amParse: 'YYYY-MM-DD HH:mm:ss' "></span>)
                        <span class="pull-right">
                            <span class="order-status">{{ getOrderStatus( order ) }}</span>
                            <button ng-click="npsPrintKitchen( order, 'kitchen_printer' )" class="btn btn-success"><i class="fa fa-print"></i></button>
                        </span>
                        <div class="booked-for order-{{ order.CODE }}" ng-show="showTimer(order)">{{ calculateTimer(order) }}</div>
                    </div>
                    <div ng-if="order.REAL_TYPE == 'dinein'" class="box-header with-border text-center">
                        <?php if( store_option( 'disable_meal_feature' ) != 'yes' ):?>
                        <strong><?php echo __( 'Area', 'gastro' );?></strong> : {{ order.AREA_NAME }} >
                        <?php endif;?>
                        <strong><?php echo __( 'Table', 'gastro' );?></strong> : {{ order.TABLE_NAME }}
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="{{ order.meals.length > 1 ? 'col-md-6' : 'col-md-12' }}" ng-repeat="( meal_code, meal ) in order.meals track by $index">
                                <ul class="list-group" style="margin-bottom:0">
                                    <!-- ng-if="categoryCheck( item )"  -->
                                    <!-- " -->
                                    <!--                                    <li class="list-group-item meal" ng-hide="meal_code == 'null'">-->
                                    <!--                                        <strong>--><?php //echo __( 'Meal', 'gastro' );?><!--</strong> : {{ meal_code }}-->
                                    <!--                                    </li>-->
                                    <li  ng-repeat="( food_index, food ) in meal track by $index" ng-click="changeItemState( order, food )" class="info list-group-item {{ food.active == true ? 'active' : '' }} item-{{ food.FOOD_STATUS }}" >
                                        <span class="badge badge-{{ food.FOOD_STATUS }}">{{ food.FOOD_STATUS == 'not_ready' ? 'PROTEIN' : (food.FOOD_STATUS == 'in_preparation' ? 'BASE' : 'READY') }}</span>
                                        {{ food.ALTERNATIVE_NAME == '' ? food.NAME : food.ALTERNATIVE_NAME }} {{ food.MEAL }} (x{{ food.QTE_ADDED }}) <br>
                                        <p class="restaurant-note" ng-repeat="modifier in food.MODIFIERS track by $index">
                                            + <strong>{{ modifier.group_name }}</strong> : {{ modifier.name }}
                                            <span ng-show="order.ONLINE_ORDER_ID"><?php echo $modifier_suffix; ?></span>
                                        </p>
                                        <!--p class="restaurant-note restaurant-notes" ng-show="order.CUSTOMER_NAME != null && order.CUSTOMER_NAME != ''"><strong><?php echo __( 'Customer Name', 'gastro' );?></strong>: {{ order.CUSTOMER_NAME }}</p-->
                                        <!--p class="restaurant-note restaurant-notes" ng-show="food.CUSTOMER_NAME != null && food.CUSTOMER_NAME != ''"><strong><?php echo __( 'Customer Name', 'gastro' );?></strong>: {{ food.CUSTOMER_NAME }}</p-->
                                        <p class="restaurant-note restaurant-notes" ng-show="food.FOOD_NOTE != null && food.FOOD_NOTE != ''"><strong><?php echo __( 'Note', 'gastro' );?></strong>: {{ food.FOOD_NOTE }}</p>
                                    </li>
                                </ul>
                                <div class="restaurant-note restaurant-notes" ng-show="order.CUSTOMER_NAME != null && order.CUSTOMER_NAME != ''"><strong><?php echo __( 'Customer Name', 'gastro' );?></strong>: {{ order.CUSTOMER_NAME }}</div>
                                <div ng-show="order.CUSTOMER_NOTE">
                                    <p class="restaurant-note restaurant-notes"><stronMg>Customer Note:</stronMg> {{ order.CUSTOMER_NOTE }}</p>
                                </div>
                            </div>
                        </div>
                        <div ng-if="orders.length == 0" class="list-group">
                            <a href="#" class="list-group-item warning"><?php echo __( 'No order for this kitchen', 'gastro' );?></a>
                        </div>
                    </div>
                    <!--div class="box-footer" style="background:inherit">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="btn-group">
                                    <div class="btn-group ">
                                        <button ng-click="selectAllItems( order )" class="btn btn-default btn-sm"><?php echo __( 'Select All', 'gastro' );?></button>
                                    </div>
                                    <div class="btn-group ">
                                        <button ng-click="unselectAllItems( order )" class="btn btn-default btn-sm"><?php echo __( 'Unselect All', 'gastro' );?></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="btn-group btn-group-justified">
        
                                    <div
                                        ng-show="ifAllSelectedItemsAre( 'not_ready', order )"
                                        ng-click="changeFoodState( order, 'in_preparation' )"
                                        class="btn-group ">
                                        <button class="btn btn-default"><?php echo __( 'Cook', 'gastro' );?></button>
                                    </div>
        
                                    <div
                                        ng-show="ifAllSelectedItemsAre( 'in_preparation', order )" 
                                        ng-click="changeFoodState( order, 'ready' )"
                                        class="btn-group ">
                                        <button class="btn btn-default"><?php echo __( 'Ready', 'gastro' );?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div--->
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12" ng-show="columns.length == 0 || noValidOrder == true">
                <?php echo tendoo_info( __( 'No order has been placed for this kitchen.', 'gastro' ) );?>
            </div>
        </div>
    </div>
    <div class="<?php echo store_option( 'gastro_show_booked_at_kitchen', 'no' ) === 'yes' ? 'col-md-4 col-lg-3 col-sm-6  col-xs-12' : 'hidden';?>">
        <div class="box box-solid box-info collapsed-box">
            <div class="box-header">
                <h3 class="box-title"><?php echo __( 'Booked Orders', 'gastro' );?></h3>
            </div>
        </div>
        <div class="booked-wrapper">
            <div ng-repeat="( index, order ) in bookedOrders" class="box box-success box-solid">
                <div class="box-header with-border">
                    <!--h3 class="box-title" v-if="order.metas.woocommerce_order_id != ''">{{ order.metas.woocommerce_order_id }}</h3-->
                    <h3 class="box-title">{{ order.CODE }}</h3>
                    <h5>&mdash; {{ calculateTime( order ) }}</h5>
                </div>
                <div class="box-body" style="padding: 5px">
                    <div class="btn-group btn-group-justified">
                        <a ng-click="showDetails( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-eye"></i> <?php echo __( 'Details', 'gastro' );?>
                        </a>
                        <a ng-click="queueOrderToKitchen( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-check"></i> <?php echo __( 'Queue', 'gastro' );?>
                        </a>
                        <a ng-click="npsPrint( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-print"></i> <?php echo __( 'Receipt', 'gastro' );?>
                        </a>
                        <a ng-click="npsPrintKitchen( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-print"></i> <?php echo __( 'Kitchen', 'gastro' );?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style media="screen">
    .restaurant-note, .restaurant-notes {
        border: solid 1px #d8d8d8;
        border-radius: 10px;
        padding: 5px 10px;
        margin: 5px 0;
        background: #F2F2F2;
    }
    .restaurant-notes {
        font-weight: normal !important;
        font-style: italic;
        color: #ff6600;
    }

    .active .restaurant-note{
        color : #333;
    }
    .order-status {
        padding: 0 20px;
        text-align: center;
        border-radius: 10px;
        font-weight: 700;
        color: #007bff;
    }
    .kitchen-box .bg-primary {
    color: #fff;
    background-color: #337ab7 !important;
    }

    .kitchen-box .bg-success {
    background-color: #dff0d8 !important;
    }

    .kitchen-box .bg-info {
    background-color: #d9edf7 !important;
    }

    .kitchen-box .bg-warning {
    background-color: #fcf8e3 !important;
    }

    .kitchen-box .bg-danger {
    background-color: #f2dede !important;
    }

    .list-group-item.meal, .booked-for {
        padding: 1rem;
        font-size: 1.52rem;
        font-weight: bold;
    }

    .kitchen-box .info {
        padding: 1.5rem;
        font-size: 2rem;
    }

    .kitchen-box .btn-sm {
        font-size: 1.5rem;
        font-weight: bold;
        border-radius: 10px !important;
        margin: 1rem;
    }

    .kitchen-box .btn-kitchen {
        font-size: 2rem;
        font-weight: bold;
        border-radius: 10px !important;
    }

    .kitchen-box .cook {
        background-color: #0000cc;
        color: white;
    }

    .kitchen-box .ready {
        background-color: #00d235;
        color: white;
    }

    .kitchen-box .badge-not_ready {
        font-size: 1.5rem;
        background-color: red;
        padding: 8px;
    }

    .kitchen-box .badge-ready {
        font-size: 1.5rem;
        background-color: green;
        padding: 8px;
    }

    .kitchen-box .badge-in_preparation {
        font-size: 1.5rem;
        background-color: orange;
        padding: 8px;
    }
</style>
<style>
.content-wrapper {
    display: flex;
    flex-direction: column;
}
.content-wrapper .content {
    flex: 1 1 100%;
    display: flex;
    flex-direction: column;
    margin-left: 0;
    margin-right: 0;
}
.content-wrapper .content .row {
    flex: 1 1 100%;
    display: flex;
    height: 100%;
    flex-grow: 1;
}
.content-wrapper .content .row > .col-md-3 {
    flex-grow: 0;
    overflow-y: auto;
    display: flex;
    flex-direction: column;
}
.content-wrapper .content .row > .col-md-3 .booked-wrapper {
    display: flex;
    flex-direction: column;
    flex: 1 0 100%;
    flex-grow: 1;
    flex-shrink: 0;
    flex-basis: 0;
    overflow-y: auto;
    height: 100%;
    flex-basis: 0;
}
</style>
<?php $this->events->do_action( 'gastro_kitchen_footer' );?>