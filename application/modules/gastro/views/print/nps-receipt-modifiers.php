<?php
if (is_string(@$product[ 'metas' ][ 'modifiers' ])) {
    $product['metas']['modifiers'] = json_decode(@$product['metas']['modifiers'], true);
}
?>
<?php if ( ! empty( @$product[ 'metas' ][ 'modifiers' ] ) ):?>
    <?php foreach( @$product[ 'metas' ][ 'modifiers' ] as $modifier ):?>
    <text-line>-> <?php echo $modifier->group_name;?> : <?php echo $modifier->name;?> (<?php echo trim( get_instance()->Nexo_Misc->cmoney_format( $modifier->price ) );?>)</text-line>
    <?php endforeach;?>
<?php endif;?>