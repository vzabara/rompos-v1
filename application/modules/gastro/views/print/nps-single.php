<?php 
$this->load->model( 'Nexo_Misc' );
$this->cache        =   new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'gastro_nps_print_status_' . store_prefix() ));
$printed_items      =   ! $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] ) ? []   :   $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] );
echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<document>
    <align mode="center">
        <bold>
            <text-line size="1:1"><?php echo store_option( 'site_name' );?></text-line>
        </bold>
    </align>
    <line-feed></line-feed>
    <text>
        <text-line><?php echo nexting([
            __( 'Table :', 'gastro' ),
            @$table[0] ? $table[0][ 'NAME' ] : __( 'N/A', 'gastro' )
        ]);
        ?></text-line>
        <line-feed></line-feed>
        
        <text-line><?php echo nexting([
            __( 'By :', 'gastro' ),
            @$order[ 'AUTHOR_NAME' ]
        ]);
        ?></text-line>
        <line-feed></line-feed>

        <text-line><?php echo nexting([
            __( 'Date :', 'gastro' ),
            @$order[ 'DATE_CREATION' ]
        ]);
        ?></text-line>
        <line-feed></line-feed>

        <text-line><?php echo nexting([
            __( 'Order :', 'gastro' ),
            @$order[ 'CODE' ]
        ]);
        ?></text-line>
        <line-feed></line-feed>
        
        <text-line><?php echo nexting([
            __( 'Kitchen :', 'gastro' ),
            @$kitchen[ 'NAME' ] ?: __( 'N/A', 'gastro' )
        ]);
        ?></text-line>
        <line-feed></line-feed>

        <?php
        if( $order ) {
            switch( $order[ 'RESTAURANT_ORDER_TYPE' ] ) {
                case 'dinein' :  $type  =   __( 'Dine in', 'gastro' ); break;
                case 'takeaway' :  $type  =   __( 'Take Away', 'gastro' ); break;
                case 'delivery' :  $type  =   __( 'Delivery', 'gastro' ); break;
                default: $type  =   __( 'Unknow Order Type', 'gastro' ); break;
            }
        }
        ?>
        <text-line><?php echo nexting([
            __( 'Type :', 'gastro' ),
            @$type
        ]);
        ?></text-line>
        <line-feed></line-feed>

    </text>
    <align mode="left">
        <text-line><?php echo nexting( [], '-' );?></text-line>
        <?php foreach( $items as $item ):?>
            <?php if ( ! in_array( $item[ 'COMMAND_PRODUCT_ID' ], $printed_items ) ):?>
                <?php $modifiers    =   json_decode( $item[ 'MODIFIERS' ], true );?>
                <text-line><?php echo nexting( [], ' ' );?></text-line>
                <text-line>Customer: <?php echo $item[ 'CUSTOMER' ];?></text-line>
                <text-line><?php echo nexting( [], '-' );?></text-line>
                <text-line><?php echo nexting([
                    $item[ 'NAME' ],
                    'x' . $item[ 'QTE_ADDED' ]
                ]);
                ?></text-line>
                    <?php if ( $modifiers ):?>
                        <?php foreach( $modifiers as $modifier ):?>
                        <text-line>-> <?php echo $modifier[ 'group_name' ] . ' : ' . $modifier[ 'name' ];?></text-line>
                        <?php endforeach;?>
                    <?php endif;?>
                <text-line><?php echo nexting( [], '-' );?></text-line>
                <text-line size="1:0"><?php echo $item[ 'FOOD_NOTE' ];?></text-line>
                <?php $printed_items[]  =   $item[ 'COMMAND_PRODUCT_ID' ];?>
            <?php endif;?>
        <?php endforeach;?>
        <?php
        $this->cache->save( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ], $printed_items, module_config( 'gastro', 'gastro.gastro_printed_status_timeout' ) );
        ?>
    </align>
    <line-feed></line-feed>
    <paper-cut></paper-cut>
</document>