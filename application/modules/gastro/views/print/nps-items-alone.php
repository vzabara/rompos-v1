<?php 
$this->load->model( 'Nexo_Misc' );
$this->cache        =   new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'gastro_nps_print_status_' . store_prefix() ));
$printed_items      =   ! $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] ) ? []   :   $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] );
echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<document>
    <?php foreach( $items as $item ):?>
    <?php if ( ! in_array( $item[ 'ITEM_ID' ], $printed_items ) ):?>
    <text>
        <text-line><?php echo nexting([
            __( 'Customer :', 'gastro' ),
            @$item[ 'CUSTOMER' ]
        ]);
        ?></text-line>

        <text-line><?php echo nexting([
            __( 'Date :', 'gastro' ),
            @$order[ 'DATE_CREATION' ]
        ]);
        ?></text-line>
        <line-feed></line-feed>

    </text>
    <align mode="left">
        <text-line><?php echo nexting( [], '-' );?></text-line>
                <?php $modifiers    =   json_decode( @$item[ 'metas' ][ 'modifiers' ], true );?>
                <text-line><?php echo nexting([
                    $item[ 'NAME' ],
                    'x' . $item[ 'QTE_ADDED' ]
                ]);
                ?></text-line>
                    <?php if ( $modifiers ):?>
                        <?php foreach( $modifiers as $modifier ):?>
                        <text-line>-> <?php echo $modifier[ 'group_name' ] . ' : ' . $modifier[ 'name' ];?></text-line>
                        <?php endforeach;?>
                    <?php endif;?>
                <text-line><?php echo nexting( [], '-' );?></text-line>
                <text-line size="1:0"><?php echo @$item[ 'metas' ][ 'restaurant_note' ];?></text-line>
                <?php $printed_items[]  =   $item[ 'ITEM_ID' ];?>
    </align>
    <line-feed></line-feed>
    <paper-cut></paper-cut>
    <?php endif;?>
    <?php endforeach;?>
    <?php
    $this->cache->save( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ], $printed_items, module_config( 'gastro', 'gastro.gastro_printed_status_timeout' ) );
    ?>
</document>