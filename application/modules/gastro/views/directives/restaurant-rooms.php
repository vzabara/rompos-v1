<script>
var restaurantRooms    =   {
    url     :   {
        template    :   '<?php echo site_url([ 'dashboard', store_slug(), 'gastro', 'templates', 'table_selection' ] );?>',
        postChange  :   '<?php echo site_url([ 'api', 'gastro', 'orders', 'move', store_get_param( '?' )]);?>'
    },
    messages         :   {
        confirmMove             :   '<?php echo _s( 'Would you like to move that order ?', 'gastro' );?>',
        requireTableSelection   :   '<?php echo _s( 'You must select at least one table, before moving that order', 'gastro' );?>',
        errorOccurredWhileChanging  :   '<?php echo _s( 'An error occurred while changing the table. Please try again.', 'gastro' );?>'
    }
}
</script>
<script src="<?php echo module_url( 'gastro' ) . 'js/restaurant-rooms.directive.js';?>"></script>