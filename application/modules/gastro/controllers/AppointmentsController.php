<?php

class AppointmentsController extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  Tables Header
     *
     * @param  void
     *
     * @return void
     **/

    private function crud_header()
    {
        if ( ! User::can('manage_restaurant_appointments')) {
            return show_error(__('This feature has been disabled', 'gastro'));
        }

        /**
         * This feature is not more accessible on main site when
         * multistore is enabled
         **/

        if (multistore_enabled() && ! is_multistore()) {
            return show_error(__('This feature has been disabled', 'gastro'));
        }

        $this->load->model( 'grocery_CRUD_Model' );
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Appointment_model' );
        $crud = new grocery_CRUD();

        $crud->set_theme('bootstrap');
        $crud->set_subject(__('Appointments', 'nexo'));
        $crud->set_table($this->db->dbprefix(store_prefix() . 'nexo_restaurant_appointments'));
        $crud->set_model( 'Nexo_Gastro_Appointment_model' );

        $fields          = array('table_id', 'client_id', 'created_at', 'start_date', 'end_date', 'author_id', 'notes');
        $required_fields = ['start_date', 'client_id', 'table_id'];

        $crud->columns('client_id', 'table_id', 'created_at', 'start_date', 'end_date', 'author_id');

        $crud->fields($fields);

        $crud->display_as('client_id', __('Client', 'gastro'));
        $crud->display_as('notes', __('Notes', 'nexo'));
        $crud->display_as('author_id', __('Author', 'nexo'));
        $crud->display_as('created_at', __('Created On', 'nexo'));
        $crud->display_as('start_date', __('Start Date', 'nexo'));
        $crud->display_as('end_date', __('End Date', 'nexo'));
        $crud->display_as('table_id', __('Table', 'nexo'));

        $crud->field_type('created_at', 'hidden');
        $crud->field_type('author_id', 'invisible');

        $crud->set_relation('client_id', store_prefix() . 'nexo_clients', 'NOM');
        $crud->set_relation('table_id', store_prefix() . 'nexo_restaurant_tables', 'NAME');
        $crud->set_relation('author_id', 'aauth_users', 'name');

        $crud->callback_before_insert(array($this, 'before_insert'));
        $crud->callback_before_update(array($this, 'before_update'));
        $crud->required_fields($required_fields);

        $crud->unset_jquery();
        $output = $crud->render();

        foreach ($output->js_files as $files) {
            $this->enqueue->js(substr($files, 0, -3), '');
        }
        foreach ($output->css_files as $files) {
            $this->enqueue->css(substr($files, 0, -4), '');
        }

        return $output;
    }


    /**
     * Insert Validation
     * @use index
     * @return void
     **/

    public function before_insert( $post )
    {
        $post['author_id']  = User::id();
        $post['created_at'] = date_now();
//flog($post);
        return $post;
    }

    public function before_update( $post )
    {
        $this->checkAvailability( $post );
        $post['author_id'] = User::id();
//flog(['update_validation', $post]);
//        echo json_encode([
//            'error_fields'     =>  [
//                'field-start_date'          =>  __( 'Unable to update appointment', 'gastro' ),
//            ],
//            'error_message'     =>  __( 'Unable to update appointment', 'gastro' ),
//            'success'           =>  false
//        ]);
//        die;

        return $post;
    }

    public function lists($page = 'index', $id = null)
    {
        global $PageNow;

        if ($page == 'index') {

            $PageNow		=	'nexo/appointments/list';

            $this->Gui->set_title(store_title(__('Appointments', 'gastro')));
        } elseif ($page == 'delete') { // Check Deletion permission

            $PageNow		=	'nexo/appointments/delete';

            nexo_permission_check('manage_restaurant_appointments');

        } elseif ($page == 'edit') { // Check Deletion permission

            $PageNow		=	'nexo/appointments/edit';

            $this->Gui->set_title(store_title(__('Edit Appointment', 'gastro')));
            nexo_permission_check('manage_restaurant_appointments');

        } else {

            $PageNow		=	'nexo/appointments/create';

            $this->Gui->set_title(store_title(__('Create Appointment', 'gastro')));
            nexo_permission_check('manage_restaurant_appointments');
        }

        $data[ 'crud_content' ]    =    $this->crud_header();

        $this->load->view('../modules/gastro/views/appointments/list.php', $data);
    }

    protected function checkAvailability( $post )
    {
        // TODO: date format is dd/mm/yyyy
        $query = $this->db->select('*')
            ->from( store_prefix() . 'nexo_restaurant_appointments' )
            ->where( 'table_id', $post['table_id'] )
            ->where( 'start_date <=', date('Y-m-d H:m:i', strtotime( $post['start_date'] ) ) )
            ->where( 'end_date >=', date('Y-m-d H:m:i', strtotime( $post['start_date'] ) ) )
        ->get();
        $data   =   $query->result_array();

//        flog([$post,$data]);

        return !count($data);
    }
}
