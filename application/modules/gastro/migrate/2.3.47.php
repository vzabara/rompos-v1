<?php
$this->load->model( 'Nexo_Stores' );

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
}

$permissions    =   [ 
    'gastro.view.cancel-button'     =>      __( 'View Item Cancel Button', 'gastro' ),
];

foreach( $permissions as $namespace => $perm ) {
    if( get_instance()->auth->get_perm_id( $namespace ) == null ) {
        get_instance()->auth->create_perm( 
            $namespace,
            $perm
        );
    }

    $this->auth->allow_group( 'master', $namespace );
    $this->auth->allow_group( 'admin', $namespace );
    $this->auth->allow_group( 'store.manager', $namespace );
    $this->auth->allow_group( 'store.cashier', $namespace );
}