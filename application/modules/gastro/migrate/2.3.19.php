<?php

$this->load->model( 'Nexo_Stores' );

foreach([ 'gastro.waiter', 'gastro.chief' ] as $role ) {
    get_instance()->auth->allow_group( $role, get_instance()->auth->get_perm_id( 'nexo.view.stores' ) );
    get_instance()->auth->allow_group( $role, get_instance()->auth->get_perm_id( 'nexo.enter.stores' ) );
    get_instance()->auth->allow_group( $role, get_instance()->auth->get_perm_id( 'edit_profile' ) );
}
