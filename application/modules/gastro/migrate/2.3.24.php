<?php
$this->load->model( 'Nexo_Stores' );

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_restaurant_modifiers_categories' );
    if( ! in_array( 'UPDATE_PRICE', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_restaurant_modifiers_categories` ADD `UPDATE_PRICE` boolean NOT NULL AFTER `FORCED`;');
    }
}

$this->auth->allow_group( 'gastro.chief', 'nexo.enter.stores' );
$this->auth->allow_group( 'gastro.waiter', 'nexo.enter.stores' );