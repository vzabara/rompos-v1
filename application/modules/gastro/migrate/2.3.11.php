<?php

$this->load->model( 'Nexo_Stores' );

$permissions    =   [ 
    'gastro.use.kitchen-screen'     =>      __( 'View the kitchen screen', 'gastro' ),
    'gastro.view.tokitchen'         =>      __( 'Send order to the kitchen', 'gastro' ),
    'gastro.view.paybutton'         =>      __( 'See the payment button', 'gastro' ),
    'gastro.view.discountbutton'    =>      __( 'See the discount button', 'gastro' ),
    'gastro.use.merge-meal'         =>      __( 'Use the merge feature', 'gastro' ),
];

foreach( $permissions as $namespace => $perm ) {
    if( get_instance()->auth->get_perm_id( $namespace ) == null ) {
        get_instance()->auth->create_perm( 
            $namespace,
            $perm
        );
    }
}

get_instance()->auth->allow_group( 'gastro.chief',  get_instance()->auth->get_perm_id( 'gastro.use.kitchen-screen' ) );
get_instance()->auth->allow_group( 'store.cashier', get_instance()->auth->get_perm_id( 'gastro.view.tokitchen' ) );
get_instance()->auth->allow_group( 'store.cashier', get_instance()->auth->get_perm_id( 'gastro.view.paybutton' ) );
get_instance()->auth->allow_group( 'store.cashier', get_instance()->auth->get_perm_id( 'gastro.view.discountbutton' ) );
get_instance()->auth->allow_group( 'store.cashier', get_instance()->auth->get_perm_id( 'gastro.use.merge-meal' ) );

foreach([ 'master', 'administrator', 'store.manager', 'store.demo' ] as $role ) {
    get_instance()->auth->allow_group( $role, get_instance()->auth->get_perm_id( 'gastro.view.discountbutton' ) );
}
