<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <itsupport@intelagy.com>
 * Date: 27.02.2019
 * Time: 14:21
 */
define('BASEPATH', __DIR__);

define('TAKEAWAY_TIMER', 10);
define('DELIVERY_TIMER', 30);

$shortopts  = "h";

$longopts  = [
    "store_id:",
    "host:",
];

$options = getopt($shortopts, $longopts);

if (isset($options['h'])) {
    die('Usage: php hold-order-status.php --store_id 1 --host http://rompos.local' . PHP_EOL);
}
if (!isset($options['store_id'])) {
    die('Usage: php hold-order-status.php --store_id 1 --host http://rompos.local' . PHP_EOL);
}

$host = $options['host'] ?? '';
if ($host && !preg_match('~^http[s]?:\/\/~', $host, $mm)) {
    die('Usage: php hold-order-status.php --store_id 1 --host http://rompos.local' . PHP_EOL);
}
$store_id = abs(intval($options['store_id']));

include_once __DIR__ . '/../modules/nexo/vendor/autoload.php';
include_once __DIR__ . '/../config/database.php';
include_once __DIR__ . '/../helpers/nexopos_helper.php';

$pdo = new PDO("mysql:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['char_set']}", $db['default']['username'], $db['default']['password']);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

$dbPrefix       =   $db['default']['dbprefix']; //
$prefix =  $store_id == 0 ? '' : 'store_' . $store_id . '_';

$stmt = $pdo->prepare("SHOW TABLES WHERE Tables_in_{$db['default']['database']}='{$dbPrefix}{$prefix}nexo_commandes'");
$stmt->execute();
$table = $stmt->fetchColumn() ?? '';
if (empty($table)) {
    die("It seems store ID is wrong. Table {$dbPrefix}{$prefix}nexo_commandes is not found." . PHP_EOL);
}

$stmt = $pdo->prepare("SELECT `key` FROM {$dbPrefix}restapi_keys LIMIT 1");
$stmt->execute();
$apiKey = $stmt->fetchColumn() ?? '';
$curl   = new Curl\Curl();
$curl->setHeader('X-API-KEY', $apiKey);

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}nexo_print_server_url' LIMIT 1");
$stmt->execute();
$printServerUrl = $stmt->fetchColumn() ?? '';

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}nexo_pos_printer' LIMIT 1");
$stmt->execute();
$printer = $stmt->fetchColumn() ?? '';

// get system timezone offset
$dt = new DateTime(null, new DateTimeZone(date('T')));
$offset1 = $dt->getOffset() / 3600;
// get MySQL timezone offset
$stmt = $pdo->query('SELECT @@system_time_zone');
$tz = $stmt->fetchColumn();
$dt = new DateTime(null, new DateTimeZone($tz));
$offset2 = $dt->getOffset() / 3600;
// offset in minutes
$offset = abs($offset1 - $offset2) * 60;
// get takeaway timer
$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}takeaway_timer' LIMIT 1");
$stmt->execute();
$takeawayTimer = $stmt->fetchColumn() ?? TAKEAWAY_TIMER;
// get delivery timer
$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}delivery_timer' LIMIT 1");
$stmt->execute();
$deliveryTimer = $stmt->fetchColumn() ?? DELIVERY_TIMER;
if ($host) {
    // print ready takeaway/dine-in orders
    $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes WHERE RESTAURANT_ORDER_STATUS=? AND RESTAURANT_ORDER_TYPE IN (?,?) AND NOW() >= DATE_SUB( `RESTAURANT_BOOKED_FOR`, INTERVAL " . ($offset + intval($takeawayTimer)) . " MINUTE)");
    $stmt->execute([
        'booking',
        'takeaway',
        'dinein',
    ]);
    $rows = $stmt->fetchAll();
    foreach ($rows as $row) {
        $url = $host . "/api/nexopos/local-print/{$row->ID}/?store_id=" . $store_id;
        $res = $curl->get($url);
        // save printing log
        $logFile = __DIR__ . '/../logs/printing-' . date('Y-m-d') . '.log';
        flog([
            'url'     => $url,
            'content' => $res,
            'printer' => $printer,
        ], $logFile);
        if (preg_match('~^<\?xml~', $res)) {
            $url = $printServerUrl . '/api/print';
            $curl->post($url, [
                'content' => $res,
                'printer' => $printer,
            ]);
        }
    }
    // print ready delivery orders
    $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes WHERE RESTAURANT_ORDER_STATUS=? AND RESTAURANT_ORDER_TYPE=? AND NOW() >= DATE_SUB( `RESTAURANT_BOOKED_FOR`, INTERVAL " . ($offset + intval($deliveryTimer)) . " MINUTE)");
    $stmt->execute([
        'booking',
        'delivery',
    ]);
    $rows = $stmt->fetchAll();
    foreach ($rows as $row) {
        $url = $host . "/api/nexopos/local-print/{$row->ID}/?store_id=" . $store_id;
        $res = $curl->get($url);
        $logFile = __DIR__ . '/../logs/printing-' . date('Y-m-d') . '.log';
        flog([
            'url'     => $url,
            'content' => $res,
            'printer' => $printer,
        ], $logFile);
        if (preg_match('~^<\?xml~', $res)) {
            $url = $printServerUrl . '/api/print';
            $curl->post($url, [
                'content' => $res,
                'printer' => $printer,
            ]);
        }
    }
}
try {
    $params = [
        'pending',
        'booking',
        'takeaway',
        'dinein',
    ];
    $stmt = $pdo->prepare("UPDATE {$dbPrefix}{$prefix}nexo_commandes SET `RESTAURANT_ORDER_STATUS`=?, `DATE_MOD`=NOW() WHERE RESTAURANT_ORDER_STATUS=? AND RESTAURANT_ORDER_TYPE IN (?,?) AND NOW() >= DATE_SUB( `RESTAURANT_BOOKED_FOR`, INTERVAL " . ($offset + intval($takeawayTimer)) . " MINUTE)");
    $stmt->execute($params);

    // check delivery hold orders
    $params = [
        'pending',
        'booking',
        'delivery',
    ];
    $stmt = $pdo->prepare("UPDATE {$dbPrefix}{$prefix}nexo_commandes SET `RESTAURANT_ORDER_STATUS`=?, `DATE_MOD`=NOW() WHERE RESTAURANT_ORDER_STATUS=? AND RESTAURANT_ORDER_TYPE=? AND NOW() >= DATE_SUB( `RESTAURANT_BOOKED_FOR`, INTERVAL " . ($offset + intval($deliveryTimer)) . " MINUTE)");
    $stmt->execute($params);
} catch (PDOException $e) {
    die($e->getMessage());
}
