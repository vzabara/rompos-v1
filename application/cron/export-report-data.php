<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <itsupport@intelagy.com>
 * Date: 26.11.2019
 * Time: 13:27
 */
define('BASEPATH', __DIR__);

$shortopts  = "hv";

$longopts  = [
    "store_id:",
    "date",
    "verbose",
    "dry-run",
    "help",
];

$options = getopt($shortopts, $longopts);

$verbose = isset($options['verbose']) || isset($options['v']);
$help = isset($options['help']) || isset($options['h']);
$dry_run = isset($options['dry-run']);

if ($help) {
    die('Usage: php export-report-data.php --store_id 1 --date 2019-11-26 -v' . PHP_EOL .
        'Or   : php export-report-data.php --store_id 1' . PHP_EOL);
}

if (!isset($options['store_id'])) {
    die('Usage: php export-report-data.php --store_id 1' . PHP_EOL);
}

$store_id = abs(intval($options['store_id']));
if (!empty($options['date'])) {
    $date = date('Y-m-d', strtotime($options['date']));
} else {
    $date = date('Y-m-d');
}

include_once __DIR__ . '/../modules/nexo/vendor/autoload.php';
include_once __DIR__ . '/../config/database.php';
include_once __DIR__ . '/../helpers/nexopos_helper.php';
include_once __DIR__ . '/../libraries/OAuth_Authorization_Header.php';

$pdo = new PDO("mysql:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['char_set']}",
    $db['default']['username'], $db['default']['password']);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
$dbPrefix       =   $db['default']['dbprefix']; //
$prefix =  $store_id == 0 ? '' : 'store_' . $store_id . '_';

$stmt = $pdo->prepare("SHOW TABLES WHERE Tables_in_{$db['default']['database']}='{$dbPrefix}{$prefix}nexo_commandes'");
$stmt->execute();
$table = $stmt->fetchColumn() ?? '';
if (empty($table)) {
    die("It seems store ID is wrong. Table {$dbPrefix}{$prefix}nexo_commandes is not found." . PHP_EOL);
}

$client_id = 0;
if (!$dry_run) {
    $stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}orders_client_id' LIMIT 1");
    $stmt->execute();
    $client_id = $stmt->fetchColumn() ?? '';
    if (empty($client_id)) {
        die("It seems client ID is wrong." . PHP_EOL);
    }
}

$date_before = date('Y-m-d', strtotime($date . ' +1 day'));
$date_after = date('Y-m-d', strtotime($date));

if (!$dry_run) {
    // check if report is already have been sent
    $stmt = $pdo->prepare("SELECT `id` FROM {$dbPrefix}reports_client_dates WHERE `client_id`=? AND `date`=?");
    $stmt->execute([
        $client_id,
        $date,
    ]);
    $id = $stmt->fetchColumn() ?? 0;
    if (intval($id) > 0) {
        die('Report is already have been sent');
    }
}

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}reports_sync_url' LIMIT 1");
$stmt->execute();
$url = $stmt->fetchColumn() ?? '';

if ( ! empty($url) || $dry_run ) {
    // collect orders
    $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes WHERE `DATE_CREATION` BETWEEN ? AND ?");
    $stmt->execute([
        $date_after,
        $date_before,
    ]);
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $orders = [];
    foreach ($rows as $row) {
        $order = [
            'code' => $row['CODE'],
            'type' => $row['RESTAURANT_ORDER_TYPE'],
            'subtype' => '',
            'total' => $row['SOMME_PERCU'],
            'payment_type' => $row['PAYMENT_TYPE'],
            'client_id' => $client_id,
            'source_id' => 1, // In Place by default
            'status' => $row['STATUS'],
            'created' => $row['DATE_CREATION'],
        ];

        // parse meta data
        $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes_meta WHERE `REF_ORDER_ID`=?");
        $stmt->execute([
            $row['ID'],
        ]);
        $points = [
            'points_earned' => 0,
            'points_redeemed' => 0,
            'redemption' => 0.0,
        ];
        $meta_rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($meta_rows as $meta_row) {
            switch ($meta_row['KEY']) {
                case 'group_order':
                    // group order flag
                    $order['subtype'] = 'group';
                    break;
                case '_ywpar_points_earned':
                    // YITH earned points
                    $points['points_earned'] = $meta_row['VALUE'];
                    break;
                case '_ywpar_redemped_points':
                    // YITH redeemed points
                    $points['points_redeemed'] = $meta_row['VALUE'];
                    break;
                case '_ywpar_coupon_amount':
                    // YITH redeemed amount
                    $points['redemption'] = $meta_row['VALUE'];
                    break;
                case 'woocommerce_order_id':
                    // Online store
                    $order['source_id'] = 2;
                    break;
            }
        }
        $order['points'] = $points;

        // collect customer info
        $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_clients WHERE `ID`=?");
        $stmt->execute([
            $row['REF_CLIENT'],
        ]);
        $customer = $stmt->fetch(PDO::FETCH_ASSOC);
        $order['customer'] = [
            'name' => trim($customer['NOM'] . ' ' . $customer['PRENOM']),
            'email' => $customer['EMAIL'],
            'phone' => $customer['TEL'],
            'created' => $customer['DATE_CREATION'],
        ];

        // collect products
        $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes_produits WHERE `REF_COMMAND_CODE`=?");
        $stmt->execute([
            $row['CODE'],
        ]);
        $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $products = [];
        foreach ($rows2 as $product) {
            $prod = [
                'title' => $product['NAME'],
                'codebar' => $product['REF_PRODUCT_CODEBAR'],
                'quantity' => $product['QUANTITE'],
                'price' => $product['PRIX'],
                'sale_price' => $product['PRIX_BRUT'],
                'total' => $product['PRIX_TOTAL'],
            ];

            // collect products meta
            $stmt = $pdo->prepare("SELECT `VALUE` FROM {$dbPrefix}{$prefix}nexo_commandes_produits_meta WHERE `REF_COMMAND_PRODUCT`=? AND `KEY`=?");
            $stmt->execute([
                $product['ID'],
                'modifiers',
            ]);
            $meta = [];
            $productMeta = $stmt->fetchColumn() ?? '';
            if (!empty($productMeta)) {
                $modifiers = json_decode($productMeta);
                if (!empty($modifiers)) {
                    foreach ($modifiers as $modifier) {
                        $meta[] = [
                            'name' => $modifier->name,
                            'price' => $modifier->price,
                        ];
                    }
                }
            }
            $prod['modifiers'] = $meta;
            $products[] = $prod;
        }
        $order['products'] = $products;


        // collect payments
        $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes_paiements WHERE `REF_COMMAND_CODE`=?");
        $stmt->execute([
            $row['CODE'],
        ]);
        $rows4 = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $payments = [];
        foreach ($rows4 as $payment) {
            $payments[] = [
                'amount'      => $payment['MONTANT'],
                'type'    => $payment['PAYMENT_TYPE'],
                'operation'   => $payment['OPERATION'],
                'created'      => $payment['DATE_CREATION'],
            ];
        }
        $order['payments'] = $payments;

        $orders[] = $order;
    }

    $serialized = serialize($orders);

    if (!$dry_run) {
        $stmt = $pdo->prepare("SELECT `key` FROM {$dbPrefix}restapi_keys LIMIT 1");
        $stmt->execute();
        $apiKey = $stmt->fetchColumn() ?? '';
        $curl = new Curl\Curl();
        $curl->setHeader('X-API-KEY', $apiKey);
        $res = $curl->post($url . '/rest/nexo/reports_import?store_id=' . $store_id, [
            'data' => $serialized,
        ]);
        if ($curl->error) {
            if ($verbose) {
                flog(['Curl error', $curl->errorMessage, $url . '/rest/nexo/reports_import?store_id=' . $store_id, $apiKey]);
            }
            die($curl->errorMessage);
        }
    }

    if ($verbose) {
        flog(['Prepared to send', unserialize($serialized)]);
    }
} else {
    die('Daily Reports Sync URL is empty');
}
