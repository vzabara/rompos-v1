<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <itsupport@intelagy.com>
 * Date: 26.07.2019
 * Time: 12:27
 */
define('BASEPATH', __DIR__);

$shortopts  = "hv";

$longopts  = [
    "store_id:",
    "host:",
    "verbose",
];

$options = getopt($shortopts, $longopts);

$verbose = isset($options['verbose']) || isset($options['v']);

if (isset($options['h'])) {
    die('Usage: php sync-orders.php --store_id 1 --host http://rompos.local -v' . PHP_EOL);
}

if (!isset($options['store_id'])) {
    die('Usage: php sync-orders.php --store_id 1 --host http://rompos.local -v' . PHP_EOL);
}

$host = $options['host'] ?? '';
if ($host && !preg_match('~^http[s]?:\/\/~', $host, $mm)) {
    die('Usage: php sync-orders.php ---store_id 1 --host http://rompos.local -v' . PHP_EOL);
}

$store_id = abs(intval($options['store_id']));

include_once __DIR__ . '/../modules/nexo/vendor/autoload.php';
include_once __DIR__ . '/../config/database.php';
include_once __DIR__ . '/../helpers/nexopos_helper.php';
include_once __DIR__ . '/../libraries/OAuth_Authorization_Header.php';

$pdo = new PDO("mysql:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['char_set']}",
    $db['default']['username'], $db['default']['password']);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
$dbPrefix       =   $db['default']['dbprefix']; //
$prefix =  $store_id == 0 ? '' : 'store_' . $store_id . '_';

$stmt = $pdo->prepare("SHOW TABLES WHERE Tables_in_{$db['default']['database']}='{$dbPrefix}{$prefix}nexo_commandes'");
$stmt->execute();
$table = $stmt->fetchColumn() ?? '';
if (empty($table)) {
    die("It seems store ID is wrong. Table {$dbPrefix}{$prefix}nexo_commandes is not found." . PHP_EOL);
}

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}orders_client_id' LIMIT 1");
$stmt->execute();
$client_id = $stmt->fetchColumn() ?? '';
if (empty($client_id)) {
    die("It seems client ID is wrong." . PHP_EOL);
}

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}orders_sync_url' LIMIT 1");
$stmt->execute();
$url = $stmt->fetchColumn() ?? '';

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}orders_overwrite_id' LIMIT 1");
$stmt->execute();
$overwrite_id = $stmt->fetchColumn() ?? '';

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}nexo_print_server_url' LIMIT 1");
$stmt->execute();
$printServerUrl = $stmt->fetchColumn() ?? '';

$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}nexo_pos_printer' LIMIT 1");
$stmt->execute();
$printer = $stmt->fetchColumn() ?? '';

if ( ! empty($url)) {
    // sync orders down
    $stmt = $pdo->prepare("SELECT `key` FROM {$dbPrefix}restapi_keys LIMIT 1");
    $stmt->execute();
    $apiKey = $stmt->fetchColumn() ?? '';
    $curl   = new Curl\Curl();
    $curl->setHeader('X-API-KEY', $apiKey);
    $res = $curl->post($url . '/api/nexopos/orders', [
        'client_id' => $client_id,
    ]);
    if ($curl->error) {
        if ($verbose) {
            flog(['Curl error', $curl->errorMessage]);
        }
        die($curl->errorMessage);
    }
    $products_to_orders = [];
    if ($res->success && $res->total) {
        if ($verbose) {
            flog(['response', $res]);
        }
        foreach ($res->orders as $order) {
            $refOrder   = $order->CODE;
            $refOrderId = $order->ID;

            $woocommerce_id = '';
            $group_order = '';
            // check meta data
            foreach ($res->meta as $meta) {
                if ($meta->REF_ORDER_ID == $refOrderId) {
                    if ($meta->KEY == 'woocommerce_order_id') {
                        $woocommerce_id = $meta->VALUE;
                    } else if ($meta->KEY == 'group_order') {
                        $group_order = $meta->VALUE;
                    }
                }
            }
            if ( ! empty( $overwrite_id ) && ! empty( $woocommerce_id )) {
                $order->CODE = $woocommerce_id . ( ! empty( $group_order ) ? ' Group' : '' );
            } else {
                $order->CODE .= '-' . $order->ID . ( ! empty( $group_order ) ? ' Group' : '' );
            }
            unset($order->ID);

            $products_to_orders[$refOrder] = [];

            // insert records into own store tables
            $insert_id = insert($pdo, "{$dbPrefix}{$prefix}nexo_commandes", (array)$order);
            foreach ($res->coupons as $coupon) {
                if ($coupon->REF_COMMAND == $refOrderId) {
                    $coupon->REF_COMMAND = $insert_id;
                    unset($coupon->ID);
                    insert($pdo, "{$dbPrefix}{$prefix}nexo_commandes_coupons", (array)$coupon);
                }
            }
            foreach ($res->meta as $meta) {
                if ($meta->REF_ORDER_ID == $refOrderId) {
                    $meta->REF_ORDER_ID = $insert_id;
                    unset($meta->ID);
                    insert($pdo, "{$dbPrefix}{$prefix}nexo_commandes_meta", (array)$meta);
                }
            }
            foreach ($res->payments as $payment) {
                if ($payment->REF_COMMAND_CODE == $refOrder) {
                    $payment->REF_COMMAND_CODE = $order->CODE;
                    unset($payment->ID);
                    insert($pdo, "{$dbPrefix}{$prefix}nexo_commandes_paiements", (array)$payment);
                }
            }
            foreach ($res->products as $product) {
                if ($product->REF_COMMAND_CODE == $refOrder) {
                    $product->REF_COMMAND_CODE = $order->CODE;
                    $odlId = $product->ID;
                    unset($product->ID);
                    $new_product_id = insert($pdo, "{$dbPrefix}{$prefix}nexo_commandes_produits", (array)$product);
                    $products_to_orders[$refOrder][$odlId] = $new_product_id;
                }
            }
            foreach ($res->products_meta as $product_meta) {
                if ($product_meta->REF_COMMAND_CODE == $refOrder) {
                    $product_meta->REF_COMMAND_CODE = $order->CODE;
                    unset($product_meta->ID);
                    $product_meta->REF_COMMAND_PRODUCT = $products_to_orders[$refOrder][$product_meta->REF_COMMAND_PRODUCT];
                    insert($pdo, "{$dbPrefix}{$prefix}nexo_commandes_produits_meta", (array)$product_meta);
                }
            }
            foreach ($res->shipping as $shipping) {
                if ($shipping->ref_order == $refOrderId) {
                    $shipping->ref_order = $insert_id;
                    unset($shipping->id);
                    insert($pdo, "{$dbPrefix}{$prefix}nexo_commandes_shippings", (array)$shipping);
                }
            }
            foreach ($res->clients as $client) {
                if (!empty($client->REF_COMMAND_CODE)) {
                    $orderId = $client->REF_COMMAND_CODE;
                    unset($client->REF_COMMAND_CODE);
                    $stmt = $pdo->prepare("SELECT `ID` FROM {$dbPrefix}{$prefix}nexo_clients WHERE `EMAIL`=? LIMIT 1");
                    $stmt->execute([
                        $client->EMAIL,
                    ]);
                    $find = $stmt->fetchColumn() ?? '';
                    if (empty($find)) {
                        // client was not found on our side
                        $ref_client_id = insert($pdo, "{$dbPrefix}{$prefix}nexo_clients", (array)$client);
                        foreach ($res->addresses as $address) {
                            if (!empty($address->REF_COMMAND_CODE)) {
                                if ($address->REF_COMMAND_CODE == $orderId) {
                                    unset($address->REF_COMMAND_CODE);
                                    $address->ref_client = $ref_client_id;
                                    insert($pdo, "{$dbPrefix}{$prefix}nexo_clients_address", (array)$address);
                                }
                            }
                        }
                    }
                }
            }
            // print order immediately
            $url = $host . "/api/nexopos/local-print/{$insert_id}/?store_id=" . $store_id;
            $response = $curl->get($url);
            // save printing log
            $logFile = __DIR__ . '/../logs/printing-' . date('Y-m-d') . '.log';
            flog([
                'url'     => $url,
                'content' => $response,
                'printer' => $printer,
            ], $logFile);
            if (preg_match('~^<\?xml~', $response)) {
                $url = $printServerUrl . '/api/print';
                $curl->post($url, [
                    'content' => $response,
                    'printer' => $printer,
                ]);
            }
        }
    }
    // sync orders up
    $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes_status LIMIT 10");
    $stmt->execute();
    $rows = $stmt->fetchAll();
    if ($rows) {
        $data = [];
        foreach ($rows as $row) {
            $data[] = [
                'id' => $row->id,
                'order' => $row->woocommerce_id,
                'status' => $row->status,
                'code' => $row->code,
            ];
        }
        if ( count( $data ) ) {
            $stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`=? LIMIT 1");
            $stmt->execute([
                "{$prefix}oauth_api_url"
            ]);
            $url = $stmt->fetchColumn() ?? '';
            $keys = [];
            foreach ( [
                'oauth_consumer_key',
                'oauth_consumer_secret',
                'oauth_token',
                'oauth_token_secret',
            ] as $key ) {
                $stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`=? LIMIT 1");
                $stmt->execute([
                    "{$prefix}$key"
                ]);
                $keys[$key] = $stmt->fetchColumn() ?? '';
            }
            $oauth = new OAuth_Authorization_Header();
            $oauth->init($keys, $url . '/wp-json/rompos/v1/statuses', 'POST');
            $curl   = new Curl\Curl();
            $header = $oauth->get_header();
            $curl->setOpt(CURLOPT_HTTPHEADER, ['Authorization: ', $header]);
            $curl->post($url . '/wp-json/rompos/v1/statuses', [
                'statuses'  => $data,
            ]);
            if ($curl->error) {
                if ($verbose) {
                    flog(['Curl error', $curl->errorMessage]);
                }
                die($curl->errorMessage);
            }
            foreach ($data as $params) {
                try {
                    $stmt = $pdo->prepare("DELETE FROM {$dbPrefix}{$prefix}nexo_commandes_status WHERE `id`=? AND `woocommerce_id`=? AND `status`=? AND `code`=?");
                    $stmt->execute( array_values( $params ) );
                } catch (PDOException $e) {
                    if ($verbose) {
                        flog(['Line ' . __LINE__, $e->getMessage()]);
                    }
                    echo $e->getMessage() . PHP_EOL;
                }
            }
        }
    }
}

function insert($pdo, $table, $values = array())
{
    global $verbose;

    $ins = [];
    foreach ($values as $field => $v) {
        $ins[] = ':' . $field;
    }
    $ins    = implode(',', $ins);
    $fields = '`' . implode('`,`', array_keys($values)) . '`';
    $sql    = "INSERT INTO $table ($fields) VALUES ($ins)";

    try {
        $stmt = $pdo->prepare($sql);
        foreach ($values as $f => $v) {
            $stmt->bindValue(':' . $f, $v);
        }
        $stmt->execute();
        if ($verbose) {
            flog([$sql, $values]);
        }

        return $pdo->lastInsertId();
    } catch (PDOException $e) {
        flog([$sql, $e->getMessage()]);
    }

    return 0;
}