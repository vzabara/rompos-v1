<?php
/**
 * Note: vendor is a directory name.
 */
return [
    [
        'title'    => __( 'Epson', 'gastro' ),
        'vendor' => 'epson',
    ],
    [
        'title'    => __( 'Star Micronics', 'gastro' ),
        'vendor' => 'star',
    ],
];
