<?xml version="1.0" encoding="UTF-8"?>
<document vendor="epson">
    {% if title is not empty %}
    <align mode="center">
        <bold><text-line size="3:3">{{template.title}}</text-line></bold>
    </align>
    <line-feed></line-feed>
    {% endif %}
    <align mode="center"><bold><text-line size="3:3">{{template.title}}</text-line></bold></align>
    <align mode="center"><text-line size="2:2">{{template.customer_name}}</text-line></align>
    {% if order.phone is not empty %}
    <align mode="center"><text-line size="2:2">Phone: {{order.phone}}</text-line></align>
    {% endif %}
    {% if order.email is not empty %}
    {#<align mode="center"><text-line>Email: {{order.email}}</text-line></align>#}
    {% endif %}
    {% if order.RESTAURANT_ORDER_TYPE == 'delivery' %}
    {% if template.delivery_address_1 is not empty %}<text-line>{{template.delivery_address_1}}</text-line>{% endif %}
    {% if template.delivery_address_2 is not empty %}<text-line>{{template.delivery_address_2}}</text-line>{% endif %}
    {% endif %}
    {% if date is not empty %}
    <align mode="center"><bold><text-line size="2:2">{{date}}</text-line></bold></align>
    {% endif %}
    {% if time is not empty %}
    <align mode="center"><bold><text-line size="2:2">{{time}}</text-line></bold></align>
    {% endif %}
    <line-feed></line-feed>
    <text>
        <text-line>{{labels.products}}</text-line>
        {% for item in items %}
        {% if item.metas.customer is not null %}
        <line-feed></line-feed>
        <align mode="left"><text-line>To: {{item.metas.customer}}</text-line></align>
        <line-feed></line-feed>
        {% endif %}
        <text-line size="2:2">{{item.product}}</text-line>
        {% for modifier in item.modifiers %}
        <align mode="left"><text-line>-> {{modifier}}</text-line></align>
        {% endfor %}
        {% if item.metas.restaurant_note is not empty %}
        <align mode="left"><text-line>Notes: {{item.metas.restaurant_note}}</text-line></align>
        <line-feed></line-feed>
        {% endif %}
        {% endfor %}
    </text>
    <line-feed></line-feed>
    {% if order.metas.customer_note is not empty %}
    <align mode="left"><text-line>{{order.metas.customer_note}}</text-line></align>
    <line-feed></line-feed>
    {% endif %}
    {% if print_prices %}
    <text>
        <text-line>{{dashes}}</text-line>
    </text>
    <bold>
        <text-line>{{subtotal}}</text-line>
        <text-line>{{deduction}}</text-line>
        {% if delivery is not empty %}<text-line>{{delivery}}</text-line>{% endif %}
        {% if fees is not empty %}
        {% for fee in fees %}
        <text-line>{{fee}}</text-line>
        {% endfor %}
        {% endif %}
        {% if taxes is not empty %}<text-line>{{taxes}}</text-line>{% endif %}
        <text-line>{{total}}</text-line>
        {% for payment in payments %}
        <text-line>{{payment}}</text-line>
        {% endfor %}
        {% if refund is not empty %}
        <text-line>{{refund}}</text-line>
        {% endif %}
        <text-line>{{collected}}</text-line>
        <text-line>{{dashes}}</text-line>
    </bold>
    <align mode="center">
        <text-line size="2:2">{{due}}</text-line>
    </align>
    <text>
        <text-line>{{dashes}}</text-line>
    </text>
    <align mode="center">
        <text-line>{{bills_notices}}</text-line>
    </align>
    {% if other_details is not empty %}
    <align mode="center"><text-line>{{other_details}}</text-line></align>
    {% endif %}
    <line-feed></line-feed>
    {% endif %}
    <paper-cut></paper-cut>
</document>