<?xml version="1.0" encoding="UTF-8"?><document vendor="epson">
    <text>
        <align mode="center"><bold><text-line size="3:3">{{order.CODE}}</text-line></bold></align>
        <align mode="center"><text-line size="2:2">{{item.CUSTOMER_NAME}}</text-line></align>
        {% if shipping.address_1 is not empty %}<text-line>{{shipping.address_1}}</text-line>{% endif %}
        {% if shipping.address_2 is not empty %}<text-line>{{shipping.address_2}}</text-line>{% endif %}
        <text-line>{{date}}</text-line>
        <line-feed></line-feed>
    </text>
    <align mode="left">
        <text-line>------------------------------------------------</text-line>
        <text-line>{{title}}</text-line>
        {% for modifier in modifiers %}
        <text-line>-> {{modifier.group_name}} : {{modifier.name}}</text-line>
        {% endfor %}
        <text-line>------------------------------------------------</text-line>
        <text-line size="1:0">{{item.metas.restaurant_note}}</text-line>
    </align>
    <line-feed></line-feed>
    <paper-cut></paper-cut>
</document>
