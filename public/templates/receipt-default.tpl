{% if not request.ignore_header %}
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{template.page_title}}</title>
<link rel="stylesheet" media="all" href="{{template.css_url}}/bootstrap.min.css" />
<link rel="stylesheet" media="all" href="{{template.module_url}}fonts/receipt-stylesheet.css" />
</head>
<body>
{% endif %}
{% if order is not empty %}
<div class="container-fluid">
    <div class="row">
        <div class="well col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="order-details">
                <div class="row">
                    {% if template.print_header == 'yes' %}
                        {% if template.logo is not empty %}
                        <div class="text-center">
                            <img src="{{template.logo.url}}" style="display:inline-block;{{template.logo.height}};{{template.logo.width}}"/>
                        </div>
                        {% else %}
                        <h2 class="text-center">{{template.site_name}}</h2>
                        {% endif %}
                    {% endif %}
                </div>
            </div>
            {% if request.is_pdf is not empty %}
            <br><br>
            {% endif %}
            <div class="row">
                <div class="text-center">
                    <h4>{{template.title}}</h4>
                </div>
            </div>
            <div class="row line">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 timeline1">
                    <div class="col-xs-6 text-left">{{template.date}}</div>
                    <div class="col-xs-6 text-right">{{template.time}}</div>
                </div>
            </div>
            {% if template.customer_name is not empty %}
            <div class="row line">
                <div class="timeline2 text-center">{{template.customer_name}}</div>
            </div>
            {% endif %}
            {% if template.delivery_date is not empty %}
            <div class="row line">
                <div class="timeline2 text-center">{{template.delivery_date}}</div>
            </div>
            {% endif %}
            {% if template.delivery_title is not empty %}
            <div class="row line">
                <div class="timeline2 text-center">{{template.delivery_title}}</div>
            </div>
            {% endif %}
            <div class="row line">
            {% for product in items %}
                {% if product.metas.customer is not null %}
                <div class="divider modifiers">To: {{product.metas.customer}}</div>
                {% else %}
                <div class="divider"></div>
                {% endif %}
                <div class="row item-row">
                    <div class="col-sm-9 text-product">
                        {% if item_name == 'only_secondary' %}
                        {{product.ALTERNATIVE_NAME}}
                        {% else %}
                        {% if product.DESIGN is not empty %}
                        {{product.DESIGN}}
                        {% else %}
                        {{product.NAME}}
                        {% endif %}
                        {{product.ALTERNATIVE_NAME}}
                        {% endif %}
                        <div class="row-modifiers">
                            {% if product.metas.modifiers is empty %}
                            <div class="spacer"></div>
                            {% else %}
                            {% for modifier in product.metas.modifiers %}
                            <div class="modifiers">{{modifier.name}}</div>
                            {% endfor %}
                            {% endif %}
                        </div>
                        {% if product.metas.restaurant_note is not empty %}
                        <div class="notes">Notes: {{product.metas.restaurant_note}}</div>
                        {% endif %}
                    </div>
                    <div class="col-sm-3 text-quantity">{{product.QUANTITE}}
                        <div class="text-price">{{product.total_price}}</div>
                    </div>
                </div>
                <div class="divider line"></div>
            {% endfor %}
            {% if order.metas.customer_note is not empty %}
            <div class="notes">Order notes: {{order.metas.customer_note}}</div>
            {% endif %}
            </div>
            <div class="divider"></div>
            <div class="row">
                <div class="col-sm-12 costs">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>{{template.sub_total_title}}:</th>
                                <th class="text-right text-costs">{{template.sub_total}}</th>
                            </tr>
                            {% if template.delivery is not empty %}
                            <tr>
                                <th>{{template.delivery_cost_title}}:</th>
                                <th class="text-right text-costs">{{template.delivery}}</th>
                            </tr>
                            {% endif %}
                            {% for fee in template.fees %}
                            <tr>
                                <th>{{fee.label}}:</th>
                                <th class="text-right text-costs">{{fee.value}}</th>
                            </tr>
                            {% endfor %}
                            <tr>
                                <th>{{template.tax_title}}:</th>
                                <th class="text-right text-costs">{{template.tax_total}}</th>
                            </tr>
                            <tr>
                                <th>{{template.grand_total_title}}</th>
                                <td class="text-right text-costs">{{template.grand_total}}</td>
                            </tr>
                            <tr>
                                <th>{{template.collected_title}}</th>
                                <td class="text-right text-costs">{{template.collected}}</td>
                            </tr>
                            <tr>
                                <th>{{template.due_title}}</th>
                                <td class="text-right text-costs">{{template.due}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="divider line"></div>
            <div class="text-center">
                <img width="50%" src="data:image/png;base64,{{template.barcode}}">
                <br>
                <h3 style="margin:5px 0">{{order.CODE}}</h3>
            </div>
            <div class="divider"></div>
            <p class="text-center">{{order.DESCRIPTION}}</p>
            <p class="text-center">{{template.nexo_bills_notices}}</p>
            {% if request.is_pdf is not empty %}
            <div class="container-fluid hideOnPrint">
                <div class="row hideOnPrint">
                    <div class="col-lg-12">
                        <a href="{{template.dashboard_url_orders}}" class="btn btn-success btn-lg btn-block">{{template.dashboard_url_orders_title}}</a>
                    </div>
                </div>
            </div>
            {% endif %}

        </div>
    </div>
</div>
{% else %}
<div class="container-fluid">{{template.error}}</div>
<div class="container-fluid hideOnPrint">
    <div class="row hideOnPrint">
        <div class="col-lg-12">
        <a href="{{template.dashboard_url_orders}}" class="btn btn-success btn-lg btn-block">{{template.dashboard_url_orders_title}}</a>
        </div>
    </div>
</div>
{% endif %}
<style>
@media print
{
    @page
    {
        size: auto;   /* auto is the initial value */
        /* this affects the margin in the printer settings */
        margin: 0mm 1mm 0mm 1mm;
    }
    html, body {
        background: #FFF;
        overflow:visible;
    }
    .shop-details {
        font-size: 10pt;
    }
    .col-sm-2 {
        width: 20% !important;
    }
}
    body
    {
        /* this affects the margin on the content before sending to printer */
        margin: 0px;
    }
    .shop-details {
        font-size: 6vw;
    }
    .hideOnPrint {
		display:none !important;
	}
	.order-details {
		font-size: 9vw;
	}
    p {
		font-size: 5vw;
	}
	.order-details h2 {
		font-size: 7vw;
	}
	h3 {
        font-size: 6vw;
        font-weight: bold;
	}
	h4 {
        font-size: 8vw;
        font-weight: bold;
	}
    .timeline1 {
        font-size: 4vw;
    }
    .timeline2, .text-bold, .modifiers {
        font-size: 6vw;
    }
    .text-bold {
        font-weight: bold;
    }
    .modifiers {
        padding-left: 5vw;
    }
    .divider {
        padding: 4vw;
    }
    .line {
        padding: 2vw 0;
        border-top: 1px solid #000;
    }
    .notes {
        font-size: 4vw;
    }
    .text-product {
        font-weight: bold;
        font-size: 7vw;
        float: left;
        width: 80%;
    }
    .text-quantity {
        font-weight: bold;
        font-size: 25vw;
        line-height: 15vw;
        text-align: right;
        float: right;
        width: 20%;
        position: relative;
    }
    .text-price {
        font-weight: normal;
        font-size: 6vw;
        text-align: right;
        white-space: nowrap;
        position: absolute;
        top: 14vw;
        right: 2vw;
    }
    .modifiers {
        font-weight: normal !important;
    }
    .costs {
        font-weight: bold;
        font-size: 5vw;
    }
    .text-costs {
        font-weight: normal !important;
        font-size: 5vw;
    }
    .spacer {
        height: 15vw;
    }

</style>
{% if request.autoprint is not empty %}
<script>
    window.print();
    window.close();
</script>
{% endif %}
{% if not request.ignore_header %}
</body>
</html>
{% endif %}
