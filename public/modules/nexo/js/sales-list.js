let salesCoreModal;
class orderBindjQueryEvent {
    constructor() {
        $( document ).ajaxComplete( () => {
            this.bind();
        });
        // initial bind
        this.bind();
    }

    /**
     * bind jquery Events
     * @return void
     */
    bind() {
        $( '.order-details' ).bind( 'click', function() {
            if ( $( this ).attr( 'scm-is-bound' ) === undefined ) {
                salesCoreModal     =   new SalesCoreModal( $( this ).data( 'order-id' ) );
                $( this ).attr( 'scm-is-bound', 'true' );
            }
        });
    }
}
$( document ).ready( function() {
    new orderBindjQueryEvent;
    // new SalesCoreModal( 1 );
});