String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

const ModalVue  =   function({ 
    namespace, 
    buttons = [
        {
            class: 'btn-primary',
            namespace: 'ok',
            label: textDomain.ok,
            disabled: false
        }, {
            class: 'btn-secondary',
            label: textDomain.cancel,
            namespace: 'cancel',
            disabled: false
        }
    ], 
    body, 
    title, 
    backdrop        = true, 
    keyboard        = true, 
    focus           = true, 
    width           = '40%', 
    height          = '30%',
    align           = 'center',
    methods         = {},
    mounted         = () => {},
    computed        = {},
    data            = {},
    buttonClasses   =   'btn',
    modalBodyClass  =   ''
}) {
    const markup        =   `
    <div class="modal fade" id="modal-vue-${namespace}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" style="display: flex">
        <div class="modal-dialog d-flex" role="document">
            <div class="modal-content">
                <div class="modal-vue modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">${title}</h5>
                    <button @click="close()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body ${modalBodyClass}">
                    ${body}
                </div>
                <div class="modal-footer">
                    <button :disabled="button.disabled" @click="clickOn( button )" v-for="button in buttons" type="button" :class="button.class" class="${buttonClasses}" data-dismiss="modal">{{ button.label }}</button>
                </div>
            </div>
        </div>
    </div>
    `;  

    $( 'body' ).append( markup );

    this.VueInstance  =   new Vue({
        el: `#modal-vue-${namespace}`,
        data: Object.assign( data, { namespace, buttons, title, body, width, height, align, markup, 
            elementSelector     :   `#modal-vue-${namespace}`
        }),
        mounted() {
            $( this.$el ).addClass( 'fade' );
            $( this.$el ).addClass( 'show' );
            $( this.$el ).css({
                background: 'rgba(51, 51, 51, 0.41)',
                alignItems: this.align
            });

            let css     =   BreakPointCSS;
            css         =   css.replaceAll('{namespace}', namespace);
            css         =   css.replaceAll('{elementSelector}', this.elementSelector);

            $( `#modal-vue-style-${namespace}` ).remove();

            /**
             * Create Stylesheet a smart way
             */
            [ 'width', 'height' ].forEach( measure => {
                if ( typeof this[ measure ] !== 'string' ) {
                    for( let bp in this[ measure ] ) {
                        if( [ 'xl', 'xs', 'lg', 'sm', 'md' ].indexOf( bp ) !== -1 ) {
                            css     =   css.replace( `/*${bp}${measure}*/`, `
                            /*${bp}${measure}*/
                            ${measure}: ${this[ measure ][bp]};
                            ` )
                        }
                    }
                } else {
                    $( this.$el ).find( '.modal-dialog' ).css({
                        [measure]: this[measure]
                    });
                }
            })

            $( 'body' ).append( css );

            $( this.$el ).find( '.modal-dialog' ).css({
                maxWidth: 'inherit',
                maxHeight: 'inherit'
            });

            /**
             * Exec Mounted
             * @type function
             */
            mounted( this );
        },
        methods: {
            clickOn( button ) {
                this.$emit( 'button.click', button );
            },

            close() {
                this.$emit( 'force.close.popup' );
            },

            ...methods
        },

        computed: {
            ...computed
        }
    });

    return new ModalListener( this.VueInstance );
}

const ModalListener     =   function( VueInstance ) {

    this.VueInstance    =   VueInstance;

    /**
     * Listen when the popup attempt to close
     */
    this.VueInstance.$on( 'force.close.popup', () => {
        this.removePopupMarkup();

        if ( this.callback !== undefined ) {
            this.callback( false );
        }
    });

    /**
     * Listen bouton click
     * @return void
     */
    this.VueInstance.$on( 'button.click', ( button ) => {

        let callbackResponse  =   undefined;

        if ( this.callback !== undefined ) {
            /**
             * If the callback doesn't return nothing
             * then we can close the popup
             */
            callbackResponse  =   this.callback( button.namespace );
        }
        if ( [ true, undefined ].indexOf( callbackResponse ) !== -1 ) {
            this.removePopupMarkup();
        }
    })

    this.removePopupMarkup  =   function() {
        $( this.VueInstance.$el ).removeClass( 'fade' );
        $( this.VueInstance.$el ).removeClass( 'show' );
        $( this.VueInstance.$el ).remove();
    }

    /**
     * Trigger Then method for the popup
     * @param {any} callback 
     */
    this.then           =   function( callback ) {
        this.callback   =   callback;
    }
    
    this.close          =   function() {
        this.VueInstance.$emit( 'force.close.popup' );
    }
}