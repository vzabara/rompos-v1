const ShippingAddressWrapper    =   function() {
    const modal     =   new ModalVue({
        namespace : 'shipping-address',
        height: {
            xl:'70%',
            lg:'70%',
            md:'70%',
            sm:'80%',
            xs:'95%',
        },
        width: {
            xl:'70%',
            lg:'70%',
            md:'70%',
            sm:'80%',
            xs:'95%',
        },
        data: {
            leftFields: [
                { 
                    label: textDomain.name,
                    placeholder: textDomain.name,
                    name: 'name',
                    value: '',
                    errors: [],
                    description: 'the name of the recipient'
                }, { 
                    label: textDomain.surname,
                    placeholder: textDomain.surname,
                    name: 'surname',
                    value: '',
                    errors: [],
                    description: 'The surname of the recipient'
                }, { 
                    label: textDomain.pobox,
                    placeholder: textDomain.pobox,
                    name: 'pobox',
                    value: '',
                    errors: [],
                    description: 'The post code address of the delivery'
                }, { 
                    label: textDomain.phone,
                    placeholder: textDomain.phone,
                    name: 'phone',
                    value: '',
                    errors: [],
                    description: 'The phone number to call before or after the delivery.'
                }
            ],
            rightFields: [
                { 
                    label: textDomain.address_1,
                    placeholder: textDomain.address_1,
                    value: '',
                    errors: [],
                    name: 'address_1',
                    description: 'Provide an address for the delivery'
                }, { 
                    label: textDomain.address_2,
                    placeholder: textDomain.address_2,
                    value: '',
                    errors: [],
                    name: 'address_2',
                    description: 'Provide an address for the delivery'
                }, { 
                    label: textDomain.country,
                    placeholder: textDomain.country,
                    value: '',
                    errors: [],
                    name: 'country',
                    description: 'The country where the delivery will be shipped'
                }, { 
                    label: textDomain.city,
                    placeholder: textDomain.city,
                    value: '',
                    errors: [],
                    name: 'city',
                    description: 'The city where the delivery will be shipped'
                }
            ]
        },
        modalBodyClass: 'modal-body oy-auto',
        body : `
        <div class="shipping-address-wrapper">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" v-for="field in leftFields">
                        <label for="exampleInputEmail1">{{ field.label }}</label>
                        <input @focus="field.errors = []" :class="{ 'is-invalid': field.errors.length > 0 }" v-model="field.value" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" :placeholder="field.placeholder">
                        <small v-if="field.errors.length === 0" class="form-text text-muted" v-html="field.description"></small>
                        <div class="invalid-feedback" v-if="field.errors.length > 0">
                            <p v-for="error in field.errors">{{ error }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" v-for="field in rightFields">
                        <label for="exampleInputEmail1">{{ field.label }}</label>
                        <input @focus="field.errors = []" :class="{ 'is-invalid': field.errors.length > 0 }" v-model="field.value" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" :placeholder="field.placeholder">
                        <small v-if="field.errors.length === 0" class="form-text text-muted" v-html="field.description"></small>
                        <div class="invalid-feedback" v-if="field.errors.length > 0">
                            <p v-for="error in field.errors">{{ error }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `,
        title: textDomain.defineShippingAddress,
        mounted() {
            /**
             * this will repopulate the form
             */
            console.log( FrontEndVue.order.shipping_address );
            [ ...this.leftFields, ...this.rightFields ].forEach( field => {
                if( FrontEndVue.order.shipping_address[ field.name ] !== undefined ) {
                    field.value    =   FrontEndVue.order.shipping_address[ field.name ];
                } else {
                    field.value    =   '';
                }
            })  
        },
        methods: {
            onConfirm() {
                
                let canProceed    =   true;
                const fields    =   [ ...this.leftFields, ...this.rightFields ];

                /**
                 * Make sure all field are filled before closing
                 */
                fields.forEach( field => {
                    field.errors    =   [];
                    if( 
                        [ 'address_1', 'address_2', 'name', 'surname' ].indexOf( field.name ) !== -1 && 
                        field.value.length === 0 ) {
                        canProceed    =   false;
                        field.errors.push( textDomain.fieldRequired )
                    }
                });

                if ( ! canProceed ) {
                    $.notify({ message: textDomain.shippingFormError }, {
                        type: 'danger',
                        z_index: 9999
                    });

                    return false;
                }

                return fields;
            }
        }
    });
    return modal;
}