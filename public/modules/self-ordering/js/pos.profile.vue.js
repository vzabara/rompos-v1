const ProfileModal  =   function( user ) {
    console.log( user );
    const modal     =   new ModalVue({
        namespace: 'profile-modal',
        backdrop: true,
        methods: {
            /**
             * Load the generated coupon for the 
             * provided user
             * @param {Object} user 
             */
            loadCouponFor( user ) {
                console.log( user );
                HttpRequest.get( `so/customer-coupons/${user.ID}?store_id=` + storeOptions.store_id ).then( result => {
                    this.coupons    =   result.data;
                    console.log( this.coupons );
                })
            },

            /**
             * redeem a coupon
             * @param {Object} coupon 
             */
            redeem( coupon ) {
                swal({
                    title: textDomain.confirmAction,
                    text: textDomain.useSelectedCoupon,
                    showCancelButton: true
                }).then( result => {
                    if ( result.value ) {
                        FrontEndVue.addCoupon( coupon );
                        modal.close();
                    }
                })
            },

            saveSettings() {
                this.proceedAsyncRequest    =   true;
                
                modal.VueInstance.buttons.forEach( button => button.disabled = true );

                HttpRequest.post( `so/save-settings?store_id=` + storeOptions.store_id, {
                    user,
                    password: this.password, 
                    password_confirm: this.password_confirm, 
                    old_password: this.old_password, 
                    [ csrf.name ] : csrf.hash
                }).then( ({data}) => {
                    if( data.status === 'failed' ) {
                        $.notify({
                            message: data.message
                        }, {
                            type: 'danger',
                            z_index: 9999
                        });
                        
                        if( data.errors_bag ) {
                            FrontEndVue.errors_bag     =   data.errors_bag;
                        }

                    } else if ( data.status === 'success' ) {
                        $.notify({
                            message: data.message
                        }, {
                            type: 'success',
                            z_index: 9999
                        });

                        /**
                         * let's reset the field since we won't use it anymore.
                         */
                        [ 'password', 'password_confirm', 'old_password' ].forEach( field => {
                            this[ field ]   =   '';
                        })

                        FrontEndVue.errors_bag      =   {};
                    }

                    modal.VueInstance.buttons.forEach( button => button.disabled = false );
                    this.proceedAsyncRequest    =   false;
                }).catch( error => {
                    $.notify({
                        message: textDomain.errorOccured
                    }, {
                        type: 'danger',
                        z_index: 9999
                    });
                });
            },

            /**
             * Set a tabe as active
             * @param {number} index 
             */
            setActive( index ) {
                this.tabs.forEach( (_tab, _index) => {
                    if ( _index === index ) {
                        _tab.active     =   true;
                    } else {
                        _tab.active     =   false;
                    }
                });

                if ( this.activatedTab.namespace === 'profile-coupons' ) {
                    this.loadCouponFor( user );
                }
            }
        },
        title: textDomain.userProfile,
        buttonClasses: 'btn btn-lg',
        height: {
            xs: '95%',
            sm: '90%',
            md: '80%',
            lg: '80%',
            xl: '80%'
        },
        width: {
            xs: '95%',
            sm: '60%',
            md: '60%',
            lg: '40%',
            xl: '40%'
        },
        data: {
            proceedAsyncRequest: false,
            tabs: [{
                label: textDomain.profileSettings,
                active: true,
                namespace: 'profile-settings'
            }, {
                label: textDomain.profileCoupons,
                active: false,
                namespace: 'profile-coupons'
            }],
            hasError: FrontEndVue.hasError,
            getErrors: FrontEndVue.getErrors,
            password : '',
            password_confirm : '',
            old_password : '',
            coupons: []
        },
        computed: {
            activatedTab() {
                const active    =   this.tabs.filter( tab => tab.active );
                return active[0]; // hope all table won't ever be disabled
            },
            usableCoupons() {
                return this.coupons.filter( coupon => {
                    return ( parseInt( coupon.USAGE_LIMIT ) > 0 && parseInt( coupon.USAGE_COUNT ) < parseInt( coupon.USAGE_LIMIT ) ) || parseInt( coupon.USAGE_LIMIT ) === 0
                })
            }
        },
        modalBodyClass: 'd-flex p-0 flex-row',
        body: `
        <div class="card border-0 w-100">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item" v-for="(tab, index) of tabs">
                        <a class="nav-link" @click="setActive( index )" :class="{ 'active' : tab.active }" href="javascript:void(0)">{{ tab.label }}</a>
                    </li>
                </ul>
            </div>
            <div class="card-body" v-if="activatedTab.namespace === 'profile-settings'">
                <div class="form-group">
                    <label for="exampleInputEmail1">${textDomain.old_password}</label>
                    <input v-model="old_password" :class="{ 'is-invalid': hasError( 'old_password' ), 'is-valid' : ! hasError( 'old_password' ) }" type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="${textDomain.old_password}">
                    <div class="invalid-feedback" v-if="hasError( 'old_password' )">
                        <p v-for="error in getErrors( 'old_password' )">{{ error }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">${textDomain.password}</label>
                    <input v-model="password" :class="{ 'is-invalid': hasError( 'password' ), 'is-valid' : ! hasError( 'password' ) }" type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="${textDomain.password}">
                    <div class="invalid-feedback" v-if="hasError( 'password' )">
                        <p v-for="error in getErrors( 'password' )">{{ error }}</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">${textDomain.password_confirm}</label>
                    <input v-model="password_confirm" :class="{ 'is-invalid': hasError( 'password_confirm' ), 'is-valid' : ! hasError( 'password_confirm' ) }" type="password" class="form-control" id="exampleInputPassword1" placeholder="${textDomain.password_confirm}">
                    <div class="invalid-feedback" v-if="hasError( 'password_confirm' )">
                        <p v-for="error in getErrors( 'password_confirm' )">{{ error }}</p>
                    </div>
                </div>
                <button @click="saveSettings()" :disabled="proceedAsyncRequest" class="btn btn-primary">${textDomain.save_settings}</button>
            </div>
            <div class="card-body" v-if="activatedTab.namespace === 'profile-coupons'">
                <h5 class="card-title">${textDomain.profileAvailableCoupons}</h5>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td>${textDomain.profileCouponCode}</td>
                            <td>${textDomain.profileCouponType}</td>
                            <td>${textDomain.profileCouponValue}</td>
                            <td>${textDomain.profileCouponExpiration}</td>
                            <td>${textDomain.action}</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="coupon in usableCoupons">
                            <td>{{ coupon.CODE }}</td>
                            <td>{{ coupon.DISCOUNT_TYPE }}</td>
                            <td v-if="coupon.DISCOUNT_TYPE === 'percentage'">{{ coupon.AMOUNT }}%</td>
                            <td v-if="coupon.DISCOUNT_TYPE === 'fixed'">{{ coupon.AMOUNT | moneyFormat }}</td>
                            <td>{{ coupon.EXPIRY_DATE }}</td>
                            <td>
                                <button @click="redeem( coupon )" class="btn btn-primary">${textDomain.redeemCoupon}</button>
                            </td>
                        </tr>
                        <tr v-if="usableCoupons.length === 0">
                            <td colspan="5">${textDomain.profileNotCoupon}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        `,
        mounted() {
            
        }
    })

    return modal;
}