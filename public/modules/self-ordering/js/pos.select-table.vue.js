const SelectTable    =   function( selectedTable ) {
    const modal     =   new ModalVue({
        namespace: 'ask-table',
        title: textDomain.whichTableIsUsed,
        buttonClasses: 'btn btn-lg',
        height: {
            xs: '95%',
            sm: '90%',
            md: '80%',
            lg: '80%',
            xl: '80%'
        },
        width: {
            xs: '95%',
            sm: '60%',
            md: '60%',
            lg: '40%',
            xl: '40%'
        },
        body : `
        <div class="seat-wrapper">
            <div class="row mx-1">
                <div @click="selectTable( table )" :class="{ 'busy-table' : table.STATUS === 'in_use', 'selected-table' : table.selected }" style="height: 250px;padding: 30px 20px" class="col-xs-6 col-sm-4 col-md-4 product-grid-item d-flex flex-column justify-content-center align-items-center" v-for="table in tables" >
                    <img style="width: 100%;max-height: 100%" :src="url + '/public/modules/self-ordering/images/restaurant.png'" alt="">
                    <p class="text-center">
                        <span>{{ table.TABLE_NAME }}</span><br>
                        <small>${textDomain.maxSeats} : {{ table.MAX_SEATS }}</small>
                    </p>
                </div>
            </div>
        </div>
        `,
        methods: {
            selectSeat( seat ) {
                this.seats.forEach( seat => seat.selected = false );
                seat.selected   =   true;
            },

            /**
             * Load Tables from Gastro
             * @return void
             */
            loadTables() {
                HttpRequest.get( this.url + 'api/gastro/tables?store_id=' + storeOptions.store_id ).then( ({data}) => {
                    this.tables     =   data.map( table => {
                        table.selected      =   false;

                        /**
                         * check if that table is already selected
                         */
                        if ( selectedTable !== null && table.TABLE_ID === selectedTable.TABLE_ID ) {
                            table.selected      =   true;
                            this.selectedTable  =   table;
                        }

                        return table;
                    });
                })
            },

            /**
             * Select a specific table
             * @param {Object} table 
             */
            selectTable( table ) {
                if ( table.STATUS === 'in_use' ) {
                    return $.notify({ message: textDomain.busyTable }, {
                        type: 'danger',
                        z_index: 9999
                    });
                }

                this.tables.forEach( table => table.selected = false );
                table.selected  =   true;
                this.selectedTable  =   table;
            }
        },
        data: Object.assign({ 
            tables  :   [],
            selectedTable   :   false
        }, storeOptions ),
        computed: {
        },
        mounted() {
            this.loadTables();
        }
    });

    return modal;
}