const LoginModalWrapper     =   function() {
    const modal     =   new ModalVue({
        namespace: 'login-modal',
        buttonClasses: 'btn btn-lg',
        body: `
        <div v-if="! isLoading" class="login-wrapper w-100">
            <div class="alert alert-info" role="alert">
                ${textDomain.loginAlert}
            </div>
            <div class="login-form">
                <div class="form-group">
                    <label for="username">${textDomain.username}</label>
                    <input v-model="username" type="text" class="form-control" id="username" placeholder="Your username / Email">
                </div>
                <div class="form-group">
                    <label for="password">${textDomain.password}</label>
                    <input v-model="password" type="password" class="form-control" id="password" placeholder="Input your password">
                </div>
            </div>
        </div>
        <div v-if="isLoading" style="margin:auto" class="loading-wrapper">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
        `,
        title: `Login`,
        width:  {
            xl: '30%',
            lg: '30%',
            sm: '50%',
            md: '70%',
            xs: '95%'
        },
        height: {
            xl: '60%',
            lg: '60%',
            sm: '60%',
            md: '80%',
            xs: '90%'
        },
        data: {
            isLoading : false,
            username: '',
            password: ''
        },
        mounted() {
            this.buttons.unshift({
                class: 'btn-secondary',
                namespace: 'login.guest',
                label: textDomain.loginAsGuest,
                disabled: false
            });

            // this.$on( 'button.click', this.watchButton );
        },
        methods: {
            // watchButton( button ) {
            //     console.log( button );
            //     return false;
            // },
            login( result ) {
                return new Promise( ( resolve, reject ) => {
                    if ( this.username === '' || this.password === '' ) {
                        new ModalVue({
                            title: textDomain.warning,
                            body: `<p>${textDomain.loginWarning}</p>`,
                            height: '20%',
                            width: '30%'
                        });

                        return reject({
                            status: 'failed',
                            message: textDomain.loginWarning
                        });
                    }

                    this.logUser().then( result => {
                        resolve( result );
                    }).catch( error => {
                        reject( error );
                    });
                })
            },

            authCriticalError({ reject, error }) {
                this.isLoading   =   false;

                $.notify({
                    message: textDomain.unexpectedErrorOccured
                }, {
                    type: 'danger',
                    z_index: 9999
                });

                /**
                 * Enable Back all buttons
                 */
                this.buttons.forEach( button => button.disabled = false );

                reject({
                    type: 'failed',
                    message: textDomain.unexpectedErrorOccured
                });
            },

            loginAsGuest() {
                return new Promise( ( resolve, reject ) => {
                    this.isLoading   =   true;

                    /**
                     * Center Loading Spinner
                     */
                    $( this.$el )
                        .find( '.modal-body' )
                        .addClass( 'd-flex align-middle' );

                    /**
                     * Disable All buttons
                     */
                    this.buttons.forEach( button => {
                        button.disabled     =   true;
                    });

                    HttpRequest.get( 'so/as-guest?store_id=' + storeOptions.store_id ).then( result => {
                        this.isLoading  =   false;
                        /**
                         * Enable Back all buttons
                         */
                        this.buttons.forEach( button => {
                            button.disabled     =   false;
                        });
    
                        if ( result.data.user !== undefined ) {
                            const user          =   result.data.user;
                            FrontEndVue.user    =   user;
                            FrontEndVue.topLeftButtons.forEach( button => {
                                if ( button.namespace === 'user.profile' ) {
                                    button.label    =   '<i class="fa fa-user"></i> ' + textDomain.welcome.replace( '#', user.NOM || user.username )
                                }
                            });

                            FrontEndVue.$forceUpdate();

                            resolve({
                                type: 'success',
                                message: 'user has been connected'
                            });

                        } else {
                            swal({
                                title:  FrontEndVue.textDomain.warning,
                                html:   result.data.message,
                                type:   'error'
                            });

                            reject({
                                status : 'failed',
                                message: result.data.message
                            });
                        }
                    }).catch( error => {
                        this.authCriticalError({ reject, error });
                    })
                })
            },

            logUser() {
                this.isLoading   =   true;

                /**
                 * Center Loading Spinner
                 */
                $( this.$el )
                    .find( '.modal-body' )
                    .addClass( 'd-flex align-middle' );

                /**
                 * Disable All buttons
                 */
                this.buttons.forEach( button => {
                    button.disabled     =   true;
                });

                /**
                 * Send Http Request to the server
                 * to test user credentials
                 */
                return new Promise( ( resolve, reject ) => {
                    HttpRequest.post( 'so/login?store_id=' + storeOptions.store_id, {
                        username : this.username,
                        password : this.password,
                        [ csrf.name ] : csrf.hash
                    }).then( result => {
                        this.isLoading  =   false;
                        /**
                         * Enable Back all buttons
                         */
                        this.buttons.forEach( button => {
                            button.disabled     =   false;
                        });
    
                        if ( result.data.user !== undefined ) {

                            const user          =   result.data.user;
                            FrontEndVue.user    =   user;
                            FrontEndVue.topLeftButtons.forEach( button => {
                                if ( button.namespace === 'user.profile' ) {
                                    button.label    =   '<i class="fa fa-user"></i> ' + textDomain.welcome.replace( '#', user.NOM || user.username )
                                }
                            });

                            resolve({
                                type: 'success',
                                message: 'user has been connected'
                            });

                        } else {
                            swal({
                                title:  FrontEndVue.textDomain.warning,
                                html:   result.data.message,
                                type:   'error'
                            });

                            reject({
                                status : 'failed',
                                message: result.data.message
                            });
                        }
                    }).catch( error => {
                        this.authCriticalError({ reject, error })
                    })    
                })
            }
        }
    });

    return modal;
}