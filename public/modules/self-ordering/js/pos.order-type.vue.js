const ModalWrapper  =   function() {
    const modal     =   new ModalVue({
        namespace : 'order-type-modal',
        width:  {
            xl: '40%',
            lg: '50%',
            sm: '50%',
            md: '70%',
            xs: '95%'
        },
        height: {
            xl: '70%',
            lg: '70%',
            sm: '80%',
            md: '80%',
            xs: '95%'
        },
        hideFooter: true,
        modalBodyClass: 'oy-auto modal-body d-flex flex-column',
        data: {
            types: [
                {
                    title: 'Dine in',
                    namespace: 'dinein',
                    image: storeOptions.url + '/public/modules/gastro/img/dinein.png'
                }, {
                    title: 'Take Away',
                    namespace: 'takeaway',
                    image: storeOptions.url + '/public/modules/gastro/img/takeaway.png'
                }, {
                    title: 'Delivery',
                    namespace: 'delivery',
                    image: storeOptions.url + '/public/modules/gastro/img/delivery.png'
                }
            ]
        },
        methods: {
            
            /**
             * Return the human name of the 
             * selected order type
             * @return string;
             */
            getOrderTypeName( orderType ) {
                if( [ 'dinein', 'takeaway', 'delivery' ].indexOf( orderType ) !== -1 ) {
                    return textDomain[ orderType ];
                }
                return textDomain.NotDefined;
            },

            selectOrderType( type ) {
                FrontEndVue.orderType  =   type.namespace;
                
                modal.close(true);
                
                FrontEndVue.topLeftButtons.forEach( button => {
                    if ( button.namespace === 'order_type' ) {
                        console.log( button );
                        button.label    =   `${textDomain.orderType} #`.replace( '#', `: ${this.getOrderTypeName( type.namespace )}` )
                        console.log( button );
                    }
                });
            }
        },
        computed: {},
        body: `
        <div class="order-type-wrapper px-2 flex-fill">    
            <div class="row p-2">
                <div @click="selectOrderType( type )" style="height: 250px;padding: 20px;" v-for="( type, index ) in types" class="p-2 col-md-6 col-xs-6 col-sm-6 product-grid-item">
                    <img alt="" :src="type.image" class="flex-fill img-grid" style="height: 100%; width: 100%">    
                    <div class="product-item-details">
                        <p class="text-center mb-1">{{ type.title }}</p>
                    </div>
                </div>
            </div>
        </div>
        `,
        title: textDomain.selectOrderType
    });

    return modal;
}