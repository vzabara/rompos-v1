const RegistrationWrapper   =   function() {
    const modal         =   new ModalVue({
        namespace : 'registration-popup',
        height: {
            xs: '95%',
            sm: '80%',
            md: '70%',
            lg: '70%',
            xl: '60%'
        },
        width: {
            xs: '95%',
            sm: '80%',
            md: '70%',
            lg: '50%',
            xl: '40%'
        },
        title: textDomain.register,
        data: {
            isLoading: false,
            username: '',
            password: '',
            password_confirm: '',
            email: '',
            errors_bag: {},
            formValidated: false,
            hasError: FrontEndVue.hasError,
            getErrors: FrontEndVue.getErrors,
        },
        methods: {
            
        },
        modalBodyClass: 'd-flex',
        body: `
        <div v-if="! isLoading" class="login-wrapper w-100">
            <form method="POST" class="needs-validation" :class="{ 'was-validated' : formValidated }">
                <div class="login-form">
                    <div class="form-group">
                        <label for="username">${textDomain.username}</label>
                        <input v-model="username" :class="{ 'is-invalid': hasError( 'username' ), 'is-valid' : ! hasError( 'username' ) }" type="text" class="form-control" id="username" placeholder="${textDomain.username}">
                        <div class="invalid-feedback" v-if="hasError( 'username' )">
                            <p v-for="error in getErrors( 'username' )">{{ error }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">${textDomain.email}</label>
                        <input v-model="email" type="email" :class="{ 'is-invalid': hasError( 'email' ), 'is-valid' : ! hasError( 'email' ) }" class="form-control" id="email" placeholder="${textDomain.email}">
                        <div class="invalid-feedback" v-if="hasError( 'email' )">
                            <p v-for="error in getErrors( 'email' )">{{ error }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">${textDomain.password}</label>
                        <input v-model="password" type="password" :class="{ 'is-invalid': hasError( 'password' ), 'is-valid' : ! hasError( 'password' ) }" class="form-control" id="password" placeholder="${textDomain.password}">
                        <div class="invalid-feedback" v-if="hasError( 'password' )">
                            <p v-for="error in getErrors( 'password' )">{{ error }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirm">${textDomain.password_confirm}</label>
                        <input v-model="password_confirm" type="password" :class="{ 'is-invalid': hasError( 'password_confirm' ), 'is-valid' : ! hasError( 'password_confirm' ) }" class="form-control" id="password_confirm" placeholder="${textDomain.password_confirm}">
                        <div class="invalid-feedback" v-if="hasError( 'password_confirm' )">
                            <p v-for="error in getErrors( 'password_confirm' )">{{ error }}</p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div v-if="isLoading" style="margin:auto" class="loading-wrapper">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
        `,
    });

    return modal;
}