const SelectSeatsWrapper    =   function( seats ) {
    const modal     =   new ModalVue({
        namespace: 'ask-person',
        title: textDomain.howManyJoinTheParty,
        buttonClasses: 'btn btn-lg',
        height: {
            xs: '95%',
            sm: '90%',
            md: '80%',
            lg: '80%',
            xl: '80%'
        },
        width: {
            xs: '95%',
            sm: '60%',
            md: '60%',
            lg: '40%',
            xl: '40%'
        },
        body : `
        <div class="seat-wrapper">
            <div class="row mx-1">
                <div style="height: 170px" :class="{ 'active' : seat.selected }" @click="selectSeat( seat )" class="col-xs-6 col-sm-4 col-md-4 product-grid-item d-flex flex-column justify-content-center align-items-center" v-for="seat in seats" >
                    <span style="font-size: 4em;">{{ seat.name }}</span>
                </div>
            </div>
        </div>
        `,
        methods: {
            selectSeat( seat ) {
                this.seats.forEach( seat => seat.selected = false );
                seat.selected   =   true;

                FrontEndVue.topLeftButtons.forEach( button => {
                    if ( button.namespace === "selected.seats" ) {
                        button.label    =   button.label.replace( /\((\d)+\)/gi, `(${seat.value})` );
                    }
                });
            }
        },
        data: { seats, _seatsSelected : false }
    });

    return modal;
}