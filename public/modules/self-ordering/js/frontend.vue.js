/**
 * to get rid of the error.
 */
const tendoo =  {
    loader: {
        show(){

        },
        hide() {

        }
    }
}

/**
 * For Debug Purpose
 */
Vue.config.performance  =   true;

let FrontEndVue;
jQuery( document ).ready( function() {
    Vue.filter( 'currency', ( value ) => {
        if ( currencyConfig.position === 'before' ) {
            return `${currencyConfig.symbol} ${value}`;
        }
        return `${value} ${currencyConfig.symbol}`;
    });
    FrontEndVue   =   new Vue({
        el : '#online-ordering',
        data: {
            cartItems           :   [],
            rawItems            :   [],
            rawCategories       :   [],
            payModal            :   null,
            loginModal          :   null,
            orderType           :   null,
            selectedTable       :   null,
            topLeftButtons      :   [],
            topRightButtons     :   [],
            returnTo            :   0,
            loadType            :   'categories',
            breadcrumbs         :   [],
            itemsQueue          :   [],
            checkoutQueue       :   [],
            errors_bag          :   {},
            seatsSelected       :   0, // specific for gastro
            user : false,
            order               :   {
                shipping_address: {}
            },
            textDomain,
            currencyConfig,
            ...storeOptions,
            tabs                :   [
                {
                    title: `${textDomain.cart} <span class="badge badge-primary total-item-badge">0</span>`,
                    namespace: 'cart',
                    active: true,
                }, {
                    title: `${textDomain.grid}`,
                    namespace: 'grid',
                    active: true,
                }
            ],
            responsive          :   new Responsive(),
            currentScreen       :   'lg'
        },
        watch: {
            user() {
                this.$forceUpdate();
            },
            currentScreen() {
                if( ! this.isComputer && this.countActive === 2 ) {
                    this.setTabActiveByNamespace( 'grid' );
                } else if ( this.isComputer && this.countActive === 1 ) {
                    // set both active
                    this.tabs.map( tab => tab.active = true );
                }
                this.$forceUpdate();
            },
            cartItems() {
                this.totalItems; // will trigger computed
                this.$forceUpdate();
            }
        },
        computed: {
            cartTabActive: function() {
                return this.tabs.filter( tab => tab.namespace == 'cart' && tab.active ).length > 0;
            },
            gridTabActive: function() {
                return this.tabs.filter( tab => tab.namespace == 'grid' && tab.active ).length > 0;
            },

            isComputer: function() {
                const assert    =   [ 'xg', 'xl', 'lg' ].indexOf( this.currentScreen ) != -1;
                return assert;   
            },

            countActive: function() {
                return this.tabs.filter( tab => tab.active ).length;
            },

            totalItems: function() {
                if( this.cartItems.length === 0 ) {
                    this.updateTotalItemsBadge(0);
                    return this.cartItems.length;
                }

                const totalItems    =   this.cartItems.map( item => {
                    return item.quantity;
                }).reduce( ( before, after ) => before + after );

                /**
                 * update total item badge
                 */
                this.updateTotalItemsBadge( totalItems );

                return totalItems;
            },
            totalPrice: function() {
                if( this.cartItems.length === 0 ) {
                    return this.cartItems.length;
                }

                return this.cartItems.map( item => {
                    return parseFloat( item.PRIX_DE_VENTE_TTC ) * item.quantity;
                }).reduce( ( before, after ) => before + after ).toFixed(2);
            },
            imageUploadPath: function() {
                return '/public/upload/items-images';
                // return this.url + '/public/upload/items-images';
            },
            maxHeight: function() {
                return 200;
            },
            maxWidth: function() {
                return 300;
            }
        },
        mounted() {
            this.currentScreen  =   this.responsive.is();
            $( window ).resize( () => {
                this.currentScreen  =   this.responsive.is();
            });

            this.buildTopButtons();
            this.buildAddItemQueue();
            this.buildToCheckoutQueue();
            this.loadCategories();
            $( '.main-loader' ).fadeOut( 500, function(){
                $( '#online-ordering > .row' ).fadeIn(500, function() {
                    $( '.main-loader' ).remove();
                });
            })
        },
        methods: {
            hasVisibleButtons( tabs ) {
                return tabs.filter( tab => tab.show() ).length > 0;
            },

            updateTotalItemsBadge( total ) {
                if ( $( '.total-item-badge' ).length > 0 ) {
                    $( '.total-item-badge' ).html( total );
                }
            },

            /**
             * Set a tab as active
             * @param {Object} tab 
             */
            setTabActive( tab ) {
                this.tabs.map( _tab => {
                    _tab.active  =   _tab.namespace === tab.namespace;
                });
            },

            setTabActiveByNamespace( namespace ) {
                return this.setTabActive({ namespace });
            },

            /**
             * Check wether a field has an error
             * @param {String} fieldName 
             */
            hasError( fieldName ) {
                if( this.errors_bag[ fieldName ] ) {
                    if( this.errors_bag[ fieldName ].length ) {
                        return false;
                    }
                    return true;
                }
                return null;
            },

            /**
             * Return Error available for a field
             * @param {String} fieldName 
             */
            getErrors( fieldName ) {
                if( this.errors_bag[ fieldName ] !== undefined ) {
                    if( this.errors_bag[ fieldName ].length === undefined ) {
                        const errors    =   [];
                        for( let type in this.errors_bag[ fieldName ] ) {
                            errors.push( this.errors_bag[ fieldName ][ type ] )
                        }
                        return errors;
                    }
                }
                return [];
            },

            /**
             * Get category Thumb
             * @return void
             */
            getThumb( resource, config = {} ) {
                console.log(resource);
                const { source = '/public/upload/categories', param = 'THUMB' } = config;
                if( resource[ param ] === '' || resource[ param ] === undefined ) {
                    return '/public/modules/nexo/images/default.png';
                }
                return `/thumb.php?src=${source}/`+resource[ param ]+`&w=200&h=200&zc=1`;
                // return `${source}/` + resource[ param ];
            },

            /**
             * Build To Checkout Queue
             * @return void
             */
            buildToCheckoutQueue() {
                /**
                 * Check if the cart is empty
                 */
                this.checkoutQueue.push( () => {
                    return new Promise( ( resolve, reject ) => {
                        if ( this.cartItems.length === 0 ) {
                            return reject({
                                status: 'failed',
                                message: this.textDomain.emptyCart
                            }); 
                        }

                        return resolve({
                            status: 'success',
                            message: 'can proceed'
                        })
                    });
                });

                /**
                 * Check if the user is connected
                 */
                this.checkoutQueue.push( () => {
                    return this.isConnected();
                });

                /**
                 * Select the order type
                 */
                this.checkoutQueue.push( () => {
                    return this.selectOrderType( true ); // skip if defined
                });

                /**
                 * if it's delivery, check if required field has been 
                 * filled
                 */
                this.checkoutQueue.push( () => {
                    return new Promise( ( resolve, reject ) => {

                        if( this.orderType === 'delivery' ) {
                            
                            /**
                             * no shpping field has been set
                             */
                            if( Object.values( this.order.shipping_address ).length === 0 ) {
                                this.openShippingAddress();
                                return reject({
                                    status: 'failed',
                                    message: textDomain.shippingFormError
                                });
                            }

                            for( field in this.order.shipping_address ) {
                                if( [ 'name', 'surname', 'address_1', 'address_2' ].indexOf( field ) !== -1 ) {
                                    if( this.order.shipping_address[ field ] === '' ) {
                                        this.openShippingAddress();
                                        return reject({
                                            status: 'failed',
                                            message: textDomain.shippingFormError
                                        });
                                    }
                                }
                            }

                        }
                        return resolve({
                            status: 'success',
                            message: 'shipping_set'
                        });
                    })
                });

                /**
                 * ASk how many person join the party
                 * @return void
                 */
                this.checkoutQueue.push( () => {

                    if( this.orderType === 'dinein' ) {
                        /**
                         * If the seats are'nt selected
                         * let's require it, otherwhise skip
                         */
                        if ( this.so_allow_table_selection === 'yes' ) {
                            if ( this.seatsSelected === 0 && this.selectedTable === null ) {
                                return this.openTableSelection();
                            } 
                        } else {
                            if ( this.seatsSelected === 0 ) {
                                return this.selectSeats();
                            } 
                        }
                    }

                    return new Promise( ( resolve ) => {
                        resolve({
                            status: 'success',
                            message: 'seat are selected'
                        });
                    })
                });
            },

            selectOrderType( skipIfDefined = false ) {
                return new Promise( ( resolve, reject ) => {
                    if( skipIfDefined ) {
                        if( FrontEndVue.orderType !== null ) {
                            return resolve({
                                status  :   'success',
                                message :   'order type defined'
                            })
                        }
                    }
    
                    const modal     =   ModalWrapper(); 
                    modal.then( result => {
                        if( result ) {
                            resolve({
                                status  :   'success',
                                message :   'order type defined'
                            });
                            
                            /**
                             * Apply this if the order type is 
                             * a dinein order
                             */
                            if ( this.orderType === 'dinein' ) {
                                /**
                                 * let's check if the table
                                 * selection is enabled
                                 */
                                if ( this.so_allow_table_selection === 'yes'  ) {
                                    this.openTableSelection();        
                                } else {
                                    this.selectSeats();
                                }
                            }

                            return true;
                        }

                        $.notify({
                            message: this.textDomain.orderTypeRequired
                        }, {
                            type: 'danger',
                            z_index: 9999
                        });
                        return false;
                    })
                })
            },

            /**
             * Select Seat
             * @return json
             */
            selectSeats() {
                return new Promise( ( resolve, reject ) => {
                    if( this.orderType === 'dinein' ) {
                        const seats     =   [];
                        for( i = 1; i <= this.maxium_seats; i++ ) {
                            seats.push({
                                name        : i,
                                selected    : FrontEndVue.seatsSelected === i ? true : false,
                                value       : i
                            });
                        }


                        let modal   =   SelectSeatsWrapper( seats );

                        modal.then( action => {
                            let selectedSeats   =   modal.VueInstance.seats.filter( seat => seat.selected );
                            if( action === 'ok' && selectedSeats.length > 0 ) {
                                
                                this.seatsSelected   =   selectedSeats[0].value;
                                this.buildTopButtons();

                                resolve({
                                    status: 'success',
                                    message: this.textDomain.seatHasBeenSelected
                                });

                            } else {
                                $.notify({
                                    message: this.textDomain.seatsRequired
                                }, {
                                    type: 'danger',
                                    z_index: 9999
                                });
                            }
                        })
                    } else {
                        resolve({
                            status: 'success',
                            message: 'no need to select seats'
                        });
                    }
                })
            },

            /**
             * Check wether a use is connected
             * @return promise
             */
            isConnected() {
                return new Promise( ( resolve, reject ) => {
                    if( this.user !== false ) {
                        return resolve({ 
                            status: 'success',
                            message: this.textDomain.userConnected
                        })
                    }
                    
                    this.requireLogin();
                    return reject({
                        status: 'failed',
                        message: this.textDomain.userNotConnected
                    })
                })
            },

            /**
             * Build Add Item Queue
             * @return void
             */
            buildAddItemQueue() {
                this.itemsQueue.push( ( item ) => {
                    return this.canIncreaseStock({ item, increaseBy: 1 });
                });

                /**
                 * Define the initial quantity
                 */
                this.itemsQueue.push( ( item ) => {
                    return new Promise( ( resolve, reject ) => {
                        item.quantity   =   1;
                        resolve({ mutables: { item }, status: 'success', message: 'quantity set' });
                    })
                });

                /**
                 * Define Metas
                 */
                this.itemsQueue.push( ( item ) => {
                    return new Promise( ( resolve, reject ) => {
                        item.metas   =   [];
                        resolve({ mutables: { item }, status: 'success', message: 'meta array defined' });
                    })
                });

                this.itemsQueue.push( ( item ) => {
                    return new Promise( ( resolve, reject ) => {
                        const modifiers     =   item.REF_MODIFIERS_GROUP.split(',');
                        if ( modifiers[0] !== '' ) {

                            /**
                             * Build Modifier Array
                             */
                            let modifierArray   =   [];
                            modifiers.forEach( id => {
                                modifierArray.push( () => {
                                    return this.askModifier({ item, id });
                                })
                            });
                            modifierArray.splice(1);
                            // modifierArray.push( this.askModifier( item , 0 ) );
                            this.queuePromises({ promises : modifierArray }).then( result => {
                                resolve( result );
                            }).catch( error => {
                                reject( error );
                            })
                        } else {
                            resolve({ mutables: { item }, type: 'success', message: 'success' });
                        }
                    })
                });
            },

            /**
             * Ask Modifier
             * @return void
             */
            askModifier({ item, id }) {
                return new Promise( ( resolve, reject ) => {
                    this.loader().show();
                    HttpRequest.get( `so/modifiers?store_id=` + storeOptions.store_id ).then(({ data }) => {
                    // HttpRequest.get( `so/modifiers/${id}?store_id=` + storeOptions.store_id ).then(({ data }) => {
                        this.loader().hide();
                        if( data.length === 0 ) {
                            return resolve({ item, status: 'success', message: this.textDomain.noModifierAvailable });
                        }

                        /**
                         * Build Default Selected
                         */
                        data.forEach( modifier => {
                            modifier.default === '0' ? modifier.selected = false: modifier.selected = true;
                            modifier.default = '1';
                        });

                        let modal   =   ModifierWrapper( data );

                        modal.then( result => {
                            if( result === 'ok' ) {
                                if ( modal.VueInstance.modifiers[0].group_forced === "1" && modal.VueInstance.modifiers.filter( modifier => modifier.selected ).length === 0 ) {
                                    $.notify({
                                        message: this.textDomain.modifierRequired
                                    }, {
                                        type: 'danger',
                                        z_index: 9999
                                    });
                                    return false;
                                }

                                let modifiers   =   modal.VueInstance.modifiers.filter( modifier => modifier.selected );
                                
                                /**
                                 * if a modifier has been added
                                 * we can push that to the metas.
                                 */
                                if ( modifiers.length > 0 ) {
                                    item.metas      =   item.metas.concat( modifiers );
                                }
                                
                                resolve({ mutables: {item}, status: 'success', message: this.textDomain.modifierAdded })

                                return true;
                            }
                        });
                    })
                })
            },

            /**
             * Reset cart
             * @return void
             */
            resetCart() {
                this.logout().then( result => {
                    this.cartItems                  =   [];
                    this.orderType                  =   null;
                    this.selectedTable              =   null;
                    this.seatsSelected              =   0;
                    this.order.shipping_address     =   {};
                    this.buildTopButtons();
                    this.loadCategories();
                    this.$forceUpdate();
                });
            },

            /**
             * Logout user
             * @return Promise
             */
            logout() {
                return new Promise( ( resolve, reject ) => {
                    this.loader().show();
                    return  HttpRequest.get( 'so/logout?store_id=' + storeOptions.store_id ).then( result => {
                        this.user               =   false;
                        this.loader().hide();
                        resolve( result );
                    });
                });
            },

            /**
             * Confirm Cart reset
             * @return void
             */
            confirmReset() {
                swal({
                    title: this.textDomain.confirm,
                    html: `<p>${this.textDomain.resetCartConfirmMessage}`,
                    showCancelButton: true
                }).then( result => {
                    if( result.value ) {
                        this.resetCart();
                    }
                })
            },

            /**
             * Send order to the cashier
             * basically save it as pending
             */
            toCheckout() {
                this.queuePromises({ promises: this.checkoutQueue }).then( result => {
                    swal({
                        title: this.textDomain.confirm,
                        html: `<p>${this.textDomain.confirmOrderMessage}</p>`,
                        showCancelButton: true,
                    }).then( result => {
                        if( result.value ) {
                            this.loader().show();

                            /**
                             * we need to update the real
                             * selling proce for each item after
                             * a modifier has been added to it
                             */
                            this.cartItems.forEach( item => {
                                item.PRIX_DE_VENTE         =   this.getSingleItemPrice( item );
                                item.PRIX_DE_VENTE_TTC     =   this.getSingleItemPrice( item );
                            });
                            
                            HttpRequest.post( 'so/order?store_id=' + storeOptions.store_id, {
                                items                   : this.cartItems,
                                default_table           : this.default_table,
                                seats                   : this.seatsSelected,
                                user                    : this.user,
                                author                  : this.author,
                                total                   : this.totalPrice,
                                order_type              : this.orderType,
                                shipping_address        : this.order.shipping_address,
                                table_enabled           : this.so_allow_table_selection,
                                selected_table          : this.selectedTable,
                                [ csrf.name ]           : csrf.hash
                            }).then(({ data }) => {
                                this.__handleCheckoutResponse( data );
                            }).catch( error => {
                                this.loader().hide();
                                console.log( error.response, error.result );
                            })
                        }
                    })
                }).catch( ({ message, status }) => {
                    $.notify({
                        message
                    }, {
                        type: [ 'failed' ].indexOf( status ) !== -1 ? 'danger' : 'info',
                        z_index: 9999
                    });
                    return false;
                });
            },

            /**
             * Handle the response provided by the 
             * server
             * @param {object} data 
             */
            __handleCheckoutResponse( data ) {
                this.loader().hide();
                /**
                 * The order has been successful
                 */
                if ( data.current_order !== undefined ) {
                    
                    $.notify({
                        message: this.textDomain.orderCreated
                    }, {
                        type: 'success'
                    });

                    /**
                     * Print using RomPOS Agent
                     */
                    if( this.printer.url ) {
                        $.ajax( 'local-print' + '/' + data.current_order[0].ID, {
                            success 	:	( printResult ) => {
                                $.ajax( this.printer.url + '/api/print', {
                                    type  	:	'POST',
                                    data 	:	{
                                        'content' 	:	printResult,
                                        'printer'	:	this.printer.name
                                    },
                                    dataType 	:	'json',
                                    success 	:	function( result ) {
                                        console.log( 'success' );
                                    },
                                    error		:	() => {
                                        console.log( 'error' );
                                    }
                                });
                            }
                        });
                    }

                    let copyTimeBeforeClosing     =   this.timerBeforeClosingPopup;
                    let timeInterval;
                    swal({
                        title: this.textDomain.orderAgain,
                        showCancelButton: true,
                        html: `<p>${this.textDomain.orderAgainDetails}</p><br><strong>${this.textDomain.closeSeconds.replace( '#', '<span class="time-left">' + (copyTimeBeforeClosing/1000) + '</span>' )}</strong>`,
                        onOpen: ( sw ) => {
                            timeInterval = setInterval(() => {

                                if( copyTimeBeforeClosing === 1000 ) {
                                    clearInterval( timeInterval );
                                    swal.close();
                                    this.resetCart();
                                }

                                $( '.time-left' ).text( copyTimeBeforeClosing / 1000 );
                                copyTimeBeforeClosing    -= 1000;

                            }, 1000 )
                        }
                    }).then( result => {
                        if( result.value ) {
                            this.cartItems      =   [];
                            this.seatsSelected  =   0;
                            this.$forceUpdate();
                            this.loadCategories();
                        } else {
                            this.resetCart();
                        }
                        clearTimeout( timeInterval );
                    })
                } else {
                    $.notify({
                        message: this.textDomain.errorOccured
                    }, {
                        type: 'danger'
                    });
                }
            },
            
            /**
             * Cancel
             * @retrun void
             */
            cancel() {
                
            },

            /**
             * require login
             * @return void
             */
            requireLogin() {
                this.loginModal   =   LoginModalWrapper();

                this.loginModal.confirm( result => {
                    return new Promise( ( resolve, reject ) => {
                        if( this.loginModal.VueInstance !== undefined ) {
                            this.loginModal.VueInstance.login( result ).then( result => {
                                resolve(true);
                            }).catch( error => {
                                resolve(false);
                            })
                        } else {
                            resolve( true );
                        }
                    })
                }).then( result => {
                    if ( result !== 'cancel' ) {
                        return new Promise( ( resolve, reject ) => {
                            this.loginModal.VueInstance.loginAsGuest().then( result => {
                                resolve( true );
                            }).catch( error => {
                                reject( false );   
                            })
                        })
                    }
                })
            },

            /**
             * OverLayClass
             * @return void
             */
            loader() {
                return new function() {
                    /**
                     * Show OverLay Loader
                     * @return void
                     */
                    this.show     =   () => {
                        $( 'body' ).append( `
                        <div class="overlay-loader" style="display:none">
                            <div class="lds-roller" style="margin: auto"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        </div>
                        ` );

                        $( '.overlay-loader' ).fadeIn(500);
                    }

                    /**
                     * Hide OverLay Loader
                     * @return void
                     */
                    this.hide           =   () => {
                        $( '.overlay-loader' ).fadeOut(500, function() {
                            $( this ).remove();
                        })
                    }
                }
            },

            /**
             * Open Registration Popup
             * @return void
             */
            openRegistrationPopup() {
                let modal   =   RegistrationWrapper();

                modal.then( action => {
                    if( action === 'ok' ) {
                        modal.VueInstance.isLoading         =   true;
                        modal.VueInstance.formValidated     =   false;
                        modal.VueInstance.buttons.forEach( button => button.disabled = true );
                        HttpRequest.post( 'so/register?store_id=' + storeOptions.store_id, {
                            username: modal.VueInstance.username,
                            email: modal.VueInstance.email,
                            password: modal.VueInstance.password,
                            password_confirm: modal.VueInstance.password_confirm,
                            [ csrf.name ]: csrf.hash
                        }).then(({ data }) => {
                            modal.VueInstance.formValidated     =   true;
                            modal.VueInstance.isLoading         =   false;
                            modal.VueInstance.buttons.forEach( button => button.disabled = false );
                            
                            if( data.status === 'failed' ) {
                                $.notify({
                                    message: data.message
                                }, {
                                    type: 'danger',
                                    z_index: 9999
                                });
                                
                                if( data.errors_bag ) {
                                    FrontEndVue.errors_bag     =   data.errors_bag;
                                }

                            } else if ( data.status === 'success' ) {
                                $.notify({
                                    message: data.message
                                }, {
                                    type: 'success',
                                    z_index: 9999
                                });
                                modal.close();
                            }

                        }).catch( error => {
                            modal.VueInstance.buttons.forEach( button => button.disabled = false );
                            modal.VueInstance.isLoading     =   false;
                            console.log( error );
                        }) 
                        return false;
                    }
                })
            },

            /**
             * Build Top Buttons
             * @return void
             */
            buildTopButtons() {
                this.topLeftButtons     =   [
                    {
                        label: '<i class="fa fa-sign-in"></i> ' + this.textDomain.login,
                        namespace: 'login',
                        show: () => {
                            return ! this.user;
                        },
                        class: 'btn btn-primary',
                        click : () => {
                            this.requireLogin();
                        }
                    }, {
                        label: '<i class="fa fa-user"></i> ' + this.textDomain.welcome,
                        namespace: 'user.profile',
                        show: () => {
                            return this.user;
                        },
                        class: 'btn btn-info',
                        click : () => {
                            console.log( 'should show profile' );
                            // FrontEndVue.resetCart();
                        }
                    },{
                        label: '<i class="fa fa-sign-out"></i> ' + this.textDomain.logout,
                        namespace: 'logout',
                        show: () => {
                            return this.user;
                        },
                        class: 'btn btn-danger',
                        click : () => {
                            FrontEndVue.resetCart();
                        }
                    }, {
                        label: this.textDomain.orderType,
                        namespace: 'order_type',
                        show: () => {
                            return this.user;
                        },
                        class: 'btn btn-info',
                        click : () => {
                            FrontEndVue.selectOrderType();
                        }
                    }, {
                        label: 'Define Seats (#)'.replace( '#', this.seatsSelected ),
                        namespace: 'login',
                        show: () => {
                            return this.orderType === 'dinein' && this.so_allow_table_selection === 'no';
                        },
                        class: 'btn btn-primary',
                        click : () => {
                            /**
                             * Only when the seat are set, we do reload the buttons
                             */
                            this.selectSeats();
                        }
                    }, {
                        label: textDomain.shippingAddress,
                        namespace: 'shipping_address',
                        show: () => {
                            return this.orderType === 'delivery';
                        },
                        class: 'btn btn-primary',
                        click : () => {
                            /**
                             * Only when the seat are set, we do reload the buttons
                             */
                            this.openShippingAddress();
                        }
                    }
                ];

                /**
                 * check if the table selection
                 * is enabled to activate his button
                 */
                if ( this.so_allow_table_selection === 'yes' ) {
                    this.topLeftButtons.push({
                        label: textDomain.selectTable,
                        namespace: 'select_table',
                        show: () => {
                            return this.orderType === 'dinein';
                        },
                        class: 'btn btn-primary',
                        click : () => {
                            /**
                             * Only when the seat are set, we do reload the buttons
                             */
                            this.openTableSelection();
                        }
                    });
                }

                this.topRightButtons    =   [
                    {
                        label: '<i class="fa fa-arrow-left"></i> ' + this.textDomain.goBackStores,
                        namespace: 'goback',
                        show: () => {
                            return this.multistore === 'enabled';
                        },
                        class: 'btn btn-primary',
                        click : () => {
                            document.location = this.url + 'so';
                        }
                    },
                    {
                        label: this.textDomain.register,
                        namespace: 'register',
                        show: () => {
                            return ! this.user;
                        },
                        class: 'btn btn-info',
                        click : () => {
                            FrontEndVue.openRegistrationPopup();
                        }
                    }, {
                        label: this.textDomain.lostPassword,
                        namespace: 'lost_pwd',
                        show: () => {
                            return ! this.user;
                        },
                        class: 'btn btn-info',
                        click : () => {
                            FrontEndVue.openLostPwdPopup();
                        }
                    }
                ]
            },

            openTableSelection() {
                return new Promise( ( resolve, reject ) => {
                    /**
                     * let's cancel the 
                     * selected table and seat
                     */

                    const modal         =   SelectTable( this.selectedTable );
                    modal.confirm( VueInstance => {
                        if ( VueInstance.selectedTable === false ) {
                            $.notify({ message: textDomain.mustSelectATable }, {
                                type: 'danger',
                                z_index: 9999
                            });
                            return false;
                        }

                        this.selectedTable  =   VueInstance.selectedTable;

                        const seats     =   [];
                        for( i = 1; i <= parseInt( VueInstance.selectedTable.MAX_SEATS ); i++ ) {
                            seats.push({
                                name        : i,
                                selected    : FrontEndVue.seatsSelected === i ? true : false,
                                value       : i
                            });
                        }


                        let modal   =   SelectSeatsWrapper( seats );

                        modal.then( action => {
                            let selectedSeats   =   modal.VueInstance.seats.filter( seat => seat.selected );
                            if( action === 'ok' && selectedSeats.length > 0 ) {
                                
                                this.seatsSelected   =   selectedSeats[0].value;
                                // this.buildTopButtons();
                                this.__writeSelectedTableAndSeat();

                                resolve({
                                    status  :   'success',
                                    message :   this.textDomain.seatHasBeenSelected,
                                    table   :   this.selectedTable,
                                    seats   :   this.selectedSeats
                                });

                            } else {
                                $.notify({
                                    message: this.textDomain.seatsRequired
                                }, {
                                    type: 'danger',
                                    z_index: 9999
                                });
                            }
                        })
                    });
                })
            },

            __writeSelectedTableAndSeat() {
                this.topLeftButtons.forEach( button => {
                    if ( 
                        button.namespace === 'select_table' && 
                        this.selectedTable !== null && 
                        this.seatsSelected !== null 
                    ) {
                        button.label    =   textDomain.selectTableShowDetails.replace( '#', this.selectedTable.TABLE_NAME + `(${this.seatsSelected})` );
                    }
                });
            },

            /**
             * Open Shipping Address
             * @return void
             */
            openShippingAddress() {
                const modal =   ShippingAddressWrapper();
                modal.confirm( result => {
                    if( result === false ) {
                        return result;
                    }

                    /**
                     * manupulate what the popup return here
                     */
                    const fields    =   {};

                    result.forEach( field => {
                        fields[ field.name ]    =   field.value;
                    });

                    this.order.shipping_address     =   fields;

                }).cancel( vue => {
                    console.log( 'has canceled', vue );
                });
            },
            
            /**
             * Lost Password Popup
             */
            openLostPwdPopup() {
                const modal     =   LostPasswordWrapper();

                modal.then( result => {
                    if( result === 'ok' ) {
                        modal.VueInstance.proceed();
                        return false;
                    }
                })
            },

            /**
             * Load Top Categories
             * @return void
             */
            loadCategories( id = 0, callback = null ) {
                return new Promise( ( resolve, reject ) => {
                    HttpRequest.get( `so/categories/${id}?store_id=` + storeOptions.store_id ).then( result => {

                        if ( typeof callback === 'function' ) {
                            callback({ data : result.data, position: 'after' });
                        }

                        /**
                         * If the id loaded is set to 0, which basically mean the root
                         * let's reset the breadcrumbs as well.
                         */
                        if( parseFloat( id ) === 0 ) {
                            this.breadcrumbs    =   [];
                        }
                    
                        this.breadcrumbs.push({
                            id,
                            name: result.data.category === null ? this.textDomain.home : result.data.category.NOM
                        });
    
                        this.loadType       =   result.data.type;
                        this.returnTo       =   result.data.return_to;
                        
                        if ( this.loadType === 'categories' ) {
                            this.rawCategories  =   result.data.categories;
                        } else {
                            this.rawItems       =   result.data.items;
                        }

                        resolve({ data : result.data, position: 'after' });
                    });
                })
            },

            /**
             * set bread index to
             * @param int
             * @return void
             */
            setBreadIndexTo( index, bread ) {
                /**
                 * Delete bread right before it's updated
                 */
                this.loadCategories( bread.id, ({ data, position }) => {
                    this.breadcrumbs.splice( index, this.breadcrumbs.length );                    
                }).then( ({ data, position }) => {
                })
            },

            /**
             * Go back to return to
             * @param int return to
             * @return void
             */
            goBackTo( index ) {
                if ( index !== 0 ) {
                    this.setBreadIndexTo( index, this.breadcrumbs[ index ])
                } else {
                    this.loadCategories( bread.id );
                }
            },

            /**
             * Add Item to cart
             * @return void
             */
            addToCart( cartItem ) {
                let item    =  Object.assign({}, cartItem );

                this.queuePromises({ promises : this.itemsQueue, param: item }).then( ({ item }) => {
                    this.cartItems.unshift( item );
                    this.$forceUpdate();
                }).catch( ({ item, status, message }) => {
                    if( status === 'failed' ) {
                        return $.notify({ message }, {
                            type: 'danger'
                        });
                    }
                })
            },

            /**
             * Promise Resolver
             * @return any
             */
            queuePromises({ promises, param, index = 0, total = 0, results = [], mutables = {} }) {

                // promisesArray, _item 
                if( total === 0 ) {
                    total   =   promises.length;
                }

                return new Promise( ( resolve, reject ) => {
                    if( promises[ index ] !== undefined ) {
                        promises[ index ]( param ).then( response => {
                            
                            results.push( response );

                            if( response.mutables !== undefined ) {
                                mutables     =   { ...response.mutables };
                            }

                            this.queuePromises({
                                promises,
                                param,
                                index   : index + 1,
                                results,
                                total,
                                mutables
                            }).then( result => {
                                resolve( result );
                            }).catch( error => {
                                reject( error );
                            });

                        }).catch( error => {                            
                            reject( error );
                        })
                    } else {
                        resolve({
                            status: 'success',
                            message: 'promise queue successful',
                            results,
                            ...mutables // merge the mutable within the result object
                        })
                    }
                });
            },
              
            /**
             * get Single Item price
             * @param {object} item object
             * @return int
             */
            getSingleItemPrice( item ) {
                if( Object.values( item.metas ).length > 0 ) {
                    
                    /**
                     * only meta which aren't mean to
                     * update the selling price of an item
                     */
                    const simpleMetas   =   item.metas.filter( meta => meta.group_update_price === '0' );
                    const mutableMetas  =   item.metas.filter( meta => meta.group_update_price === '1' );

                    /**
                     * let's update the item price
                     * according to the mutable option.
                     * the last is the one to edit the sale price
                     */
                    mutableMetas.forEach( meta => {
                        item.PRIX_DE_VENTE_TTC  =   parseFloat( meta.price );
                        item.PRIX_DE_VENTE      =   parseFloat( meta.price );
                    });

                    return ( parseFloat( item.PRIX_DE_VENTE_TTC ) + Object.values( simpleMetas )
                        .map( meta => parseFloat( meta.price ) ).reduce( ( prev, current ) => {
                        return parseFloat( prev ) + parseFloat( current );
                    } ) ).toFixed(2);
                }
                return ( parseFloat( item.PRIX_DE_VENTE_TTC ) ).toFixed(2);
            },

            /**
             * get total item price
             * @return int
             */
            getTotalItemPrice( item ) {
                if( Object.values( item.metas ).length > 0 ) {
                    return ( this.getSingleItemPrice( item ) * item.quantity ).toFixed(2);
                }
                return ( parseFloat( item.PRIX_DE_VENTE_TTC ) * item.quantity ).toFixed(2);
            },

            /**
             * Refresh Cart
             * @return void
             */
            refreshCart() {
                this.$forceUpdate();
            },

            /**
             * Can Increase item Stock
             * @return void
             */
            canIncreaseStock({ item, increaseBy }) {
                return new Promise( ( resolve, reject ) => {

                    if( item.STOCK_ENABLED !== '0' ) {
                        
                        let totalQuantityOnCart     =   0;
                        
                        this.cartItems.forEach( cartItem => {
                            if( cartItem.CODEBAR === item.CODEBAR ) {
                                totalQuantityOnCart     +=   cartItem.quantity;
                            }
                        });

                        if( ( totalQuantityOnCart ) - parseFloat( item.QUANTITE_RESTANTE ) === 0 ) {
                            return reject({
                                status: 'failed',
                                message: this.textDomain.stockExausted,
                                mutables: { item }
                            });
                        }                        
                    } 

                    return resolve({
                        status: 'success',
                        message: this.textDomain.itemAdded,
                        mutables: { item }
                    });
                })
            },

            /**
             *  increase quantity 
             */
            increaseQuantity( item, increaseBy = 1 ) {      
                /**
                 * if the stock management is enabled
                 */
                this.canIncreaseStock({ item, increaseBy }).then( success => {
                    item.quantity   +=  increaseBy;

                    /**
                     * Notify if the item has been added
                     */
                    $.notify({
                        message: textDomain.itemAdded
                    }, {
                        type: 'success',
                        delay: 2000                    
                    });
                }).catch( error => {
                    $.notify({
                        message: textDomain.stockExausted
                    }, {
                        type: 'danger'
                    });
                })
            },

            /**
             * Decrease Quantity
             * @return void
             */
            decreaseQuantity( item, decreaseBy = 1, index = null ) {
                if( item.quantity - decreaseBy >= 1 ) {
                    item.quantity--;
                } else {
                    /**
                     * Remove item from cart
                     */
                    this.cartItems.splice( index, 1 );
                }
            }
        }
    })
})