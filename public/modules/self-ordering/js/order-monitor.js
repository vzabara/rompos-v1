console.log( window );
const tendoo        =   {
    loader : {
        show() {},
        hide() {},
    }
};

/**
 * Create Moment Filter
 */
Vue.filter( 'date', function( dateString, format ) {
    return moment( dateString ).format( format );
});

const MonitorVue    =   new Vue({
    el: '#monitor-vue',
    data: {
        rawOrders:     [],
        fetchFromInterval: true,
        announcedOngoingOrders: []
    },
    mounted() {
        this.fetchOrders();
        setInterval( () => {
            this.fetchFromInterval  =   true;
            this.fetchOrders();
        }, 3000 );
    },
    watch: {
        rawOrders() {
            if( this.ongoingOrders.length > 0 && this.fetchFromInterval ) {
                this.ongoingOrders.forEach( order => {
                    /**
                     * if the order hasn't yet been annouced
                     * @todo include option to disble speech
                     */
                    if( [ this.announcedOngoingOrders ].indexOf( order.CODE ) === -1 ) {
                        // let orderSplitted   =   order.CODE.split('').map( value => value + ', ,,' ).join('');
                        // let text    =   textDomain.processingOrder.replace( '#', orderSplitted );
                        // this.speak( text );
                    }
                })
            }
        }
    },
    methods: {

        /**
         * Format the datetime provided by each orders
         * @param {string} datetime datetime of the order
         * @return {string} formated hour
         */
        showHours( datetime ) {
            return moment( datetime ).format( 'LT' );
        },

        /**
         * Speak and return a promise
         * @param {String} text 
         */
        speak( text ) {
            return new Promise(resolve => {
                const utterance = new SpeechSynthesisUtterance(text);
                utterance.lang      =   'en-US';
                utterance.onend     =   resolve;
                speechSynthesis.speak(utterance);
            });
        },
        fetchOrders() {
            HttpRequest.get( '/so/gastro/orders' ).then( ({ data }) => {
                this.rawOrders     =   data;
            });
        },
        getMealStatus( items ) {
            const totalItems    =   items.length;
            const ready         =   items.filter( item => item.metas.restaurant_food_status == 'ready' ).length;
            return `${this.zeroFill(ready, 2)} / ${this.zeroFill(totalItems,2)}`;
        },
        zeroFill( number, width ) {
            width -= number.toString().length;
            if ( width > 0 ){
                return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
            }
            return number + ""; // always return a string
        }
    },
    computed: {
        lastReadyOrder() {
            const readyOrders   =   this.rawOrders.filter( order => order.RESTAURANT_ORDER_STATUS === 'ready' );
            return readyOrders[ 0 ] || false;
        },
        beforeLastReadyOrder() {
            const readyOrders   =   this.rawOrders.filter( order => order.RESTAURANT_ORDER_STATUS === 'ready' );
            return readyOrders[ 1 ] || false;
        },
        ongoingOrders() {
            return this.rawOrders.filter( order => [ 'ongoing', 'partially' ].indexOf( order.RESTAURANT_ORDER_STATUS ) != -1 );
        },
        readyOrders() {
            return this.rawOrders.filter( order => order.RESTAURANT_ORDER_STATUS === 'ready' );
        }
    }
})