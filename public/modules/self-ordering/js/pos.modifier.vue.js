const ModifierWrapper   =   function( data ) {
    const modal     =   new ModalVue({
        title: textDomain.selectModifier,
        height: {
            xs: '95%',
            sm: '90%',
            md: '90%',
            lg: '80%',
            xl: '80%'
        },
        width: {
            xs: '95%',
            sm: '70%',
            md: '70%',
            lg: '50%',
            xl: '50%'
        },
        buttonClasses: 'btn btn-lg',
        namespace: 'modifier-modal',
        data: {
            modifiers   :   data
        },
        methods: {
            selectModifier( modifier, index ) {
                if( modifier.group_multiselect === '0' ) {
                    this.modifiers.filter( ( modifier, _index ) => _index !== index )
                        .forEach( modifier => modifier.selected = false );
                    this.modifiers[ index ].selected    =   ! this.modifiers[ index ].selected;
                } else {
                    this.modifiers[ index ].selected    =   ! this.modifiers[ index ].selected;
                }
            },
            getImage( modifier ) {
                if( modifier.image === '' ) {
                    return '/public/modules/nexo/images/default.png';
                }
                return storeOptions.store_id !== null ? `/public/upload/items-images/${modifier.image}` : `/public/upload/store_${storeOptions.store_id}/items-images/${modifier.image}`;
            }
        },
        body: `
        <h3 class="text-center mb-2">${textDomain.modifierGroup}: ${data[0].group_name}</h3>
        <div class="modifier-wrapper px-2">
            <div class="row p-2">
                <div @click="selectModifier( modifier, index )" :class="{ 'active' : modifier.selected }" v-for="( modifier, index ) in modifiers" class="p-2 col-xs-3 col-sm-3 col-md-3 col-lg-3 product-grid-item modifiers-item">
                        <p class="text-center mb-1">{{ modifier.name }}</p>
                        <p class="text-center">
                            <strong class="">{{ modifier.price | currency }}</strong>
                        </p>
                </div>
            </div>
        </div>
        `
        // body: `
        // <h3 class="text-center mb-2">${textDomain.modifierGroup}: ${data[0].group_name}</h3>
        // <div class="modifier-wrapper px-2 flex-fill">
        //     <div class="row p-2">
        //         <div @click="selectModifier( modifier, index )" style="height: 200px" :class="{ 'active' : modifier.selected }" v-for="( modifier, index ) in modifiers" class="p-2 col-sm-6 col-lg-4 col-xs-6 product-grid-item">
        //             <img :src="getImage( modifier )" alt="" class="flex-fill img-grid" style="height: 100%; width: 100%">
        //             <div class="product-item-details">
        //                 <p class="text-center mb-1">{{ modifier.name }}</p>
        //                 <p class="text-center">
        //                     <strong class="">{{ modifier.price | currency }}</strong>
        //                 </p>
        //             </div>
        //         </div>
        //     </div>
        // </div>
        // `
    });

    return modal;
}