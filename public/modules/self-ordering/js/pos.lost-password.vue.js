const LostPasswordWrapper   =   function() {
    const modal     =   new ModalVue({
        namespace: 'lost-pwd-modal',
        modalBodyClass: 'modal-body modal-body d-flex flex-column align-items-center p-0 oy-auto',
        mounted() {
            this.hasError    =   FrontEndVue.hasError;
            this.getErrors   =   FrontEndVue.getErrors;
        },
        body: `
        <div v-if="! isLoading" class="card w-100 d-flex flex-fill rounded-0 border-0">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs">
                    <li @click="selectLostPwdTab( tab )" class="nav-item" v-for="tab in lostPwdTabs">
                        <a class="nav-link" :class="{ 'active' : tab.activated }" href="#">{{ tab.title }}</a>
                    </li>
                </ul>
            </div>
            <div class="card-body text-center" v-if="selectedTab.namespace === 'lost_pwd'">
                <h5 class="card-title">${textDomain.unableToLogin}</h5>
                <p class="card-text">${textDomain.lostPwdDetails}</p>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">${textDomain.email}</span>
                    </div>
                    <input v-model="email" name="email" type="email" class="form-control" placeholder="${textDomain.email}" aria-label="${textDomain.email}" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="card-body" v-if="selectedTab.namespace === 'enter_code'">
                <h5 class="card-title">${textDomain.alreadyHasCode}</h5>
                <p class="card-text">${textDomain.hasCodeDetails}</p>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">${textDomain.password}</span>
                    </div>
                    <input :class="{ 'is-invalid': hasError( 'password' ), 'is-valid' : ! hasError( 'password' ) }" v-model="password" type="password" class="form-control" placeholder="${textDomain.password}" aria-label="${textDomain.password}" aria-describedby="basic-addon1">
                    <div class="invalid-feedback" v-if="hasError( 'password' )">
                        <p class="m-0" v-for="error in getErrors( 'password' )">{{ error }}</p>
                    </div>

                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">${textDomain.password_confirm}</span>
                    </div>
                    <input :class="{ 'is-invalid': hasError( 'password_confirm' ), 'is-valid' : ! hasError( 'password_confirm' ) }" v-model="password_confirm" type="password" class="form-control" placeholder="${textDomain.password_confirm}" aria-label="${textDomain.password_confirm}" aria-describedby="basic-addon1">
                    <div class="invalid-feedback" v-if="hasError( 'password_confirm' )">
                        <p class="m-0" v-for="error in getErrors( 'password_confirm' )">{{ error }}</p>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">${textDomain.reset_code}</span>
                    </div>
                    <input v-model="reset_code" type="text" class="form-control" placeholder="${textDomain.reset_code}" aria-label="${textDomain.reset_code}" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>
        <div v-if="isLoading" style="margin:auto" class="loading-wrapper">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
        `,
        height: {
            xs: '95%',
            sm: '80%',
            md: '80%',
            lg: '70%',
            xl: '70%'
        },
        width: {
            xs: '95%',
            sm: '80%',
            md: '80%',
            lg: '50%',
            xl: '50%'
        },
        data: {
            isLoading           :   false,
            password            :   '',
            password_confirm    :   '',
            reset_code          :   '',
            email               :   '',
            lostPwdTabs         :   [
                {
                    title : textDomain.lostPasswordTitle,
                    namespace: 'lost_pwd',
                    activated: true,
                }, {
                    title : textDomain.enterRecoveryCode,
                    namespace: 'enter_code',
                    activated: false
                }
            ],
            hasError: null,
            getErrors: null,
        },
        methods: {
            selectLostPwdTab( tab ) {
                this.lostPwdTabs.forEach( tab => tab.activated = false );
                tab.activated   =   true;
            },
            proceed() {
                console.log( this.selectedTab.namespace );
                switch( this.selectedTab.namespace ) {
                    case 'lost_pwd' :
                        this.proceedPasswordReset();
                    break;
                    case 'enter_code' :
                        this.proceedEnterCode();
                    break;
                }
            },
            proceedPasswordReset() {
                this.isLoading  =   true;
                HttpRequest.post( '/so/request-password-reset?store_id=' + storeOptions.store_id, { 
                    email           : this.email,
                    [ csrf.name ]   : csrf.hash
                }).then( ({data}) => {
                    this.isLoading  =   false;
                    if( data.status === 'failed' ) {
                        return $.notify({ message: data.message }, {
                            type: 'danger',
                            z_index: 9999
                        });
                    } else {
                        this.setActive( 'enter_code' );
                        return $.notify({ message: data.message }, {
                            type: 'success',
                            z_index: 9999
                        });
                    }
                })
            },
            setActive( namespace ) {
                /**
                 * check if the namespace exists
                 */
                const namespaces    =   this.lostPwdTabs.map( tab => tab.namespace );
                if( namespaces.indexOf( namespace ) != -1 ) {
                    this.lostPwdTabs.forEach( tab => {
                        if( tab.namespace === namespace ) {
                            tab.activated   =   true;
                        } else {
                            tab.activated   =   false;
                        }
                    });
                }
            },
            proceedEnterCode() {
                this.isLoading  =   true;
                HttpRequest.post( '/so/change-password?store_id=' + storeOptions.store_id, { 
                    password: this.password,
                    password_confirm: this.password_confirm,
                    reset_code: this.reset_code,
                    [ csrf.name ]   : csrf.hash
                }).then( ({data}) => {
                    FrontEndVue.errors_bag  =   {};
                    this.isLoading  =   false;
                    if( data.status === 'failed' ) {
                        /**
                         * only if the error bag is returned
                         */
                        if( data.errors_bag !== undefined ) {
                            FrontEndVue.errors_bag     =   data.errors_bag;
                        }
                        
                        return $.notify({ message: data.message }, {
                            type: 'danger',
                            z_index: 9999
                        });
                    } else {
                        /**
                         * let's try to close the current modal
                         */
                        modal.close();
                        FrontEndVue.requireLogin();
                        return $.notify({ message: data.message }, {
                            type: 'success',
                            z_index: 9999
                        });
                    }
                })
            }
        },
        computed: {
            selectedTab() {
                return this.lostPwdTabs.filter( tab => tab.activated )[0]
            }
        },
        title: textDomain.passwordRecovery
    });

    return modal;
}