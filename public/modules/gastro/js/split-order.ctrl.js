tendooApp.controller( 'splittingCTRL', [ 
    '$scope', '$http', '$timeout', '$compile', '$filter', '$rootScope',
    function( $scope, $http, $timeout, $compile, $filter, $rootScope ) {
    $scope.orders                   =   [];
    $scope.selectedOrderItems       =   [];
    $scope.availableParts           =   0;
    $scope.availablePartsArray      =   [];
    $scope.ordersParts              =   [];
    $scope.spinner                  =   {
        openOrder       :   true
    };
    $scope.customers                =   gastroSplitData.customers;

    /**
     * Split Order
     */
    $scope.openSplitOrder   =   function() {
        
        $scope.cancelSelection();

        NexoAPI.Bootbox().dialog({
            title       :   textDomain.boxTitle,
            message     :   `
            <div class="row insert-here" style="
                    display: flex;
                    flex-grow: 1;
                    margin:0;
                ">
                
            </div>
            `,
            buttons     :   {
                ok          :   {
                    label       :   textDomain.closeBox,
                    className   :   'btn btn-default'
                }
            },
            animate         :   false,
            className       :   'splitting-box'
        });

        $scope.windowHeight				=	window.innerHeight;
        $scope.wrapperHeight			=	$scope.windowHeight - ( ( 56 * 2 ) + 30 );

        var content         =   `
        <div ng-show="! selectedOrder()" class="col-md-12 col-sm-12 col-xs-12 no-padding" style="height: 100%;display: flex;flex-grow: initial;flex-direction: row; background: #f5f5f5;">
            <div class="row" style="height:100%;background: #ecebeb;width: 100%;margin:0;overflow: auto;">
                <div class="col-md-12 search-order-field" style="padding:15px">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button ng-click="loadOrders()" class="btn btn-default" type="button">
                                <i class="fa fa-refresh"></i> 
                            </button>
                        </span>
                        <span class="input-group-addon" id="basic-addon1">${textDomain.searchOrder}</span>
                        <input ng-model="searchOrder" type="text" class="form-control" placeholder="${textDomain.orderCode}" aria-describedby="basic-addon1">
                        
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="row order-container" style="margin:0;padding-top:5px;">
                        <div class="col-md-4" ng-repeat="order in orders | filter: searchOrder">
                            <div class="box" ng-class="{ 'order-selected' : order.selected }">
                                <div class="box-header with-border">
                                    <strong>{{ order.CODE }} <span class="pull-right">{{ getType( order ) }}</span></strong>
                                </div>
                                <div class="box-body no-padding">
                                    <ul class="list-group" style="margin-bottom: 0px;">
                                        <li class="list-group-item no-border-lr">${textDomain.customer} <span class="pull-right">{{ order.customer.NOM }}</li>
                                        <li class="list-group-item no-border-lr">${textDomain.total} <span class="pull-right">{{ order.TOTAL | moneyFormat }}</span></li>
                                        <li class="list-group-item no-border-lr">${textDomain.table} <span class="pull-right">{{ ! order.table.NAME ? '---' : order.table.NAME }}</span></li>
                                        <li class="list-group-item no-border-lr">${textDomain.created} <span class="pull-right">{{ order.DATE_CREATION | date }}</span></li>
                                        <li class="list-group-item no-border-lr">
                                            <div class="btn-group btn-group-sm btn-group-justified">
                                                <div class="btn-group" role="group">
                                                    <button class="btn btn-sm btn-success" ng-click="openCheckout( order )">${textDomain.checkout}</button>
                                                </div>
                                                <div class="btn-group" role="group">
                                                    <button class="btn btn-sm btn-primary" ng-click="selectOrder( order )">${textDomain.splitOrder}</button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div ng-show="orders.length == 0" class="col-md-12 text-center">
                            ${textDomain.noOrderAvailable}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div ng-show="selectedOrder()" class="col-md-12 col-sm-12 col-xs-12 no-padding" style="height: 100%;display: flex;flex-grow: initial;flex-direction: row;">
            <div class="row" style="height:100%;width: 100%;margin:0;">
                <div class="col-md-3" style="height: 100%;display: flex;flex-grow: initial;flex-direction: row;">
                    <div style="width: 100%">
                        <h3 class="text-center">${textDomain.order} : {{ selectedOrder().CODE }}</h3>
                        <ul class="list-group">
                            <li ng-repeat="item in selectedOrderItems" class="list-group-item list-group-item-flex">
                                <span class="item-text">
                                    {{ item.DESIGN }}
                                </span>
                                <span class="item-button">x{{ item.QTE_ADDED }}</span>
                                <span ng-click="addToSelectedOrder( item )" class="item-button"><i class="fa fa-plus"></i></span>
                            </li>
                        </ul>
                        <button ng-click="submitSplittedOrders()" ng-disabled="! validateSelections()" class="btn btn-primary">${textDomain.proceedSplitting}</button>
                    </div>
                </div>
                <div class="col-md-9 no-padding" style="height: 100%;display: flex;flex-grow: initial;flex-direction: row;">
                    <div class="row" style="height:100%;background: #ecebeb;width: 100%;margin:0;overflow-y:scroll">
                        <div class="col-md-12 search-order-field" style="padding:15px">
                            <div class="alert alert-success" role="alert">${textDomain.piecesAlert}</div>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button ng-click="cancelSelection()" class="btn btn-danger" type="button">
                                            <i class="fa fa-remove"></i> 
                                            ${textDomain.cancel}
                                        </button>
                                    </span>
                                    <span class="input-group-addon" id="basic-addon1">${textDomain.spliceInHowManyParts}</span>
                                    <select ng-change="generateOrderBoxes()" ng-model="parts" type="text" class="form-control" aria-describedby="basic-addon1">
                                        <option ng-show="$index + 1 != 1" ng-repeat="piece in availablePartsArray track by $index">{{ $index + 1 }}</option>
                                    </select>                        
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row order-container" style="padding-top:5px;">
                                    <div class="col-md-6" ng-repeat="order in ordersParts">
                                        <div class="box" ng-click="selectIncludedOrder( order )" ng-class="{ 'order-selected' : order.selected, 'order-error' : order.error }">
                                            <div class="box-header with-border">
                                                ${textDomain.order} {{ $index + 1 }}
                                            </div>
                                            <div class="box-body no-padding">
                                                <ul class="list-group" style="margin-bottom: 0px;">
                                                    <li ng-repeat="item in order.items" class="list-group-item no-border-lr list-group-item-flex">
                                                        <span class="item-text">
                                                            {{ item.DESIGN }}
                                                        </span>
                                                        <span class="item-button">{{ item.PRIX * item.QTE_ADDED | moneyFormat }}</span>
                                                        <span class="item-button">x{{ item.QTE_ADDED }}</span>
                                                        <span ng-click="returnToSelectedOrder( item, $index )" class="item-button"><i class="fa fa-minus"></i></span>
                                                    </li>
                                                    <li ng-show="order.items.length == 0" class="list-group-item no-border-lr">${textDomain.noItemAdded}</li>
                                                    <li class="list-group-item no-border-lr">${textDomain.total} <span class="pull-right">{{ getPartTotal( order ) | moneyFormat }}</span></li>
                                                    <li class="list-group-item no-border-lr">
                                                        <div class="input-group" ng-init="order.customer = null">
                                                            <span class="input-group-addon" id="basic-addon1">${textDomain.selectCustomer}</span>
                                                            <select ng-model="order.customer" type="text" class="form-control customers-select" placeholder="Username" aria-describedby="basic-addon1">
                                                                <option value="">${textDomain.chooseAnOption}</option>
                                                                <option ng-repeat="customer in customers track by customer.ID" value="{{ customer.ID }}">{{ customer.NOM }} {{ customer.PRENOM }} - {{ customer.EMAIL }}</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <the-spinner spinner-obj="spinner" namespace="openOrder" />
        `;

        $timeout( function(){
            angular.element( '.splitting-box .modal-dialog' ).css( 'width', '98%' );
            angular.element( '.splitting-box .modal-body' ).css( 'height', $scope.wrapperHeight );
            angular.element( '.splitting-box .bootbox-body' ).css({
                'display': 'flex',
                'flex-grow': 'inherit'
            });
            angular.element( '.splitting-box .modal-body' ).css( 'overflow-x', 'hidden' );
            angular.element( '.splitting-box .modal-body' ).css({
                'display': 'flex',
                'width': '100%',
                'flex-grow': '1',
                'flex-direction' : 'column'
            });
            angular.element( '.splitting-box .modal-body' ).addClass( 'no-padding' );
            angular.element( '.splitting-box .order-container' ).height( $( '.modal-body' ).height() - 5 - $( '.search-order-field' ).outerHeight() );
            angular.element( '.splitting-box .order-container' ).css({
                'overflow-y' : 'scroll'
            });

            angular.element( '.insert-here' ).html( content );
            
            $( '.splitting-box .modal-footer' ).append( '<a ng-show="getSelectedOrders()[0].TYPE == \'nexo_order_devis\' && countSelected() == 1" ng-click="checkoutOrder()" href="javascript:void(0)" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Button</a>' )
            $( '.splitting-box' ).html( $compile( $( '.splitting-box').html() )( $scope ) );

            $scope.loadOrders();
        }, 100 );
    }

    /**
     * When paybox is being closed
     */
    NexoAPI.events.addFilter( 'test_order_type', ( param ) => {
        $scope.loadOrders();
        return param;
    });

    NexoAPI.events.addAction( 'close_paybox', () => {
        $timeout( () => {
            $scope.spinner.openOrder    =   false;
        }, 100 );
    })

    /**
     * Open Checkout
     * @param object order
     * @return void
     */
    $scope.openCheckout         =   function( order ) { 

        NexoAPI.showConfirm({
            message:    textDomain.proceedCheckout
        }).then(( result ) => {
            if ( result.value ) {
                
                /**
                 * Change order type for each order
                 * since we're assume only dine order can be splitted.
                 */
                $rootScope.$emit( 'gastro.orderType', 'dinein' );

                v2Checkout.emptyCartItemTable();
                v2Checkout.CartItems 			=	order.items;

                _.each( v2Checkout.CartItems, function( value, key ) {
                    value.QTE_ADDED		        =	value.QUANTITE;
                    value.PRIX_DE_VENTE         =   value.PRIX_BRUT;
                    value.PRIX_DE_VENTE_TTC     =   value.PRIX
                });

                // @added CartRemisePercent
                // @since 2.9.6
                v2Checkout.CartRemise     = 0;
                v2Checkout.CartRabais     = 0;
                v2Checkout.CartRistourne  = 0;

                if( order.REMISE_TYPE != '' ) {
                    v2Checkout.CartRemiseType			    =	order.REMISE_TYPE;
                    v2Checkout.CartRemise				    =	NexoAPI.ParseFloat( order.REMISE );
                    v2Checkout.CartRemisePercent			=	NexoAPI.ParseFloat( order.REMISE_PERCENT );
                    v2Checkout.CartRemiseEnabled			=	true;
                }

                if( parseFloat( order.GROUP_DISCOUNT ) > 0 ) {
                    v2Checkout.CartGroupDiscount 			=	parseFloat( order.GROUP_DISCOUNT ); // final amount
                    v2Checkout.CartGroupDiscountAmount 	    =	parseFloat( order.GROUP_DISCOUNT ); // Amount set on each group
                    v2Checkout.CartGroupDiscountType 		=	'amount'; // Discount type
                    v2Checkout.CartGroupDiscountEnabled 	=	true;
                }

                v2Checkout.CartCustomerID 			=	order.REF_CLIENT;
                // @since 2.7.3
                v2Checkout.CartNote 				=	order.DESCRIPTION;
                v2Checkout.CartTitle 				=	order.TITRE;

                // @since 3.1.2
                v2Checkout.CartShipping 				=	parseFloat( order.SHIPPING_AMOUNT );
                $scope.price 						    =	v2Checkout.CartShipping; // for shipping directive
                $( '.cart-shipping-amount' ).html( $filter( 'moneyFormat' )( $scope.price ) );

                // Restore Custom Ristourne
                v2Checkout.restoreCustomRistourne();

                // Refresh Cart
                // Reset Cart state
                v2Checkout.buildCartItemTable();
                v2Checkout.refreshCart();
                v2Checkout.refreshCartValues();
                v2Checkout.ProcessURL				=	urls.processURL + '/' + order.ID + urls.processEnd;
                v2Checkout.ProcessType				=	'PUT';
                

                // // Restore Shipping
                // // @since 3.1
                _.each( order.shipping, ( value, key ) => {
                    $scope[ key ] 	=	value;
                });

                $scope.wasOpenFromTableHistory      =   true;
                $rootScope.$emit( 'payBox.openPayBox' );

                /**
                 * Hidding orders to prevent clics
                 */
                $scope.spinner.openOrder    =   true;
            }
        })
    }

    /**
     * Submit Splitted Orders
     * @return void
     */
    $scope.submitSplittedOrders     =   function() {
        let hasError            =   false;
        let hasSomeValid        =   false;
        let hasCustomerError    =   false;
        let hasRemainingItems   =   $scope.selectedOrderItems.map( item => item.QTE_ADDED ).reduce( ( before, after) => before + after );

        if ( hasRemainingItems !== 0 ) {
            return NexoAPI.showError({
                message: textDomain.itemsRemainToMove
            });
        }

        $scope.ordersParts.forEach( parts => {
            if ( parts.items.length == 0 ) {
                hasError    =   true;
            } else {
                hasSomeValid    =   true;
            }
            
            if ( parts.customer == null || parts.customer == '' ) {
                parts.error         =   true;
                hasCustomerError    =   true;
            } else {
                parts.error     =   false;
            }
        });

        if ( hasCustomerError ) {
            return NexoAPI.showError({
                message     :   textDomain.customerError
            });
        }

        if ( hasError && ! hasSomeValid ) {
            return NexoAPI.showError({
                message     :   textDomain.noItemsError
            });
        } else {
            NexoAPI.showConfirm({
                message     :   textDomain.hasSomeValid
            }).then( ( action ) => {
                if ( action.value ) {
                    $scope.proceedToSplit();
                }
            });
        }
    }

    /**
     * Proceed to Split order
     * @return void
     */
    $scope.proceedToSplit   =   function() {
        let order   =   $scope.selectedOrder();
        let parts   =   $scope.ordersParts;
        
        $scope.spinner.openOrder    =   true;

        $http.post( urls.submitOrder, {
            order,
            parts  :   parts.filter( part => part.items.length > 0 )
        }, {
            headers     :   {
                [ tendoo.rest.key ]     :   tendoo.rest.value
            }
        }).then( result => {
            NexoAPI.Toast()( textDomain.splitSuccess );
            $scope.spinner.openOrder        =   false;
            $scope.loadOrders();
            $scope.cancelSelection();
        });
    }

    /**
     * Validate Selection
     * @return selection
     */
    $scope.validateSelections   =   function() {
        return $scope.ordersParts.length > 0;
    }

    /**
     * get part total
     * @param object current items
     * @return number current items values
     */
    $scope.getPartTotal         =   function( order ) {
        let total   =   0;
        
        order.items.forEach( item => {
            total   +=  ( item.QTE_ADDED * parseFloat( item.PRIX ) );
        });

        order.total     =   total;

        return total;
    }

    /**
     * Add Selected Order
     * @param object item
     * @return void
     */
    $scope.addToSelectedOrder   =   function( item ) {
        /**
         * only if an order part is selected
         */
        if ( $scope.getSelectedOrderPart().length == 0 ) {
            return NexoAPI.showError({
                message     :   textDomain.selectOrderFirst
            });
        }    
        
        /**
         * Treat the added item
         */
        $scope.ordersParts.forEach( order => {
            if ( order.selected ) {
                $scope.addItemTo( order, item );
            }
        })
    }

    /**
     * Return to selected orders
     * @param object item
     * @return void
     */
    $scope.returnToSelectedOrder    =   function( item, index ) {
        $scope.selectedOrderItems.forEach( _item => {
            if ( item.CODEBAR === _item.CODEBAR ) {
                item.QTE_ADDED--;
                _item.QTE_ADDED++;

                /**
                 * remove the item is the stock is equal to "0"
                 */
                if ( item.QTE_ADDED == 0 ) {
                    $scope.ordersParts.forEach( order => {
                        if ( order.selected ) {
                            order.items.splice( index, 1 );
                        }
                    })
                }
            }
        });
    }
    
    /**
     * Return All Orders
     * @param 
     */
    $scope.returnAllitems       =   function() {
        $scope.ordersParts.forEach( order => {
            order.items.forEach( item => {
                $scope.selectedOrderItems.forEach( _item => {
                    if ( item.CODEBAR === _item.CODEBAR ) {
                        _item.QTE_ADDED     +=  parseFloat( item.QTE_ADDED );
                    }
                });
            });
        });
    }

    /**
     * Add item to the selected part
     * @param object order
     * @param object item
     */
    $scope.addItemTo        =   function( orderPart, item ) {
        
        item.QTE_ADDED    =   parseInt( item.QTE_ADDED );

        /**
         * Let's loop first to check if this item already 
         * exists
         */
        let itemExists  =   orderPart.items.filter( _item => _item.CODEBAR === item.CODEBAR );

        /**
         * The item quantity should be greather than 0
         */
        if ( item.QTE_ADDED == 0 ) {
            return NexoAPI.Toast()( textDomain.noMoreQuantity );
        }

        /**
         * let's reduce the quantity of the added item
         */
        item.QTE_ADDED--;

        if ( itemExists.length > 0 ) {
            orderPart.items.forEach( _item => {
                if ( _item.CODEBAR === item.CODEBAR ) {
                    _item.QTE_ADDED++;
                }
            });

        } else {
            let newItem         =   angular.copy( item, {});
            newItem.QTE_ADDED   =   1;
            orderPart.items.push( newItem );
        }
    }

    /**
     * get Selected Order parts
     * @return object orders
     */
    $scope.getSelectedOrderPart     =   function() {
        return $scope.ordersParts.filter( order => order.selected === true );
    }

    /**
     * Select Order
     * @param  object order
     * @return void
     */
    $scope.selectOrder          =   function( order ) {

        $scope.spinner.openOrder    =   true;
        $scope.ordersParts          =   [];
        // unselect all orders
        $scope.orders.forEach( _order => _order.selected = false );

        if( typeof order.selected == 'undefined' ) {
            order.selected  =   true;
        } else {
            order.selected  =  !order.selected;
        }
        $scope.openOrderDetails( order.ID );
    }

    /**
     * generate order boxes
     * @return void
     */
    $scope.generateOrderBoxes       =   function() {
        if ( $scope.selectedOrder() ) {
            if ( $scope.ordersParts.length > 0 && $( '.splitting-box' ).length > 0 ) {
                NexoAPI.showConfirm({
                    message     :   textDomain.changeOrderNumber
                }).then( ( action ) => {
                    if ( action.value ) {
                        $scope.returnAllitems();
                        $scope._generateBoxes();
                    }
                });
            } else {
                $scope._generateBoxes();
            }
        }
    }

    /**
     * Select a single order part.
     * @return void
     */
    $scope.selectIncludedOrder      =   function( order ) {
        // unselect all orders
        $scope.ordersParts.forEach( _order => _order.selected = false );

        if( typeof order.selected == 'undefined' ) {
            order.selected  =   true;
        } else {
            order.selected  =  !order.selected;
        }

        /**
         * Cancel Order Error
         */
        order.error     =   false;
    }

    /**
     * generate boxes
     * @return void
     */
    $scope._generateBoxes   =   function() {
        let parts   =   parseInt( $scope.parts );
        $scope.ordersParts          =   [];
        if ( parts >= 1 ) {
            for ( let i = 0; i < parts; i++ ) {
                $scope.ordersParts.push({
                    items       :   [],
                    orderType   :   '',
                    total       :   '',
                    customer    :   0
                });
            }
        }

        $( '.customers-select' ).selectpicker();
    }

    /**
     * Get Selected Order
     * @return object of selected order
     */
    $scope.selectedOrder    =   function() {
        let selectedOrder   =   false;
        $scope.orders.forEach( _order => {
            if ( _order.selected ) {
                selectedOrder   =   _order;
            }
        });

        return selectedOrder;
    }

    /**
     * Cancel table Selection
     * @return void
     */
    $scope.cancelSelection      =   function(){
        $scope.orders.forEach( order => order.selected = false );
        $scope.selectedOrderItems       =   [];
        $scope.availableParts           =   0;
        $scope.availablePartsArray      =   [];
    }

    $scope.openOrderDetails			=	function( order_id ) {
		$http.get( urls.fullOrder + '/' + order_id + urls.storeParam, {
			headers			:	{
				[ tendoo.rest.key ]     :   tendoo.rest.value
			}
		}).then(function( returned ){
            let details     =   returned.data;
            let order       =   details.order[0];
            let items       =   details.products;

            $scope.spinner.openOrder    =   false;
            let totalItems     =   items.map( item => item.QUANTITE )
                .reduce( ( prev, next ) => parseFloat( prev ) + parseFloat( next ) );

            if ( totalItems <= 1 ) {
                
                $scope.cancelSelection();

                return NexoAPI.showError({
                    message     :   textDomain.cantSplitSingleItemOrders
                });
            }

            $scope.selectedOrderItems   =   items;
            items.forEach( item => {
                $scope.availableParts   +=  parseInt( item.QTE_ADDED );
            });
            $scope.availablePartsArray  =   new Array( $scope.availableParts );
		});
	};

    /**
     * Load Orders
     * @return void
     */
    $scope.loadOrders       =   function(){
        $http.get( urls.loadOrders, {
            headers     :   {
                [ tendoo.rest.key ]     :   tendoo.rest.value 
            }
        }).then( result => {
            $scope.orders   =   result.data;
            $scope.spinner.openOrder    =   false;
        });
    }
}])