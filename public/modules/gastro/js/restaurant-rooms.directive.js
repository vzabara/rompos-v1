tendooApp.directive( 'restaurantRooms', function(){
    return {
        templateUrl        :  restaurantRooms.url.template,
        restrict            :   'E',
        controller          :   [ '$scope', '$http', '$rootScope', function( $scope, $http, $rootScope ) {
            $scope.windowHeight				=	window.innerHeight;
            $scope.wrapperHeight			=	$scope.windowHeight - ( ( 56 * 2 ) + 30 );
            $scope.showMove                 =   false;
            $scope.selectedSeat             =   0;

            $rootScope.$on( 'tables-has-loaded', function( events, tables ) {
                $scope.tables   =   tables;
            });

            /**
             * Get Show status
             * @return boolean
             */
            $scope.getShowStatus    =   function(){
                return $scope.showMove;
            }
            
            /**
             * Cancel Change
             * @return void
             */
            $scope.cancelChange         =   function( order ){
                order.showMove          =   false;
                $scope.tables.forEach( table => table.selected = false );
            }

            /**
             * Confirm Move
             * @return void
             */
            $scope.confirmChange           =   function( order ){
                let selectedTableForMove      =   {};

                /**
                 * Let's check if at least a table is selected
                 */
                $scope.tables.forEach( _table => {
                    if ( _table.selected ) {
                        selectedTableForMove  =   _table;
                    }
                });

                if ( selectedTableForMove.TABLE_ID != undefined ) {
                    NexoAPI.showConfirm({
                        message     :   restaurantRooms.messages.confirmMove
                    }).then( action => {
                        if ( action.value != undefined ) {
                            
                            let data    =   {
                                table_id    :   selectedTableForMove.TABLE_ID,
                                order_id    :   order.ORDER_ID,
                                seat_used   :   $scope.selectedSeat
                            }
                            
                            $scope.showSpinner      =   true;

                            $http.post( restaurantRooms.url.postChange, data, {
                                headers     :   {
                                    [ tendoo.rest.key ]     :   tendoo.rest.value
                                }
                            }).then( result => {
                                $scope.showSpinner      =   false;
                                $scope.loadRoomAreas();
                                $scope.closeHistory();
                            }, error => {
                                $scope.showSpinner      =   false;
                                NexoAPI.showError({
                                    message     :   restaurantRooms.messages.errorOccurredWhileChanging
                                });
                            });
                        }
                    });
                } else {
                    NexoAPI.showError({
                        message     :   restaurantRooms.messages.requireTableSelection
                    });
                }
            }

            
            /**
             * Show move
             * @return void
             */
            $scope.openMoveOrder             =   function( order ) {
                order.showMove          =   true;
                console.log( $scope.tables );
            }

            /**
             * Select Table
             * @param object table
             * @return void
             */
            $scope.selectTableForMoving          =   function( table ) {
                $scope.tables.forEach( _table => _table.selected = false );
                table.selected                  =   true;
                $scope.selectedSeat             =   0;
            }

            /**
             * Is table Selected for move
             * @return boolean
             */
            $scope.isTableSelectedForMove   =       function(){
                let hasSelected     =   false;
                $scope.tables.forEach( _table => {
                    if ( _table.selected ) {
                        hasSelected     =   true;
                    }
                });
                return hasSelected;
            }

            /**
             * get Selected table for move
             * @return object
             */
            $scope.getSelectedTableForMove  =   function(){
                let selectedTableForMove      =   {};

                /**
                 * Let's check if at least a table is selected
                 */
                $scope.tables.forEach( _table => {
                    if ( _table.selected ) {
                        selectedTableForMove  =   _table;
                    }
                });
                return selectedTableForMove;
            }

            /**
             * Get Table Seats
             * @return int
             */
            $scope.getSeat      =   function( table ){
                let seats       =   [];
                if ( table.MAX_SEATS != undefined ) {
                    for ( let i = 1; i <= parseInt( table.MAX_SEATS ); i++ ) { 
                        seats.push(i);
                    }
                }
                return seats;
            }

            /**
             * Is Seat selected for move
             * @return boolean
             */
            $scope.selectSeat       =   function( seat ) {
                $scope.selectedSeat     =   seat;
            }

            /**
             * isSeatSelectedForMove
             * @return boolean
             */
            $scope.isSeatSelectedForMove    =   function(){
                if ( $scope.selectedSeat == 0 ) {
                    return false;
                }
                return true;
            }
        }]
    }
});