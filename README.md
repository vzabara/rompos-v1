## ROMPOS - Multipurpose Point Of Sale ## 

ROMPOS is a cloud based point of sale system that fit various businesses from restaurant to grocery stores.

## XAMPP settings ##

### PHP ###

- error_reporting=0
- display_errors=Off
- log_errors=Off
- date.timezone=America/New_York
- opcache.enable=1
- zend_extension="C:\xampp\php\ext\php_opcache.dll"
- opcache.memory_consumption=256
- memory_limit=1G

### MySQL ###

- innodb_buffer_pool_size=1G
- innodb_log_file_size=1G
- innodb_log_buffer_size=128M

